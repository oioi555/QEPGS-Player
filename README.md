# QEPGS-Player
[EPGStation](https://github.com/l3tnun/EPGStation)(TV録画サーバー)専用の動画再生プレイヤーです  
勉強がてら、LinuxデスクトップPCでの視聴を便利にするために作った自分用のQtアプリケーションですが、おすそ分けで公開します  

$`\textcolor{maroon}{\text{EPGStation v2に対応しました}}`$

## 特徴
作者の環境では、多チャンネルチューナー(PX-Q3PE4)で、ジャンルごと全録したりしています  
複数チャンネルの同時録画が走るので、録画サーバーに不要な負荷を掛けたくありません  
~~エンコードもサーバーに負荷を掛けるので、再生は`TSファイル`のみ扱います~~  
~~`TSファイル`運用でも~~ HDDの容量不足にならないように、  
録り溜めた動画を 「**探す**」、「**視聴する**」、「**削除する** 」 の、一連の消化作業を効率的に行うことに重点を置いています  

[EPGStation v2](https://github.com/l3tnun/EPGStation)では、録画済のエンコードした`h.264ファイル`などの直接再生や、ストレージ容量を節約するための、エンコード操作にも対応しました

また、録画サーバーの負荷を下げるにはハードウェアエンコードが有効なので   
別途エンコードスクリプト[[EPGS-multiCodec-encjs](https://gitlab.com/oioi555/epgs-multicodec-encjs)]も用意しました   
こちらは、エンコード失敗の軽減やエンコード失敗時の自動削除問題等も改善できます 

## 主な機能
+ 内蔵ブラウザでEPGStaion
    + 録画予約など、通常通りの操作ができます
    + 動画再生時にダウンロードするプレイリスト(m3u8)は増殖しません
    + 本アプリから番組表の呼び出し
    + 番組詳細ページとの同期（V2のみ）
+ 録画済リスト
    + EPGStationのDBに、直接アクセスすることで、検索機能を拡張します
    + 個別フィールドの表示非表示指定
    + 個別フィールドでフィルター
    + 個別フィールドで並び替え
    + 複数のフィールドから検索（設定で該当フィールド指定可）
    + ビデオファイル（TS、h.264など）一覧で確認（V2のみ）
+ エンコード（V2のみ）
    + リアルタイムでエンコード進歩確認
    + 複数録画選択やTagから、まとめてエンコード指定
    + 重複したエンコード済ファイルを、複数録画選択やTagから、まとめて削除
    + エンコード済のTSファイルを、複数録画選択やTagから、まとめて削除
    + 全部、またはTag単位など、まとめてエンコードキャンセル指定
+ 内蔵プレイヤー
    + [mpv](https://github.com/mpv-player/mpv)(libmpv)ベースのシンプルなプレイヤーです
    + レジューム機能で、視聴を再開できます
    + 音声多重（トラック・ステレオ）切換え
    + 字幕放送 一部対応
        + H.264に埋め込んだ字幕は表示できます
        + TSの字幕は、内蔵プレイヤーのMPVでは表示できません
    + ミニスクリーン（位置サイズ可変のフレームレス表示）
    + フルスクリーン
    + マルチプレイ（複数のプレイヤーを画面分割して同時視聴）
    + インターレース解除
    + スナップショット（撮影後、Tagや録画レコードのサムネイル登録可）
    + マウスホイール操作
        + 再生中　ボリューム
        + 再生中 + Ctrl 再生スピード
        + 一時停止中 シーク
            + Ctrl + 微調整
    + MPRIS
        + KDEConnect、AVRCP(Bluetooth)などから操作可能
+ 検索Tag
    + 番組タイトルからTagを作成しグループ化できます
    + Tagから視聴済みだけを削除するなど、まとめて動画を管理できます
    + ドラマやアニメの連続番組では、新番組検出でTagを自動で作成できます
    + 連続番組指定のTagでは、番組終了も自動で検出し、フラグを立てます
    + Tagには、画像検索から容易にカバーを登録できます
+ その他
    + 外部プレイヤーで再生(m3u8に関連付けされたプレイヤー)
    + プレイリストファイルの作成
    + ライブTV視聴
    + ダークテーマ用アイコン切替（システム依存で、アプリ単独でのダークテーマは非対応）
    + DB拡張のバックアップ・リストア・削除

## スクリーンショット
|![overview](https://gitlab.com/oioi555/QEPGS-Player/uploads/9ec74662c28af85fcb7740c9209378ae/overview.png)|![dark](https://gitlab.com/oioi555/QEPGS-Player/uploads/33058f14191179404b7a97bbf3c2f30a/dark.png)
|---|---|
## デモ
![demo](https://gitlab.com/oioi555/QEPGS-Player/uploads/b278e82de7ca5338d87e5b827ae89b1d/output.gif)

## ミニ・フルスクリーン・マルチプレイ　デモ
![demo](https://gitlab.com/oioi555/QEPGS-Player/uploads/02a7382d289ea50ac42cfcf4b44ce44a/output.gif)
## 必要条件
+ [EPGStation](https://github.com/l3tnun/EPGStation) v1.6.x or 2.6.x or 2.7.x
  + [MySQL](https://www.mysql.com/jp/)/[MariaDB](https://mariadb.org/)
  + [PostgreSQL](https://www.postgresql.org/)【v2から廃止なので非推奨】
+ [Qt5](https://www.qt.io/licensing/) (>=5.11.3) or [Qt6](https://www.qt.io/product/qt6) (>=6.2.4)
+ [mpv](https://github.com/mpv-player/mpv) libmpv-dev (>=0.29.0)
+ 動作確認デスクトップ環境
  + [KDE Plasma](https://kde.org/ja/plasma-desktop/)
  + [Cinnamon](https://projects.linuxmint.com/cinnamon/)
  + [Mate](https://mate-desktop.org/ja/)
  + [Xfce](https://xfce.org/)
  + [LXQt](https://lxqt-project.org/)
  + [Gnome](https://www.gnome.org/)

## インストール
`QEPGS-Playerは、DB接続設定後、必要なテーブルなどを追加してDBを拡張します`  
とても行儀の悪いアプリなので、気になる方は、インストールしないで下さい  

データベースへの接続が必須なので、Dockerで運用している方は、**MySQL**のポート指定を追加して下さい  

### Qt6について
`Qt6`では、内蔵ブラウザや、QMLのパフォーマンスが向上しますが  
`Qt6`から`Qt5`にダウングレードした場合、アプリの設定ファイルが文字化けして正常に機能しない現象を確認しています  
ダウングレードした際は、設定画面から、[`設定をリセット`]を実行して下さい  
また、デスクトップ環境が、[`KDE Plasma5`]の場合、KDE専用テーマの[`Breeze`] [`Oxygen`]は適用されません  

#### `Qt6`対応ディストリビューション  
Arch系 (Manjaroなど)、Fedora40、Debian12(bookworm)、Ubuntu22.04LTS(LinuxMint21)以降  
インストールスクリプト(arch.sh、fedora.sh、debian.sh)の引数に`Qt6`をつけることでビルドできます  

### [Qt5,Qt6] Arch系 (Manjaroなど)
ダウンロードは、[リリース](https://gitlab.com/oioi555/QEPGS-Player/-/releases)の`[arch:pkg]`
```
git clone https://gitlab.com/oioi555/QEPGS-Player.git/
cd ./QEPGS-Player/setup
./arch.sh
```
### [Qt5,Qt6] Fedora 40
ダウンロードは、[リリース](https://gitlab.com/oioi555/QEPGS-Player/-/releases)の`[fedora:rpm]`  
`h.264ファイル`が再生できない場合は、マルチメディアコーデックをインストールして下さい  
[`GStreamer Multimedia Codecs - H.264`]
```
git clone https://gitlab.com/oioi555/QEPGS-Player.git/
cd ./QEPGS-Player/setup/
./fedora.sh
```
### Debian系(Ubuntu 、LinuxMintなど)
```
git clone https://gitlab.com/oioi555/QEPGS-Player.git/
cd ./QEPGS-Player/setup/
./debian.sh
```
バイナリパッケージをコマンドでインストールする場合は、[`dpkg`]では、  
依存関係をインストールできないので[`apt`]を使用して下さい

#### [Qt5] Debian10(Buster) Debian11(bullseye) Ubuntu20.04LTS（LinuxMint20）
ダウンロードは、[リリース](https://gitlab.com/oioi555/QEPGS-Player/-/releases)の`[debian:deb]`

#### [Qt5,Qt6] Ubuntu22.04LTS(LinuxMint21)
ダウンロードは、[リリース](https://gitlab.com/oioi555/QEPGS-Player/-/releases)の`[ubuntu:deb]`

#### [Qt5,Qt6] Debian12(bookworm) Ubuntu23.04以降
ダウンロードは、[リリース](https://gitlab.com/oioi555/QEPGS-Player/-/releases)の`[debian12:deb]`

## 補足
### DockerでのMySQLポート指定
`docker-compose.yml`の`mysql`ブロック
```
ports:
    - "3306:3306"
```
録画サーバーに、すでに**MySQL/MariaDB**をインストールしてる方は
```
ports:
    - "3307:3306"
```
の様に、ポート番号をずらして下さい  
**注意**：
この場合、本アプリのDB設定は、`localhost`で接続できないので、ホストPCであっても、クライアントPCと同じ様に、IPアドレスを設定して下さい

### アンイストール
本アプリは、EPGStationのDBに、テーブルやビューを追加して動作する、行儀の悪いアプリなので、アンインストールするときは、行儀良く本アプリ内の  
`設定` > `データベース` > `バックアップ`　の`DB拡張の削除`を実行して、DBに加えた拡張を削除してください  
そのあとで、アプリを消して下さい
#### アンイストールで削除されない個別ファイル
```
~/.config/Unknown Organization/QEPGS-Player[-profile].conf
~/.local/share/QEPGS-Player[-profile]/
```

### Wayland環境での利用
KDEPlasma6から、デフォルトがWaylandになったので、以下の変更は適用済になっています

Waylandは、その仕様でアプリ自身によるウィンドウ位置の復元が許可されていないそうです  
本アプリでは、ミニスクリーン（位置サイズ可変のフレームレス表示）  
のハックに多用しているので、そのまま使うと不具合がでてしまいます  

もしWayland環境で使いたい場合は、`QEPGS-Player.desktop` ファイルの `Exec`の値を以下にように~~変更してください~~  

`/opt/QEPGS-Player/bin/QEPGS-Player -platform xcb`

起動オプションに、`-platform xcb`を付ける事で、Wayland環境であってもアプリをX11で動かせます 

## Licence
[GNU General Public License v2.0.](LICENSE)
