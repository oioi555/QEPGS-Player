cmake_minimum_required(VERSION 3.13)

# プロジェクトバージョンをGitTagから取得
execute_process(COMMAND bash -c "git describe --abbrev=0 --tags | sed -z 's/^v//;s/\\n//;'" OUTPUT_VARIABLE PROJECT_VERSION)
message("ProjectVersion: ${PROJECT_VERSION}")
project(QEPGS-Player
    VERSION ${PROJECT_VERSION}
    DESCRIPTION "An application to watch videos recorded by EPGStation."
    HOMEPAGE_URL "https://gitlab.com/oioi555/QEPGS-Player"
    LANGUAGES C CXX
)

# アプリバージョン表記用のGitTagバージョンを取得し受け渡し
execute_process(COMMAND bash -c "git describe --abbrev=8 --tags | sed -z 's/\\n//;'" OUTPUT_VARIABLE GIT_TAG_VERSION)
message("GitTagVersion: ${GIT_TAG_VERSION}")
add_compile_definitions(VERSION="${GIT_TAG_VERSION}")

# c++ 17
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Qtの基本設定
set(QT_MIN_VERSION "5.11.3")
include(GNUInstallDirs)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(PkgConfig REQUIRED) # for mpv

find_package(QT NAMES Qt5 Qt6 REQUIRED COMPONENTS Core)
message("QtCore: Qt${QT_VERSION_MAJOR}")

# Qt5 Qt6 共通のコンポーネント
set(QT_COMPONENTS Core DBus Gui Network QuickControls2 QuickWidgets Sql WebEngineWidgets Widgets Xml)
if (${QT_VERSION_MAJOR} GREATER 5) # only Qt6
    list(APPEND QT_COMPONENTS OpenGLWidgets Core5Compat)
endif()
message("QT_COMPONENTS:${QT_COMPONENTS}")
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS ${QT_COMPONENTS})

# ソースの読み込み
add_subdirectory(src)

# インストール
install(TARGETS ${PROJECT_NAME}
    DESTINATION /opt/${PROJECT_NAME}/bin
)
install(FILES etc/QEPGS-Player.svg
    DESTINATION /usr/share/icons/hicolor/scalable/apps
)
install(FILES etc/QEPGS-Player.desktop
    DESTINATION /usr/share/applications
)

# ----------------------------------------------------
# インストールパッケージの作成
# ----------------------------------------------------
# CPack 共通設定
set(CPACK_PACKAGE_NAME ${CMAKE_PROJECT_NAME})
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY ${PROJECT_DESCRIPTION})
set(CPACK_PACKAGE_VENDOR "oioi555")
set(CPACK_PACKAGE_CONTACT "oioi555@gmail.com")
set(CPACK_PACKAGING_INSTALL_PREFIX "/opt")

# ----------------------------------------------------
# CPack DEB
# ----------------------------------------------------
set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "amd64")
set(CPACK_DEBIAN_FILE_NAME
    "${PROJECT_NAME}_${PROJECT_VERSION}_${CPACK_DEBIAN_PACKAGE_ARCHITECTURE}_Qt${QT_VERSION_MAJOR}.deb"
)
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "${CPACK_PACKAGE_VENDOR}<${CPACK_PACKAGE_CONTACT}>")
set(CPACK_DEBIAN_PACKAGE_SECTION "video")
set(CPACK_DEBIAN_PACKAGE_HOMEPAGE ${CMAKE_PROJECT_HOMEPAGE_URL})

# 依存関係
set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)
set(DEB_LIBSQL "libqt${QT_VERSION_MAJOR}sql${QT_VERSION_MAJOR}")
set(DEB_LIBSVG "libqt${QT_VERSION_MAJOR}svg${QT_VERSION_MAJOR}")
set(DEB_PKG_DEPENDS breeze-icon-theme ${DEB_LIBSVG} ${DEB_LIBSQL}-psql ${DEB_LIBSQL}-mysql)

if (${QT_VERSION_MAJOR} GREATER 5)
    # Qt6
    list(APPEND DEB_PKG_DEPENDS
        qt6-qpa-plugins
        qml6-module-qtquick-controls
        qml6-module-qtquick-window
        qml6-module-qtqml-workerscript
        qml6-module-qtquick-templates
        qml6-module-qtquick-layouts
        qml6-module-qt5compat-graphicaleffects
        # Ubuntu22.04で自動検出されない
        libqt6webenginecore6-bin
    )
else()
    # Qt5
    list(APPEND DEB_PKG_DEPENDS
        qml-module-qtquick-controls
        qml-module-qtquick-controls2
    )
endif()

set(CPACK_DEBIAN_PACKAGE_DEPENDS ${DEB_PKG_DEPENDS})
#message("CPACK_DEBIAN_PACKAGE_DEPENDS: ${CPACK_DEBIAN_PACKAGE_DEPENDS}")

# ----------------------------------------------------
# CPack RPM
# ----------------------------------------------------
set(CPACK_RPM_PACKAGE_ARCHITECTURE "x86_64")
set(CPACK_RPM_FILE_NAME
    "${PROJECT_NAME}_${PROJECT_VERSION}_${CPACK_RPM_PACKAGE_ARCHITECTURE}_Qt${QT_VERSION_MAJOR}.rpm"
)
set(CPACK_RPM_PACKAGE_LICENSE "GPLv2")
set(CPACK_RPM_PACKAGE_GROUP "Multimedia")

# 依存関係
set(RPM_LIBSQL "qt${QT_VERSION_MAJOR}-qtbase")
set(RPM_PKG_DEPENDS mpv-libs breeze-icon-theme ${RPM_LIBSQL}-postgresql ${RPM_LIBSQL}-mysql)
string(REPLACE ";" ", " RPM_PKG_DEPENDS "${RPM_PKG_DEPENDS}") # ,区切りに置換

set(CPACK_RPM_PACKAGE_REQUIRES ${RPM_PKG_DEPENDS})
#message("CPACK_RPM_PACKAGE_REQUIRES: ${RPM_PKG_DEPENDS}")

# ファイル配置の競合エラー対策
set(CPACK_RPM_EXCLUDE_FROM_AUTO_FILELIST_ADDITION
    /opt
    /usr/share/applications
    /usr/share/icons
    /usr/share/icons/hicolor
    /usr/share/icons/hicolor/scalable
    /usr/share/icons/hicolor/scalable/apps
)
#message("CPACK_RPM_EXCLUDE_FROM_AUTO_FILELIST_ADDITION: ${CPACK_RPM_EXCLUDE_FROM_AUTO_FILELIST_ADDITION}")

include(CPack)
