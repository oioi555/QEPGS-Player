#include "dock_live.h"
#include "ui_dock_live.h"

#include <QProcess>
#include <QStandardItemModel>

DockLive::DockLive(QWidget *parent) :
    QDockWidget(parent)
  , sqlModel(new SqlModel("services", "id"))
  , menuPrograms(new Menu)
  , treeModel(new QStandardItemModel(this))
  , main(qobject_cast<MainWindow*>(parent))
  , ui(new Ui::DockLiveTv)
{
    ui->setupUi(this);
    this->setTitleBarWidget(new QWidget(this));

    // v2対応
    if (main->getApi()->isV2)
        sqlModel->setInit("channel", "id");

    // setting
    bool switchView = main->getSettings()->valueObj(this, "switchView", false).toBool();
    checkedIds = main->getSettings()->valueObj(this, "checkedId", QStringList()).toStringList();

    // mainwindow
    connect(main, &MainWindow::mainStatusChanged, this, [=] (MainStatus status) {
        if (status == MainStatus::Database)
            loadTree(ui->actionCheckItem->isChecked());
    });
    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    // ui header
    auto icon = main->getIcon("video-television");
    ui->laIcon->setPixmap(icon.pixmap(icon.actualSize(QSize(22, 22))));

    ui->toolFilter->setChecked(switchView);
    connect(ui->toolFilter, &QToolButton::clicked, ui->actionCheckItem, &QAction::triggered);
    connect(ui->toolPrograms, &QToolButton::clicked, this, [=] () {
        main->openEPGProgram();
    });

    QAction* actionGR = new QAction("地上波"); actionGR->setData("GR");
    QAction* actionBS = new QAction("BS放送"); actionBS->setData("BS");
    QAction* actionCS = new QAction("CS放送"); actionCS->setData("CS");
    actionPrograms << actionGR << actionBS << actionCS;
    foreach (auto action, actionPrograms)
        action->setIcon(main->getIcon("mergecell-horizontal"));

    menuPrograms->addActions(actionPrograms);
    ui->toolPrograms->setMenu(menuPrograms);
    connect(menuPrograms, &Menu::triggered, this, [=] (QAction* action) {
        main->openEPGProgram(action->data().toString());
    });

    // action
    needSelectActions << ui->actionPlay << ui->actionPlayOuter << ui->actionOpenEPGChannel;
    connect(ui->actionOpenEPGProgram, &QAction::triggered, this, [=] {
        if (auto index = getSelectedIndex(); index.isValid()) {
            auto type = treeModel->hasChildren(index)? treeModel->data(index, Qt::UserRole) : treeModel->data(treeModel->parent(index), Qt::UserRole);
            main->openEPGProgram(type.toString());
        }
    });
    connect(ui->actionOpenEPGChannel, &QAction::triggered, this, [=] {
        if (auto selectedId = getSelectedId(); !selectedId.isEmpty())
            main->openEPGChannel(selectedId);
    });
    connect(ui->actionPlay, &QAction::triggered, this, [=] {
        if (auto selectedId = getSelectedId(); !selectedId.isEmpty())
            main->openPlayerLive(selectedId, getSelectedText());
    });
    connect(ui->actionPlayOuter, &QAction::triggered, this, [=] {
        if (auto selectedId = getSelectedId(); !selectedId.isEmpty()) {
            Playlist playlist;
            playlist.addItemLive(selectedId, getSelectedText());
            if (QString saveFileName = "live.m3u8"; playlist.saveFile(saveFileName))
                QProcess::startDetached("xdg-open", {saveFileName});
        }
    });
    connect(ui->actionSetting, &QAction::triggered, this, [=] {
        main->openSettings(SettingMenu::LiveTV);
    });
    connect(ui->actionCreatePlaylist, &QAction::triggered, this, [=] {
        if (!checkedIds.count()) return;
        Playlist playlist;
        foreach (auto item, treeModel->findItems("*", Qt::MatchWildcard | Qt::MatchRecursive)) {
            if (item->checkState() == Qt::Checked)
                playlist.addItemLive(item->data(Qt::UserRole).toString(), item->text());
        }
        auto dir = main->getSettings()->value("DockPlaylist/editPlaylistPath").toString();
        if (auto filePath = playlist.getSaveFileName(dir, "ライブTV_プレイリスト", this); !filePath.isEmpty()) {
            if (playlist.saveFile(filePath))
                main->updatePlaylist(filePath);
        }
    });
    connect(ui->actionCheckItem, &QAction::triggered, this, [=] (bool checked) {
        ui->actionCheckItem->setChecked(checked);
        ui->toolFilter->setChecked(checked);
        auto selectedId =  getSelectedId();  // 選択チャンネルの保存;
        loadTree(checked); // 再表示
        if (!selectedId.isEmpty()) { // 選択チャンネルの復元
            foreach (auto item, treeModel->findItems("*", Qt::MatchWildcard | Qt::MatchRecursive)) {
                if (auto id = getId(item->index()); id == selectedId) {
                    ui->treeView->selectionModel()->select(item->index(), QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
                    ui->treeView->scrollTo(item->index());
                }
            }
        }
    });
    ui->actionCheckItem->setChecked(switchView);

    // tree
    connect(ui->treeView, &::QTreeView::doubleClicked, this, [=] (const QModelIndex &index) {
        if (treeModel->hasChildren(index)) {
            ui->actionOpenEPGProgram->trigger();
            return;
        }
        if (main->getSettings()->valueObj(this, "chkInnerPlayerLive", true).toBool())
            ui->actionPlay->trigger();
        else
            ui->actionPlayOuter->trigger();
    });
    connect(ui->treeView, &::QTreeView::clicked, this, [=] (const QModelIndex &index) {
        if (treeModel->hasChildren(index)) return;
        if (auto id = getId(index); !id.isEmpty()) {
            main->setDockInfoId(InfoType::Live, id);
            // 単極表示
            if (main->getSettings()->valueObj(this, "chkLiveSelectedChannelView", false).toBool())
                main->openEPGChannel(id);
        }
    });

    connect(treeModel, &QStandardItemModel::itemChanged, this, [=] (QStandardItem *item) {
        QString id = item->data(Qt::UserRole).toString();
        bool checked = (item->checkState() == Qt::Checked);
        if (checked && checkedIds.contains(id))
            checkedIds << id;
        else
            checkedIds.removeOne(id);
    });

    loadTree(switchView);
    ui->treeView->setModel(treeModel);
    ui->treeView->expandAll();
}

DockLive::~DockLive()
{
    qDeleteAll(actionPrograms);
    delete menuPrograms;
    delete sqlModel;
    delete treeModel;
    delete ui;
}

void DockLive::loadTree(bool checked)
{
    treeModel->clear();
    treeModel->setHorizontalHeaderLabels({"チャンネル"});
    setChilds("GR", "地上波", checked);
    setChilds("BS", "BS放送", checked);
    setChilds("CS", "CS放送", checked);
    ui->treeView->expandAll();
}

void DockLive::setChilds(const QString &channelType, const QString &channelName, bool checked)
{
    sqlModel->setWhere(tr("channeltype='%1'").arg(channelType));
    auto model = sqlModel->getModelList(SelectType::selectWhere);
    if (!model.count()) return;

    auto *itemRoot = new QStandardItem(main->getIcon("object-columns"), channelName);
    itemRoot->setData(channelType, Qt::UserRole);
    itemRoot->setEditable(false);

    foreach (auto rs, model) {
        auto id = rs["id"].toString();
        auto name = (main->getApi()->isV2)? "halfWidthName" : "name";
        bool isChecked = checkedIds.contains(id);
        if (!checked || isChecked) {
            auto icon = rs["hasLogoData"].toBool()? main->getApi()->getIcon(tr("/api/channels/%1/logo").arg(id)) : main->getIcon("video-television");
            auto *item = new QStandardItem(icon, rs[name].toString());
            item->setFlags(item->flags() | Qt::ItemIsUserCheckable | Qt::ItemIsSelectable);
            item->setCheckState(isChecked? Qt::Checked : Qt::Unchecked);
            item->setData(id, Qt::UserRole);
            item->setEditable(false);
            itemRoot->appendRow(item);
        }
    }
    treeModel->invisibleRootItem()->appendRow(itemRoot);
}

QModelIndex DockLive::getSelectedIndex()
{
    QModelIndex index;
    if (auto indexes = ui->treeView->selectionModel()->selectedIndexes(); !indexes.isEmpty())
        index = indexes[0];
    return index;
}

QString DockLive::getSelectedId()
{
    QString selectedId;
    if (auto index = getSelectedIndex(); index.isValid())
        selectedId = getId(index);
    return selectedId;
}

QString DockLive::getSelectedText()
{
    QString text;
    if (auto index = getSelectedIndex(); index.isValid())
        text = treeModel->data(index, Qt::DisplayRole).toString();
    return text;
}

QString DockLive::getId(const QModelIndex &index)
{
    QString id;
    if (index.isValid())
        id = treeModel->data(index, Qt::UserRole).toString();
    return id;
}

void DockLive::on_treeView_customContextMenuRequested(const QPoint &pos)
{
    Menu menu(this);
    auto index = ui->treeView->indexAt(pos);
    if (index.isValid()) {
        if (treeModel->hasChildren(index)) ui->treeView->expand(index);

        menu.addAction(ui->actionOpenEPGProgram);

        auto type = treeModel->hasChildren(index)? treeModel->data(index, Qt::UserRole) : treeModel->data(treeModel->parent(index), Qt::UserRole);
        ui->actionOpenEPGProgram->setText(tr("[%1] 番組表を開く").arg(type.toString()));

        foreach (auto action, needSelectActions)
            action->setEnabled(!treeModel->hasChildren(index));

        menu.addAction(ui->actionOpenEPGChannel);
        menu.addSeparator();
        menu.addAction(ui->actionPlay);
        menu.addAction(ui->actionPlayOuter);
        menu.addSeparator();
        menu.addAction(ui->actionCreatePlaylist);
        menu.addSeparator();
        menu.addAction(ui->actionCheckItem);
        menu.addSeparator();
        menu.addAction(ui->actionSetting);

        menu.exec(ui->treeView->viewport()->mapToGlobal(pos));
    }
}

void DockLive::closeEvent(QCloseEvent */*event*/)
{
    main->getSettings()->setValueObj(this, "switchView", ui->actionCheckItem->isChecked());
    main->getSettings()->setValueObj(this, "checkedId", QVariant::fromValue(checkedIds));
}
