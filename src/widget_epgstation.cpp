#include "widget_epgstation.h"
#include "ui_widget_epgstation.h"

#include <QDir>
#include <QFileInfo>
#include <QWebEngineProfile>
#include <QRegularExpression>

// 固定ダウンロードファイル名
const QString playlistFileName = "download_playlist.m3u8";

WidgetEPGStation::WidgetEPGStation(QWidget *parent) :
    QWidget(parent)
  , main(qobject_cast<MainWindow*>(parent))
  , ui(new Ui::WidgetEPGStation)
{
    ui->setupUi(this);
    auto icon = windowIcon();
    setWindowIcon(QIcon(icon.pixmap(icon.actualSize(QSize(22, 22)))));

    connect(ui->actionSetting,&QAction::triggered, this, [=] {
        main->openSettings(SettingMenu::EPGStation);
    });

    ui->webEngineView->setContextMenuPolicy(Qt::CustomContextMenu);
    QString url = main->getSettings()->value("url").toString();
    ui->webEngineView->load(QUrl(url));

    #if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
    connect(QWebEngineProfile::defaultProfile(), &QWebEngineProfile::downloadRequested, this, [=] (QWebEngineDownloadRequest *download) {
    #else
    connect(QWebEngineProfile::defaultProfile(), &QWebEngineProfile::downloadRequested, this, [=] (QWebEngineDownloadItem *download) {
    #endif
    #if QT_VERSION > QT_VERSION_CHECK(5, 14, 0)
        #if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
        connect(download, &QWebEngineDownloadRequest::isFinishedChanged, this, [=] () {
        #else
        connect(download, &QWebEngineDownloadItem::finished, this, [=] () {
        #endif
            QString filePath = QDir(main->getAppDataPath()).filePath(download->downloadFileName());
            if (getSuffix(filePath) == "m3u8")
                main->openPlayerFile(filePath);
        });
        if (getSuffix(download->downloadFileName()) == "m3u8") {
            download->setDownloadDirectory(main->getAppDataPath());   // qt5.14から
            download->setDownloadFileName(playlistFileName);          // qt5.14から
            download->accept(); // ダウンロード許可
        }
    #else
        connect(download, &QWebEngineDownloadItem::finished, this, [=] () {
            QString filePath = download->path();
            if (getSuffix(filePath) == "m3u8")
                main->openPlayerFile(filePath);
        });
        QFileInfo fileInfo(download->path());
        if (fileInfo.suffix().toLower() == "m3u8") {
            download->setSavePageFormat(QWebEngineDownloadItem::SingleHtmlSaveFormat);
            download->setPath(QDir(main->getAppDataPath()).filePath(playlistFileName));
            download->accept(); // ダウンロード許可
        }
    #endif
    });

    connect(ui->webEngineView, &QWebEngineView::urlChanged, this, [=] (const QUrl &url) {
        auto urlStr = url.toString();

        // v2 詳細ページと、情報パネルを同期
        if (urlStr.contains("/recorded/detail/")) {
            urlStr.replace(QRegularExpression(".*detail\\/|\\?.*"), "");
            main->setDockInfoId(InfoType::Recorded, urlStr);
            return;
        }
        // v2 再生ページ（あまり意味ないかも）
        if (urlStr.contains("/recorded/watch?videoId=")) {
            urlStr.replace(QRegularExpression(".*recordedId=|&timestamp=.*"), "");
            main->setDockInfoId(InfoType::Recorded, urlStr);
            return;
        }
    });
}

WidgetEPGStation::~WidgetEPGStation()
{
    delete ui;
}

void WidgetEPGStation::load(const QString &path)
{
    QString url = main->getSettings()->value("url").toString();
    url = path.isEmpty()? url : url + "/#/" + path;
    ui->webEngineView->setUrl(QUrl(url));
}

void WidgetEPGStation::on_webEngineView_customContextMenuRequested(const QPoint &pos)
{
    Menu menu(this);
    menu.addAction(getActionDefault(QWebEnginePage::Back, "戻る", "go-previous"));
    menu.addAction(getActionDefault(QWebEnginePage::Forward, "進む", "go-next"));
    menu.addSeparator();
    menu.addAction(getActionDefault(QWebEnginePage::Copy, "コピー", "edit-copy"));
    menu.addAction(getActionDefault(QWebEnginePage::Paste, "貼付け", "edit-paste"));
    menu.addSeparator();
    menu.addAction(getActionDefault(QWebEnginePage::Reload, "更新", "view-refresh"));
    menu.addSeparator();
    menu.addAction(main->getDockNaviAction());
    menu.addAction(main->getDockInfoAction());
    menu.addSeparator();
    menu.addAction(main->getSimpleViewAction());
    menu.addSeparator();
    menu.addAction(ui->actionSetting);

    menu.exec(this->mapToGlobal(pos));
}

QAction *WidgetEPGStation::getActionDefault(QWebEnginePage::WebAction type, const QString &text, const QString &iconName)
{
    auto action = ui->webEngineView->pageAction(type);
    action->setText(text);
    action->setIcon(main->getIcon(iconName));
    return action;
}
