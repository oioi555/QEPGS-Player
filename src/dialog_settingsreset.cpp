#include "dialog_settingsreset.h"
#include "ui_dialog_settingsreset.h"

#include <QMessageBox>
#include <QSettings>
#include <QProcess>

DialogSettingsReset::DialogSettingsReset(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSettingsReset)
{
    ui->setupUi(this);

    menuItems = {
        {"EPGStationの接続設定を残す", true},
        {"データベースの接続設定を残す", true},
        {"LiveTvのチャンネルフィルター設定を残す", true},
        {"ダイアログなどのサイズ位置記憶のみ削除する", false}
    };

    foreach (auto nemuItem, menuItems) {
        auto item = new QListWidgetItem();
        item->setText(nemuItem.name);
        item->setIcon(QIcon::fromTheme(nemuItem.checked? "document-save" : "delete"));
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable );
        item->setCheckState(nemuItem.checked? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
        ui->listWidget->addItem(item);
    }

    // 残すと削除のチェックボックス排他制御
    connect(ui->listWidget->model(), &QAbstractItemModel::dataChanged, this, [=]
        (const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles) {
            Q_ASSERT(topLeft == bottomRight);
            if (!roles.contains(Qt::CheckStateRole)) return;
            if (topLeft.row() != 3 && topLeft.data(Qt::CheckStateRole) == Qt::Checked) {
                ui->listWidget->item(3)->setData(Qt::CheckStateRole, Qt::Unchecked);
                return;
            }
            for (int i = 0; i < ui->listWidget->model()->rowCount(); i++){
                if (topLeft.row() == 3 && topLeft.data(Qt::CheckStateRole) == Qt::Checked) {
                    if (i == topLeft.row())
                        continue;
                    ui->listWidget->item(i)->setData(Qt::CheckStateRole, Qt::Unchecked);
                }
            }
        });
}

DialogSettingsReset::~DialogSettingsReset()
{
    delete ui;
}

void DialogSettingsReset::on_buttonBox_accepted()
{
    QMessageBox msgBox(qobject_cast<QWidget*>(this->window()));
    msgBox.setWindowTitle("設定リセット確認");
    msgBox.setText("本当にリセットしますか？\nリセット後に再起動します。");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    msgBox.setIcon(QMessageBox::Question);
    if (msgBox.exec() != QMessageBox::Yes) return;

    QSettings settings;
    foreach (auto key, settings.allKeys()) {
        // ダイアログなどのサイズ位置記憶のみ削除する
        if (ui->listWidget->item(3)->data(Qt::CheckStateRole) == Qt::Checked) {
            if (key.contains("/WindowSize")) settings.remove(key);
            if (key.contains("MainWindow/")) settings.remove(key);
            continue;
        }
        // EPGStationの接続設定を残す
        if (ui->listWidget->item(0)->data(Qt::CheckStateRole) == Qt::Checked) {
            if (key == "url") continue;
        }
        // データベースの接続設定を残す
        if (ui->listWidget->item(1)->data(Qt::CheckStateRole) == Qt::Checked) {
            if (key.contains("DB/")) continue;
        }
        // LiveTvのチャンネルフィルター設定を残す
        if (ui->listWidget->item(2)->data(Qt::CheckStateRole) == Qt::Checked) {
            if (key.contains("DockLive/checkedId")) continue;
        }
        settings.remove(key);
    }
    accept();
}

