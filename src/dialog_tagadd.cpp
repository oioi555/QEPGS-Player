#include "dialog_tagadd.h"
#include "ui_dialog_tagadd.h"

DialogTagAdd::DialogTagAdd(int id, QWidget *parent) :
    QDialog(parent)
  , id(id)
  , sqlModel(new SqlModel("recorded" , "id", "v_recorded"))
  , sqlModelTags(new SqlModel("tags" , "id"))
  , main(qobject_cast<MainWindow*>(parent))
  , ui(new Ui::DialogTagAdd)
{
    ui->setupUi(this);

    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);
    connect(sqlModelTags, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    // window
    setWindowTitle("Tag検索・登録");

    // action
    connect(ui->actionSearch, &QAction::triggered, this, [=] {
        auto items = ui->treeWidget->selectedItems();
        if (!items.count()) return;
        QString tag = items[0]->text(0);
        main->setSearchTag(tag);
    });
    connect(ui->actionSearchNewTab, &QAction::triggered, this, [=] {
        if (main->openRecorded())
            ui->actionSearch->trigger();
    });
    connect(ui->actionFiter, &QAction::triggered, this, [=] {
        auto items = ui->treeWidget->selectedItems();
        if (!items.count()) return;
        QString field = items[0]->parent()->text(0);
        QString tag = items[0]->text(0);
        main->setFilterTag(field, tag);
    });
    connect(ui->actionFilterNewTab, &QAction::triggered, this, [=] {
        if (main->openRecorded())
            ui->actionFiter->trigger();
    });

    // combo
    ui->cmbField->addItems(cmbfieldsList);
    QStringList genres = {nullStr};
    genres << sqlModel->getOneFieldValues("select name from genre1");
    ui->cmbGenre->addItems(genres);

    // tree
    connect(ui->treeWidget, &QTreeWidget::itemDoubleClicked, this, [=] (QTreeWidgetItem*, int) {
        ui->actionSearch->trigger();
    });
    connect(ui->treeWidget, &QTreeWidget::currentItemChanged, this, [=] (QTreeWidgetItem* item) {
        if (item->parent() == nullptr) {
            item->setExpanded(true);
            return;
        }
        ui->cmbField->setCurrentText(item->parent()->text(0));
        ui->editTag->setText(item->text(0));
        QString genre = sqlModel->getValue("ジャンル", id).toString();
        ui->cmbGenre->setCurrentText(genre);
    });

    // tree load tag
    rs = sqlModel->getRecord(id);
    if (!rs.isEmpty()) {
        foreach (auto field, cmbfieldsList)
            prseProgram(field, rs.value(field).toString());
    }

    // auto tag
    ui->btnTagAddEndflag->setIcon(main->getIcon("tag"));
    ui->toolTagSetting->setIcon(main->getIcon("configure"));
    ui->txtTitle->setText(rs.value("番組タイトル").toString());

    // 画面サイズの復元
    restoreGeometry( main->getSettings()->valueObj(this, "WindowSize").toByteArray());
}

DialogTagAdd::~DialogTagAdd()
{
    // 画面サイズの保存
    main->getSettings()->setValueObj(this, "WindowSize", saveGeometry());

    delete sqlModelTags;
    delete sqlModel;
    delete ui;
}

void DialogTagAdd::prseProgram(const QString &field, const QString &value)
{
    QStringList parse;
    if (field == "番組タイトル")
        parse << main->parseStrTitle(value);

    parse.append(main->parseStr(value));
    if (!parse.length()) return;

    auto *itemRoot = new QTreeWidgetItem;
    itemRoot->setText(0 , field);
    itemRoot->setIcon(0 , main->getIcon("object-columns"));
    foreach (auto tag, parse) {
        if (!value.isEmpty()) {
            auto *item = new QTreeWidgetItem;
            item->setText(0, tag);
            item->setIcon(0, main->getIcon("tag"));
            itemRoot->addChild(item);
        }
    }
    ui->treeWidget->addTopLevelItem(itemRoot);
    itemRoot->setExpanded(true);
}

void DialogTagAdd::on_treeWidget_customContextMenuRequested(const QPoint &pos)
{
    auto items = ui->treeWidget->selectedItems();
    if (!items.count()) return;
    if (items[0]->parent() == nullptr) return;

    Menu menu(this);
    menu.addAction(ui->actionSearch);
    menu.addAction(ui->actionSearchNewTab);
    menu.addSeparator();
    menu.addAction(ui->actionFiter);
    menu.addAction(ui->actionFilterNewTab);

    menu.exec(ui->treeWidget->viewport()->mapToGlobal(pos));
}

void DialogTagAdd::on_cmbField_currentIndexChanged(int index)
{
    ui->grpOption->setEnabled(index == 0);
}

void DialogTagAdd::on_buttonBox_accepted()
{
    QString field = ui->cmbField->currentText();
    QString tag = ui->editTag->text();
    QString genre = ui->cmbGenre->currentText();

    if (tag.isEmpty()) {
        QMessageBox::warning(this, tr("Tag登録"), tr("Tagを入力して下さい"));
        main->getPop()->show(tr("Tagを入力して下さい"));
        return;
    }

    sqlModelTags->setValueClear();
    sqlModelTags->setValue("field", field);
    sqlModelTags->setValue("tag", tag);
    sqlModelTags->setValue("endflag", ui->chkEndflag->isChecked());
    if (genre == nullStr)
        sqlModelTags->setValue("genre");
    else
        sqlModelTags->setValue("genre", genre);

    addTagId = sqlModelTags->saveValues();

    accept();
}


void DialogTagAdd::on_btnTagAddEndflag_clicked()
{
    auto tag = main->parseStrTitle(rs.value("番組タイトル").toString());
    ui->cmbField->setCurrentText("番組タイトル");
    ui->editTag->setText(tag);
    ui->cmbGenre->setCurrentText(rs.value("ジャンル").toString());
    ui->chkEndflag->setChecked(true);
}

void DialogTagAdd::on_toolTagSetting_clicked()
{
    main->openSettings("Tag", 1);
}

