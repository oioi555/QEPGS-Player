import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import QtGraphicalEffects 1.0    // qt6で廃止
//import Qt5Compat.GraphicalEffects
import "../parts/"

PanePlain {
    id: root
    //anchors.fill: parent //ubuntu fedora(gnome)で、何も表示されなくなる
    bkColor: palette.dark
    bkOpacity: 1
    property color lineColor: "#eee"
    property int paddingText: 5
    property int marginWidth: 10    // セル間のマージン
    property int minColCount: 3     // maxCellWidth基準で、minCellWidthに切替える列数
    property int minCellWidth: 180  // 親の幅が狭い場合のセル幅
    property int maxCellWidth: 240  // 親の幅が広い場合のセル幅
    property int minFontSize: 10
    property int maxFontSize: 11
    property real aspect16x9: 0.54
    Component.onCompleted: { }

    Connections {
        target: WidgetTag
        function onCurrentIndexChanged(currentIndex) {
            gridView.currentIndex = currentIndex;
            //console.log("currentIndexChanged:" + currentIndex);
        }
    }

    Component {
        id: tagDelegate
        ItemDelegate {
            id: itemDelegate
            height: gridView.cellHeight - marginWidth
            width: gridView.cellWidth - marginWidth
            background: Rectangle {
                id: mask
                radius: 5; color: palette.base
            }
            TagItem {
                id: tagItem
                anchors.fill: mask
                layer.enabled: true
                layer.effect: OpacityMask {
                    maskSource: mask
                }
            }
        }
    }

    GridView {
        id: gridView
        anchors {
            fill: parent
            topMargin: marginWidth
            bottomMargin: marginWidth
            leftMargin: marginWidth
            rightMargin: 0
        }
        clip: true
        focus: true
        model: TagModel
        delegate: tagDelegate
        snapMode: GridView.SnapToRow

        // cell幅の計算
        property int innerWidth: width - (anchors.leftMargin - anchors.rightMargin)

        property int maxColumns: Math.max(1, Math.floor(innerWidth / (maxCellWidth + marginWidth)));
        property int maxCellGap: Math.floor((innerWidth % (maxCellWidth * maxColumns)) / maxColumns);

        property int minColumns: Math.max(1, Math.floor(innerWidth / (minCellWidth + marginWidth)));
        property int minCellGap: Math.floor((innerWidth % (minCellWidth * minColumns)) / minColumns);

        property bool isMaxMode: (maxColumns > minColCount)
        property int cellFontSize: isMaxMode? maxFontSize : minFontSize

        property int imageHeight: Math.floor(cellWidth * aspect16x9)

        cellWidth: isMaxMode? maxCellWidth + maxCellGap : minCellWidth + minCellGap
        cellHeight: imageHeight + Math.floor((cellFontSize * 5) + (paddingText * 5))

        currentIndex: WidgetTag.currentIndex;
        highlight: highlight
        highlightFollowsCurrentItem: false
        keyNavigationEnabled: true
        ScrollBar.vertical: ScrollBar {}
    }

    Component {
        id: highlight
        Rectangle {
            width: gridView.cellWidth - marginWidth
            height: gridView.imageHeight
            z:2
            color: palette.highlight
            opacity: 0.6
            radius: 3
            x: (gridView.currentItem)? gridView.currentItem.x : 0
            y: (gridView.currentItem)? gridView.currentItem.y : 0
            Behavior on x { SpringAnimation { spring: 15; damping: 0.8 } }
            Behavior on y { SpringAnimation { spring: 15; damping: 0.8 } }
        }
    }
}
