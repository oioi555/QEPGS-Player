import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

Rectangle {
    property int lineHeight: 1

    Layout.fillWidth: true;
    height: lineHeight;
    color: lineColor
}
