import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

Item {
    property bool horizontal: false
    property bool vertical: false
    Layout.fillWidth: horizontal
    Layout.fillHeight: vertical
}
