import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

ToolBar {
    readonly property color bkColorDefault: palette.button
    property color bkColor: bkColorDefault
    property real bkOpacity: 1

    padding: 0
    background: Rectangle {
        color: parent.bkColor
        opacity: parent.bkOpacity;
    }
}
