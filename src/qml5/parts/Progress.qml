import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

ProgressBar {
    id: control
    property int lineHeight: 4
    readonly property color colorDefault: palette.highlight
    property color barColor: colorDefault
    implicitHeight: lineHeight
    from: 0; to: 100

    background: Rectangle {
        implicitWidth: 200
        implicitHeight: control.lineHeight
        color: palette.window
    }

    contentItem: Item {
        implicitWidth: 200
        implicitHeight: control.lineHeight

        Rectangle {
            width: control.visualPosition * parent.width
            height: parent.height
            color: barColor
        }
    }
}
