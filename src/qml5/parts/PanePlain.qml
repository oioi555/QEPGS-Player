import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
Pane {
    readonly property color bkColorDefault: palette.base
    property color bkColor: bkColorDefault
    property real bkOpacity: 0

    padding: 0
    background: Rectangle {
        color: bkColor
        opacity: parent.bkOpacity;
    }
}
