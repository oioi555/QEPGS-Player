import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

Label {
    id: label
    property color bkColorDefault: palette.base
    property color bkColor: bkColorDefault
    property real bkOpacity: 0
    property color textColor: palette.text

    padding: paddingText
    Layout.preferredWidth: warpTextWidth
    wrapMode: Label.Wrap;
    color: textColor
    background: Rectangle {
        color: label.bkColor
        opacity: label.bkOpacity;
    }
}
