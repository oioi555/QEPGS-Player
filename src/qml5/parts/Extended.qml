import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

ColumnLayout {
    Line { lineHeight: 5 }

    ScrollView {
        id: detailInner
        Layout.fillHeight: true
        Layout.fillWidth: true

        contentHeight: extended.height
        contentWidth: warpTextWidth;

        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff

        TextSelect {
            id: extended
            text: DockInfo.Map["extended"]
            onLinkActivated: Qt.openUrlExternally(link)
            width: warpTextWidth
        }
    }
}
