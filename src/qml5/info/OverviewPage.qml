import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

import "../parts/"

PanePlain {
    id: page
    Component.onCompleted: {
        serverContents.visible = DockInfo.overElement.viewServerContents;
        tagContents.visible = DockInfo.overElement.viewTagContents;
        recordedContents.visible = DockInfo.overElement.viewRecrodedContents;
    }
    ColumnLayout {
        id: content
        width: parent.width
        spacing: 0
        Toolbar {
            Layout.fillWidth: true;
            RowLayout {
                anchors.fill: parent
                spacing: 0
                ToolButton {
                    icon.name: "computer"
                    icon.height: 22; icon.width: 22
                    onClicked: DockInfo.openEPGStation()
                }
                LabelNoWarp { text: "録画サーバー" }
                Spacer { horizontal: true}
                ToolButton {
                    icon.name: serverContents.visible? "go-down" : "go-up"
                    onClicked: {
                        DockInfo.overElement.viewServerContents = !DockInfo.overElement.viewServerContents;
                        serverContents.visible = DockInfo.overElement.viewServerContents;
                    }
                }
                ToolButton {
                    icon.name: "overflow-menu"
                    onClicked: menuServer.open()
                    Menu {
                        id: menuServer
                        x: parent.width - width
                        transformOrigin: Menu.BottomRight
                        Action {
                            icon.name: "configure"
                            text: "EPGStation設定"
                            onTriggered: DockInfo.openSettings("EPGStation")
                        }
                        Action {
                            icon.name: "configure"
                            text: "データベース設定"
                            onTriggered: DockInfo.openSettings("Database")
                        }
                    }
                }
            }
        }
        PanePlain {
            id: serverContents
            Layout.fillWidth: true;
            padding: paddingText; topPadding: paddingText *2; bottomPadding: topPadding
            bkOpacity: 1;
            Flow {
                spacing: 5
                width: parent.width
                LabelMark {
                    text: "EPGStation: " + DockInfo.overElement.epgsVersion
                    isLight : true
                }
                LabelMark {
                    text: "DB: " + DockInfo.overElement.dbType
                    isLight : true
                }
                LabelMark {
                    text: "使用率: " + String(DockInfo.overElement.usedPercent) + " %"
                    isLight : true
                }
                LabelMark {
                    text: "空容量: " + DockInfo.overElement.freeSpase
                    isLight : true
                }
            }
        }
        Progress {
            Layout.fillWidth: true;
            value: DockInfo.overElement.usedPercent
        }
        Line{ lineHeight: 1}
        Toolbar {
            Layout.fillWidth: true;
            RowLayout {
                anchors.fill: parent
                spacing: 0
                ToolButton {
                    icon.name: "tag"
                    icon.height: 22; icon.width: 22
                    onClicked: DockInfo.openTagWidget()
                }
                LabelNoWarp { text: "検索Tag" }
                Spacer { horizontal: true}
                ToolButton {
                    icon.name: tagContents.visible? "go-down" : "go-up"
                    onClicked: {
                        DockInfo.overElement.viewTagContents = !DockInfo.overElement.viewTagContents;
                        tagContents.visible = DockInfo.overElement.viewTagContents;
                    }
                }
                ToolButton {
                    icon.name: "overflow-menu"
                    onClicked: menuTag.open()
                    Menu {
                        id: menuTag
                        x: parent.width - width
                        transformOrigin: Menu.BottomRight
                        Action {
                            icon.name: "configure"
                            text: "検索Tag設定"
                            onTriggered: DockInfo.openSettings("Tag")
                        }
                    }
                }
            }
        }
        PanePlain {
            id: tagContents
            Layout.fillWidth: true;
            padding: paddingText; topPadding: paddingText *2; bottomPadding: topPadding
            bkOpacity: 1;
            Flow {
                spacing: 5
                width: parent.width
                LabelMark {
                    text: "総数: " + String(DockInfo.overElement.totalTag)
                    isLight : true
                }
                LabelMark {
                    text: "録画有: " + String(DockInfo.overElement.totalTagHasVideo)
                    isLight : true
                }
                LabelMark {
                    text: "録画無: " + String(DockInfo.overElement.totalTag - DockInfo.overElement.totalTagHasVideo)
                    isLight : true
                }
                LabelMark {
                    text: "連続番組: " + String(DockInfo.overElement.totalEndflag)
                    isLight : true
                }
                LabelMark {
                    text: "新番組検出: " + String(DockInfo.overElement.totalNewflag)
                    isLight : true
                }
                LabelMark {
                    text: "番組終了: " + String(DockInfo.overElement.totalTagisEndProgram)
                    isLight : true
                }
                LabelMark {
                    text: "未視聴有: " + String(DockInfo.overElement.totalTagNotWatched)
                    isLight : true
                }
                LabelMark {
                    text: "デフォルト: " + String(DockInfo.overElement.totalDefaultflag)
                    isLight : true
                }
                LabelMark {
                    text: "ブラックリスト: " + String(DockInfo.overElement.totalBlackflag)
                    isLight : true
                }
            }
        }
        Progress {
            Layout.fillWidth: true;
            value: ((DockInfo.overElement.totalTagHasVideo - DockInfo.overElement.totalTagNotWatched) / DockInfo.overElement.totalTagHasVideo) * 100
        }
        Line{ lineHeight: 1}
        Toolbar {
            Layout.fillWidth: true;
            RowLayout {
                anchors.fill: parent
                spacing: 0
                ToolButton {
                    icon.name: "folder-videos"
                    icon.height: 22; icon.width: 22
                    onClicked: DockInfo.openRecoded()
                }
                LabelNoWarp { text: "録画済" }
                Spacer { horizontal: true}
                ToolButton {
                    icon.name: recordedContents.visible? "go-down" : "go-up"
                    onClicked: {
                        DockInfo.overElement.viewRecrodedContents = !DockInfo.overElement.viewRecrodedContents;
                        recordedContents.visible = DockInfo.overElement.viewRecrodedContents;
                    }
                }
                ToolButton {
                    icon.name: "overflow-menu"
                    onClicked: menuRecorded.open()
                    Menu {
                        id: menuRecorded
                        x: parent.width - width
                        transformOrigin: Menu.BottomRight
                        Action {
                            icon.name: "configure"
                            text: "録画済リスト設定"
                            onTriggered: DockInfo.openSettings("Recorded")
                        }
                    }
                }
            }
        }
        PanePlain {
            id: recordedContents
            Layout.fillWidth: true;
            padding: paddingText; topPadding: paddingText *2; bottomPadding: topPadding
            bkOpacity: 1;
            Flow {
                spacing: 5
                width: parent.width
                LabelMark {
                    text: "総数: " + String(DockInfo.overElement.totalRecoded)
                    isLight : true
                }
                LabelMark {
                    text: "視聴済: " + String(DockInfo.overElement.totalWatched)
                    isLight : true
                }
                LabelMark {
                    text: "未視聴: " + String(DockInfo.overElement.totalRecoded - DockInfo.overElement.totalWatched)
                    isLight : true
                }
            }
        }
        Progress {
            Layout.fillWidth: true;
            value: (DockInfo.overElement.totalWatched / DockInfo.overElement.totalRecoded) * 100
        }
        Line{ lineHeight: 1}
        Toolbar {
            id: recTitol
            Layout.fillWidth: true;
            RowLayout {
                anchors.fill: parent
                spacing: 0
                ToolButton {
                    icon.name: "media-record"
                    onClicked: DockInfo.openProgram()
                }
                ToolButton {
                    text: "録画中"
                    onClicked: DockInfo.openEPGStation("recording");
                }
                ToolButton {
                    icon.name: "tools-media-optical-burn"
                    text: "エンコード中"
                    onClicked: DockInfo.openEPGStation("encode");
                    visible: DockInfo.Map["isV2"]
                }
                Spacer { horizontal: true}
                ToolButton {
                    icon.name: "overflow-menu"
                    onClicked: menuRecording.open()
                    Menu {
                        id: menuRecording
                        x: parent.width - width
                        transformOrigin: Menu.BottomRight
                        Action {
                            icon.name: "configure"
                            text: "ライブTV設定"
                            onTriggered: DockInfo.openSettings("LiveTV")
                        }
                        Action {
                            icon.name: "process-stop"
                            text: "全てのエンコードをキャンセル"
                            onTriggered: DockInfo.cancelEncord()
                            enabled: DockInfo.Map["isV2"] && DockInfo.hasEncord
                        }
                    }
                }
            }
        }
    }
    RecordedList {
        id: recList
        anchors.top: content.bottom
        anchors.bottom: parent.bottom
    }
}
