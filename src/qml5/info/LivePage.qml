import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import "../parts/"

PanePlain {
    id: page
    bkOpacity: 1
    Content { id: contents }
    Progress {
        id: progress
        anchors.top: contents.bottom
        Layout.fillWidth: true;
        barColor: DockInfo.Map["isRecording"]? "firebrick" : colorDefault
    }
    Extended {
        id: loadExtend
        anchors.top: progress.bottom
        anchors.bottom: parent.bottom
        clip: true
    }
}
