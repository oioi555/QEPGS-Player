import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import "../parts/"

PanePlain {
    id: page
    bkOpacity: 1

    Toolbar {
        id: toolbar
        anchors.left: parent.left
        anchors.right: parent.right

        RowLayout {
            anchors.fill: parent
            ToolButton {
                icon.name: "go-previous"
                visible: DockInfo.Map["isBack"]
                property string toolTipText: "戻る"
                ToolTip.text: toolTipText
                ToolTip.visible: toolTipText ? mouseAreaBack.containsMouse : false
                MouseArea {
                    id: mouseAreaBack
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: DockInfo.backPage()
                }
            }
            ToolButton {
                icon.name: "news-subscribe"
                visible: (DockInfo.currentIndex !== -1)
                property string toolTipText: "選択中の番組情報を表示"
                ToolTip.text: toolTipText
                ToolTip.visible: toolTipText ? mouseArea.containsMouse : false
                MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: DockInfo.setQmlRecordedID(RecModel.get(recList.currentIndex, "id"))
                }
            }
            Spacer{ horizontal: true }
            ToolButton {
                icon.name: "overflow-menu"
                onClicked: optionsMenu.open()
                Menu {
                    id: optionsMenu
                    x: parent.width - width
                    transformOrigin: Menu.TopRight
                    Action {
                        icon.name: "document-save"
                        text: "プレイリストの保存"
                        onTriggered: DockInfo.actionPlaylistSave()
                    }
                }
            }
        }
    }
    RecordedList {
        id: recList
        anchors.top: toolbar.bottom
        anchors.bottom: parent.bottom
    }
}
