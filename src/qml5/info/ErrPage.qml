import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4
import "../parts/"

PanePlain {
    id: page
    bkOpacity: 1
    ColumnLayout {
        LabelWarp { text: DockInfo.Map["description"] }
        Line {}
    }
}
