#include "dialog_regtest.h"
#include "ui_dialog_regtest.h"

DialogRegTest::DialogRegTest(QWidget *parent)
    : QDialog(parent)
    , sqlModel(new SqlModel("regtest", "id"))
    , main(qobject_cast<MainWindow*>(parent))
    , delegate(new RegDelegate(main))
    , ui(new Ui::DialogRegTest)
{
    ui->setupUi(this);

    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    // window
    setWindowTitle("Tag検索・連続番組Tag生成　テスト確認");

    connect(ui->actionAutoSize, &QAction::triggered, this, [=] {
        ui->tableView->autoSizeColRow();
    });

    connect(ui->actionRestFilter, &QAction::triggered, this, [=] {
        ui->tableView->resetFiler();
    });
    connect(ui->tableView, &TableViewSqlFilter::headerResized, this, [=] (int logical, int /*oldSize*/, int /*newSize*/) {
        if (logical == 0)
            updateRowHeight();
    });
    connect(ui->tableView, &TableViewSqlFilter::columnsStateChanged, this, [=] {
        main->getSettings()->setValueObj(this, "columnsState", ui->tableView->getColumnsState());
    });

    sqlModel->execSql("drop table if exists regtest;");
    sqlModel->execSql(tr("CREATE TEMPORARY TABLE regtest(id int, チャンネル text, ジャンル text, 番組タイトル text, 番組名抽出 text)"));

    QApplication::setOverrideCursor(Qt::WaitCursor);

    auto model = sqlModel->getModel("select * from v_recorded");
    for (int i = 0; i < model->rowCount(); i++) {
        auto rs = model->record(i);
        auto result = main->parseStrTitle(rs.value("番組タイトル").toString());

        sqlModel->setValueClear();
        sqlModel->setValue("id", rs.value("id").toInt());
        sqlModel->setValue("番組名抽出", result);
        sqlModel->setValue("チャンネル", rs.value("チャンネル"));
        sqlModel->setValue("ジャンル", rs.value("ジャンル"));
        sqlModel->setValue("番組タイトル", rs.value("番組タイトル"));
        sqlModel->execSql(sqlModel->getSqlInsertFieldsValues());
    }

    ui->tableView->setSqlModel(sqlModel);
    ui->tableView->setItemDelegate(delegate);
    ui->tableView->initSql();
    ui->tableView->autoSizeColRow();
    updateRowHeight();

    QApplication::restoreOverrideCursor();
    showMaximized();
}

DialogRegTest::~DialogRegTest()
{
    delete delegate;
    delete sqlModel;
    delete ui;
}

void DialogRegTest::on_tableView_customContextMenuRequested(const QPoint &pos)
{
    Menu menu(this);

    menu.addAction(ui->actionRestFilter);
    menu.addAction(ui->actionAutoSize);

    menu.exec(ui->tableView->viewport()->mapToGlobal(pos));
}

void DialogRegTest::updateRowHeight()
{
    bool chkViewThumbnail = main->getSettings()->value("WidgetRecorded/chkViewThumbnail").toBool();
    if (chkViewThumbnail && !ui->tableView->horizontalHeader()->isSectionHidden(0)) {
        int width = ui->tableView->horizontalHeader()->sectionSize(0);
        ui->tableView->verticalHeader()->setDefaultSectionSize(static_cast<int>(width * 0.56));
    } else
        ui->tableView->verticalHeader()->setDefaultSectionSize(0);
}

