#ifndef DIALOG_SCREENSHOT_H
#define DIALOG_SCREENSHOT_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {
class DialogScreenShot;
}

class DialogScreenShot : public QDialog
{
    Q_OBJECT

public:
    explicit DialogScreenShot(int id, QWidget *parent);
    ~DialogScreenShot();

private:
    void loadNewestScreenShot();
    void updateTagListItem();
    void updateImageFile();

    int id;
    QString fileFilterId;
    QString screenshotsPath;
    QString imageFilePath;

    SqlModel *sqlModel;
    SqlModel *sqlModelTag;
    MainWindow* main;
    Ui::DialogScreenShot *ui;

    // QWidget interface
protected:
    virtual void resizeEvent(QResizeEvent *event) override;

    // QWidget interface
protected:
    virtual void showEvent(QShowEvent *event) override;
private slots:
    void on_buttonBox_accepted();
};

#endif // DIALOG_SCREENSHOT_H
