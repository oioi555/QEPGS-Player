#include "dialog_tagedit.h"
#include "dialog_tagthumbnail.h"
#include "ui_dialog_tagedit.h"

#include <QMimeData>
#include <QProcess>

#include "util/thumbnailtag.h"

DialogTagEdit::DialogTagEdit(QWidget *parent, int id, const QString &field, const QString &genre) :
    QDialog(parent)
  , id(id)
  , sqlModel(new SqlModel("tags" , "id"))
  , main(qobject_cast<MainWindow*>(parent))
  , ui(new Ui::DialogTagEdit)
{
    ui->setupUi(this);

    setWindowTitle((id == -1? "Tagの追加" : "Tagの編集"));

    ui->cmbField->addItems(cmbfieldsList);

    QStringList genres = {nullStr};
    genres << sqlModel->getOneFieldValues("select name from genre1");
    ui->cmbGenre->addItems(genres);

    if (id != -1) {
        QSqlRecord rs = sqlModel->getRecord(id);
        ui->editTag->setText(rs.value("tag").toString());
        ui->cmbField->setCurrentText(rs.value("field").toString());
        ui->chkEndflag->setChecked(rs.value("endflag").toBool());
        ui->chkBlack->setChecked(rs.value("blackflag").toBool());
        ui->cmbGenre->setCurrentText(rs.value("genre").toString());

        ui->laDate->setText("登録日：" + rs.value("date").toDateTime().toString("M/d (ddd) hh:mm"));
        ui->chkDefault->setChecked(rs.value("defaultflag").toBool());
        ui->chkNewflag->setChecked(rs.value("newflag").toBool());
        ui->grpOption->setEnabled(ui->cmbField->currentIndex() == 0);
    } else {
        if (!field.isEmpty())
            ui->cmbField->setCurrentText(field);
        if (!genre.isEmpty()) {
            if (genre == "もう見ないリスト")
                ui->chkBlack->setChecked(true);
            else
                ui->cmbGenre->setCurrentText(genre);
        }
        QDateTime date = QDateTime::currentDateTime();
        ui->laDate->setText("登録日：" + date.toString("M/d (ddd) hh:mm"));

        // TagIDが未確定なので無効化する
        ui->groupThumbnail->setEnabled(false);
    }

    // ui setting
    connect(ui->btnWebSearch, &QPushButton::clicked, this, [=] () {
        DialogTagThumbnail dlg(id, main);
        if (dlg.exec() == QDialog::Accepted) updateImageFile();
    });
    connect(ui->btnDelete, &QPushButton::clicked, this, [=] () {
        if (imgTag->remove()) updateImageFile();
    });
    connect(ui->btnSelectFile, &QPushButton::clicked, this, [=] () {
        if (imgTag->openFileSelectDlg()) updateImageFile();
    });

    // Thumbnail
    imgTag = new ThumbnailTag(main, id);
    updateImageFile();
    connect(ui->laImage, &LabelDrop::changed, this, &DialogTagEdit::on_dropFile);

    ui->editTag->setFocus();

    // 画面サイズの復元
    restoreGeometry( main->getSettings()->valueObj(this, "WindowSize").toByteArray());
}

DialogTagEdit::~DialogTagEdit()
{
    // 画面サイズの保存
    main->getSettings()->setValueObj(this, "WindowSize", saveGeometry());

    delete sqlModel;
    delete imgTag;
    delete ui;
}

void DialogTagEdit::on_buttonBox_accepted()
{
    QString field = ui->cmbField->currentText();
    QString tag = ui->editTag->text();
    QString genre = ui->cmbGenre->currentText();

    if (tag.isEmpty()) {
        QMessageBox::warning(this, tr("Tag登録"), tr("Tagを入力して下さい"));
        return;
    }

    sqlModel->setValue("field", field);
    sqlModel->setValue("tag", tag);
    sqlModel->setValue("endflag", ui->chkEndflag->isChecked());
    sqlModel->setValue("blackflag", ui->chkBlack->isChecked());
    sqlModel->setValue("defaultflag", ui->chkDefault->isChecked());
    sqlModel->setValue("newflag", ui->chkNewflag->isChecked());

    if (genre == nullStr)
        sqlModel->setValue("genre");
    else
        sqlModel->setValue("genre", genre);

    id = sqlModel->saveValues(id);
    accept();
}

void DialogTagEdit::on_cmbField_currentIndexChanged(int index)
{
    ui->grpOption->setEnabled(index == 0);
}

void DialogTagEdit::on_dropFile(const QMimeData *mimeData)
{
    if (!mimeData) return;
    if (mimeData->hasText()) {
        //qDebug() << "mimeData->hasText(): " << mimeData->text();
        if (!imgTag->saveDropUrlFile(QUrl(mimeData->text()))) return;
        updateImageFile();
    }
}

void DialogTagEdit::updateImageFile()
{
    QPixmap pix = QPixmap(imgTag->getImageFilePath(false));
    ui->laImage->setPixmap(pix.scaled(ui->laImage->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
}

void DialogTagEdit::resizeEvent(QResizeEvent */*event*/)
{
    updateImageFile();
}
