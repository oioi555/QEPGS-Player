#ifndef WIDGET_EPGSTATION_H
#define WIDGET_EPGSTATION_H

#include <QNetworkRequest>
#include <QWebEnginePage>
#include <QWidget>
#include "mainwindow.h"

namespace Ui {class WidgetEPGStation;}

class WidgetEPGStation : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetEPGStation(QWidget *parent = nullptr);
    ~WidgetEPGStation();
    void load(const QString &path);

private slots:
    void on_webEngineView_customContextMenuRequested(const QPoint &pos);

private:
    QString getSuffix(const QString &filePath) {
        QFileInfo fileInfo(filePath);
        return fileInfo.suffix().toLower();
    }
    QAction* getActionDefault(QWebEnginePage::WebAction type, const QString &text, const QString &iconName);
    MainWindow* main;
    Ui::WidgetEPGStation *ui;
};

#endif // WIDGET_EPGSTATION_H
