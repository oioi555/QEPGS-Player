#include "dock_info.h"
#include "ui_dock_info.h"
#include "util/tagitem.h"

#include <QQmlContext>
#include <QRegularExpression>

DockInfo::DockInfo(QWidget *parent) :
    QDockWidget(parent)
  , sqlModel(new SqlModel("recorded", "id", "v_recorded"))
  , sqlModelDetail(new SqlModel("recorded", "id", "v_recorded_detail"))
  , sqlModelLive(new SqlModel("programs", "id", "v_programs_detail"))
  , sqlModelTag(new SqlModel("tags", "id"))
  , recModel(new RecModel())
  , tagModel(new TagModel())
  , videoModel(new VideoModel())
  , imgTag(new ThumbnailTag(parent))
  , menuIds(new Menu())
  , tagItem(new TagItem())
  , timerCheckRecording(new QTimer(this))
  , main(qobject_cast<MainWindow*>(parent))
  , ui(new Ui::DockVideoInfo)
{
    ui->setupUi(this);
    this->setTitleBarWidget(new QWidget(this));

    actionIds << new QAction(main->getIcon("folder-videos"), "録画ID")
              << new QAction(main->getIcon("video-television"), "チャンネルID")
              << new QAction(main->getIcon("tag"), "TagID");

    menuIds->addActions(actionIds);
    menuIds->addSeparator();
    menuIds->addAction(new QAction(main->getIcon("go-home"), "概要"));
    ui->toolReload->setMenu(menuIds);
    connect(menuIds, &Menu::triggered, this, [=] (QAction* action) {
        InfoType type = InfoType::Recorded;
        if (action->text() == "チャンネルID") type = InfoType::Live;
        if (action->text() == "TagID") type = InfoType::Tag;
        if (action->text() == "概要") type = InfoType::Overview;
        loadPage(type, ui->editID->text());
    });
    connect(ui->toolReload, &QToolButton::clicked, this, [=] () { reload(); });
    connect(ui->editID, &QLineEdit::editingFinished, ui->toolReload, &QToolButton::click);
    connect(ui->toolHide, &QToolButton::clicked, this, [=] () { main->setDockInfoState(false); });

    // mainwindow
    connect(main, &MainWindow::mainStatusChanged, this, [=] (MainStatus status) {
        if (status == MainStatus::VideoDeleted)
            reload();
        if (status == MainStatus::TagTabel) {
            if (!initLoad) {
                loadPage(InfoType::Overview);
                initLoad = true;
            }
            if (infoType == InfoType::Tag)
                loadPage(InfoType::Tag, QString::number(tagId));
        }
    });

    connect(main, &MainWindow::playerStatusChanged, this, [=] (PlayerStatus status, int id) {
        if (status == PlayerStatus::ControlBarHide)
            animeCtlBarHide->start();
        if (status == PlayerStatus::ControlBarShow)
            animeCtlBarShow->start();
        if (status == PlayerStatus::PlayerSeek)
            updateStatus(id);
    });

    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);
    connect(sqlModelDetail, &SqlModel::sqlError, main, &MainWindow::onSqlError);
    connect(sqlModelLive, &SqlModel::sqlError, main, &MainWindow::onSqlError);
    connect(sqlModelTag, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    // quickWidget
    QFile file(":/css/qml.css");
    if (file.open(QFile::ReadOnly)) {
        qmlCss = QString(file.readAll());
        file.close();
    }

    clearMap();
    ui->quickWidget->setResizeMode(QQuickWidget::SizeRootObjectToView);
    ui->quickWidget->rootContext()->setContextProperty("DockInfo", this);
    ui->quickWidget->rootContext()->setContextProperty("RecModel", recModel);
    ui->quickWidget->rootContext()->setContextProperty("TagModel", tagModel);
    ui->quickWidget->rootContext()->setContextProperty("VideoModel", videoModel);

#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
    ui->quickWidget->setSource(QUrl("qrc:/qml/info/main.qml"));
#else
    ui->quickWidget->setSource(QUrl("qrc:/qml5/info/main.qml"));
#endif

    // 録画中検出インターバルタイマー
    timerCheckRecording->setInterval(3000);
    timerCheckRecording->setTimerType(Qt::CoarseTimer);
    timerCheckRecording->setSingleShot(false);
    timerCheckRecording->start();
    connect(timerCheckRecording, &QTimer::timeout, this, [=] {
        static int prevRecCount = 0;
        int recCount = updateRecordingList();
        bool isRecCountChanged = (recCount != prevRecCount);
        if (isRecCountChanged) prevRecCount = recCount;

        static int prevEncCount = 0;
        int encCount = main->updateEncordingList();
        bool isEecCountChanged = (encCount != prevEncCount);
        if (isEecCountChanged) prevEncCount = encCount;

        if (isRecCountChanged || isEecCountChanged) {
            reload();
            emit recordingChanged(recCount, encCount);
        } else {
            if (!main->isPlaying()) // 再生中は、シークの更新で処理
                updateStatus(lastStausId);
        }
    });
    animeCtlBarHide = new QPropertyAnimation(ui->fmTootBar, "geometry");
    animeCtlBarHide->setDuration(300);
    connect(animeCtlBarHide, &QPropertyAnimation::finished, this, [=] () {
        ui->fmTootBar->hide();
    });
    animeCtlBarShow = new QPropertyAnimation(ui->fmTootBar, "geometry");
    animeCtlBarShow->setDuration(200);
    connect(animeCtlBarShow, &QPropertyAnimation::finished, this, [=] () {
        ui->fmTootBar->show();
    });
    // overviewのアコーディオン状態記録
    overElement.viewServerContents = main->getSettings()->valueObj(this, "viewOverServerContents", true).toBool();
    overElement.viewTagContents = main->getSettings()->valueObj(this, "viewTagOverContents", true).toBool();
    overElement.viewRecrodedContents = main->getSettings()->valueObj(this, "viewOverRecrodedContents", true).toBool();

    updateRecordingList();
}

DockInfo::~DockInfo()
{
    ui->quickWidget->setSource(QUrl()); // 終了時のエラーメッセージ抑制
    qDeleteAll(actionIds);
    delete sqlModel;
    delete sqlModelDetail;
    delete sqlModelLive;
    delete sqlModelTag;
    delete videoModel;
    delete recModel;
    delete tagModel;
    delete imgTag;
    delete tagItem;
    delete timerCheckRecording;
    delete animeCtlBarHide;
    delete animeCtlBarShow;
    delete ui;
}

void DockInfo::reload(int id /*= -1*/)
{
    QString idStr = (id == -1)? ui->editID->text() : QString::number(id);
    loadPage(infoType, idStr);
}

void DockInfo::loadPage(InfoType type, const QString &idStr, const QString &searchWord)
{
    if (!main->isPlaying())
        animeCtlBarShow->start();

    // 番組情報の戻る処理
    if (type == InfoType::Recorded &&
        (infoType == InfoType::Tag || infoType == InfoType::Playlist)) {
        backInfoType = infoType;
        backTagId = tagId;
        isBack = true;
    } else isBack = false;

    static const QStringList typeNames = { "概要", "録画ID", "チャンネルID", "TagID", "プレイリスト" };

    infoType = type;
    ui->editID->setText(idStr);
    ui->laID->setText(typeNames[static_cast<int>(type)]);

    clearMap();

    if (type != InfoType::Overview && type != InfoType::Playlist) {
        setMapData("state", "err");
        setMapData("name", typeNames[static_cast<int>(type)] + "情報を表示できません");

        QString img = type == InfoType::Live? "qrc:/images/livetv.png" : "qrc:/images/noimage.png";
        setMapData("thumbnail", img);

        if (idStr.toInt() == -1 || idStr.isEmpty()) {
            setMapData("description", typeNames[static_cast<int>(type)] + "情報のIDが未指定です");
            ui->editID->setText("");
            emit mapChanged(map); return;
        }
    }

    switch (type) {
    case InfoType::Recorded:
        setRecordedId(idStr.toInt(), searchWord); break;
    case InfoType::Live:
        setLiveTvId(idStr); break;
    case InfoType::Tag:
        setTagId(idStr.toInt()); break;
    case InfoType::Overview:
        loadOverview(); break;
    case InfoType::Playlist:
        loadPlaylist(); break;
    }
    emit tagElementChanged();
    emit mapChanged(map);
    emit currentIndexChanged(currentIndex);
}

void DockInfo::loadOverview()
{
    setMapData("state", "overview");
    setMapData("thumbnail", "qrc:/images/overview.png");
    setMapData("name", qApp->applicationName() + " : " + qApp->applicationVersion());

    // server
    auto epgsVersion = main->getApi()->epgsVersion;
    overElement.epgsVersion = epgsVersion.isEmpty()? "v1" : epgsVersion;
    overElement.dbType = sqlModel->getDriverName();
    auto json = main->getApi()->getStorage();
    overElement.freeSpase = main->getFileSizeStr(json["free"].toDouble());
    overElement.usedPercent = (json["used"].toDouble() / json["total"].toDouble()) * 100;

    // tag count
    sqlModelTag->setWhere("");
    overElement.totalTag = sqlModelTag->getRecordCount();
    overElement.totalTagWatched = sqlModelTag->getRecordCountWhere("tag_watched_count > 0");
    overElement.totalTagNotWatched = sqlModelTag->getRecordCountWhere("tag_video_count > 0 and tag_watched_count <> tag_video_count");
    overElement.totalEndflag = sqlModelTag->getRecordCountWhere("endflag = true");
    overElement.totalTagHasVideo = sqlModelTag->getRecordCountWhere("tag_video_count > 0");
    overElement.totalTagisEndProgram = sqlModelTag->getRecordCountWhere("tag_is_endprogram = true");
    overElement.totalNewflag = sqlModelTag->getRecordCountWhere("newflag = true");
    overElement.totalDefaultflag = sqlModelTag->getRecordCountWhere("defaultflag = true");
    overElement.totalBlackflag = sqlModelTag->getRecordCountWhere("blackflag = true");

    // recorded count
    sqlModel->setWhere("");
    overElement.totalRecoded = sqlModel->getRecordCount();
    QString fix = "::integer";
    overElement.totalWatched = sqlModel->getRecordCountWhere(
        tr("再生 > get_var('Threshold')%1").arg(sqlModel->isPgsql()? fix : ""));

    // recording
    int index = 0;
    recModel->resetModel();

    QStringList ids;
    foreach (auto id, recordingIds)
        ids << QString::number(id);
    foreach (auto item, main->getEncordingItems())
        ids << QString::number(item.id);

    if (!ids.count()) {
        currentIndex = -1;
        emit currentIndexChanged(currentIndex);
        return;
    }

    sqlModelDetail->setOrderBy("datestart desc");
    sqlModelDetail->setWhere(tr("id in (%1)").arg(ids.join(",")));
    auto model = sqlModelDetail->getModel(SelectType::selectWhere);
    for (int i = 0; i < model->rowCount(); i++) {
        auto rs = model->record(i);
        int id = rs.value("id").toInt();
        RecElement element = main->getEncordElement(id);
        element.id = id;
        element.name = escape(rs.value("name"), "tag");
        element.startTime = rs.value("datestart").toDateTime();
        element.endTime = rs.value("datestop").toDateTime();
        element.date = getDateTimeFormat(&rs);
        element.isRecording = checkRecording(element.id);
        element.percent = getPercent(element.isRecording, element.id);
        element.isPlay = (element.id == recordedId);
        recModel->append(element);

        if (element.isPlay) index = i;
    }

    lastStausId = -1;
    currentIndex = index;
    emit overElementChanged();
}

void DockInfo::setRecordedId(int id, const QString &searchWord)
{
    recordedId = id;
    QSqlRecord rs = sqlModelDetail->getRecord(recordedId);
    if (rs.isEmpty()) {
        setMapData("description", tr("%1: 指定されたIDが見つかりません").arg(recordedId) );
        return;
    }

    setMapData("state", "recorded");
    setMapData("isViewContents", main->getSettings()->valueObj(this, "viewRecordedContents", true).toBool());

    // videofileリストの作成
    videoModel->resetModel();
    if (main->getApi()->isV2) {
        auto model = sqlModel->getModel(tr("select * from video_file Where recordedId=%1").arg(id));
        for (int i = 0; i < model->rowCount(); i++) {
            auto rs = model->record(i);
            VideoElement video;
            video.id = rs.value("id").toInt();
            video.recordedId = id;
            video.name = tr("%1 (%2)")
                             .arg(rs.value("name").toString(), main->getFileSizeStr(rs.value("size").toDouble()));
            videoModel->append(video);
        }
    } else {
        VideoElement video;
        video.id = id;
        video.recordedId = id;
        video.name = tr("%1 (%2)").arg(rs.value("videotype").toString(),
                        main->getFileSizeStr(rs.value("filesize").toDouble()));
        videoModel->append(video);
    }

    // 本体
    bool isRecording = checkRecording(recordedId);
    lastStausId = recordedId;

    auto name = rs.value("name").toString();
    setMapData("id", &rs);
    setMapData("thumbnail", main->getApi()->getThumbnailPath(QString::number(recordedId), true));
    setMapDataHtml("name", name, searchWord);
    setMapDataHtml("description", &rs, searchWord);
    setMapData("channel", &rs);
    setMapData("date", getDateTimeFormat(&rs));
    setMapData("progress", getPercent(isRecording, recordedId));
    setMapData("genre", getGenre(&rs));
    setMapData("isRecording", isRecording);
    setMapDataHtml("extended", &rs, searchWord);

    // encord
    RecElement element = main->getEncordElement(id);
    if (element.encId != -1) {
        setMapData("encId", element.encId);
        setMapData("isEncording", element.isEncording);
        setMapData("encPercent", element.encPercent);
        setMapData("encMode", element.encMode);
    } else {
        setMapData("encId", -1);
    }

    // tagの検出
    QSqlRecord rsv = sqlModel->getRecord(recordedId);
    tagModel->resetModel();
    sqlModelTag->setWhere("");
    sqlModelTag->setOrderBy("field desc");
    auto model = sqlModelTag->getModel(SelectType::selectWhere);
    for (int i = 0; i < model->rowCount(); i++) {
        const auto rsTag = model->record(i);
        auto field = rsTag.value("field").toString();
        if (rsv.value(field).toString().contains(rsTag.value("tag").toString(), Qt::CaseInsensitive)) {
            tagModel->append(tagItem->getElement(rsTag));
        }
    }
}

void DockInfo::setTagId(int id)
{
    tagId = id;
    auto rs = sqlModelTag->getRecord(tagId);
    if (rs.isEmpty()) {
        setMapData("description", tr("%1: 指定されたIDが見つかりません").arg(tagId) );
        return;
    }
    imgTag->setId(id);
    tagElement = tagItem->getElement(rs);
    tagElement.hasThumbnail = imgTag->isFileExist();

    setMapData("state", "tag");
    setMapData("tagid", tagId);
    setMapData("thumbnail", imgTag->getImageFilePath());
    setMapDataHtml("name", tagElement.tag);
    setMapData("isViewContents", main->getSettings()->valueObj(this, "viewTagContents", true).toBool());

    int index = 0;
    recModel->resetModel();

    QString escapedTag = sqlModel->escape(tagElement.tag);
    auto ids = sqlModel->getValues("id", tr("%1 like '%%2%'").arg(tagElement.field, escapedTag));
    if (!ids.count()) {
        currentIndex = -1;
        emit currentIndexChanged(currentIndex);
        return;
    }

    sqlModelDetail->setOrderBy("datestart desc");
    sqlModelDetail->setWhere(tr("id in (%1)").arg(ids.join(",")));
    auto model = sqlModelDetail->getModel(SelectType::selectWhere);
    for (int i = 0; i < model->rowCount(); i++) {
        auto rsRec = model->record(i);
        int id = rsRec.value("id").toInt();
        RecElement element = main->getEncordElement(id);
        element.id = id;
        element.name = escape(rsRec.value("name"), "tag");
        element.startTime = rsRec.value("datestart").toDateTime();
        element.endTime = rsRec.value("datestop").toDateTime();
        element.date = getDateTimeFormat(&rsRec);
        element.isRecording = checkRecording(element.id);
        element.percent = getPercent(element.isRecording, element.id);
        element.isPlay = (element.id == recordedId);
        recModel->append(element);

        if (element.isPlay) index = i;
    }
    lastStausId = -1;
    currentIndex = index;
}

void DockInfo::loadPlaylist()
{
    auto playlist = main->getActivePlayerPlaylist();
    if (playlist == nullptr) {
        loadPage(InfoType::Overview, "");
        return;
    }

    setMapData("state", "playlist");
    setMapData("thumbnail", "qrc:/images/noimage.png");
    setMapDataHtml("name", "プレイリスト");

    int index = 0;
    recModel->resetModel();

    auto playItems = playlist->getItems();
    if (!playItems.count()) {
        currentIndex = -1;
        emit currentIndexChanged(currentIndex);
        return;
    }

    for (int i = 0; i < playItems.count(); i++) {
        auto rs = sqlModelDetail->getRecord(playItems[i].recordedId);
        int id = rs.value("id").toInt();
        RecElement element = main->getEncordElement(id);
        element.id = id;
        element.name = escape(rs.value("name"), "tag");
        element.startTime = rs.value("datestart").toDateTime();
        element.endTime = rs.value("datestop").toDateTime();
        element.date = getDateTimeFormat(&rs);
        element.isRecording = checkRecording(element.id);
        element.percent = getPercent(element.isRecording, element.id);
        element.isPlay = (element.id == recordedId);
        recModel->append(element);

        if (element.isPlay) index = i;
    }
    setMapData("thumbnail", main->getApi()->getThumbnailPath(QString::number(recordedId), true));
    lastStausId = -1;
    currentIndex = index;
}

void DockInfo::setLiveTvId(const QString &idStr)
{
    liveId = idStr;
    auto now = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm");
    sqlModelLive->setWhere(tr("channelid='%1' and (datestart <= '%2' and datestop > '%2')").arg(liveId, now));
    sqlModelLive->setOrderBy("datestart");
    sqlModelLive->setLimit(LimitType::limitTop, 1);
    //qDebug() << sqlModelLive->getSqlSelectWhere();
    auto model = sqlModelLive->getModel(SelectType::selectWhere);

    if (model->rowCount() == 0) {
        setMapData("description", "番組情報は見つかりませんでした");
        return;
    }

    auto rs = model->record(0);

    programId = rs.value("id").toString(); // 録画中判定に使用する
    setMapData("state", "live");
    setMapDataHtml("name", &rs);
    setMapDataHtml("description", &rs);
    setMapData("channel", &rs);
    setMapData("date", getDateTimeFormat(&rs));
    setMapData("progress", getPercent(true));
    setMapData("genre", getGenre(&rs));
    setMapData("isRecording", checkRecording());
    setMapDataHtml("extended", &rs);
}

void DockInfo::clearMap()
{
    const QStringList fields =
      { "state", "id", "tagid", "name", "description", "channel", "date", "genre", "extended" };
    foreach (auto field, fields)
        setMapData(field, "");
    setMapData("progress", QVariant(0));
    setMapData("isRecording", false);
    setMapData("isViewContents", true);
    setMapData("isV2", main->getApi()->isV2);
    setMapData("isBack", isBack);
    // encord
    setMapData("encId", QVariant(-1));
    setMapData("isEncording", false);
    setMapData("encPercent", QVariant(0));
    setMapData("encMode", "");
}

void DockInfo::setPlayingId(int id, const QString &searchWord)
{
    // 状況に応じて、どのパネルを選択するか判断して更新する
    // liveは、対象外なので、idは数値
    if (infoType == InfoType::Tag || infoType == InfoType::Playlist) {
        // 表示中のTagに該当レコードがあればindexの変更
        bool isCurrentIndexChanged = false;
        for (int row = 0; row < recModel->rowCount(); row++) {
            auto item = recModel->getRow(row);
            if (item.id == id) {
                currentIndex = row;
                recordedId = id;
                isCurrentIndexChanged = true;
            }
            item.isPlay = (item.id == id);
            recModel->replace(row, item);
        }
        if (isCurrentIndexChanged) {
            if (infoType == InfoType::Playlist) {
                setMapData("thumbnail", main->getApi()->getThumbnailPath(QString::number(recordedId), true));
                emit mapChanged(map);
            }
            emit currentIndexChanged(currentIndex);
            return;
        }
    }
    if (infoType == InfoType::Recorded) {
        if (recordedId == id)  return;
    }

    //qDebug() << "DockInfo::setPlayId: Recorded";
    loadPage(InfoType::Recorded, QString::number(id), searchWord);
}

auto DockInfo::escape(const QVariant &value, const QString &idName, const QString &searchWord) -> QString
{
    auto result = value.toString().toHtmlEscaped();
    result.replace("\r", "\n");
    result.replace("\n\n", "\n");
    result.replace("'" , "&#39;");
    result.replace("\n", "<br/>");
    result.replace("<br/><br/>", "<br/>");

    if (!searchWord.isEmpty()) {
        QStringList words = searchWord.split(" ");
        result.replace(QRegularExpression("(" + words.join("|") + ")"), R"(<span class="highlight">\1</span>)");
    }
    if (idName == "name") {
        result.replace(QRegularExpression("\\[(.)\\]"), R"(&nbsp;<span class="mark">&nbsp;\1&nbsp;</span>&nbsp;)");
        result = "<h2>" + result + "</h2>";
    }
    if (idName == "tag") {
        result.replace(QRegularExpression("\\[(.)\\]"), R"(&nbsp;<span class="mark">&nbsp;\1&nbsp;</span>&nbsp;)");
    }
    if (idName == "description") {
        result.replace(QRegularExpression("\\[(.*?)\\]"), "&nbsp;<span class='markTag'>&nbsp;\\1&nbsp;</span>&nbsp;");
    }
    if (idName == "extended") {
        result = result + "<br/>";
        result.replace(QRegularExpression("https://(.*?)(<br/>|\\s)"), "<a href=\"https://\\1\">https://\\1</a>\\2");
        result.replace(QRegularExpression("http://(.*?)(<br/>|\\s)"), "<a href=\"http://\\1\">http://\\1</a>\\2");
        result.replace(QRegularExpression("◇(.*?)<br/>"), R"(<h3>\1</h3>)");
        result.replace(QRegularExpression("【(.*?)】"), "<span class=\"tag\">【\\1】</span>");
    }

    result = QString(htmlFormat).arg(qmlCss, result);
    //qDebug() << result;
    return result;
}

auto DockInfo::getDateTimeFormat(QSqlRecord *rs) -> QString
{
    startDateTime = rs->value("datestart").toDateTime();
    endDateTime = rs->value("datestop").toDateTime();
    return getDateTimeFormat(startDateTime, endDateTime);
}

QString DockInfo::getDateTimeFormat(const QDateTime &startTime, const QDateTime &endTime)
{
    uint time = (endTime.toSecsSinceEpoch() - startTime.toSecsSinceEpoch()) / 60;
    return tr("%1 〜 %2 (%3m)")
                       .arg(startTime.toString("MM/dd (ddd) hh:mm")
                            ,endTime.toString("hh:mm")).arg(time);
}

auto DockInfo::getGenre(QSqlRecord *rs) -> QString
{
    QStringList genre = QStringList() << rs->value("genre1_name").toString();
    if (!rs->value("genre2_name").toString().isEmpty())
            genre << rs->value("genre2_name").toString();
    return genre.join("｜");
}

int DockInfo::getPercent(bool isOntime, int id /*= -1*/)
{
    if (!isOntime)
        return sqlModelDetail->getValue("percent", id).toInt();

    // live or recording
    QDateTime now = QDateTime::currentDateTime();
    uint timeFull = (endDateTime.toSecsSinceEpoch() - startDateTime.toSecsSinceEpoch());
    uint current = now.toSecsSinceEpoch() - startDateTime.toSecsSinceEpoch();
    return ((float)current / (float)timeFull) * 100;
}

int DockInfo::getPercent(bool isOntime, int id, const QDateTime &startTime, const QDateTime &endTime)
{
    startDateTime = startTime;
    endDateTime = endTime;
    return getPercent(isOntime, id);
}

bool DockInfo::checkRecording(int id)
{
    int checkId = id;
    if (infoType == InfoType::Live) {
        if (programId.isEmpty()) return false;
        auto sql = tr("select id from recorded where programId=%1").arg(programId);
        checkId = sqlModel->getScholar(sql).toInt();
    }
    return recordingIds.contains(checkId);
}

int DockInfo::updateRecordingList()
{
    QString sql = "select id from v_recorded_detail where ";
    if (sqlModel->isPgsql())
        sql += "to_timestamp(datestart, 'YYYY-MM-DD HH24:MI') < now() and to_timestamp(datestop, 'YYYY-MM-DD HH24:MI') > now()";
    else
        sql += "datestart < now() and datestop > now();";

    auto values = sqlModel->getOneFieldValues(sql);
    recordingIds.clear();
    if (values.count())
        foreach (auto value , values)
            recordingIds << value.toInt();
    return recordingIds.count();
}

void DockInfo::updateStatus(int id /*= -1*/)
{
    switch (infoType) {
    case InfoType::Live: {
        int percent = getPercent(true);
        if (percent <= 100) {
            setMapData("progress", percent);
            setMapData("isRecording", checkRecording());
            emit progressChanged();
        }
        break;
    }
    case InfoType::Recorded: {
        if (recordedId == id) {
            bool isRecording = checkRecording(id);
            setMapData("progress", getPercent(isRecording, id));
            setMapData("isRecording", isRecording);
            auto element = main->getEncordElement(id);
            if (element.encId != -1) {
                setMapData("encPercent", element.encPercent);
                setMapData("isEncording", element.isEncording);
                emit encProgressChanged();
            }
            emit progressChanged();
        }
        break;
    }
    case InfoType::Overview:
    case InfoType::Playlist:
    case InfoType::Tag: {
        static bool prevHasEncord = false;
        hasEncord = false;
        for (int row = 0; row < recModel->rowCount(); row++) {
            auto item = recModel->getRow(row);
            item.isRecording = checkRecording(item.id);
            item.percent = getPercent(item.isRecording, item.id, item.startTime, item.endTime);
            if (item.encId != -1) {
                item.encPercent = main->getEncordElement(item.id).encPercent;
                hasEncord = true;
            }
            recModel->replace(row, item);
        }
        if (prevHasEncord != hasEncord)
            emit hasEncordChanged();

        break;
    }}
    lastStausId = id;
}

void DockInfo::resizeEvent(QResizeEvent *event)
{
    static const int ctlHeight = 40;
    QSize sizeCtl(event->size().width(), ctlHeight);
    QPoint ptShow(0, event->size().height() - ctlHeight);
    QPoint ptHide(0, event->size().height());

    animeCtlBarHide->setStartValue(QRect(ptShow, sizeCtl));
    animeCtlBarHide->setEndValue(QRect(ptHide, sizeCtl));
    animeCtlBarShow->setEndValue(QRect(ptShow, sizeCtl));
}

/*---------------------------------------------------
 * QML Q_INVOKABLE Functions
----------------------------------------------------*/
void DockInfo::actionSearchNewTab(const QString &word)
{
    if (word.isEmpty()) return;
    if (main->openRecorded())
        actionSearch(word);
}

void DockInfo::actionSearch(const QString &word)
{
    QString text = word;
    if (text.isEmpty()) return;
    text.truncate(30);
    main->setSearchTag(text);
}

void DockInfo::setBlackList(int id, bool checked)
{
    main->setBlackList(id, checked);
}

void DockInfo::openSearchWebImageDlg(int id)
{
    if (main->openTagThumbnail(id)) {
        main->updateTagWidget();
        reload();
    }
}

void DockInfo::openImageFileSelectDlg(int id)
{
    if (imgTag->openFileSelectDlg(id)) {
        main->updateTagWidget();
        reload();
    }
}

void DockInfo::deleteImageFile(int id)
{
    if (imgTag->remove(id)) {
        main->updateTagWidget();
        reload();
    }
}

void DockInfo::saveDropUrlFile(const QString &url, int id)
{
    if (imgTag->saveDropUrlFile(QUrl(url), id)){
        main->updateTagWidget();
        reload();
    }
}

void DockInfo::deleteRecorded(int id)
{
    main->deleteRecorded(QList<int>{id}); // 再描画は、signalで実行される
}

void DockInfo::actionFullParsent(int id, bool isSendTaglist)
{
    // tagのリストからなら、singalで更新される
    recordedId = id;
    main->setFullParsent(QList<int>{id});
    if (!isSendTaglist) reload(id);
}

void DockInfo::actionDeleteParsent(int id, bool isSendTaglist)
{
    recordedId = id;
    main->deleteParsent(QList<int>{id});
    if (!isSendTaglist) reload(id);
}

void DockInfo::deleteVideoFile(const int &videoId)
{
    if (!main->deleteConfirm(this)) return;
    main->getApi()->deleteVideoFile(videoId);
    reload();
}

void DockInfo::actionPlaylistSave()
{
    auto playlist = main->getActivePlayerPlaylist();
    if (playlist == nullptr) return;

    QString dir = main->getSettings()->value("DockPlaylist/editPlaylistPath").toString();
    QString filePath = playlist->getSaveFileName(dir, windowTitle(), this);
    if (filePath.isEmpty()) return;
    if (playlist->saveFile(filePath))
        main->updatePlaylist(filePath);
}

void DockInfo::deletePlaylistId(int id)
{
    auto playlist = main->getActivePlayerPlaylist();
    if (playlist == nullptr) return;
    playlist->removeRecordedId(id);
    reload();
}

