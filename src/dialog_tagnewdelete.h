#ifndef DIALOG_TAGNEWDELETE_H
#define DIALOG_TAGNEWDELETE_H

#include <QDialog>
#include "mainwindow.h"
#include "util/tagitem.h"

namespace Ui {
class DialogTagNewDelete;
}

class QStandardItemModel;

class DialogTagNewDelete : public QDialog
{
    Q_OBJECT

public:
    explicit DialogTagNewDelete(QWidget *parent = nullptr);
    ~DialogTagNewDelete();

private slots:
    void on_buttonBox_accepted();
    void on_treeView_customContextMenuRequested(const QPoint &pos);

private:
    void autoSize();
    void addTreeItem(TagItem* item);
    TagItem* getTagItem(const QModelIndex &index);
    SqlModel *sqlModel;
    SqlModel *sqlModelTags;
    QStandardItemModel* treeModel;
    MainWindow* main;
    Ui::DialogTagNewDelete *ui;

    // QObject interface
public:
    virtual bool event(QEvent *event) override {
        if (event->type() == QEvent::Resize || event->type() == QEvent::Show)
            autoSize();
        return QWidget::event(event);
    }
};

#endif // DIALOG_TAGNEWDELETE_H
