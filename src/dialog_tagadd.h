#ifndef DIALOG_TAGADD_H
#define DIALOG_TAGADD_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {class DialogTagAdd;}

class DialogTagAdd : public QDialog
{
    Q_OBJECT

public:
    explicit DialogTagAdd(int id, QWidget *parent = nullptr) ;
    ~DialogTagAdd();

    int addTagId = -1;
private slots:
    void on_treeWidget_customContextMenuRequested(const QPoint &pos);
    void on_cmbField_currentIndexChanged(int index);
    void on_buttonBox_accepted();
    void on_btnTagAddEndflag_clicked();
    void on_toolTagSetting_clicked();

private:
    void prseProgram(const QString &field, const QString &value);

    int id;
    SqlModel *sqlModel;
    SqlModel *sqlModelTags;
    QSqlRecord rs;

    MainWindow* main;
    Ui::DialogTagAdd *ui;
};

#endif // DIALOG_TAGADD_H
