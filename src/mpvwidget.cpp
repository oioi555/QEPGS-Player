#include "mpvwidget.h"
#include "qthelper.h"

#include <QOpenGLContext>
#include <QMouseEvent>
#include <QTimer>
#include <QLabel>
#include <QPropertyAnimation>
#include <QGraphicsOpacityEffect>

// MPV関連のデバッグ情報表示
//#define MPV_DEBUG

static void wakeup(void *ctx)
{
    QMetaObject::invokeMethod(static_cast<MpvWidget*>(ctx), "on_mpv_events", Qt::QueuedConnection);
}

static auto get_proc_address(void *ctx, const char *name) -> void * {
    Q_UNUSED(ctx)
    QOpenGLContext *glctx = QOpenGLContext::currentContext();
    if (!glctx)
        return nullptr;
    return reinterpret_cast<void *>(glctx->getProcAddress(QByteArray(name)));
}

MpvWidget::MpvWidget(QWidget *parent, Qt::WindowFlags f)
    : QOpenGLWidget(parent, f)
    , timerClick(new QTimer(this))
    , labelStatus(new QLabel(this))
    , labelOSD(new QLabel(this))
    , effectOSD(new QGraphicsOpacityEffect(this))
    , animeOSDFadeOut(new QPropertyAnimation(this))
    , timerOSDFadeOut(new QTimer(this))
{
    mpv = mpv_create();
    if (!mpv)
        throw std::runtime_error("could not create mpv context");

#ifdef MPV_DEBUG
    setProperty("terminal", "yes");
    setProperty("msg-level", "all=v");
#else
    setProperty("terminal", "no");
#endif

    setProperty("vo", "libmpv");        // MPV 0.38対応で追加
    setProperty("cache", "auto");
    setProperty("cache-pause-initial", "yes");
    setProperty("cache-pause", "yes");
    setProperty("screenshot-format", "jpg");
    setProperty("audio-client-name", "QEPGS-Player");

    if (mpv_initialize(mpv) < 0)
        throw std::runtime_error("could not initialize mpv context");

    // Request hw decoding, just for testing.
    setProperty("hwdec", "no");
    setProperty("hwdec-codecs", "all"); //mpeg2は、デフォルトでは対象外

    mpv_observe_property(mpv, 0, "duration", MPV_FORMAT_INT64);
    mpv_observe_property(mpv, 0, "time-pos", MPV_FORMAT_INT64);
    mpv_observe_property(mpv, 0, "ao-mute", MPV_FORMAT_FLAG);
    mpv_observe_property(mpv, 0, "ao-volume", MPV_FORMAT_INT64);
    mpv_observe_property(mpv, 0, "speed", MPV_FORMAT_DOUBLE);
    mpv_observe_property(mpv, 0, "pause", MPV_FORMAT_FLAG);
    mpv_observe_property(mpv, 0, "cache-buffering-state", MPV_FORMAT_INT64);
    mpv_set_wakeup_callback(mpv, wakeup, this);

    // シングル,ダブルクリック判定タイマー
    timerClick->setInterval(300);
    timerClick->setTimerType(Qt::PreciseTimer);
    timerClick->setSingleShot(true);
    connect(timerClick,&QTimer::timeout, this, [=] {
        emit clicked();
    });
    setMouseTracking(true);

    // OSD message
    labelOSD->setProperty("class", "labelOSD");
    labelOSD->setAlignment(Qt::AlignCenter);
    labelOSD->setGraphicsEffect(effectOSD);
    animeOSDFadeOut->setPropertyName("opacity");
    animeOSDFadeOut->setTargetObject(effectOSD);
    animeOSDFadeOut->setStartValue(1.0);
    animeOSDFadeOut->setEndValue(0.0);
    connect(animeOSDFadeOut, &QAbstractAnimation::finished, this, [=] {
        labelOSD->setVisible(false);
    });

    timerOSDFadeOut->setInterval(3000);
    timerOSDFadeOut->setSingleShot(true);
    connect(timerOSDFadeOut, &QTimer::timeout, this, [=] {
        timerOSDFadeOut->stop();
        animeOSDFadeOut->start();
    });

    labelStatus->setProperty("class", "labelOSD");
    labelStatus->setVisible(false);
}

MpvWidget::~MpvWidget()
{
    makeCurrent();
    if (mpv_gl)
        mpv_render_context_free(mpv_gl);
    mpv_terminate_destroy(mpv);
}

void MpvWidget::command(const QVariant& params)
{
    mpv::qt::command_variant(mpv, params);
    emit propertyChanged();
}

void MpvWidget::setProperty(const QString& name, const QVariant& value)
{
    mpv::qt::set_property_variant(mpv, name, value);
    emit propertyChanged();
}

auto MpvWidget::getProperty(const QString &name) const -> QVariant
{
    return mpv::qt::get_property_variant(mpv, name);
}

void MpvWidget::initializeGL()
{
    // NOTE: MPV 0.35から、mpv_opengl_init_params.extra_exts フィールドを削除された
    mpv_opengl_init_params gl_init_params{get_proc_address, nullptr};
    mpv_render_param params[]{
        {MPV_RENDER_PARAM_API_TYPE, const_cast<char *>(MPV_RENDER_API_TYPE_OPENGL)},
        {MPV_RENDER_PARAM_OPENGL_INIT_PARAMS, &gl_init_params},
        {MPV_RENDER_PARAM_INVALID, nullptr}
    };

    if (mpv_render_context_create(&mpv_gl, mpv, params) < 0)
        throw std::runtime_error("failed to initialize mpv GL context");
    mpv_render_context_set_update_callback(mpv_gl, MpvWidget::on_update, reinterpret_cast<void *>(this));
}

void MpvWidget::paintGL()
{
    // 任意のスケーリングに対応
    int scalingWidth = width() * devicePixelRatioF();
    int scalingheight = height() * devicePixelRatioF();

    mpv_opengl_fbo mpfbo{static_cast<int>(defaultFramebufferObject()), scalingWidth, scalingheight, 0};
    int flip_y{1};

    mpv_render_param params[] = {
        {MPV_RENDER_PARAM_OPENGL_FBO, &mpfbo},
        {MPV_RENDER_PARAM_FLIP_Y, &flip_y},
        {MPV_RENDER_PARAM_INVALID, nullptr}
    };
    // See render_gl.h on what OpenGL environment mpv expects, and
    // other API details.
    mpv_render_context_render(mpv_gl, params);
}

void MpvWidget::on_mpv_events()
{
    // Process all events, until the event queue is empty.
    while (mpv) {
        mpv_event *event = mpv_wait_event(mpv, 0);
        if (event->event_id == MPV_EVENT_NONE) {
            break;
        }
        handle_mpv_event(event);
    }
}

void MpvWidget::handle_mpv_event(mpv_event *event)
{
    switch (event->event_id) {
    case MPV_EVENT_PROPERTY_CHANGE: {
        auto *prop = static_cast<mpv_event_property *>(event->data);
        const QString propName(prop->name);
#ifdef MPV_DEBUG
        qDebug() << "MPV_EVENT_PROPERTY_CHANGE: " << propName << " data:" << prop->data;
#endif
        if (propName == "time-pos")
            emit positionChanged(getProperty("time-pos").toInt());
        else if (propName == "duration") {
            duration = getProperty("duration").toLongLong();
            emit durationChanged(duration);
        } else if (propName == "speed")
            emit speedChanged(getSpeed());
        else if (propName == "ao-mute")
            ;// eventが発生しないので、setMuteで処理
        else if (propName == "ao-volume")
            ;// eventが発生しないので、setVolumeで処理
        else if (propName == "pause") {
            // NOTE: MPV 0.35から、PAUSEとUNPAUSEイベントが削除されたので、こちらに移行
            QStringList msgs;
            if (getPlay()) {
                msgs << "再生" << playTitle;
                auto hwDec = getProperty("hwdec-current").toString();
                msgs << (hwDec.isEmpty()? "" : "ハードウェアデコード: " + hwDec);
                auto deinterlace = getProperty("deinterlace").toString();
                msgs << (deinterlace.isEmpty()? "" : "インターレース解除: " + deinterlace);
                msgs << (videoType.isEmpty()? "" : "ビデオタイプ: " + videoType);

            } else {
                msgs << "一時停止";
            }
            popup(msgs);
            emit playngChanged(getPlay());
        } else if (propName == "cache-buffering-state") {
            auto state = getProperty("cache-buffering-state").toInt();
            setStatusText(tr("バッファー中... %1%").arg(state), (state < 100));
        }
        break;
    }
    case MPV_EVENT_FILE_LOADED: {
        QList<int> audioIds;
        auto tracks = getProperty("track-list").toList();
        foreach (auto track, tracks) {
            auto trackMap = track.toMap();
#ifdef MPV_DEBUG
            qDebug() << "MPV_EVENT_FILE_LOADED Type:" << trackMap["type"].toString();
            foreach (auto key, trackMap.keys()) qDebug() << "\t" << key << ": " << trackMap[key];
#endif
            // ビデオタイプ
            if (trackMap["type"].toString() == "video") {
                videoType = trackMap["decoder-desc"].toString();
            }
            // オーディオトラック（音声多重）
            if (trackMap["type"].toString() == "audio") {
                audioIds << trackMap["id"].toInt();
            }
            /**
             * NOTE: TSの字幕コーデックは、"arib_caption"が表示できないので
             * H.264の、"mov_text"の場合だけ有効にする
             */
            if (trackMap["type"].toString() == "sub") {
                sid = 0;
                if (trackMap["codec"].toString() == "mov_text")
                    sid = trackMap["id"].toInt();
            }
        }
        emit fileLoaded(audioIds);
        break;
    }
    case MPV_EVENT_END_FILE: {
        auto *endFile = static_cast<mpv_event_end_file*>(event->data);
        if (endFile->reason == MPV_END_FILE_REASON_EOF) {
            emit endfileEvent();
        }
        if (endFile->reason == MPV_END_FILE_REASON_ERROR && endFile->error != MPV_ERROR_SUCCESS) {
            QString err = "";
            switch (endFile->error) {
            case MPV_ERROR_EVENT_QUEUE_FULL: err = "イベントリングバッファがいっぱいです。"; break;
            case MPV_ERROR_NOMEM: err = "メモリの割り当てに失敗しました。"; break;
            case MPV_ERROR_UNINITIALIZED : err = "mpvコアはまだ構成および初期化されていません。"; break;
            case MPV_ERROR_INVALID_PARAMETER: err = "パラメータが無効またはサポートされていない値に設定されている場合の一般的なキャッチオールエラー。"; break;
            case MPV_ERROR_OPTION_NOT_FOUND: err = "存在しないオプションを設定しようとしています。"; break;
            case MPV_ERROR_OPTION_FORMAT: err = "サポートされていないMPV_FORMATを使用してオプションを設定しようとしています。"; break;
            case MPV_ERROR_OPTION_ERROR : err = "オプションの設定に失敗しました。"; break;
            case MPV_ERROR_PROPERTY_NOT_FOUND: err = "アクセスされたプロパティは存在しません。"; break;
            case MPV_ERROR_PROPERTY_FORMAT: err = "サポートされていないMPV_FORMATを使用してプロパティを設定または取得しようとしています。"; break;
            case MPV_ERROR_PROPERTY_UNAVAILABLE: err = "プロパティは存在しますが、利用できません。"; break;
            case MPV_ERROR_PROPERTY_ERROR: err = "プロパティの設定または取得中にエラーが発生しました。"; break;
            case MPV_ERROR_COMMAND: err = "mpv_commandなどを使用してコマンドを実行すると一般的なエラーが発生します。"; break;
            case MPV_ERROR_LOADING_FAILED : err = "ロード時の一般的なエラー"; break;
            case MPV_ERROR_AO_INIT_FAILED: err = "音声出力の初期化に失敗しました。"; break;
            case MPV_ERROR_VO_INIT_FAILED: err = "ビデオ出力の初期化に失敗しました。"; break;
            case MPV_ERROR_NOTHING_TO_PLAY: err = "再生するオーディオまたはビデオデータはありませんでした。"; break;
            case MPV_ERROR_UNKNOWN_FORMAT: err = "ファイルを読み込もうとしたときに、ファイル形式を判別できなかったか、ファイルが壊れすぎて開くことができませんでした。"; break;
            case MPV_ERROR_UNSUPPORTED : err = "特定のシステム要件が満たされていないことを通知するための一般的なエラー。"; break;
            case MPV_ERROR_NOT_IMPLEMENTED: err = "呼び出されたAPI関数はスタブのみです。"; break;
            case MPV_ERROR_GENERIC:
            default: err = "不特定のエラー。";
            }
            setStatusText(tr("エラー: %1").arg(err));
        }
        break;
    }
    default: ;
        // Ignore uninteresting or unknown events.
    }
}

// Make Qt invoke mpv_opengl_cb_draw() to draw a new/updated video frame.
void MpvWidget::maybeUpdate()
{
    // If the Qt window is not visible, Qt's update() will just skip rendering.
    // This confuses mpv's opengl-cb API, and may lead to small occasional
    // freezes due to video rendering timing out.
    // Handle this by manually redrawing.
    // Note: Qt doesn't seem to provide a way to query whether update() will
    //       be skipped, and the following code still fails when e.g. switching
    //       to a different workspace with a reparenting window manager.
    if (window()->isMinimized()) {
        makeCurrent();
        paintGL();
        context()->swapBuffers(context()->surface());
        doneCurrent();
    } else {
        update();
    }
}

void MpvWidget::on_update(void *ctx)
{
    QMetaObject::invokeMethod(static_cast<MpvWidget*>(ctx), "maybeUpdate");
}

/*----------------------------------------------------------
 * プレイヤーからの操作
 ----------------------------------------------------------*/
void MpvWidget::openPlayMedia(const QString &url, const QString &playTitle /*= ""*/) {
    this->playTitle = playTitle;
    command(QStringList() << "loadfile" << url);
}

void MpvWidget::setPlayToggle() {
    setProperty("pause", getPlay());
}

auto MpvWidget::getPlay() -> bool {
    return !getProperty("pause").toBool();
}

void MpvWidget::setPlay(bool play /*= true*/)
{
    setProperty("pause", !play);
}

void MpvWidget::setPlayBegin()
{
    setSeek(0);
}

void MpvWidget::setStop() {
    command(QStringList() << "stop");
}

void MpvWidget::setSeek(qlonglong pos) {
    command(QVariantList() << "seek" << pos << "absolute");
    setOSDText(tr("シーク：%1").arg(getFormatTime(pos)));
}

void MpvWidget::setSeekUp(bool isSlow)
{
    int value = getSeek();
    int step = isSlow? 1 :seekWheelStepSec;
    setSeek((value >= duration)? duration : value + step);
}

void MpvWidget::setSeekDown(bool isSlow)
{
    int value = getSeek();
    int step = isSlow? 1 :seekWheelStepSec;
    setSeek((value <= 0 )? 0 : value - step);
}

qlonglong MpvWidget::getSeek()
{
    return getProperty("time-pos").toLongLong();
}

qlonglong MpvWidget::getDuration()
{
    return getProperty("duration").toLongLong();
}

void MpvWidget::setSkipNext() {
    setOSDText(tr("スキップ:%1秒").arg(skip));
    command(QStringList() << "seek" << QString::number(skip));
}

void MpvWidget::setSkipBack() {
    setOSDText(tr("スキップ戻し:%1秒").arg(skipBack));
    command(QStringList() << "seek" << tr("-%1").arg(skipBack));
}

void MpvWidget::setMute(bool isMute)
{
    command(QStringList() << "set" << "ao-mute" << (isMute? "yes" : "no"));
    if (isMute) setOSDText("ミュート", true);
    emit muteChanged(isMute);
}

void MpvWidget::setMuteToggle() {
    setMute(!getMute());
}

auto MpvWidget::getMute() -> bool {
    return getProperty("ao-mute").toBool();
}

void MpvWidget::setVolume(int volume) {
    setOSDText(tr("ボリューム：%1 %").arg(volume), true);
    setProperty("ao-volume", volume);
    emit volumeChanged(volume);
}

void MpvWidget::setVolumeUp()
{
    int value = getVolume();
    value = value - (value % 5);
    setVolume((value >= 100)? 100 : value + 5);
}

void MpvWidget::setVolumeDown()
{
    int value = getVolume();
    value = value - (value % 5);
    setVolume((value <= 0 )? 0 : value - 5);
}

int MpvWidget::getVolume()
{
    return getProperty("ao-volume").toInt();
}

void MpvWidget::setAudioStereo()
{
    setOSDText(tr("ステレオ"));
    setProperty("af", "lavfi=[pan=stereo|c0=c0|c1=c1]");
}

void MpvWidget::setAudioLeft()
{
    setOSDText(tr("左チャンネル（主音声）"));
    setProperty("af", "lavfi=[pan=1c|c0=c0]");
}

void MpvWidget::setAudioRight()
{
    setOSDText(tr("右チャンネル（副音声）"));
    setProperty("af", "lavfi=[pan=1c|c0=c1]");
}

void MpvWidget::setDeinterlace(bool enable)
{
    setProperty("deinterlace", enable? "yes" : "no");
    auto deinterlace = getProperty("deinterlace");
    setOSDText(tr("インターレース解除: %1").arg(deinterlace.toString()));
    emit deinterlaceChanged(deinterlace.toBool());
}

void MpvWidget::setHwDec(const QString &hwDec) {
    mpv::qt::set_option_variant(mpv, "hwdec", hwDec);
}

void MpvWidget::setSpeed(double speed)
{
    setOSDText(tr("再生速度：%1 倍").arg(QString::number(speed, 'f', 1)));
    setProperty("speed", speed);
}

void MpvWidget::setSpeedUp()
{
    double value = getSpeed();
    setSpeed((value >= 2.0)? 2.0 : value + 0.1);
}

void MpvWidget::setSpeedDown()
{
    double value = getSpeed();
    setSpeed((value < 0.0 )? 0.0 : value - 0.1);
}

void MpvWidget::setSpeedReset()
{
    setSpeed(1.0);
}

double MpvWidget::getSpeed()
{
    return getProperty("speed").toDouble();
}

void MpvWidget::setContinuous(bool checked)
{
    setOSDText(tr("連続再生:%1").arg(checked? "ON" : "OFF"));
    emit continuousChanged(checked);
}

void MpvWidget::setSub(bool checked)
{
    // sidが、0なら表示できない字幕コーデック
    if (checked && sid != 0) {
        setProperty("sid", sid);
    } else {
        setProperty("sid", "no");
    }
}

/*----------------------------------------------------------
 * マウスイベント
 ----------------------------------------------------------*/
void MpvWidget::mousePressEvent(QMouseEvent *event) {
    if (event->button() == Qt::LeftButton)
        timerClick->start();
}

void MpvWidget::mouseDoubleClickEvent(QMouseEvent* /*event*/) {
    timerClick->stop();
    emit doubleClick();
}

void MpvWidget::contextMenuEvent(QContextMenuEvent *event) {
    emit mpvContextMenuEvent(event);
}

void MpvWidget::mouseMoveEvent(QMouseEvent *event) {
    setOSDText(playTitle, false);
    emit mouseMove();
    QWidget::mouseMoveEvent(event);
}

void MpvWidget::wheelEvent(QWheelEvent *event)
{
    bool isWheelUp = (event->angleDelta().y() > 0);
    bool isCtrl = (event->modifiers() & Qt::ControlModifier);
    //bool isShift = (event->modifiers() & Qt::ShiftModifier);
    if (getPlay()) {
        // 再生中なら
        if (isCtrl)
            if (isWheelUp) setSpeedUp(); else setSpeedDown();
        else
            if (isWheelUp) setVolumeUp(); else setVolumeDown();
    } else {
        // 一時停止なら
        if (isWheelUp) setSeekUp(isCtrl); else setSeekDown(isCtrl);
    }
}

/*----------------------------------------------------------
 * メッセージ
 ----------------------------------------------------------*/
// NOTE:mpv0.38 + Qt6.7で、paintEventで描画すると動画全体がピンク色になってしまう問題
// があり、直接描画をやめて、QLabelに変更した
void MpvWidget::setOSDText(const QString &text, bool isDispCenter /*= true*/)
{
    IsDispCenterOSD = isDispCenter;
    labelOSD->setText(text);
    reSizeOSD();

    animeOSDFadeOut->setCurrentTime(0);
    effectOSD->setOpacity(1.0);
    timerOSDFadeOut->start();
}

void MpvWidget::reSizeOSD()
{
    QRect rcLabel = rect();
    QFont font = labelOSD->font();
    int fontSize = (rect().width() > 800)? 20 : 12;
    int padding = fontSize * 4;

    // テキストのサイズ計算
    font.setPointSize(fontSize);
    QFontMetrics fm(font);
    labelOSD->setFont(font);
    rcLabel.setWidth(rect().width() - padding);
    QRect rcText = fm.boundingRect(rcLabel, Qt::AlignCenter|Qt::TextWrapAnywhere, labelOSD->text());

    // 表示位置とサイズの計算
    if (IsDispCenterOSD) {
        rcLabel.setWidth(rcText.width() + padding);
        rcLabel.setHeight(rcText.height() + (padding / 2));
        rcLabel.moveCenter(rect().center());
        labelOSD->setStyleSheet("border-radius: 10px;");
    } else {
        rcLabel.setWidth(rect().width());
        rcLabel.setHeight(rcText.height() + (padding / 2));
        labelOSD->setStyleSheet("border-radius: 0px;");
    }
    labelOSD->setGeometry(rcLabel);
    labelOSD->setVisible(true);
}

void MpvWidget::resizeEvent(QResizeEvent *event)
{
    if (labelOSD->isVisible())
        reSizeOSD();

    QOpenGLWidget::resizeEvent(event);
}

// バッファーの状態や、エラー表示に使う
void MpvWidget::setStatusText(const QString &text, bool show /*= true*/)
{
    labelStatus->setVisible(show);
    labelStatus->setText(text);
    labelStatus->setGeometry(0, 0, width(), 50);
}

QString MpvWidget::getFormatTime(long value)
{
    long sec = value;
    long hour = sec / 3600;
    sec %= 3600;
    long minute = sec / 60;
    sec %= 60;

    return tr("%1:%2:%3").arg(
        QString::number(hour).rightJustified(2, '0'),
        QString::number(minute).rightJustified(2, '0'),
        QString::number(sec).rightJustified(2, '0'));
}
