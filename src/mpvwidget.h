#ifndef MPVWIDGET_H
#define MPVWIDGET_H

#include <mpv/client.h>
#include <mpv/render_gl.h>      // libmpv(>=0.29.0)
#include <QOpenGLWidget>

class QLabel;
class QPropertyAnimation;
class QGraphicsOpacityEffect;

class MpvWidget Q_DECL_FINAL: public QOpenGLWidget
{
    Q_OBJECT

public:
    MpvWidget(QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
    ~MpvWidget() ;
    void command(const QVariant &params);
    void setProperty(const QString &name, const QVariant& value);
    QVariant getProperty(const QString &name) const;
    QSize sizeHint() const override { return QSize(480, 270);}

    // オリジナルで追加
    void setOSDText(const QString &text, bool center = true);
    void popup(const QStringList &texts, bool center = true) {
        setOSDText(texts.join("\n"), center);
    }
    void openPlayMedia(const QString &url, const QString &playTitle = "");
    void setPlayToggle();
    bool getPlay();
    void setPlay(bool play = true);
    void setPlayBegin();
    void setStop();

    void setSeek(qlonglong pos);
    void setSeekUp(bool isSlow);
    void setSeekDown(bool isSlow);
    qlonglong getSeek();
    qlonglong getDuration();
    void setSeekWheelStepSec(int seekWheelStepSec) {
        this->seekWheelStepSec = seekWheelStepSec;
    }
    void setSkipNext();
    void setSkipBack();

    void setMute(bool isMute);
    void setMuteToggle();
    bool getMute();
    void setVolume(int volume);
    void setVolumeUp();
    void setVolumeDown();
    int getVolume();
    void setAudioStereo();
    void setAudioLeft();
    void setAudioRight();
    void setSkipSec(int skip,int skipBack) {
        this->skip = skip;
        this->skipBack = skipBack;
    }
    void setDeinterlace(bool enable);
    void setHwDec(const QString &hwDec);
    void setSpeed(double speed);
    void setSpeedUp();
    void setSpeedDown();
    void setSpeedReset();
    double getSpeed();
    void setContinuous(bool checked);
    void setSub(bool checked);
    bool isSubEnabled() { return (sid != 0); };
    void setStatusText(const QString &text, bool show = true);
    QString getFormatTime(long value);

signals:
    void propertyChanged();
    void playngChanged(bool isPlay);
    void durationChanged(qlonglong value);
    void positionChanged(qlonglong value);
    void speedChanged(double value);
    void muteChanged(bool isMute);
    void volumeChanged(int volume);
    void fileLoaded(QList<int> audioIds);
    void endfileEvent();
    void mpvContextMenuEvent(QContextMenuEvent *event);
    void clicked();
    void doubleClick();
    void mouseMove();
    void continuousChanged(bool checked);
    void deinterlaceChanged(bool checked);

private slots:
//    void swapped();
    void on_mpv_events();
    void maybeUpdate();

protected:
    void initializeGL() Q_DECL_OVERRIDE;
    void paintGL() Q_DECL_OVERRIDE;

private:
    void handle_mpv_event(mpv_event *event);
    static void on_update(void *ctx);

    mpv_handle* mpv;
    mpv_render_context *mpv_gl;

    long duration = 0;
    int skip = 5;
    int skipBack = 5;
    int seekWheelStepSec = 1;
    QTimer *timerClick;
    QString videoType;
    QString playTitle;
    qlonglong sid = 0;

    // popup
    QLabel *labelStatus;
    QLabel *labelOSD;
    QGraphicsOpacityEffect *effectOSD;
    QPropertyAnimation *animeOSDFadeOut;
    void reSizeOSD();
    QTimer *timerOSDFadeOut;
    bool IsDispCenterOSD = false;

    // QWidget interface
protected:
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void contextMenuEvent(QContextMenuEvent *event) override;
    virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;
    virtual void wheelEvent(QWheelEvent *event) override;
    virtual void resizeEvent(QResizeEvent *event) override;
};

#endif // MPVWIDGET_H
