import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

import "../parts/"

Item {
    id: tagItem
    height: parent.height
    width: parent.width

    // image
    PanePlain {
        id: paneImage
        width: parent.width
        height: gridView.imageHeight
        padding: 0; spacing: 0
        bkColor: "black"
        bkOpacity: 0.8
        Image {
            id: image
            anchors.centerIn: parent
            smooth: true
            cache: false
            fillMode: Image.PreserveAspectFit
            source: model.thumbnail
            height: parent.height
            width: parent.width
        }
    }
    // banner upper side
    PanePlain {
        id: paneImageTop
        anchors.top: paneImage.top; width: parent.width
        padding: 5
        RowLayout {
            spacing: 10
            width: parent.width
            LabelMark {
                text: "番組終了"
                bkColor: "orangered"
                bkOpacity: 0.8
                visible: model.isEndProgram
            }
            Spacer { horizontal: true }
            LabelMark {
                property int notWatched: model.count - model.watched;
                text: "未視聴：" + String(notWatched)　+ " / " + String(model.count)
                bkColor: (notWatched > 0)? bkColorDefault : "gray"
                bkOpacity: 0.8
            }
        }
    }
    // banner lower side
    PanePlain {
        id: paneImageButtom
        anchors.bottom: paneImage.bottom; width: parent.width
        padding: 5
        RowLayout {
            spacing: 10
            width: parent.width
            LabelMark {
                text: "NEW"
                bkColor: "red"
                font.pointSize: 8
                bkOpacity: 0.3
                visible: model.newflag
            }
            Spacer { horizontal: true }
            LabelMark {
                text: "Thumbnail"
                bkColor: "green"
                font.pointSize: 8
                bkOpacity: 0.3
                visible: model.hasThumbnail
            }
        }
    }
    Progress {
        id: progress
        anchors.top: paneImageButtom.bottom
        width: parent.width
        value: model.watched
        to: model.count
    }
    // tagname / submenu
    PanePlain {
        id: paneTag
        anchors.top: progress.bottom; width: parent.width
        padding: paddingText; bottomPadding: 0; rightPadding: 0
        RowLayout {
            spacing: 0
            width: parent.width
            LabelNoWarp {
                id: tagName
                Layout.preferredWidth: parent.width - menuButton.width
                padding: 0
                font.pointSize: gridView.cellFontSize
                font.bold: true
                text: model.tag
                toolTipText: model.tag
            }
            Spacer { id: specer; horizontal: true }
            ToolButton {
                id: menuButton
                icon.name: "overflow-menu"
                implicitWidth: 30   // gnome fix
                implicitHeight: 30  // gnome fix
                onClicked: optionsMenu.open()
                Menu {
                    id: optionsMenu
                    x: parent.width - width
                    transformOrigin: Menu.TopRight
                    Action {
                        icon.name: "media-playback-start"
                        text: "再生"
                        onTriggered: WidgetTag.openPlayerTag(model.id);
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "search"
                        text: "検索"
                        onTriggered: WidgetTag.actionSearch(model.tag)
                    }
                    Action {
                        icon.name: "search"
                        text: "新しいタブで検索"
                        onTriggered: WidgetTag.actionSearchNewTab(model.tag)
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "view-filter"
                        text: "フィルター"
                        onTriggered: WidgetTag.actionFilter(model.field, model.tag)
                    }
                    Action {
                        icon.name: "view-filter"
                        text: "新しいタブでフィルター"
                        onTriggered: WidgetTag.actionFilterNewTab(model.field, model.tag)
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "search"
                        text: "Web検索"
                        onTriggered: WidgetTag.actionSearchWeb(model.tag)
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "tag-edit"
                        text: "Tagを編集"
                        onTriggered: WidgetTag.openTagEdit(model.id)
                    }
                    Action {
                        icon.name: "tag-delete"
                        text: "Tagを削除"
                        onTriggered: WidgetTag.openTagDelete(model.id)
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "view-hidden"
                        text: "もう見ないリスト化"
                        checkable: true
                        checked: model.blackflag
                        onTriggered: WidgetTag.setBlackList(model.id, checked)
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "download"
                        text: "画像検索 (ダウンロード)"
                        onTriggered: WidgetTag.openSearchWebImageDlg(model.id)
                    }
                    Action {
                        icon.name: "folder"
                        text: "サムネイルを指定"
                        onTriggered: WidgetTag.openImageFileSelectDlg(model.id)
                    }
                    Action {
                        icon.name: "delete"
                        text: "サムネイルを削除"
                        enabled: model.hasThumbnail
                        onTriggered: WidgetTag.deleteImageFile(model.id)
                    }
                }
            }
        }
    }
    // date / genre
    PanePlain {
        id: paneBottom
        anchors.top: paneTag.bottom; width: parent.width
        padding: paddingText; topPadding: 0
        RowLayout {
            spacing: paddingText
            width: parent.width
            LabelNoWarp {
                id: lastUpdate
                text: model.lastUpdate
                font.pointSize: gridView.cellFontSize
                padding: 0;
                bkOpacity: 0
            }
            Spacer {id: space; horizontal: true }
            LabelNoWarp {
                id: genre
                padding: 0; rightPadding: paddingText
                Layout.preferredWidth: parent.width - lastUpdate.width - space.width - parent.spacing
                horizontalAlignment: Label.AlignRight
                font.pointSize: gridView.cellFontSize
                text: model.genre
                toolTipText: model.genre
                visible: (model.genre !== "")
                bkOpacity: 0
            }
        }
    }
    MouseArea {
        anchors.fill: paneImage
        onClicked: function () {
            WidgetTag.setTagID(model.id);
            gridView.currentIndex = index;
        }
        onDoubleClicked: function () {
            WidgetTag.openPlayerTag(model.id);
        }
    }
    DropArea {
        id: dropArea;
        anchors.fill: paneImage
        onEntered: {
            image.visible = false;
            drag.accept (Qt.LinkAction);
        }
        onDropped: {
            console.log(drop.urls)
            image.visible = true;
            WidgetTag.saveDropUrlFile(drop.urls, model.id)
        }
        onExited: {
            image.visible = true;
        }
    }
}
