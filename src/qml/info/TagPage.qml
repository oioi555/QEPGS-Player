import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import "../parts/"

PanePlain {
    id: page
    bkOpacity: 1
    property int notWatched: DockInfo.tagElement.count - DockInfo.tagElement.watched

    function openEncordDlg() {
        DockInfo.openEncordDlg(DockInfo.tagElement.field, DockInfo.tagElement.tag);
    }
    function cancelEncord() {
        DockInfo.cancelEncord(DockInfo.tagElement.field, DockInfo.tagElement.tag);
    }

    Toolbar {
        id: toolbar
        anchors.left: parent.left
        anchors.right: parent.right
        RowLayout {
            anchors.fill: parent
            ToolButton {
                icon.name: "news-subscribe"
                visible: (DockInfo.currentIndex !== -1)
                property string toolTipText: "選択中の番組情報を表示"
                ToolTip.text: toolTipText
                ToolTip.visible: toolTipText ? mouseArea.containsMouse : false
                MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: DockInfo.setQmlRecordedID(RecModel.get(recList.currentIndex, "id"))
                }
            }
            ToolButton {
                icon.name: "tools-media-optical-burn"
                visible: DockInfo.Map["isV2"]
                property string toolTipText: "エンコードする"
                ToolTip.text: toolTipText
                ToolTip.visible: toolTipText ? mouseAreaEncord.containsMouse : false
                MouseArea {
                    id: mouseAreaEncord
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: openEncordDlg()
                }
            }
            ToolButton {
                icon.name: "process-stop"
                visible: DockInfo.Map["isV2"] && DockInfo.hasEncord
                property string toolTipText: "エンコードをキャンセルする"
                ToolTip.text: toolTipText
                ToolTip.visible: toolTipText ? mouseAreaEncord.containsMouse : false
                MouseArea {
                    id: mouseAreaEncordCancel
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: cancelEncord()
                }
            }
            Spacer{ horizontal: true }
            ToolButton {
                icon.name: contents.visible? "go-down" : "go-up"
                onClicked: {
                    contents.visible = !contents.visible;
                    DockInfo.saveSetting("viewTagContents", contents.visible);
                }
            }
            ToolButton {
                icon.name: "overflow-menu"
                onClicked: optionsMenu.open()
                Menu {
                    id: optionsMenu
                    x: parent.width - width
                    transformOrigin: Menu.TopRight
                    Action {
                        icon.name: "media-playback-start"
                        text: "再生"
                        onTriggered: DockInfo.openPlayerTag(DockInfo.tagElement.id);
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "tag"
                        text: "検索Tagタブで開く"
                        onTriggered: DockInfo.actionTagWidget()
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "search"
                        text: "検索"
                        onTriggered: DockInfo.actionSearch(DockInfo.tagElement.tag)
                    }
                    Action {
                        icon.name: "search"
                        text: "新しいタブで検索"
                        onTriggered: DockInfo.actionSearchNewTab(DockInfo.tagElement.tag)
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "view-filter"
                        text: "フィルター"
                        onTriggered: DockInfo.actionFilter()
                    }
                    Action {
                        icon.name: "view-filter"
                        text: "新しいタブでフィルター"
                        onTriggered: DockInfo.actionFilterNewTab()
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "search"
                        text: "Web検索"
                        onTriggered: DockInfo.actionSearchWeb(DockInfo.tagElement.tag)
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "tag-edit"
                        text: "Tagを編集"
                        onTriggered: DockInfo.openTagEdit()
                    }
                    Action {
                        icon.name: "tag-delete"
                        text: "Tagを削除"
                        onTriggered: DockInfo.openTagDelete()
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "view-hidden"
                        text: "もう見ないリスト化"
                        checkable: true
                        checked: DockInfo.tagElement.blackflag
                        onTriggered: DockInfo.setBlackList(DockInfo.tagElement.id, checked)
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "download"
                        text: "画像検索 (ダウンロード)"
                        onTriggered: DockInfo.openSearchWebImageDlg(DockInfo.tagElement.id)
                    }
                    Action {
                        icon.name: "folder"
                        text: "サムネイルを指定"
                        onTriggered: DockInfo.openImageFileSelectDlg(DockInfo.tagElement.id)
                    }
                    Action {
                        icon.name: "delete"
                        text: "サムネイルを削除"
                        enabled: DockInfo.Map["hasThumbnail"]
                        onTriggered: DockInfo.deleteImageFile(DockInfo.tagElement.id)
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "tools-media-optical-burn"
                        text: "エンコード"
                        onTriggered: openEncordDlg()
                        enabled: DockInfo.Map["isV2"]
                    }
                    Action {
                        icon.name: "process-stop"
                        text: "エンコードをキャンセル"
                        onTriggered: cancelEncord()
                        enabled: DockInfo.Map["isV2"] && DockInfo.hasEncord
                    }
                }
            }
        }
    }
    ColumnLayout {
        id: contents
        spacing: 0
        anchors.top: toolbar.bottom
        visible: DockInfo.Map["isViewContents"]
        PanePlain {
            id: paneFlags
            Layout.fillWidth: true
            padding: paddingText
            spacing: 5
            Flow {
                spacing: paddingText
                width: parent.width
                LabelMark {
                    text: "新番組検出"
                    toolTipText: "自動新番組検出によって登録されたTag"
                    isLight : true
                    visible: DockInfo.tagElement.newflag
                }
                LabelMark {
                    text: "連続番組"
                    toolTipText: "番組終了をタイトルから自動的に検出します"
                    isLight : true
                    visible: DockInfo.tagElement.endflag
                }
                LabelMark {
                    text: "サムネイル"
                    toolTipText: "手動でイメージを登録している"
                    isLight : true
                    visible: DockInfo.tagElement.hasThumbnail
                }
                LabelMark {
                    text: "デフォルト"
                    toolTipText: "設定のデフォルト指定によって登録されたTag"
                    isLight : true
                    visible: DockInfo.tagElement.defaultflag
                }
                LabelMark {
                    text: "もう見ないリスト"
                    toolTipText: "プラックリスト"
                    isLight : true
                    visible: DockInfo.tagElement.blackflag
                }
            }
        }
        Line {}
        LabelWarp { text: "ジャンル：" + DockInfo.tagElement.genre }
        Line {}
        LabelWarp { text: "検索対象：" + DockInfo.tagElement.field }
        LabelWarp { text: "登録日：" + DockInfo.tagElement.date }
    }
    Progress {
        id: progress
        anchors.top: contents.visible? contents.bottom : toolbar.bottom
        width: parent.width;
        value: (DockInfo.tagElement.watched / DockInfo.tagElement.count) *100;
    }
    ColumnLayout {
        id: marks
        width: parent.width
        anchors.top: progress.bottom
        PanePlain {
            id: paneWatch
            Layout.fillWidth: true
            padding: paddingText; bottomPadding: paddingText/2;
            Flow {
                spacing: paddingText
                width: parent.width
                LabelMark {
                    text: "番組終了"
                    toolTipText: "番組終了のタイトルが含まれています"
                    bkColor: "orangered"
                    visible: DockInfo.tagElement.isEndProgram
                }
                LabelMark {
                    text: "未視聴：" + String(page.notWatched) + " / " + String(DockInfo.tagElement.count)
                    toolTipText: "未視聴の動画があります\n[未視聴/動画数]"
                    bkColor: (page.notWatched > 0)? bkColorDefault : "gray"
                }
            }
        }
        Line { lineHeight: 5 }
    }
    RecordedList {
        id: recList
        anchors.top: marks.bottom
        anchors.bottom: parent.bottom
    }
}
