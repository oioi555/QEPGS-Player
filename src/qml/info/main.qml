import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import "../parts/"

PanePlain {
    id: root

    state: DockInfo.Map["state"];

    property color lineColor: palette.window
    property int paddingText: 5
    property int warpTextWidth: root.width - (paddingText * 2)

    Header { id: header }
    StackView {
        id: stackView
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        initialItem: "ErrPage.qml"
    }

    states: [
        State {
            name: "err"
            StateChangeScript {
                script: stackView.replace("ErrPage.qml", StackView.PopTransition)
            }
        },
        State {
            name: "recorded"
            StateChangeScript {
                script: stackView.replace("RecordedPage.qml", StackView.Transition)
            }
        },
        State {
            name: "live"
            StateChangeScript {
                script: stackView.replace("LivePage.qml")
            }
        },
        State {
            name: "tag"
            StateChangeScript {
                script: stackView.replace("TagPage.qml", StackView.PopTransition)
            }
        },
        State {
            name: "playlist"
            StateChangeScript {
                script: stackView.replace("PlaylistPage.qml", StackView.PopTransition)
            }
        },
        State {
            name: "overview"
            StateChangeScript {
                script: stackView.replace("OverviewPage.qml", StackView.PopTransition)
            }
        }
    ]
}
