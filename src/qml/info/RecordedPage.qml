import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import "../parts/"

PanePlain {
    id: page
    bkOpacity: 1

    function openEncordDlg() {
        DockInfo.openEncordDlg(DockInfo.Map["id"]);
    }
    function cancelEncord() {
        DockInfo.cancelEncord(DockInfo.Map["encId"])
    }

    Toolbar {
        id: toolbar
        anchors.left: parent.left
        anchors.right: parent.right

        RowLayout {
            anchors.fill: parent
            ToolButton {
                icon.name: "go-previous"
                visible: DockInfo.Map["isBack"]
                property string toolTipText: "戻る"
                ToolTip.text: toolTipText
                ToolTip.visible: toolTipText ? mouseAreaBack.containsMouse : false
                MouseArea {
                    id: mouseAreaBack
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: DockInfo.backPage()
                }
            }
            ToolButton {
                id: tagButton
                icon.name: "tag"
                visible: (TagModel.rowCount() > 0)
                property string toolTipText: "関連Tagを表示"
                ToolTip.text: toolTipText
                ToolTip.visible: toolTipText ? mouseArea.containsMouse : false
                MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: tagMenu.open()
                }
                Menu {
                    id: tagMenu
                    transformOrigin: Menu.TopRight
                    Repeater {
                        model: TagModel
                        MenuItem {
                            icon.name: "tag"
                            text: model.tag
                            onClicked: DockInfo.setQmlTagID(model.id, DockInfo.Map["id"])
                        }
                    }
                }
            }
            ToolButton {
                icon.name: "computer"
                visible: DockInfo.Map["isV2"]
                property string toolTipText: "EPGStationの詳細ページを表示"
                ToolTip.text: toolTipText
                ToolTip.visible: toolTipText ? mouseAreaEpgs.containsMouse : false
                MouseArea {
                    id: mouseAreaEpgs
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: DockInfo.openDetailPage(DockInfo.Map["id"])
                }
            }
            ToolButton {
                icon.name: "tools-media-optical-burn"
                visible: DockInfo.Map["isV2"]
                property string toolTipText: "エンコードする"
                ToolTip.text: toolTipText
                ToolTip.visible: toolTipText ? mouseAreaEncord.containsMouse : false
                MouseArea {
                    id: mouseAreaEncord
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: openEncordDlg()
                }
            }
            Spacer{ horizontal: true}
            ToolButton {
                icon.name: contents.visible? "go-down" : "go-up"
                onClicked: {
                    contents.visible = !contents.visible;
                    DockInfo.saveSetting("viewRecordedContents", contents.visible);
                }
            }
            ToolButton {
                icon.name: "overflow-menu"
                onClicked: optionsMenu.open()
                Menu {
                    id: optionsMenu
                    x: parent.width - width
                    transformOrigin: Menu.BottomRight
                    Action {
                        icon.name: "media-playback-start"
                        text: "再生する"
                        onTriggered: DockInfo.openPlayer(DockInfo.Map["id"])
                    }
                    Action {
                        icon.name: "computer"
                        text: "EPGS詳細ページを表示"
                        onTriggered: DockInfo.openDetailPage(DockInfo.Map["id"])
                        enabled: DockInfo.Map["isV2"]
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "tools-media-optical-burn"
                        text: "エンコード"
                        onTriggered: openEncordDlg()
                        enabled: DockInfo.Map["isV2"] && (DockInfo.Map["encId"] === -1)
                    }
                    Action {
                        icon.name: "process-stop"
                        text: "エンコードをキャンセル"
                        onTriggered: cancelEncord()
                        enabled: DockInfo.Map["isV2"] && (DockInfo.Map["encId"] !== -1)
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "checkmark"
                        text: "視聴済にする"
                        onTriggered: { DockInfo.actionFullParsent(DockInfo.Map["id"]) }
                    }
                    Action {
                        icon.name: "draw-eraser"
                        text: "未視聴にする"
                        onTriggered: { DockInfo.actionDeleteParsent(DockInfo.Map["id"]) }
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "tag-new"
                        text: "Tag検索・登録"
                        onTriggered: { DockInfo.actionTagAddTag(DockInfo.Map["id"]) }
                    }
                    Action {
                        icon.name: "tag-new"
                        text: "連続番組Tag登録"
                        onTriggered: { DockInfo.actionTagAddEndflag(DockInfo.Map["id"]) }
                    }
                    MenuSeparator {}
                    Action {
                        icon.name: "delete"
                        text: "削除"
                        onTriggered: DockInfo.deleteRecorded(DockInfo.Map["id"])
                    }
                }
            }
        }
    }
    Content {
        id: contents
        anchors.top: toolbar.bottom
        visible: DockInfo.Map["isViewContents"];
    }
    Progress {
        id: progress
        anchors.top: contents.visible? contents.bottom : toolbar.bottom
        Layout.fillWidth: true;
        barColor: DockInfo.Map["isRecording"]? "firebrick" : colorDefault
    }
    ColumnLayout {
        id: marks
        width: parent.width
        anchors.top: progress.bottom
        Line {}
        PanePlain {
            Layout.fillWidth: true;
            padding: paddingText
            bottomPadding: paddingText * 2
            Flow {
                spacing: paddingText
                width: parent.width
                Repeater {
                    model: VideoModel
                    LabelMark {
                        text: model.name
                        property string toolTipText: "動画ファイルを再生"
                        ToolTip.text: toolTipText
                        ToolTip.visible: toolTipText ? mouseAreaVideo.containsMouse : false
                        MouseArea {
                            id: mouseAreaVideo
                            anchors.fill: parent
                            hoverEnabled: true
                            acceptedButtons: Qt.LeftButton | Qt.RightButton
                            onClicked: {
                                if (mouse.button === Qt.LeftButton) {
                                    DockInfo.playVideo(model.id);
                                } else if (mouse.button === Qt.RightButton) {
                                    // 最低一つは残すので、1つだけなら削除メニューを表示しない
                                    if (VideoModel.rowCount() > 1 && DockInfo.Map["isV2"]) {
                                        selectionMenu.x = mouse.x; selectionMenu.y = mouse.y;
                                        selectionMenu.open();
                                    }
                                }
                            }
                        }
                        Menu {
                            id: selectionMenu
                            x: parent.width - width
                            transformOrigin: Menu.TopRight
                            Action {
                                icon.name: "delete"
                                text: "録画を個別削除"
                                onTriggered: DockInfo.deleteVideoFile(model.id)
                            }
                        }
                    }
                }
            }
        }
        PanePlain {
            padding: paddingText
            Layout.fillWidth: true;
            RowLayout {
                anchors.fill: parent
                LabelMark {
                    padding: 0
                    text: DockInfo.Map["encMode"]
                }
                Label {
                    padding: 0; leftPadding: 10
                    text: (DockInfo.Map["isEncording"])? "エンコード中" : "待機中"
                    font.bold: DockInfo.Map["isEncording"]
                }
                Spacer{ horizontal: true }
                ToolButton {
                    icon.name: "tools-media-optical-burn-image"
                    onClicked: DockInfo.openEPGStation("encode")
                }
                ToolButton {
                    icon.name: "process-stop"
                    onClicked: cancelEncord()
                }
            }
            visible: (DockInfo.Map["encId"] !== -1)
        }
        Progress {
            id: encProgress
            Layout.fillWidth: true;
            lineHeight: 4
            visible: (DockInfo.Map["encId"] !== -1)
        }
    }
    Extended {
        id: loadExtend
        anchors.top: marks.bottom
        anchors.bottom: parent.bottom
        clip: true
    }

}
