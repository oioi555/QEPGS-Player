import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import "../parts/"

PanePlain {
    id: page
    bkOpacity: 1
    ColumnLayout {
        LabelWarp { text: DockInfo.Map["description"] }
        Line {}
    }
}
