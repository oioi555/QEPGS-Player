import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

PanePlain {
    width: parent.width
    height: parent.height
    bkColor: palette.dark
    bkOpacity: 1
    padding: 0
    property int currentIndex: listView.currentIndex
    Component.onCompleted: {
        listView.currentIndex = -1;
    }
    Connections {
        target: DockInfo
        function onCurrentIndexChanged(currentIndex) {
            listView.currentIndex = currentIndex;
            //console.log("currentIndexChanged:" + currentIndex);
        }
    }
    function openPlayer(recordecId) {
        if (DockInfo.Map["state"] === "tag")
            DockInfo.openPlayerTag(DockInfo.tagElement.id, recordecId);
        if (DockInfo.Map["state"] === "playlist")
            DockInfo.openPlayerPlaylist(recordecId);
        if (DockInfo.Map["state"] === "overview")
            DockInfo.openPlayerPlaylist(recordecId);
    }

    Component {
        id: tagDelegate
        ItemDelegate {
            id: itemDelegate
            height: contents.height
            width: listView.width - listView.leftMargin - listView.rightMargin
            background: Rectangle { radius: 3; color: palette.light }
            ColumnLayout {
                id: contents
                width: parent.width
                spacing: 0
                PanePlain {
                    padding: paddingText
                    Layout.fillWidth: true;
                    RowLayout {
                        anchors.fill: parent
                        Label {
                            padding: 0
                            text: model.date
                            font.bold: model.isPlay
                        }
                        Spacer{ horizontal: true }
                        Label {
                            padding: 0; rightPadding: 10
                            text: "▶"
                            visible: model.isPlay
                        }
                    }
                }
                Line {}
                Label {
                    padding: paddingText
                    textFormat: Text.RichText
                    Layout.preferredWidth: itemDelegate.width - (leftPadding + rightPadding)
                    text: model.name

                    wrapMode: Label.Wrap
                }
                Progress {
                    Layout.fillWidth: true;
                    value: model.percent
                    lineHeight: 4
                    barColor: model.isRecording? "firebrick" : colorDefault
                }
                PanePlain {
                    padding: paddingText
                    Layout.fillWidth: true;
                    RowLayout {
                        anchors.fill: parent
                        LabelMark {
                            padding: 0
                            text: model.encMode
                        }
                        Label {
                            padding: 0; leftPadding: 10
                            text: (model.isEncording)? "エンコード中" : "待機中"
                            font.bold: model.isEncording
                        }
                        Spacer{ horizontal: true }
                        ToolButton {
                            icon.name: "tools-media-optical-burn-image"
                        }
                    }
                    visible: (model.encId !== -1)
                }
                Progress {
                    Layout.fillWidth: true;
                    value: model.encPercent
                    lineHeight: 4
                    visible: (model.encId !== -1)
                }
            }
            MouseArea {
                id: mouseArea
                anchors.fill: parent
                hoverEnabled: true
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                onClicked: {
                    listView.currentIndex = index
                    if (mouse.button === Qt.RightButton) {
                        selectionMenu.x = mouse.x; selectionMenu.y = mouse.y;
                        selectionMenu.open()
                    }
                }
                onDoubleClicked: openPlayer(model.id)
            }
            Menu {
                id: selectionMenu
                x: parent.width - width
                transformOrigin: Menu.TopRight
                Action {
                    icon.name: "media-playback-start"
                    text: "再生する"
                    onTriggered: openPlayer(model.id)
                }
                Action {
                    icon.name: "news-subscribe"
                    text: "番組情報を表示"
                    onTriggered: DockInfo.setQmlRecordedID(model.id)
                }
                Action {
                    icon.name: "computer"
                    text: "EPGS詳細ページを表示"
                    onTriggered: DockInfo.openDetailPage(model.id)
                    enabled: DockInfo.Map["isV2"]
                }
                MenuSeparator {}
                Action {
                    icon.name: "tools-media-optical-burn"
                    text: "エンコード"
                    onTriggered: DockInfo.openEncordDlg(model.id)
                    enabled: DockInfo.Map["isV2"] && (model.encId === -1)
                }
                Action {
                    icon.name: "process-stop"
                    text: "エンコードをキャンセル"
                    onTriggered: DockInfo.cancelEncord(model.encId)
                    enabled: DockInfo.Map["isV2"] && (model.encId !== -1)
                }
                MenuSeparator {}
                Action {
                    icon.name: "checkmark"
                    text: "視聴済にする"
                    onTriggered: DockInfo.actionFullParsent(model.id, true)
                }
                Action {
                    icon.name: "draw-eraser"
                    text: "未視聴にする"
                    onTriggered: DockInfo.actionDeleteParsent(model.id, true)
                }
                MenuSeparator { visible: (DockInfo.Map["state"] === "playlist") }
                MenuItem {
                    icon.name: "tag-delete"
                    text: "プレイリストから削除"
                    onTriggered: DockInfo.deletePlaylistId(model.id)
                    visible: (DockInfo.Map["state"] === "playlist")
                }
                MenuSeparator {}
                Action {
                    icon.name: "delete"
                    text: "録画を削除"
                    onTriggered: DockInfo.deleteRecorded(model.id)
                }
            }
        }
    }
    ListView {
        id: listView
        clip: true;

        topMargin: paddingText; bottomMargin: paddingText; leftMargin: paddingText; rightMargin: paddingText;
        spacing: paddingText
        width: parent.width; height: parent.height;
        focus: true
        model: RecModel
        delegate: tagDelegate
        currentIndex: DockInfo.currentIndex
        keyNavigationEnabled: true  // 機能していない
        highlightFollowsCurrentItem: true
        highlightMoveDuration: 200
        highlightMoveVelocity: 1000

        highlight: Rectangle {
            z:2
            color: palette.highlight
            opacity: 0.4
            radius: 3
        }

        ScrollBar.vertical: ScrollBar {
            anchors.right: parent.right
            width: paddingText
        }
    }
}
