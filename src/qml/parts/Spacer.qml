import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Item {
    property bool horizontal: false
    property bool vertical: false
    Layout.fillWidth: horizontal
    Layout.fillHeight: vertical
}
