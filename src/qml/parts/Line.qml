import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Rectangle {
    property int lineHeight: 1

    Layout.fillWidth: true;
    height: lineHeight;
    color: lineColor
}
