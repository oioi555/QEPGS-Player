import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Label {
    readonly property color bkColorDefault: Qt.rgba(63/254, 81/254, 181/254, 0.9)
    property bool isLight: false
    property color bkColor: isLight ? palette.midlight : bkColorDefault
    property color textColor: isLight ? palette.text : "#fff"
    property color borderColor: palette.mid
    property real bkOpacity: 1
    property string toolTipText: ""

    background: Rectangle {
        radius: 3
        color: bkColor
        opacity: bkOpacity
        border.width: isLight ? 1 : 0
        border.color: borderColor
    }

    topPadding: paddingText / 2; bottomPadding: topPadding
    leftPadding: paddingText; rightPadding: leftPadding
    color: textColor

    ToolTip.text: toolTipText
    ToolTip.visible: toolTipText ? mouseArea.containsMouse : false
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
    }
}
