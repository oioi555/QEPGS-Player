import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
Label {
    id: label
    property color bkColorDefault: palette.light
    property color bkColor: bkColorDefault
    property real bkOpacity: 0
    property color textColor: palette.text
    property string toolTipText: ""

    padding: paddingText
    wrapMode: Label.Wrap;
    color: textColor
    background: Rectangle {
        color: parent.bkColor
        opacity: parent.bkOpacity;
    }

    elide: Label.ElideRight
    maximumLineCount: 1

    ToolTip.text: toolTipText
    ToolTip.visible: toolTipText ? mouseArea.containsMouse : false
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
    }
}
