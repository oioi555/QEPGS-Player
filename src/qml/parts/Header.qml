import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

PanePlain {
    id: headerPane
    bkColor: palette.dark
    bkOpacity: 1
    width: parent.width
    contentHeight: thumbnail.implicitHeight

    Image {
        id: thumbnail
        smooth: true
        cache: false
        source: DockInfo.Map["thumbnail"];
        width: parent.width
        fillMode: Image.PreserveAspectFit
        anchors.horizontalCenter: parent.horizontalCenter
    }
    LabelWarp {
        id: name
        anchors.bottom: thumbnail.bottom
        width: parent.width
        z: 2
        textColor: "#fff"
        bkColor: "#000"
        bkOpacity: 0.5
        textFormat: Text.RichText
        text: DockInfo.Map["name"];
    }

    DropArea {
        id: dropArea;
        visible: (DockInfo.Map["state"] === "tag")
        anchors.fill: thumbnail
        onEntered: {
            thumbnail.visible = false;
            drag.accept (Qt.LinkAction);
        }
        onDropped: {
            //console.log(drop.urls)
            DockInfo.saveDropUrlFile(drop.urls, DockInfo.Map["tagid"])
            thumbnail.visible = true;
        }
        onExited: {
            thumbnail.visible = true;
        }
    }
}
