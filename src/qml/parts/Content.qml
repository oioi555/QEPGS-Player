import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

PanePlain {
    id: contents

    Connections {
        target: DockInfo
        function onMapChanged(value) {
            progress.value = DockInfo.Map["progress"];
        }
        function onProgressChanged() {
            progress.value = DockInfo.Map["progress"];
        }
        function onEncProgressChanged() {
            encProgress.value = DockInfo.Map["encPercent"];
        }
    }

    ColumnLayout {
        spacing: 0
        TextSelect  { text: DockInfo.Map["description"] }
        Line {}
        LabelWarp   { text: DockInfo.Map["genre"] }
        Line {}
        LabelWarp   { text: DockInfo.Map["channel"] }
        Line {}
        LabelWarp   { text: DockInfo.Map["date"] }
    }
}
