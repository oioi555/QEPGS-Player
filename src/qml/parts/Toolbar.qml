import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

ToolBar {
    readonly property color bkColorDefault: palette.button
    property color bkColor: bkColorDefault
    property real bkOpacity: 1

    padding: 0
    background: Rectangle {
        color: parent.bkColor
        opacity: parent.bkOpacity;
    }
}
