import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

TextEdit {
    id: textSelect
    padding: paddingText; Layout.preferredWidth: warpTextWidth; wrapMode: Text.Wrap; readOnly: true; selectByMouse: true;
    persistentSelection: true
    textFormat: Text.RichText
    color: palette.text

    MouseArea {
        id: mouseArea
        acceptedButtons: Qt.RightButton
        anchors.fill: parent
        onClicked: {
            if (mouse.button === Qt.RightButton) {
                contextMenuDescription.x = mouse.x; contextMenuDescription.y = mouse.y;
                contextMenuDescription.open()
            }
        }
    }
    Menu {
        id: contextMenuDescription
        MenuItem {
            icon.name: "edit-copy"
            text: qsTr("コピー")
            enabled: textSelect.selectedText
            onTriggered: textSelect.copy()
        }
        MenuSeparator {}
        MenuItem {
            icon.name: "search"
            text: qsTr("新しいタブで検索")
            enabled: textSelect.selectedText
            onTriggered: DockInfo.actionSearchNewTab(textSelect.selectedText)
        }
        MenuItem {
            icon.name: "search"
            text: qsTr("検索")
            enabled: textSelect.selectedText
            onTriggered: DockInfo.actionSearch(textSelect.selectedText)
        }
        MenuSeparator {}
        MenuItem {
            icon.name: "search"
            text: qsTr("Web検索")
            enabled: textSelect.selectedText
            onTriggered: DockInfo.actionSearchWeb(textSelect.selectedText)
        }
    }
}
