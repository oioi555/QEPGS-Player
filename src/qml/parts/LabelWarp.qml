import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Label {
    id: label
    property color bkColorDefault: palette.base
    property color bkColor: bkColorDefault
    property real bkOpacity: 0
    property color textColor: palette.text

    padding: paddingText
    Layout.preferredWidth: warpTextWidth
    wrapMode: Label.Wrap;
    color: textColor
    background: Rectangle {
        color: label.bkColor
        opacity: label.bkOpacity;
    }
}
