import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Rectangle {
    id: videotypeRect
    property color markBkColor: Qt.rgba(63/254, 81/254, 181/254, 0.9)
    property color markTextColor: palette.light
    property alias text: videotype.text
    Layout.fillWidth: true
    height: videotype.height + (paddingText * 2)
    Rectangle {
        anchors.verticalCenter: parent.verticalCenter; x: paddingText
        width: childrenRect.width; height: childrenRect.height
        radius: 3
        color: markBkColor
        Text {
            id: videotype
            topPadding: paddingText / 2; bottomPadding: topPadding
            leftPadding: paddingText; rightPadding: leftPadding
            color: markTextColor
        }
    }
}
