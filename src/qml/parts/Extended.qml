import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

ColumnLayout {
    Line { lineHeight: 5 }

    ScrollView {
        id: detailInner
        Layout.fillHeight: true
        Layout.fillWidth: true

        contentHeight: extended.height
        contentWidth: warpTextWidth;

        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff

        TextSelect {
            id: extended
            text: DockInfo.Map["extended"]
            onLinkActivated: Qt.openUrlExternally(link)
            width: warpTextWidth
        }
    }
}
