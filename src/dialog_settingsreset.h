#ifndef DIALOG_SETTINGSRESET_H
#define DIALOG_SETTINGSRESET_H

#include <QDialog>

namespace Ui {
class DialogSettingsReset;
}

class DialogSettingsReset : public QDialog
{
    Q_OBJECT

public:
     explicit DialogSettingsReset(QWidget *parent = nullptr);
    ~DialogSettingsReset();

     private slots:
         void on_buttonBox_accepted();

private:
    struct menuItem {
        QString name; bool checked;
    };
    QList<menuItem> menuItems;
    Ui::DialogSettingsReset *ui;
};

#endif // DIALOG_SETTINGSRESET_H
