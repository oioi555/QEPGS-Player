#include "dialog_tagdelete.h"
#include "ui_dialog_tagdelete.h"
#include "util/thumbnailtag.h"

#include <QStringListModel>

DialogTagDelete::DialogTagDelete(TagItem *tagItem, QWidget *parent) :
    QDialog(parent)
  , isDeletedTag(false)
  , id(tagItem->id)
  , tagItem(tagItem)
  , sqlModel(new SqlModel("recorded", "id", "v_recorded"))
  , sqlModelTags(new SqlModel("tags" , "id"))
  , main(qobject_cast<MainWindow*>(parent))
  , ui(new Ui::DialogTagDelete)
{
    ui->setupUi(this);

    // window
    setWindowTitle("Tag削除");

    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    // check
    connect(ui->chkDeleteWatched, &QCheckBox::clicked, this, [=] (bool checked) {
        if (checked) ui->chkDeleteVideoAll->setChecked(false);
    });
    connect(ui->chkDeleteVideoAll, &QCheckBox::clicked, this, [=] (bool checked) {
        if (checked) ui->chkDeleteWatched->setChecked(false);
    });

    ui->editTag->setText(tagItem->tag);
    ui->chkDeleteTag->setChecked(true);
}

DialogTagDelete::~DialogTagDelete()
{
    delete sqlModel;
    delete ui;
}

void DialogTagDelete::on_buttonBox_accepted()
{
    if (ui->chkDeleteTag->isChecked()) {
        if (sqlModelTags->deleteRecord(id)) {
            isDeletedTag = true;
            ThumbnailTag imgTag(main, id);
            imgTag.remove(id, false);
        }
    }

    QList<int> requestDeleteIds;
    QString sql;
    if (ui->chkDeleteWatched->isChecked()) {
        QString fix = "::numeric";
        sql = tr("%1 like '%%2%' and 再生 > get_var('Threshold')%3").arg(tagItem->field, tagItem->tag, sqlModel->isPgsql()? fix : "");
        requestDeleteIds << sqlModel->getValuesId(sql);
    }
    if (ui->chkDeleteVideoAll->isChecked()) {
        sql = tr("%1 like '%%2%'").arg(tagItem->field, tagItem->tag);
        requestDeleteIds << sqlModel->getValuesId(sql);
    }
    if (requestDeleteIds.count())
        main->deleteRecorded(requestDeleteIds);
}
