#include "dock_tag.h"
#include "ui_dock_tag.h"
#include "dialog_tagdeletegroup.h"
#include "util/combo_sort.h"

#include <QElapsedTimer>
#include <QStandardItemModel>

DockTag::DockTag(QWidget *parent) : QDockWidget(parent)
    , sqlModel(new SqlModel("recorded", "id", "v_recorded"))
    , sqlModelTag(new SqlModel("tags", "id"))
    , updateFlag(false)
    , treeModel(new QStandardItemModel(this))
    , main(qobject_cast<MainWindow*>(parent))
    , ui(new Ui::DockTag)
{
    ui->setupUi(this);
    this->setAcceptDrops(true);
    this->setTitleBarWidget(new QWidget(this));

    // mainwindow
    connect(main, &MainWindow::mainStatusChanged, this, [=] (MainStatus status) {
        if (status == MainStatus::TagTabel) {
            if (saveSelectedId == -1) {
                // delete update
                if (auto currentId = getSelectedId(); currentId) {
                    if (auto idx = getTagIdIndex(currentId); idx.isValid()) {
                        // 一つ下のアイテムを選択させる
                        if (idx = ui->treeView->indexBelow(idx); idx.isValid())
                            saveSelectedId = getTagItem(idx)->id;
                        else {
                            // 下にアイテムが無い場合、一つ上を選択させる
                            if (idx = ui->treeView->indexAbove(idx); idx.isValid())
                                saveSelectedId = getTagItem(idx)->id;
                        }
                    }
                }
            }
            showTags(saveSelectedId);
        }
    });
    connect(main->getApi(), &RsetApi::updatedStorage, this, [=] (const QJsonValue &json) {
        // disk v2対応
        auto freeSizeStr = main->getFileSizeStr(json["free"].toDouble());
        int usedPercent = (json["used"].toDouble() / json["total"].toDouble()) * 100;
        ui->progDisk->setMaximum(100);
        ui->progDisk->setValue(usedPercent);
        ui->progDisk->setFormat(tr("used:%1% (free:%2)").arg(usedPercent).arg(freeSizeStr));
    });

    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);
    connect(sqlModelTag, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    // ui header
    auto icon = main->getIcon("tag");
    ui->laIcon->setPixmap(icon.pixmap(icon.actualSize(QSize(22, 22))));

    // search
    connect(ui->editSearch, &EditSearch::editingFinished, this, [=] () {
        showTags();
    });
    connect(ui->editSearch, &EditSearch::searchClicked, this, [=] () {
        showTags();
    });

    // tool
    connect(ui->toolReflesh, &QToolButton::clicked, ui->actionReflesh, &QAction::triggered);
    connect(ui->toolSetting, &QToolButton::clicked, ui->actionSetting, &QAction::triggered);

    int sortIndex = main->getSettings()->valueObj(this, "sortIndex", 0).toInt();
    ui->cmbSort->setCurrentIndex(sortIndex);
    connect(ui->cmbSort, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, [=] (int /*index*/) {
        showTags(getSelectedId());
    });

    // action
    needSelectActions << ui->actionSearch << ui->actionSearchNewTab
                      << ui->actionFiter << ui->actionFilterNewTab
                      << ui->actionEdit << ui->actionDelete << ui->actionSearchWeb;

    connect(ui->actionPlay, &QAction::triggered, this, [=] () {
        auto tagItem = getSelectedTagItem();
        if (!tagItem) return;
        if (tagItem->isTag())
            main->openPlayerTag(tagItem->id);
    });
    connect(ui->actionSearch, &QAction::triggered, this, [=] () {
        auto tagItem = getSelectedTagItem();
        if (!tagItem) return;
        if (tagItem->isTag())
            main->setSearchTag(tagItem->tag);
    });
    connect(ui->actionSearchNewTab, &QAction::triggered, this, [=] () {
        if (getSelectedId() == -1) return;
        if (main->openRecorded())
            ui->actionSearch->trigger();
    });
    connect(ui->actionFiter, &QAction::triggered, this, [=] () {
        auto tagItem = getSelectedTagItem();
        if (!tagItem) return;
        if (tagItem->isTag())
            main->setFilterTag(tagItem->field, tagItem->tag);
    });
    connect(ui->actionFilterNewTab, &QAction::triggered, this, [=] () {
        if (getSelectedId() == -1) return;
        if (main->openRecorded())
            ui->actionFiter->trigger();
    });
    connect(ui->actionSearchWeb, &QAction::triggered, this, [=] () {
        auto tagItem = getSelectedTagItem();
        if (!tagItem) return;
        if (tagItem->isTag()) {
            main->searchWeb(tagItem->tag);
        }
    });
    connect(ui->actionAdd, &QAction::triggered, this, [=] () {
        auto indexes = ui->treeView->selectionModel()->selectedIndexes();
        auto index = indexes[0];
        if (!treeModel->hasChildren(index)) {
            if (!treeModel->parent(index).isValid())
                return;
            index = treeModel->parent(index);
        }
        if (auto tagItem = getTagItem(index); tagItem) {
            main->openTagEditDlg(tagItem->field, tagItem->genre);
        }
    });
    connect(ui->actionEdit, &QAction::triggered, this, [=] () {
        auto id = getSelectedId();
        if (id) main->openTagEditDlg(id);
    });
    connect(ui->actionDelete, &QAction::triggered, this, [=] () {
        auto id = getSelectedId();
        if (id) main->openTagDelete(id);
    });
    connect(ui->actionReflesh, &QAction::triggered, this, [=] () {
        main->updateTag(getSelectedId());
    });
    connect(ui->actionBlacklist, &QAction::triggered, this, [=] (bool clicked) {
        int id = getSelectedId();
        if (id == -1) return;
        main->setBlackList(id, clicked);
    });
    connect(ui->actionDeleteGroup, &QAction::triggered, this, [=] () {
        QList<TagItem*> tagItems;
        auto indexes = ui->treeView->selectionModel()->selectedIndexes();
        auto index = indexes[0];
        if (!treeModel->hasChildren(index)) {
            if (!treeModel->parent(index).isValid())
                return;
            index = treeModel->parent(index);
        }
        auto item = treeModel->itemFromIndex(index);
        for (int row = 0; row < item->rowCount(); row++) {
            auto tagItem = dynamic_cast<TagItem *>(item->child(row)->data(Qt::UserRole).value<QObject *>());
            if (tagItem->isTag()) tagItems << tagItem;
        }
        openTagDeleteGroup(item->text(), tagItems);
    });
    connect(ui->actionSetting, &QAction::triggered, this, [=] () {
        main->openSettings(SettingMenu::Tag);
    });
    connect(ui->actionTagWidget, &QAction::triggered, this, [=] () {
        auto tagItem = getSelectedTagItem();
        if (!tagItem) return;
        if (tagItem->blackflag)   // もう見ないリスト
            main->openTagWidget(tagItem->id, "番組タイトル", "もう見ないリスト");
        else
            main->openTagWidget(tagItem->id, tagItem->field, tagItem->genre);
    });
    connect(ui->actionEncord, &QAction::triggered, this, [=] {
        auto tagItem = getSelectedTagItem();
        main->openEncordDlg(tagItem->field, tagItem->tag);
    });
    connect(ui->actionCancelEncord, &QAction::triggered, this, [=] {
        auto tagItem = getSelectedTagItem();
        main->cancelEncord(tagItem->field, tagItem->tag);
    });

    // tree
    ui->treeView->setModel(treeModel);
    connect(ui->treeView, &QTreeView::customContextMenuRequested, this, &DockTag::showContextMenu);
    connect(ui->treeView, &QTreeView::doubleClicked, this, [=] (const QModelIndex &index) {
        // group tag
        if (treeModel->hasChildren(index)) {
            ui->actionTagWidget->trigger();
            return;
        }
        // recorded
        auto btnGrpTagDbClickIndex = main->getSettings()->valueObj(this, "btnGrpTagDbClickIndex").toInt();
        switch (btnGrpTagDbClickIndex) {
        case 0: ui->actionTagWidget->trigger(); break;
        case 1: ui->actionFiter->trigger(); break;
        case 2: ui->actionPlay->trigger(); break;
        }
    });
    connect(ui->treeView->selectionModel(), &QItemSelectionModel::selectionChanged, this, [=] () {
        auto indexes = ui->treeView->selectionModel()->selectedIndexes();
        if (indexes.isEmpty()) return;
        if (treeModel->hasChildren(indexes[0])) return;
        saveSelectedId = getSelectedId();
        main->setDockInfoId(InfoType::Tag, QString::number(saveSelectedId));
    });
}

DockTag::~DockTag()
{
    delete sqlModel;
    delete sqlModelTag;
    delete treeModel;
    delete ui;
}

int DockTag::getSelectedId()
{
    auto item = getSelectedTagItem();
    if (item) return item->id;
    return -1;
}

TagItem* DockTag::getSelectedTagItem()
{
    auto indexes = ui->treeView->selectionModel()->selectedIndexes();
    if (!indexes.isEmpty()) {
        auto tagItem = getTagItem(indexes[0]);
        if (tagItem)
            return tagItem;
    }
    return nullptr;
}

QModelIndex DockTag::getTagIdIndex(int id)
{
    auto index = QModelIndex();
    foreach (auto item, treeModel->findItems("*", Qt::MatchWildcard | Qt::MatchRecursive)) {
        auto tagItem = getTagItem(item->index());
        if (tagItem->id == id) {
            index = item->index();
            break;
        }
    }
    return index;
}

void DockTag::selectTag(int id)
{
    if (id == -1) return;
    auto index = getTagIdIndex(id);
    if (index.isValid()) {
        ui->treeView->selectionModel()->select(index, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
        ui->treeView->scrollTo(index);
        saveSelectedId = id;
    }
}

TagItem *DockTag::getTagItem(const QModelIndex &index){
    return static_cast<TagItem *>(treeModel->data(index, Qt::UserRole).value<QObject *>());
}

void DockTag::showTags(int id /*= -1*/)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    main->getApi()->updateStorage();

    QElapsedTimer timer;
    timer.start();

    // 展開の状態を保存する
    expandeds.clear();
    foreach (auto item, treeModel->findItems("*", Qt::MatchWildcard | Qt::MatchRecursive)) {
        if (ui->treeView->isExpanded(item->index()))
            expandeds << item->text();
    }

    // モデルの構築
    treeModel->clear();
    treeModel->setHorizontalHeaderLabels({"Tag", "数"});
    foreach (const QString &field, cmbfieldsList)
        setRoot(field);

    // 展開
    if (expandeds.count()) {
        // 更新まえの状態を復元する
        foreach (auto text, expandeds) {
            foreach(auto item, treeModel->findItems(text, Qt::MatchFixedString | Qt::MatchRecursive))
                ui->treeView->setExpanded(item->index(), true);
        }
    } else {
        // 初期状態、ルートアイテムは展開する
        for (int row = 0; row < treeModel->invisibleRootItem()->rowCount(); row++) {
            auto index = treeModel->invisibleRootItem()->child(row)->index();
            ui->treeView->setExpanded(index, true);
        }
    }

    // 選択状態を復元
    selectTag(id);

    QApplication::restoreOverrideCursor();
    autoSize();
    updateFlag = false;
    qDebug() << tr("DockTag::showTags() 実行時間: %1秒").arg(static_cast<double>(timer.elapsed()) / 1000, 0, 'f', 2);


}

void DockTag::setRoot(const QString &field)
{
    QString where = tr("field='%1'").arg(field);
    QString escapedTag = sqlModel->escape(ui->editSearch->text());
    if (!ui->editSearch->text().isEmpty())
        where += tr(" and tag like '%%1%'").arg(escapedTag);
    sqlModelTag->setWhere(where);
    sqlModelTag->setOrderBy(ui->cmbSort->currentData().toString());

    if (!sqlModelTag->getRecordCount()) return;

    auto *tagItemRoot = new TagItem(-1, field, field);
    auto itemRoot = tagItemRoot->getTreeItemGroup();

    // blacklist
    QString blackWhere = where + " and blackflag = true";
    bool isBlack = (sqlModelTag->getRecordCountWhere(blackWhere));
    if (isBlack) {
        auto *tagItem = new TagItem(-1, "もう見ないリスト", "番組タイトル", "もう見ないリスト", "もう見ないリスト");
        auto itemGroup = tagItem->getTreeItemGroup();
        setChilds(itemGroup, blackWhere);
        itemRoot->appendRow(itemGroup);
    }

    // genre
    where += " and blackflag = false";
    sqlModelTag->setWhere(where);
    QStringList genres = sqlModelTag->getGroupByValues("genre", OrderType::asc, true);
    genres.removeAll(QString(""));

    foreach (auto genre, genres) {
        auto *tagItem = new TagItem(-1, genre, field, genre);
        auto itemGroup = tagItem->getTreeItemGroup();
        setChilds(itemGroup, tr("%1 and genre = '%2'").arg(where, genre));
        itemRoot->appendRow(itemGroup);
    }

    // not genre
    if (genres.length())
        where = tr("(%1) and (genre is null or genre = '')").arg(where);

    setChilds(itemRoot, where);
    treeModel->invisibleRootItem()->appendRow(itemRoot);
}

void DockTag::setChilds(QStandardItem* itemParent, const QString &where)
{
    sqlModelTag->setWhere(where);
    for (auto rs: sqlModelTag->getModelList()) {
        auto item = new TagItem(rs, main);
        itemParent->appendRow(item->getTreeItem());
    }
}

void DockTag::openTagDeleteGroup(const QString &goupName, const QList<TagItem *> &tagItems)
{
    DialogTagDeleteGroup dlg(goupName, tagItems, main);
    if (dlg.exec() == QDialog::Accepted)
        if (dlg.isDeletedTag)
            main->updateTag(-1);
}

void DockTag::showContextMenu(const QPoint &pos)
{
    Menu menu(this);
    auto index = ui->treeView->indexAt(pos);
    if (index.isValid()) {
        if (treeModel->hasChildren(index)) {
            // カテゴリの操作
            ui->treeView->expand(index);
            menu.addAction(ui->actionTagWidget);
            menu.addAction(ui->actionAdd);
            menu.addSeparator();
            menu.addAction(ui->actionDeleteGroup);
        } else {
            int id = getSelectedId();
            bool isSelected = (id > 0);

            foreach (auto action, needSelectActions)
                action->setEnabled(isSelected);

            auto tagItem = getTagItem(index);
            ui->actionBlacklist->setChecked(tagItem->blackflag);
            ui->actionBlacklist->setEnabled(tagItem->field == "番組タイトル");
            ui->actionPlay->setEnabled((tagItem->count > 0));
            menu.addSeparator();
            menu.addAction(ui->actionPlay);
            menu.addSeparator();
            menu.addAction(ui->actionTagWidget);
            menu.addSeparator();
            menu.addAction(ui->actionSearch);
            menu.addAction(ui->actionSearchNewTab);
            menu.addSeparator();
            menu.addAction(ui->actionFiter);
            menu.addAction(ui->actionFilterNewTab);
            menu.addSeparator();
            menu.addAction(ui->actionSearchWeb);
            menu.addSeparator();
            menu.addAction(ui->actionAdd);
            menu.addAction(ui->actionEdit);
            menu.addAction(ui->actionDelete);
            menu.addAction(ui->actionDeleteGroup);
            menu.addSeparator();
            menu.addAction(ui->actionBlacklist);
            if (main->getApi()->isV2) {
                menu.addSeparator();
                menu.addAction(ui->actionEncord);
                menu.addAction(ui->actionCancelEncord);
            }
        }
        menu.addSeparator();
    } else {
        menu.addAction(ui->actionAdd);
    }

    menu.addAction(ui->actionReflesh);
    menu.addSeparator();
    menu.addAction(ui->actionSetting);

    menu.exec(ui->treeView->viewport()->mapToGlobal(pos));
}

void DockTag::autoSize()
{
    int fontWidth = ui->treeView->fontInfo().pixelSize();
    int width = ui->treeView->size().width() - (fontWidth * 6);
    ui->treeView->header()->resizeSection(0, width);
}

bool DockTag::event(QEvent *event)
{
    if (event->type() == QEvent::Resize || event->type() == QEvent::Show)
        autoSize();
    return QDockWidget::event(event);
}

void DockTag::closeEvent(QCloseEvent */*event*/)
{
    main->getSettings()->setValueObj(this, "sortIndex", ui->cmbSort->currentIndex());
}
