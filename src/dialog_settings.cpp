#include "dialog_settings.h"
#include "ui_dialog_settings.h"
#include "dialog_regtest.h"
#include "dialog_settingsreset.h"
#include "dialog_tagnewdelete.h"

#include <QDir>
#include <QProcess>
#include <QStringListModel>
#include <QStyleFactory>

DialogSettings::DialogSettings(SettingMenu menu, QWidget *parent, int tabIndex /*= -1*/) :
    QDialog(parent)
  , sqlModel(new SqlModel("search", "id"))
  , main(qobject_cast<MainWindow*>(parent))
  , ui(new Ui::DialogSettings)
{
    ui->setupUi(this);

    // window
    setWindowTitle("設定");

    // list menu
    static const QList<menuItem> menuItems = {
        {"EPGStation", "computer"},
        {"データベース", "network-server-database"},
        {"全般", "configure"},
        {"プレイヤー", "media-playback-start"},
        {"録画済リスト", "folder-videos"},
        {"検索Tag", "tag"},
        {"プレイリスト", "view-media-playlist"},
        {"ライブTV", "video-television"},
        {"シンプル表示", "zoom"},
        {"ヘルプ", "help-about"}
    };
    foreach (auto menu, menuItems) {
        auto *item = new  QListWidgetItem(menu.name, ui->listWidget);
        item->setIcon(main->getIcon(menu.iconName));
    }

    connect(ui->listWidget, &QListWidget::currentRowChanged, this, [=] (int currentRow) {
        ui->stackedWidget->setCurrentIndex(currentRow);
    });
    setListMenu(menu, tabIndex);

    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    // serach history
    ui->tableView->setSqlModel(sqlModel);
    ui->tableView->initSql();
    ui->tableView->autoSizeColRow();

    connect(ui->btnDeleteHistory, &QPushButton::clicked, this, [=] {
        if (!main->deleteConfirm(this)) return;
        sqlModel->deleteTable("search");
        ui->tableView->reQuery();
    });
    connect(ui->actionHistoryDelete, &QAction::triggered, this, [=] {
        QList<int> ids = ui->tableView->getSelectedIds();
        if (!ids.count()) return;
        foreach (auto id, ids)
            sqlModel->deleteRecord(id);
        ui->tableView->reQuery();
    });

    // select player
    selectPlayer << ui->chkInnerPlayerEPG << ui->chkInnerPlayerLive
                 << ui->chkInnerPlayerRecorded;

    connect(ui->btnPlayerInner, &QPushButton::clicked, this, [=] {
        foreach (auto item, selectPlayer)
            item->setChecked(true);
        ui->btnGrpPlaylistDbClick->buttons()[0]->setChecked(true);
    });
    connect(ui->btnPlayerOuter, &QPushButton::clicked, this, [=] {
        foreach (auto item, selectPlayer)
            item->setChecked(false);
        ui->btnGrpPlaylistDbClick->buttons()[1]->setChecked(true);
    });

    // setting
    Settings *settings = main->getSettings();

    // epgstation
    ui->editUrl->setText(settings->value("url").toString());
    ui->chkInnerPlayerEPG->setChecked(settings->value("chkInnerPlayerEPG").toBool());
    ui->chkVideoEncPlay->setChecked(settings->value("chkVideoEncPlay").toBool());
    ui->chkStartEPG->setChecked(settings->value("chkStartEPG").toBool());
    ui->chkStartRecorded->setChecked(settings->value("chkStartRecorded").toBool());
    ui->chkStartTagWidget->setChecked(settings->value("chkStartTagWidget").toBool());
    auto epgsVersion = main->getApi()->epgsVersion;
    ui->laEpgsVersion->setText(epgsVersion.isEmpty()? "v1系、または不明なバージョン" : epgsVersion);

    // database
    ui->cmbDbType->addItems(QStringList() << "MySQL：MariaDB" << "PostgreSQL");
    ui->cmbDbType->setItemData(0, "QMYSQL", Qt::UserRole);
    ui->cmbDbType->setItemData(1, "QPSQL", Qt::UserRole);

    settings->beginGroup("DB");
    auto dbType = settings->value("dbType").toString();
    ui->cmbDbType->setCurrentIndex((dbType == "QPSQL")? 1 : 0);

    ui->chkDbPortDefault->setChecked(settings->value("chkDbPortDefault").toBool());
    ui->editDbPort->setText(settings->value("editDbPort").toString());

    ui->editIP->setText(settings->value("host").toString());
    ui->ecitDbName->setText(settings->value("dbName").toString());
    ui->editUser->setText(settings->value("user").toString());
    ui->editPass->setText(settings->value("pass").toString());
    ui->editDbBackupPath->setText(settings->value("editDbBackupPath").toString());
    settings->endGroup();

    connect(ui->cmbDbType, &QComboBox::currentTextChanged, this, [=] (QString) { updateDbPort(); });
    connect(ui->chkDbPortDefault, &QCheckBox::clicked, this, [=] (bool) { updateDbPort(); });
    updateDbPort();

    connect(ui->btnDbBuckup, &QPushButton::clicked, this, [=] () { backupDb(true); uodateListDbRestore(); });
    connect(ui->btnDbReset, &QPushButton::clicked, this, [=] () { resetDb(); });
    connect(ui->editDbBackupPath, &EditPath::editingFinished, this, [=] { uodateListDbRestore(); });
    connect(ui->btnDbRestore, &QPushButton::clicked, this, [=] () { restoreDb(); });
    connect(ui->btnDbOpenBackupFolder, &QPushButton::clicked, this, [=] (bool) {
        QProcess::startDetached("xdg-open", QStringList() << ui->editDbBackupPath->text());
    });
    uodateListDbRestore();

    connect(ui->actionRestoreRun, &QAction::triggered, this, [=] () { restoreDb(); });
    connect(ui->actionRestoreOpen, &QAction::triggered, this, [=] () {
        if (ui->listDbRestore->currentRow() == -1) return;
        auto filePath = ui->listDbRestore->currentItem()->data(Qt::UserRole).toString();
        QProcess::startDetached("xdg-open", QStringList() << filePath);
    });
    connect(ui->actionRestoreDelete, &QAction::triggered, this, [=] () {
        if (ui->listDbRestore->currentRow() == -1) return;
        if (!main->deleteConfirm(this)) return;
        auto filePath = ui->listDbRestore->currentItem()->data(Qt::UserRole).toString();
        QFile file(filePath);
        file.remove();
        uodateListDbRestore();
    });
    connect(ui->actionRestoreReflesh, &QAction::triggered, this, [=] () { uodateListDbRestore(); });

    // style
    ui->chkDarkIcon->setChecked(settings->value("chkDarkIcon").toBool());
    ui->toolTipOpacity->setValue(settings->value("toolTipOpacity").toInt());
    QString style = settings->value("style").toString();
    ui->treeStyle->setHeaderLabels({"スタイル", "対応環境"});
    QList<styleItem> styles =
    {{"Fusion", "KDE Cinnamon Mate XFce Gnome etc"}, {"Windows", "KDE Cinnamon Mate XFce Gnome etc"}, {"Breeze", "KDE"}, {"Oxygen", "KDE"},};
    foreach (auto styleItem, styles) {
        auto *item = new QTreeWidgetItem;
        item->setText(0, styleItem.name);
        item->setText(1, styleItem.explanation);
        ui->treeStyle->addTopLevelItem(item);
        if (style == styleItem.name)
            ui->treeStyle->setCurrentItem(item);
    }

    // Player
    settings->beginGroup("WidgetPlayer");
    ui->chkMultiPlayer->setChecked(settings->value("chkMultiPlayer").toBool());
    ui->chkAutoHideControlBar->setChecked(settings->value("chkAutoHideControlBar").toBool());
    ui->chkFullScreen->setChecked(settings->value("chkFullScreen").toBool());
    ui->chkMiniScreen->setChecked(settings->value("chkMiniScreen").toBool());
    ui->spinSkip->setValue(settings->value("skipSec").toInt());
    ui->spinSkipBack->setValue(settings->value("skipBackSec").toInt());
    ui->chkInhibitSleep->setChecked(settings->value("chkInhibitSleep").toBool());
    ui->chkMPRIS->setChecked(settings->value("chkMPRIS").toBool());
    ui->cmbHwDec->setCurrentText(settings->value("HwDec").toString());
    ui->chkHwDecAutoOff->setChecked(settings->value("chkHwDecAutoOff").toBool());
    ui->spinSeekWheelStepSec->setValue(settings->value("spinSeekWheelStepSec").toInt());
    ui->editScreenshotsPath->setText(settings->value("editScreenshotsPath").toString());
    settings->endGroup();
    connect(ui->chkFullScreen, &QCheckBox::clicked, this, [=] (bool clicked) {
        if (clicked) ui->chkMiniScreen->setChecked(!clicked);
    });
    connect(ui->chkMiniScreen, &QCheckBox::clicked, this, [=] (bool clicked) {
        if (clicked) ui->chkFullScreen->setChecked(!clicked);
    });
    connect(ui->btnOpenScreenshots, &QPushButton::clicked, this, [=] {
        QDir dir(ui->editScreenshotsPath->text());
        QProcess::startDetached("xdg-open", QStringList() << dir.path());
    });

    // Recorded
    settings->beginGroup("WidgetRecorded");
    ui->chkInnerPlayerRecorded->setChecked(settings->value("chkInnerPlayerRecorded").toBool());
    ui->editMaxViewHistory->setText(settings->value("editMaxViewHistory").toString());
    QStringList serachField = settings->value("listSearchField").toStringList();
    static const QStringList fileds = {"録画日時", "ルール", "ジャンル", "サブジャンル", "番組タイトル", "番組詳細", "番組情報"};
    foreach (auto value, fileds) {
        auto *item = new  QListWidgetItem(value, ui->listSearchField);
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable | Qt::ItemIsSelectable);
        bool isChecked = serachField.contains(value);
        item->setCheckState((isChecked || !serachField.length())? Qt::Checked : Qt::Unchecked);
        item->setIcon(main->getIcon("object-columns"));
    }

    ui->chkViewThumbnail->setChecked(settings->value("chkViewThumbnail").toBool());
    ui->spinThunmbnailWidth->setValue(settings->value("spinThunmbnailWidth").toInt());
    ui->editThumbnailPath->setText(settings->value("editThumbnailPath").toString());
    connect(ui->btnDeleteCache, &QPushButton::clicked, this, [=] {
        if (!main->deleteConfirm(this)) return;
        QDir dir = QDir(ui->editThumbnailPath->text());
        dir.setNameFilters(QStringList() << "*.jpg");
        dir.setFilter(QDir::Files | QDir::NoSymLinks);
        foreach (auto fileInfo, dir.entryInfoList())
            QFile::remove(fileInfo.filePath());
    });

    connect(ui->btnReloadThumbnail, &QPushButton::clicked, this, [=] {
        auto api = main->getApi()->isV2? "/api/thumbnails" : "/api/recorded/thumbnail";
        main->getApi()->post(tr(api), QJsonObject());
    });
    ui->btnReloadThumbnail->setEnabled(main->getApi()->epgsVersion != "unknown");
    connect(ui->btnOpenThumbnailPath, &QPushButton::clicked, this, [=] {
        QDir dir(ui->editThumbnailPath->text());
        QProcess::startDetached("xdg-open", QStringList() << dir.path());
    });
    QVariantList columnsState = main->getSettings()->value("columnsState").toList();
    settings->endGroup();

    // DockTag
    settings->beginGroup("DockTag");
    ui->btnGrpTagDbClick->setId(ui->radioTagOpenTagWidget, 0);
    ui->btnGrpTagDbClick->setId(ui->radioTagOpenRecorded, 1);
    ui->btnGrpTagDbClick->setId(ui->radioTagOpenPlayer, 2);
    int btnGrpTagDbClickIndex = settings->value("btnGrpTagDbClickIndex").toInt();
    for (int i = 0; i < ui->btnGrpTagDbClick->buttons().count(); i++ )
        if (btnGrpTagDbClickIndex == i) ui->btnGrpTagDbClick->buttons()[i]->setChecked(true);

    ui->chkDefaultTag->setChecked(settings->value("chkDefaultTag").toBool());
    ui->chkAutoNewTag->setChecked(settings->value("chkAutoNewTag").toBool());
    ui->spinThreshold->setValue(sqlModel->getScholar(tr("select get_var('Threshold')")).toInt());
    QStringList listDefaultTags = settings->value("listDefaultTags").toStringList();
    QStringList listRegForward = settings->value("listRegForward").toStringList();
    QStringList listRegBackward = settings->value("listRegBackward").toStringList();
    ui->editThumbnailTagPath->setText(settings->value("editThumbnailTagPath").toString());
    settings->endGroup();

    // DockTag DefaltList
    connect(ui->chkDefaultTag, &QCheckBox::clicked, ui->listDefaultTags, &QListWidget::setEnabled);
    ui->listDefaultTags->setEnabled(ui->chkDefaultTag->isChecked());
    static const QStringList tags = {"チャンネル", "ルール", "ジャンル", "サブジャンル"};
    foreach (auto value, tags) {
        auto *item = new  QListWidgetItem(value, ui->listDefaultTags);
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable | Qt::ItemIsSelectable);
        bool isChecked = listDefaultTags.contains(value);
        item->setCheckState(((isChecked || !listDefaultTags.length())? Qt::Checked : Qt::Unchecked));
        item->setIcon(main->getIcon("bookmark-new"));
    }

    // DockTag New program reg list
    regForwardModel = new QStringListModel(listRegForward);
    ui->listRegForward->setModel(regForwardModel);

    connect(ui->actionRegForwardUp, &QAction::triggered, this, [=] {
        int currentIndex = ui->listRegForward->currentIndex().row();
        ui->listRegForward->model()->moveRow(QModelIndex(), currentIndex, QModelIndex(), currentIndex -1);
    });
    connect(ui->actionRegForwardDown, &QAction::triggered, this, [=] {
        int currentIndex = ui->listRegForward->currentIndex().row();
        ui->listRegForward->model()->moveRow(QModelIndex(), currentIndex +1, QModelIndex(), currentIndex);
    });
    connect(ui->actionRegForwardAdd, &QAction::triggered, this, [=] {
        int currentIndex = ui->listRegForward->currentIndex().row();
        regForwardModel->insertRows(currentIndex, 1);
        QModelIndex index = regForwardModel->index(currentIndex);
        ui->listRegForward->setCurrentIndex(index);
        ui->listRegForward->edit(index);
    });
    connect(ui->actionRegForwardEdit, &QAction::triggered, this, [=] {
        QModelIndexList indexes = ui->listRegForward->selectionModel()->selectedRows();
        if (indexes.count() == 0) return;
        ui->listRegForward->edit(indexes[0]);
    });
    connect(ui->actionRegForwardDelete, &QAction::triggered, this, [=] {
        regForwardModel->removeRows(ui->listRegForward->currentIndex().row(),1);
    });

    regBackwardModel = new QStringListModel(listRegBackward);
    ui->listRegBackward->setModel(regBackwardModel);

    connect(ui->actionRegBackwardUp, &QAction::triggered, this, [=] {
        int currentIndex = ui->listRegBackward->currentIndex().row();
        ui->listRegBackward->model()->moveRow(QModelIndex(), currentIndex, QModelIndex(), currentIndex -1);
    });
    connect(ui->actionRegBackwardDown, &QAction::triggered, this, [=] {
        int currentIndex = ui->listRegBackward->currentIndex().row();
        ui->listRegBackward->model()->moveRow(QModelIndex(), currentIndex +1, QModelIndex(), currentIndex);
    });
    connect(ui->actionRegBackwardAdd, &QAction::triggered, this, [=] {
        int currentIndex = ui->listRegBackward->currentIndex().row();
        regBackwardModel->insertRows(currentIndex, 1);
        QModelIndex index = regBackwardModel->index(currentIndex);
        ui->listRegBackward->setCurrentIndex(index);
        ui->listRegBackward->edit(index);
    });
    connect(ui->actionRegBackwardEdit, &QAction::triggered, this, [=] {
        QModelIndexList indexes = ui->listRegBackward->selectionModel()->selectedRows();
        if (indexes.count() == 0) return;
        ui->listRegBackward->edit(indexes[0]);
    });
    connect(ui->actionRegBackwardDelete, &QAction::triggered, this, [=] {
        regBackwardModel->removeRows(ui->listRegBackward->currentIndex().row(),1);
    });

    connect(ui->btnRegReset, &QPushButton::clicked, this, [=] {
        regForwardModel = new QStringListModel(settings->defaultValues["DockTag/listRegForward"].toStringList());
        regBackwardModel = new QStringListModel(settings->defaultValues["DockTag/listRegBackward"].toStringList());
        ui->listRegForward->setModel(regForwardModel);
        ui->listRegBackward->setModel(regBackwardModel);
    });
    connect(ui->btnRegTest, &QPushButton::clicked, this, [=] {
        if (!saveSettings()) return;
        main->updateRegSetting();
        DialogRegTest dlg(main);
        dlg.exec();
    });

    // DockTag checkbox button
    connect(ui->btnDeleteTagNew, &QPushButton::clicked, this, [=] {
        if (!main->deleteConfirm(this)) return;
        QString sql = tr("delete from tags where newflag = true");
        sqlModel->execSql(sql);
    });
    connect(ui->chkAutoNewTag, &QCheckBox::clicked, this, [=] (bool checked) {
        ui->btnDeleteTagNew->setEnabled(!checked);
    });
    ui->btnDeleteTagNew->setEnabled(!ui->chkAutoNewTag->isChecked());
    connect(ui->btnOpenThumbnailTagPath, &QPushButton::clicked, this, [=] {
        QDir dir(ui->editThumbnailTagPath->text());
        QProcess::startDetached("xdg-open", QStringList() << dir.path());
    });

    // DockPlaylist
    settings->beginGroup("DockPlaylist");
    ui->btnGrpPlaylistDbClick->setId(ui->radioPlaylistInnerPlayer, 0);
    ui->btnGrpPlaylistDbClick->setId(ui->radioPlaylistOuterPlayer, 1);
    ui->btnGrpPlaylistDbClick->setId(ui->radioPlaylistOpenPlaylist, 2);
    int btnGrpPlaylistDbClickIndex = settings->value("btnGrpPlaylistDbClickIndex").toInt();
    for (int i = 0; i < ui->btnGrpPlaylistDbClick->buttons().count(); i++)
        if (btnGrpPlaylistDbClickIndex == i) ui->btnGrpPlaylistDbClick->buttons()[i]->setChecked(true);

    ui->editPlaylistPath->setText(settings->value("editPlaylistPath").toString());
    settings->endGroup();

    // DockLive
    settings->beginGroup("DockLive");
    ui->chkInnerPlayerLive->setChecked(settings->value("chkInnerPlayerLive", true).toBool());
    ui->chkLiveSelectedChannelView->setChecked(settings->value("chkLiveSelectedChannelView", false).toBool());
    ui->btnGrpLiveDefault->setId(ui->radioLiveDefaultGR, 0);
    ui->btnGrpLiveDefault->setId(ui->radioLiveDefaultBS, 1);
    ui->btnGrpLiveDefault->setId(ui->radioLiveDefaultCS, 2);
    int btnGrpLiveDefaultIndex = settings->value("btnGrpLiveDefault").toInt();
    for (int i = 0; i < ui->btnGrpLiveDefault->buttons().count(); i++ )
        if (btnGrpLiveDefaultIndex == i) ui->btnGrpLiveDefault->buttons()[i]->setChecked(true);
    settings->endGroup();

    // SimpleView
    settings->beginGroup("SimpleView");
    ui->chkEnableToolBar->setChecked(settings->value("chkEnableToolBar").toBool());
    ui->chkEnableDock->setChecked(settings->value("chkEnableDock").toBool());
    ui->chkEnableInfo->setChecked(settings->value("chkEnableInfo").toBool());
    ui->chkAutoEPG->setChecked(settings->value("chkAutoEPG").toBool());
    ui->chkAutoRecorded->setChecked(settings->value("chkAutoRecorded").toBool());
    ui->chkAutoPlayer->setChecked(settings->value("chkAutoPlayer").toBool());
    ui->chkAutoTag->setChecked(settings->value("chkAutoTag").toBool());
    settings->endGroup();

    // about
    ui->chkVersionCheck->setChecked(settings->value("chkVersionCheck").toBool());
    ui->laAppName->setText(tr("%1 - %2").arg(qApp->applicationName(), qApp->applicationVersion()));
    connect(ui->btnAboutQt, &QPushButton::clicked, this, [=] {
        qApp->aboutQt();
    });

    // 画面サイズの復元
    restoreGeometry( main->getSettings()->valueObj(this, "WindowSize").toByteArray());
}

DialogSettings::~DialogSettings()
{
    // 画面サイズの保存
    if (!isResetClose) main->getSettings()->setValueObj(this, "WindowSize", saveGeometry());
    // 再起動が必要な設定変更
    if (isRestart) restartApp();
    qDeleteAll(selectPlayer);
    delete regForwardModel;
    delete regBackwardModel;
    delete sqlModel;
    delete ui;
}

void DialogSettings::setCurrentTab(int index)
{
    auto currentIndex = ui->stackedWidget->currentIndex();
    auto widget = ui->stackedWidget->widget(currentIndex);
    QList<QTabWidget *> tabs = widget->findChildren<QTabWidget *>();
    if (!tabs.count()) {
        tabs[0]->setCurrentIndex(index);
    }
}

void DialogSettings::setListMenu(SettingMenu menu, int tabIndex /* = -1*/)
{
    int index = static_cast<int>(menu);
    ui->listWidget->setCurrentRow(index);
    ui->stackedWidget->setCurrentIndex(index);

    // set Tab
    if (tabIndex == -1) return;
    auto tabs = ui->stackedWidget->widget(index)->findChildren<QTabWidget *>();
    if (tabs.count()) {
        tabs[0]->setCurrentIndex(tabIndex);
    }
}

void DialogSettings::on_buttonBox_accepted()
{
    if (!saveSettings()) return;
    this->accept();
}

void DialogSettings::on_tableView_customContextMenuRequested(const QPoint &pos)
{
    Menu menu(this);
    menu.addAction(ui->actionHistoryDelete);
    ui->actionHistoryDelete->setEnabled(ui->tableView->isSelected());
    menu.exec(ui->tableView->viewport()->mapToGlobal(pos));
}

void DialogSettings::on_listRegForward_customContextMenuRequested(const QPoint &pos)
{
    Menu menu(this);
    int currentIndex = ui->listRegForward->currentIndex().row();
    int rowCount = ui->listRegForward->model()->rowCount();
    ui->actionRegForwardUp->setEnabled(currentIndex > 0);
    ui->actionRegForwardDown->setEnabled(currentIndex < (rowCount -1));
    menu.addAction(ui->actionRegForwardUp);
    menu.addAction(ui->actionRegForwardDown);
    menu.addSeparator();
    menu.addAction(ui->actionRegForwardAdd);
    menu.addAction(ui->actionRegForwardEdit);
    menu.addAction(ui->actionRegForwardDelete);
    menu.exec(ui->listRegForward->viewport()->mapToGlobal(pos));
}

void DialogSettings::on_listRegBackward_customContextMenuRequested(const QPoint &pos)
{
    Menu menu(this);
    int currentIndex = ui->listRegBackward->currentIndex().row();
    int rowCount = ui->listRegBackward->model()->rowCount();
    ui->actionRegBackwardUp->setEnabled(currentIndex > 0);
    ui->actionRegBackwardDown->setEnabled(currentIndex < (rowCount -1));
    menu.addAction(ui->actionRegBackwardUp);
    menu.addAction(ui->actionRegBackwardDown);
    menu.addSeparator();
    menu.addAction(ui->actionRegBackwardAdd);
    menu.addAction(ui->actionRegBackwardEdit);
    menu.addAction(ui->actionRegBackwardDelete);
    menu.exec(ui->listRegBackward->viewport()->mapToGlobal(pos));
}

void DialogSettings::on_listDbRestore_customContextMenuRequested(const QPoint &pos)
{
    Menu menu(this);
    menu.addAction(ui->actionRestoreRun);
    menu.addAction(ui->actionRestoreOpen);
    menu.addAction(ui->actionRestoreDelete);
    menu.addAction(ui->actionRestoreReflesh);
    menu.exec(ui->listDbRestore->viewport()->mapToGlobal(pos));
}

void DialogSettings::on_btnReset_clicked()
{
    DialogSettingsReset dlg(this);
    if (dlg.exec() == QDialog::Accepted) {
        isResetClose = true;  // 自身のGeometry保存を阻止する
        restartApp();
    }
}

void DialogSettings::on_btnDbConnectTest_clicked()
{
    if (dbConnectionTest()) {
        QMessageBox::information(this, "DB接続テスト", "接続成功");
    }
}

bool DialogSettings::saveConnectSettings()
{
    QString version = "unknown";
    auto errMsg = main->getApi()->readEpgstationTest(ui->editUrl->text(), version);
    if (!errMsg.isEmpty()) {
        QStringList msg;
        msg << "EPGStationに接続できませんでした";
        msg << "接続設定を確認して下さい\n";
        msg << "エラー詳細：";
        msg << errMsg;

        QMessageBox::critical(this, "EPGStation接続", msg.join("\n"));
        setListMenu(SettingMenu::EPGStation);
        return false;
    }

    // DBに接続できない状態で、保存 → 再接続問合せ
    // になると異常終了するので、事前にエラーチェックを行う
    if (!dbConnectionTest()) {
        setListMenu(SettingMenu::Database);
        return false;
    }

    Settings *settings = main->getSettings();

    // epgstation
    settings->setValue("url", ui->editUrl->text());
    settings->setValue("chkInnerPlayerEPG", ui->chkInnerPlayerEPG->isChecked());
    settings->setValue("chkVideoEncPlay", ui->chkVideoEncPlay->isChecked());
    settings->setValue("chkStartEPG", ui->chkStartEPG->isChecked());
    settings->setValue("chkStartRecorded", ui->chkStartRecorded->isChecked());
    settings->setValue("chkStartTagWidget", ui->chkStartTagWidget->isChecked());
    settings->setValue("chkVideoEncPlay", ui->chkVideoEncPlay->isChecked());

    // database
    settings->beginGroup("DB");
    settings->setValue("dbType", (ui->cmbDbType->currentData(Qt::UserRole).toString()));
    settings->setValue("chkDbPortDefault", ui->chkDbPortDefault->isChecked());
    settings->setValue("editDbPort", ui->editDbPort->text());
    settings->setValue("host", ui->editIP->text());
    settings->setValue("dbName", ui->ecitDbName->text());
    settings->setValue("user", ui->editUser->text());
    settings->setValue("pass", ui->editPass->text());
    settings->endGroup();

    return true;
}

bool DialogSettings::saveSettings()
{
    if (!saveConnectSettings()) return false;

    Settings *settings = main->getSettings();

    // epgstation
    settings->setValue("url", ui->editUrl->text());
    settings->setValue("chkInnerPlayerEPG", ui->chkInnerPlayerEPG->isChecked());
    settings->setValue("chkStartEPG", ui->chkStartEPG->isChecked());
    settings->setValue("chkStartRecorded", ui->chkStartRecorded->isChecked());
    settings->setValue("chkStartTagWidget", ui->chkStartTagWidget->isChecked());

    // database
    settings->beginGroup("DB");
    settings->setValue("dbType", (ui->cmbDbType->currentData(Qt::UserRole).toString()));
    settings->setValue("chkDbPortDefault", ui->chkDbPortDefault->isChecked());
    settings->setValue("editDbPort", ui->editDbPort->text());
    settings->setValue("host", ui->editIP->text());
    settings->setValue("dbName", ui->ecitDbName->text());
    settings->setValue("user", ui->editUser->text());
    settings->setValue("pass", ui->editPass->text());
    settings->setValue("editDbBackupPath", ui->editDbBackupPath->text());
    settings->endGroup();

    // style
    auto style = ui->treeStyle->selectedItems()[0]->text(0);
    if (ui->chkDarkIcon->isChecked() != settings->value("chkDarkIcon") ||
        ui->toolTipOpacity->value() !=  settings->value("toolTipOpacity") ||
        style != settings->value("style"))
        isRestart = true;
    settings->setValue("chkDarkIcon", ui->chkDarkIcon->isChecked());
    settings->setValue("style", style);
    settings->setValue("toolTipOpacity", ui->toolTipOpacity->value());

    // player
    settings->beginGroup("WidgetPlayer");
    settings->setValue("chkMultiPlayer", ui->chkMultiPlayer->isChecked());
    settings->setValue("chkAutoHideControlBar", ui->chkAutoHideControlBar->isChecked());
    settings->setValue("chkFullScreen", ui->chkFullScreen->isChecked());
    settings->setValue("chkMiniScreen", ui->chkMiniScreen->isChecked());
    settings->setValue("skipSec", ui->spinSkip->value());
    settings->setValue("skipBackSec", ui->spinSkipBack->value());
    settings->setValue("chkInhibitSleep", ui->chkInhibitSleep->isChecked());
    settings->setValue("chkMPRIS", ui->chkMPRIS->isChecked());
    settings->setValue("HwDec", ui->cmbHwDec->currentText());
    settings->setValue("chkHwDecAutoOff", ui->chkHwDecAutoOff->isChecked());
    settings->setValue("spinSeekWheelStepSec", ui->spinSeekWheelStepSec->value());
    settings->setValue("editScreenshotsPath", ui->editScreenshotsPath->text());
    settings->endGroup();

    // Recoded
    QStringList serachField;
    for (int i=0; i < ui->listSearchField->count(); i++) {
        auto item = ui->listSearchField->item(i);
        if (item->checkState() == Qt::Checked) serachField << item->text();
    }
    settings->beginGroup("WidgetRecorded");
    settings->setValue("chkInnerPlayerRecorded", ui->chkInnerPlayerRecorded->isChecked());
    settings->setValue("editMaxViewHistory", ui->editMaxViewHistory->text());
    settings->setValue("listSearchField", serachField);
    settings->setValue("chkViewThumbnail", ui->chkViewThumbnail->isChecked());
    settings->setValue("spinThunmbnailWidth", ui->spinThunmbnailWidth->value());
    settings->setValue("editThumbnailPath", ui->editThumbnailPath->text());
    settings->endGroup();

    // DockTag
    settings->beginGroup("DockTag");
    settings->setValue("btnGrpTagDbClickIndex", ui->btnGrpTagDbClick->checkedId());
    settings->setValue("chkDefaultTag", ui->chkDefaultTag->isChecked());
    settings->setValue("chkAutoNewTag", ui->chkAutoNewTag->isChecked());
    sqlModel->execSql(tr("select put_var('Threshold', '%1' )").arg(ui->spinThreshold->value()));
    settings->setValue("editThumbnailTagPath", ui->editThumbnailTagPath->text());

    QStringList listDeaultTags;
    for (int i = 0; i < ui->listDefaultTags->count(); i++) {
        auto item = ui->listDefaultTags->item(i);
        if (item->checkState() == Qt::Checked) listDeaultTags << item->text();
    }
    settings->setValue("listDefaultTags", listDeaultTags);

    settings->setValue("listRegForward", qobject_cast<QStringListModel*>(ui->listRegForward->model())->stringList());
    settings->setValue("listRegBackward", qobject_cast<QStringListModel*>(ui->listRegBackward->model())->stringList());
    settings->endGroup();

    // DockPlaylist
    settings->beginGroup("DockPlaylist");
    settings->setValue("btnGrpPlaylistDbClickIndex", ui->btnGrpPlaylistDbClick->checkedId());
    settings->setValue("editPlaylistPath", ui->editPlaylistPath->text());
    settings->endGroup();

    // DockLive
    settings->beginGroup("DockLive");
    settings->setValue("chkInnerPlayerLive", ui->chkInnerPlayerLive->isChecked());
    settings->setValue("chkLiveSelectedChannelView", ui->chkLiveSelectedChannelView->isChecked());
    settings->setValue("btnGrpLiveDefault", ui->btnGrpLiveDefault->checkedId());
    settings->endGroup();

    // SimpleView
    settings->beginGroup("SimpleView");
    settings->setValue("chkEnableToolBar", ui->chkEnableToolBar->isChecked());
    settings->setValue("chkEnableDock", ui->chkEnableDock->isChecked());
    settings->setValue("chkEnableInfo", ui->chkEnableInfo->isChecked());
    settings->setValue("chkAutoEPG", ui->chkAutoEPG->isChecked());
    settings->setValue("chkAutoRecorded", ui->chkAutoRecorded->isChecked());
    settings->setValue("chkAutoPlayer", ui->chkAutoPlayer->isChecked());
    settings->setValue("chkAutoTag", ui->chkAutoTag->isChecked());
    settings->endGroup();

    // about
    settings->setValue("chkVersionCheck", ui->chkVersionCheck->isChecked());

    return true;
}

bool DialogSettings::dbConnectionTest()
{
    auto driver = (ui->cmbDbType->currentIndex() == 1)? "QPSQL" : "QMYSQL";
    auto host = ui->editIP->text();
    auto dbName = ui->ecitDbName->text();
    auto user = ui->editUser->text();
    auto pass = ui->editPass->text();

    auto portStr = ui->editDbPort->text();
    auto portDefault = ui->chkDbPortDefault->isChecked();

    bool isNum = false;
    auto port = portStr.toInt(&isNum);
    if (!isNum || portDefault)
        port = -1;

    QSqlError err;
    QSqlDatabase db = QSqlDatabase::addDatabase(driver, "test");
    db.setDatabaseName(dbName);
    db.setHostName(host);
    db.setPort(port);
    if (!db.open(user, pass))
        err = db.lastError();

    db = QSqlDatabase();
    QSqlDatabase::removeDatabase("test");

    if (err.type() != QSqlError::NoError) {
        QStringList msg;
        msg << "データベースに接続できませんでした";
        msg << "接続設定を確認して下さい\n";
        msg << "エラー詳細：";
        msg << err.text();

        QMessageBox::critical(this, "DB接続エラー", msg.join("\n"));
        return false;
    }
    return true;
}

void DialogSettings::updateDbPort()
{
    bool isPortDefault = ui->chkDbPortDefault->isChecked();
    if (isPortDefault) {
        bool isPgsql = (ui->cmbDbType->currentIndex() == 1);
        ui->editDbPort->setText(main->getSettings()->getDbPortDefault(isPgsql));
    }
    ui->editDbPort->setEnabled(!isPortDefault);
}

void DialogSettings::uodateListDbRestore()
{
    ui->listDbRestore->clear();

    QDir dir(ui->editDbBackupPath->text());
    auto files = dir.entryList(QStringList() << "*backup.sql", QDir::Files);
    foreach (auto file, files) {
        auto *item = new  QListWidgetItem(file, ui->listDbRestore);
        item->setData(Qt::UserRole, dir.filePath(file));
        item->setIcon(main->getIcon("run-build-file"));
    }
    ui->listDbRestore->sortItems(Qt::DescendingOrder);
}

bool DialogSettings::backupDb(bool confirm /*=false*/)
{
    auto now = QDateTime::currentDateTime();
    static const QStringList extTables = { "temp_vars", "play", "search", "tags" };
    auto writeList = QStringList();
    writeList << tr("-- %1").arg(now.toString("yyyy年 MM月 dd日 hh:mm"))
              << tr("-- databese driver: %1").arg(QSqlDatabase::database().driverName())
              << tr("-- tabls: %1").arg(extTables.join(","))
              << "\n";

    foreach (auto table, extTables)
        writeList << sqlModel->getSqlTableDump(table);

    auto fileName = QString("%1_backup.sql").arg(now.toString("yyyy-MM-dd_hh:mm"));
    auto filePath = QDir(ui->editDbBackupPath->text()).filePath(fileName);

    QFile file(filePath);    // 保存するファイル
    if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream out(&file);
        foreach (QString value, writeList)
            out << value + '\n';
        file.close();

        if (!file.error()) {
            if (!confirm) return true;

            if (QMessageBox::Yes == QMessageBox::information(this, "バックアップ確認", "バックアップ内容を確認しますか？", QMessageBox::Yes | QMessageBox::Cancel))
                QProcess::startDetached("xdg-open", QStringList() << filePath);
        }
    }

    if (file.error() && confirm)
        QMessageBox::information(this, "バックアップ確認", tr("バックアップ問題がありました。\n%1").arg(file.errorString()));

    return false;
}

void DialogSettings::restoreDb()
{
    // NOTE: そのまま復元だと、tempテーブルの更新などやることが多良いので再起動させる
    if (ui->listDbRestore->currentRow() == -1) {
        QMessageBox::warning(this, "リストア", "バックアップファイルを選択して下さい");
        return;
    }

    auto fileName = ui->listDbRestore->currentItem()->data(Qt::UserRole).toString();
    if (!QFileInfo::exists(fileName)) {
        QMessageBox::warning(this, "リストア", "指定されたバックアップファイルが存在しません");
        uodateListDbRestore();
        return;
    }

    QStringList msg;
    msg << "リストア処理を実行すると再起動されます";
    msg << "本当に実行しますか？\n";
    msg << tr("[%1]").arg(fileName);
    if (QMessageBox::Yes != QMessageBox::critical(this, "リストア", msg.join("\n"), QMessageBox::Yes | QMessageBox::Cancel))
        return;

    if (!saveSettings()) return;

    main->getSettings()->setValue("DB/restoreDb", fileName);
    restartApp();
}

void DialogSettings::resetDb()
{
    QStringList msg;
    msg << "アプリ起動時に、自動でDBの拡張を行うので";
    msg << "完全にアンイストールしたい場合には、削除処理の実行後に";
    msg << "本アプリを起動しないで下さい\n";
    msg << "本当に実行しますか？\n";
    if ( QMessageBox::Yes != QMessageBox::critical(this, "DB拡張の削除", msg.join("\n"), QMessageBox::Yes | QMessageBox::Cancel))
        return;

    if (!dbConnectionTest()) {
        setListMenu(SettingMenu::Database);
        return;
    }

    if (QMessageBox::Yes == QMessageBox::information(this, "バックアップの確認", "「DB拡張の削除」の実行前にバックアップを作成しますか？", QMessageBox::Yes | QMessageBox::No)) {
        if (!backupDb(false)) {
            QMessageBox::information(this, "バックアップの失敗", "バックアップに失敗しました、処理を中断します");
            return;
        }
    }
    sqlModel->execSqlFile(":/sql/delete_all.sql");
    qApp->quit();
}

void DialogSettings::restartApp()
{
    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
}

void DialogSettings::on_btnConnectTest_clicked()
{
    QString version = "unknown";
    auto errMsg = main->getApi()->readEpgstationTest(ui->editUrl->text(), version);
    if (errMsg.isEmpty()) {
        auto text = version.isEmpty()? "v1系、または不明なバージョン" : version;
        QMessageBox::information(this, "接続テスト", "接続成功\n\n検出バージョン：" + text);
    } else {
        QStringList msg;
        msg << "EPGStationに接続できませんでした";
        msg << "接続設定を確認して下さい\n";
        msg << "エラー詳細：";
        msg << errMsg;

        QMessageBox::critical(this, "EPGStation接続テスト", msg.join("\n"));
    }
}

void DialogSettings::on_btnTagsUpdateFull_clicked()
{
    main->updateTagTableFull();
}

void DialogSettings::on_btnDeleteTagNewSurplus_clicked()
{
    if (!saveSettings()) return;
    main->updateRegSetting();
    DialogTagNewDelete dlg(main);
    dlg.exec();
}

