#ifndef DIALOG_SETTINGS_H
#define DIALOG_SETTINGS_H

#include <QDialog>
#include "mainwindow.h"

class QStringListModel;

namespace Ui {class DialogSettings;}

class DialogSettings : public QDialog
{
    Q_OBJECT

public:
    explicit DialogSettings(SettingMenu menu = SettingMenu::General, QWidget *parent = nullptr, int tabIndex = -1);
    ~DialogSettings();

    void setCurrentTab(int index);

private slots:
    void on_buttonBox_accepted();
    void on_tableView_customContextMenuRequested(const QPoint &pos);
    void on_listDbRestore_customContextMenuRequested(const QPoint &pos);
    void on_listRegBackward_customContextMenuRequested(const QPoint &pos);
    void on_listRegForward_customContextMenuRequested(const QPoint &pos);
    void on_btnReset_clicked();
    void on_btnDbConnectTest_clicked();
    void on_btnConnectTest_clicked();
    void on_btnTagsUpdateFull_clicked();
    void on_btnDeleteTagNewSurplus_clicked();

private:
    void setListMenu(SettingMenu menu, int tabIndex = -1);
    bool saveSettings();
    bool saveConnectSettings();
    bool dbConnectionTest();
    void updateDbPort();
    void uodateListDbRestore();
    bool backupDb(bool confirm = false);
    void restoreDb();
    void resetDb();
    void restartApp();
    struct menuItem {
        QString name; QString iconName;
    };
    struct styleItem {
        QString name; QString explanation;
    };
    QList<QCheckBox*> selectPlayer;
    SqlModel *sqlModel;
    QStringListModel* regForwardModel;
    QStringListModel* regBackwardModel;
    bool isRestart = false;
    bool isResetClose = false;
    MainWindow* main;
    Ui::DialogSettings *ui;
};

#endif // DIALOG_SETTINGS_H
