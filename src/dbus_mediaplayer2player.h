/**
 * MPRIS D-Bus Interface 2.2
 * https://specifications.freedesktop.org/mpris-spec/latest/index.html
 *
 * Interface MediaPlayer2.Player
 * https://specifications.freedesktop.org/mpris-spec/latest/Player_Interface.html
*/

#ifndef DBUS_MEDIAPLAYER2PLAYER_H
#define DBUS_MEDIAPLAYER2PLAYER_H

#include <QDBusAbstractAdaptor>
#include <QDBusObjectPath>

class MainWindow;
class WidgetPlayer;

class DBusMediaPlayer2Player : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.mpris.MediaPlayer2.Player")

    Q_PROPERTY(QString PlaybackStatus READ PlaybackStatus)
    Q_PROPERTY(QString LoopStatus READ LoopStatus WRITE setLoopStatus)
    Q_PROPERTY(double Rate READ Rate WRITE setRate)
    // プロパティを非公開にすると、Plasmaメディアプレイヤーや、KDEConnectでUIを無効にできる
    //Q_PROPERTY(bool Shuffle READ Shuffle WRITE setShuffle)
    Q_PROPERTY(QVariantMap Metadata READ Metadata)
    Q_PROPERTY(double Volume READ Volume WRITE setVolume)
    Q_PROPERTY(qlonglong Position READ Position)
    Q_PROPERTY(double MinimumRate READ MinimumRate)
    Q_PROPERTY(double MaximumRate READ MaximumRate)

    Q_PROPERTY(bool CanGoNext READ CanGoNext)
    Q_PROPERTY(bool CanGoPrevious READ CanGoPrevious)
    Q_PROPERTY(bool CanPlay READ CanPlay)
    Q_PROPERTY(bool CanPause READ CanPause)
    Q_PROPERTY(bool CanSeek READ CanSeek)
    Q_PROPERTY(bool CanControl READ CanControl)

public:
    explicit DBusMediaPlayer2Player(WidgetPlayer* player, QObject *parent);

public slots:
    void Next();
    void Previous();
    void Pause();
    void PlayPause();
    void Stop();
    void Play();
    void Seek(qlonglong Offset);
    void SetPosition(const QDBusObjectPath& TrackId, qlonglong Position);
    void OpenUri(QString uri);

signals:
    void Seeked(qlonglong Position);

private:
    QString PlaybackStatus() const;
    QString LoopStatus() const;
    void setLoopStatus(const QString& loopStatus) const;
    double Rate() const;
    void setRate(double rate) const;
    QVariantMap Metadata() const;
    double Volume() const;
    void setVolume(double volume) const;
    qlonglong Position() const;
    double MinimumRate() const;
    double MaximumRate() const;
    bool CanGoNext() const;
    bool CanGoPrevious() const;
    bool CanPlay() const;
    bool CanPause() const;
    bool CanSeek() const;
    bool CanControl() const;

    QVariantMap getDefaultProperties();
    QVariantMap noTrackMetadata;
    MainWindow* main;
    WidgetPlayer* widgetPlayer;
};

#endif // DBUS_MEDIAPLAYER2PLAYER_H
