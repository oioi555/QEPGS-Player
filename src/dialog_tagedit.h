#ifndef DIALOG_TAGEDIT_H
#define DIALOG_TAGEDIT_H

#include <QDialog>
#include <QFileInfo>

#include "mainwindow.h"

class ThumbnailTag;

namespace Ui {class DialogTagEdit;}

class DialogTagEdit : public QDialog
{
    Q_OBJECT

public:
    explicit DialogTagEdit(QWidget *parent, int id, const QString &field = "", const QString &genre = "");
    ~DialogTagEdit();

    int getId() const {return id;}

private slots:
    void on_buttonBox_accepted();
    void on_cmbField_currentIndexChanged(int index);
    void on_dropFile(const QMimeData *mimeData);
private:
    void updateImageFile();

    int id;
    ThumbnailTag *imgTag;
    SqlModel *sqlModel;
    MainWindow* main;
    Ui::DialogTagEdit *ui;

    // QWidget interface
protected:
    virtual void resizeEvent(QResizeEvent *event) override;
};
#endif // DIALOG_TAGEDIT_H
