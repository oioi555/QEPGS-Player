#include "widget_recorded.h"
#include "ui_widget_recorded.h"

#include <QFileInfo>
#include <QProcess>

WidgetRecorded::WidgetRecorded(QWidget *parent) :
    QWidget(parent)
  , playlistView(false)
  , menuSearch(new QMenu)
  , sqlModel(new SqlModel("v_recorded", "id"))
  , sqlModelSearch(new SqlModel("search", "id"))
  , sqlModelTag(new SqlModel("tags", "id"))
  , main(qobject_cast<MainWindow*>(parent))
  , delegate(new TableViewDelegate(main))
  , ui(new Ui::WidgetRecorded)
{
    ui->setupUi(this);

    // window   
    setWindowTitle("録画済リスト");
    auto icon = windowIcon();
    setWindowIcon(QIcon(icon.pixmap(icon.actualSize(QSize(22, 22)))));

    // mainwndow
    connect(main, &MainWindow::playerStatusChanged, this, &WidgetRecorded::on_playerStatusChanged);
    connect(main, &MainWindow::mainStatusChanged, this, &WidgetRecorded::on_mainStatusChanged);

    // shortcut
    ui->tableView->addActions(this->findChildren<QAction*>());

    // search
    connect(ui->editSearch, &EditSearch::editingFinished, this, [=] {
        setSearch(ui->editSearch->text());
    });
    connect(ui->editSearch, &EditSearch::searchClicked, this, [=] {
        setSearch(ui->editSearch->text());
    });

    updateSearchMenu();
    ui->toolSaveList->setMenu(menuSearch);

    connect(menuSearch, &QMenu::triggered, this, [=] (QAction* action) {
        setSearch(action->data().toString());
    });

    connect(ui->btnRestFilter, &QToolButton::clicked, ui->actionRestFilter, &QAction::trigger);
    connect(ui->btnReflesh, &QToolButton::clicked, ui->actionReflesh, &QAction::trigger);

    // action
    needSelectActions << ui->actionPlay << ui->actionPlayBegin << ui->actionPlayOuter
            << ui->actionPlaylistSave << ui->actionDelete << ui->actionDeleteParsent
            << ui->actionTagAdd << ui->actionTagAddEndflag << ui->actionFullParsent;

    connect(ui->actionPlay, &QAction::triggered, this, [=] {
        auto ids = ui->tableView->getSelectedIds();
        if (ids.count() == 0) return;
        if (ids.count() == 1) {
            // 単選択では、できるだけTagを検出して、Tagとして開く
            auto tagId = getTagId(ids[0]);
            if (tagId != -1) main->openPlayerTag(tagId, ids[0]);
            else main->openPlayerRecordedIds(ids);
        } else {
            // 複数選択状態では、プレイリストとして開く
            if (main->getSettings()->value("WidgetPlayer/chkMultiPlayer").toBool()) {
                QStringList msg;
                msg << "複数のプレイヤーで再生しますか？";
                msg << "複数再生時に、ミニスクリーン、フルスクリーン表示で";
                msg << "複数の動画を並べて同時視聴できます";
                if (QMessageBox::Yes == QMessageBox::information(this, "マルチプレイ", msg.join("\n"), QMessageBox::Yes | QMessageBox::No)) {
                    foreach (auto id, ui->tableView->getSelectedIds())
                        main->openPlayerRecordedIds({id});
                    return;
                }
            }
            main->openPlayerRecordedIds(ids);
        }
    });
    connect(ui->actionPlayOuter, &QAction::triggered, this, [=] {
        QList<int> ids = ui->tableView->getSelectedIds();
        Playlist playlist;
        playlist.setRecordedIds(ids);
        if (!playlist.hasItem()) return;

        QString saveFileName = "recoeded_playlist.m3u8";
        if (playlist.saveFile(saveFileName))
            QProcess::startDetached("xdg-open", QStringList() << saveFileName);
    });
    connect(ui->actionPlaylistSave, &QAction::triggered, this, [=] {
        QList<int> ids = ui->tableView->getSelectedIds();
        Playlist playlist;
        playlist.setRecordedIds(ids);
        if (!playlist.hasItem()) return;

        QString dir = main->getSettings()->value("DockPlaylist/editPlaylistPath").toString();
        QString filePath = playlist.getSaveFileName(dir, windowTitle(), this);
        if (filePath.isEmpty()) return;
        if (playlist.saveFile(filePath))
            main->updatePlaylist(filePath);
    });
    connect(ui->actionPlayBegin, &QAction::triggered, this, [=] {
        ui->actionDeleteParsent->trigger();
        ui->actionPlay->trigger();
    });
    connect(ui->actionDelete, &QAction::triggered, this, [=] {
        QList<int> ids = ui->tableView->getSelectedIds();
        main->deleteRecorded(ids);
    });
    connect(ui->actionDeleteParsent, &QAction::triggered, this, [=] {
        QList<int> ids = ui->tableView->getSelectedIds();
        main->deleteParsent(ids);
        ui->tableView->reQuery();
        updateRowHeight();
    });
    connect(ui->actionFullParsent, &QAction::triggered, this, [=] {
        QList<int> ids = ui->tableView->getSelectedIds();
        main->setFullParsent(ids);
        ui->tableView->reQuery();
        updateRowHeight();
    });
    connect(ui->actionEncord, &QAction::triggered, this, [=] {
        QList<int> ids = ui->tableView->getSelectedIds();
        if (main->openEncordDlg(ids)) {
            ui->tableView->reQuery();
            updateRowHeight();
        }
    });
    connect(ui->actionCancelEncord, &QAction::triggered, this, [=] {
        QList<int> ids = ui->tableView->getSelectedIds();
        if (main->cancelEncord(ids)) {
            ui->tableView->reQuery();
            updateRowHeight();
        }
    });
    connect(ui->actionTagAdd, &QAction::triggered, this, [=] {
        int id = ui->tableView->getSelectedId();
        main->openTagAddDlg(id);
    });
    connect(ui->actionTagAddEndflag, &QAction::triggered, this, [=] {
        auto ids = ui->tableView->getSelectedIds();
        main->addTagEndFlag(ids);
    });
    connect(ui->actionRestFilter, &QAction::triggered, this, [=] {
        ui->tableView->resetFiler();
        setSearch("");
    });
    connect(ui->actionAutoSize, &QAction::triggered, this, [=] {
        ui->tableView->autoSizeColRow();
        auto width = main->getSettings()->valueObj(this, "spinThunmbnailWidth").toInt();
        ui->tableView->setColumnWidth(0, width);
        ui->tableView->sortByColumn(2, Qt::SortOrder::DescendingOrder);
        updateRowHeight();
    });
    connect(ui->actionReflesh, &QAction::triggered, this, [=] {
        delegate->updateData();
        ui->tableView->reQuery();
        updateRowHeight();
    });
    connect(ui->actionSetting,&QAction::triggered, this, [=] {
        main->openSettings(SettingMenu::Recorded);
    });

    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);
    connect(sqlModelSearch, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    // tableview
    ui->tableView->setSqlModel(sqlModel);
    ui->tableView->setItemDelegate(delegate);
    ui->tableView->initSql();
    connect(ui->tableView, &TableViewSqlFilter::doubleClicked, this, [=] (const QModelIndex &/*index*/) {
        bool inner = main->getSettings()->valueObj(this, "chkInnerPlayerRecorded").toBool();
        if (inner)
            ui->actionPlay->trigger();
        else
            ui->actionPlayOuter->trigger();
    });
    connect(ui->tableView, &TableViewSqlFilter::selectionChangedId, this, [=] (int id) {
        if (main->isActiveWindow(this)) main->setDcokInfoPlayingId(id);
    });
    connect(ui->tableView, &TableViewSqlFilter::updateFilterEvent, this, [=] (const QString &filters) {
        titleFilter = filters;
        updateWindowTitle();
        updateRowHeight();
    });
    connect(ui->tableView, &TableViewSqlFilter::headerResized, this, [=] (int logical, int /*oldSize*/, int /*newSize*/) {
         if (logical == 0)
            updateRowHeight();

         main->getSettings()->setValueObj(this, "columnsSave", ui->tableView->saveState());
    });
    connect(ui->tableView, &TableViewSqlFilter::columnsStateChanged, this, [=] {
         main->getSettings()->setValueObj(this, "columnsState", ui->tableView->getColumnsState());
    });
    connect(ui->tableView, &TableViewSqlFilter::mousePress, this, [=] (QMouseEvent *event) {
        auto anchor = anchorAt(event->pos());
        if (!anchor.isEmpty()) {
            QString escapedTag = sqlModel->escape(anchor);
            QString sql = tr("select id from tags where field='番組タイトル' and tag like '%%1%'").arg(escapedTag);
            int tagid = sqlModel->getScholar(sql).toInt();
            if (tagid)
                main->selectTag(tagid);
            else
                main->getPop()->show(anchor + "\nは見つかりませんでした");
        }
    });
    connect(ui->tableView, &TableViewSqlFilter::mouseRelease, this, [=] (QMouseEvent */*event*/) {
        QApplication::restoreOverrideCursor();
    });
    connect(ui->tableView, &TableViewSqlFilter::mouseMove, this, [=] (QMouseEvent *event) {
        auto anchor = anchorAt(event->pos());
        if (lastHoveredAnchor != anchor) {
            lastHoveredAnchor = anchor;
            if (!lastHoveredAnchor.isEmpty())
                QApplication::setOverrideCursor(QCursor(Qt::PointingHandCursor));
            else
                QApplication::restoreOverrideCursor();
        }
    });
}

void WidgetRecorded::showEvent(QShowEvent *event)
{
    if (isInitialize) return;

    // Note: Qt6で、設定の復元が機能しないのでコンストラクターから移動
    auto state = main->getSettings()->valueObj(this, "columnsSave", "").toByteArray();
    bool initState = state.isEmpty();   // 設定リセット直後
    if (!initState)
        ui->tableView->restoreState(state);

    if (initState) {    // 設定リセット直後
        ui->tableView->sortByColumn(2, Qt::SortOrder::DescendingOrder);
        ui->actionAutoSize->trigger();
    } else {
        updateRowHeight();
    }

    isInitialize = true;
}

WidgetRecorded::~WidgetRecorded()
{
    main->getSettings()->setValueObj(this, "columnsSave", ui->tableView->saveState());

    delete menuSearch;
    qDeleteAll(searchActions);
    delete sqlModel;
    delete sqlModelSearch;
    delete ui;
}

void WidgetRecorded::setSearch(const QString &text)
{
    QStringList fileds = main->getSettings()->valueObj(this, "listSearchField").toStringList();
    QString wordSearch = getTextTrim(text);

    QStringList words = wordSearch.split(" ");
    QStringList where;
    foreach (auto word, words) {
        QStringList section;
        QString escapedWord = sqlModel->escape(word);
        foreach (auto filed, fileds)
            section << tr("%1 like '%%2%'").arg(filed, escapedWord);
        where << tr("(%1)").arg(section.join(" OR "));
    }

    ui->editSearch->setText(wordSearch);

    if (wordSearch.isEmpty())
        sqlModel->setWhere("");
    else
        sqlModel->setWhere(where.join(" AND "));
    ui->tableView->reQuery(false);
    updateWindowTitle();
    updateRowHeight();

    // 検索履歴に追加
    addSearchMenu(sqlModel->escape(wordSearch));
}

bool WidgetRecorded::setFilter(const QString &field, const QString &tag, const int &selectID/* = -1*/)
{
    bool result = ui->tableView->setFilter(field, tag, selectID);
    updateRowHeight();
    return result;
}

void WidgetRecorded::resetFiler()
{
    ui->tableView->resetFiler();
    updateRowHeight();
}

bool WidgetRecorded::openPlaylist(const QString &file)
{
    playlistView = true;
    playlistFileName = file;
    ui->headerSubLable->setText("プレイリスト");
    ui->editSearch->setHidden(true);
    ui->toolSaveList->setHidden(true);
    ui->btnRestFilter->setHidden(true);

    Playlist list;
    if (!list.readPlayFile(file)) return false;
    auto idStrs = list.getIdStrs();
    if (!idStrs.length()) return false;

    sqlModel->setWhere(tr("id in (%1)").arg(idStrs.join(",")));
    ui->tableView->reQuery(false);
    updateWindowTitle();
    return true;
}

void WidgetRecorded::updateWindowTitle()
{
    QString title = "録画済リスト";
    if (playlistView) {
        ui->laFilePath->setText(playlistFileName);
        QFileInfo info(playlistFileName);
        title = playlistFileName.isEmpty()? "プレイリスト" : info.baseName();
        main->setWindowIcon("view-media-playlist", this);
    } else {
        QString searchWord = ui->editSearch->text();
        QStringList titles;
        if (!searchWord.isEmpty()) titles << searchWord;
        if (!titleFilter.isEmpty()) titles << titleFilter;
        title = (titles.count())? titles.join(" - ") : title;
    }
    const static int trimCount = 20;
    if (trimCount < title.length()) {
        title.truncate(trimCount);
        title += "…";
    }
    setWindowTitle(title);
}

void WidgetRecorded::updateSearchMenu()
{
    menuSearch->clear();
    qDeleteAll(searchActions);
    searchActions.clear();
    int editMaxViewHistory = main->getSettings()->valueObj(this, "editMaxViewHistory").toInt();
    sqlModelSearch->setWhere("");
    sqlModelSearch->setOrderBy("date desc");
    sqlModelSearch->setLimit(LimitType::limitTop, editMaxViewHistory);
    auto model = sqlModelSearch->getModel(SelectType::selectWhere);
    if (!model->rowCount()) return;

    for (int i = 0; i < model->rowCount(); i++) {
        QSqlRecord rs = model->record(i);
        QDateTime date = rs.value("date").toDateTime();
        QString word = rs.value("word").toString();

        QString text = tr("%1｜%2").arg(date.toString("MM/dd (ddd) hh:mm"),word);
        auto *action = new QAction(text);
        action->setData(word);
        action->setIcon(QIcon::fromTheme("dialog-messages"));
        searchActions << action;
    }
    menuSearch->addActions(searchActions);
}

void WidgetRecorded::addSearchMenu(const QString &text)
{
    if (text.isEmpty()) return;

    // 重複確認して、重複してたら日付のアップデート
    QString where = tr("word='%1'").arg(text);
    sqlModelSearch->setWhere(where);
    auto model = sqlModelSearch->getModel(SelectType::selectWhere);
    if (!model->rowCount()) {
        // insert
        sqlModelSearch->setValueClear();
        sqlModelSearch->setValue("word", text);
        sqlModelSearch->saveValues();
    } else {
        // update
        int id = model->record(0).value("id").toInt();
        sqlModelSearch->setValueClear();
        sqlModelSearch->setValue("date", QDateTime::currentDateTime());
        sqlModelSearch->saveValues(id);
    }
    updateSearchMenu();
}

// アスペクト比に沿って、画像の幅から高さを設定
void WidgetRecorded::updateRowHeight()
{
    bool chkViewThumbnail = main->getSettings()->value("WidgetRecorded/chkViewThumbnail").toBool();
    if (chkViewThumbnail && !ui->tableView->horizontalHeader()->isSectionHidden(0)) {
        int width = ui->tableView->horizontalHeader()->sectionSize(0);
        ui->tableView->verticalHeader()->setDefaultSectionSize(static_cast<int>(width * 0.56));
    } else
        ui->tableView->verticalHeader()->setDefaultSectionSize(0);
}

auto WidgetRecorded::anchorAt(const QPoint &pos) const -> QString
{
    if (auto index = ui->tableView->indexAt(pos); index.isValid()) {
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
        auto delegate = ui->tableView->itemDelegateForIndex(index);
#else
        auto delegate = ui->tableView->itemDelegate(index);
#endif
        auto tableDelegate = qobject_cast<TableViewDelegate *>(delegate);
        if (tableDelegate != nullptr) {
            auto itemRect = ui->tableView->visualRect(index);
            auto relativeClickPosition = pos - itemRect.topLeft();
            auto text = ui->tableView->model()->data(index, Qt::DisplayRole).toString();
            return tableDelegate->anchorAt(text, relativeClickPosition);
        }
    }

    return QString();
}

// NOTE: Tagの検出優先順位
// 番組タイトル、ルール、チャンネル、ジャンル、サブジャンル
int WidgetRecorded::getTagId(int recordedId)
{
    // tagの検出
    const auto rs = sqlModel->getRecord(recordedId);
    sqlModelTag->setWhere("field='番組タイトル' or field='ルール'");
    sqlModelTag->setOrderBy("field desc");
    auto model = sqlModelTag->getModel(SelectType::selectWhere);
    for (int i = 0; i < model->rowCount(); i++) {
        const auto rsTag = model->record(i);
        auto field = rsTag.value("field").toString();
        if (rs.value(field).toString().contains(rsTag.value("tag").toString())) {
            return rsTag.value("id").toInt();
        }
    }
    return -1;
}

auto WidgetRecorded::getTextTrim(const QString &text) -> QString
{
    QString wordSearch = text.trimmed();
    wordSearch.replace("　", " ");
    wordSearch.replace('"', " ");
    wordSearch = wordSearch.simplified();
    wordSearch = wordSearch.trimmed();
    return wordSearch;
}

void WidgetRecorded::on_playerStatusChanged(PlayerStatus status, int /*id*/)
{
    if (status == PlayerStatus::PlayerClose)
        ui->tableView->reQuery();
}

void WidgetRecorded::on_mainStatusChanged(MainStatus status)
{
    if (status == MainStatus::Database)
        ui->tableView->setColumnsState(main->getSettings()->valueObj(this, "columnsState").toList());
    if (status == MainStatus::Database || status == MainStatus::VideoDeleted) {
        updateSearchMenu();
        ui->actionReflesh->trigger();
    } else if (status == MainStatus::TagTabel) {
        ui->actionReflesh->trigger();
    }
}

void WidgetRecorded::on_tableView_customContextMenuRequested(const QPoint &pos)
{
    bool isSelected = (ui->tableView->getSelectedId() != -1);
    Menu menu(this);

    foreach (auto action, needSelectActions)
        action->setEnabled(isSelected);

    menu.addAction(ui->actionPlay);
    menu.addAction(ui->actionPlayBegin);
    menu.addSeparator();
    menu.addAction(ui->actionPlayOuter);
    menu.addAction(ui->actionPlaylistSave);
    menu.addSeparator();
    menu.addAction(ui->actionDelete);
    menu.addAction(ui->actionDeleteParsent);
    menu.addAction(ui->actionFullParsent);
    if (main->getApi()->isV2) {
        menu.addSeparator();
        menu.addAction(ui->actionEncord);
        menu.addAction(ui->actionCancelEncord);
    }
    menu.addSeparator();
    menu.addAction(ui->actionTagAdd);
    menu.addAction(ui->actionTagAddEndflag);
    menu.addSeparator();
    menu.addAction(ui->actionRestFilter);
    menu.addAction(ui->actionAutoSize);
    menu.addAction(ui->actionReflesh);
    menu.addSeparator();
    menu.addAction(main->getDockNaviAction());
    menu.addAction(main->getDockInfoAction());
    menu.addSeparator();
    menu.addAction(main->getSimpleViewAction());
    menu.addSeparator();
    menu.addAction(ui->actionSetting);
    menu.exec(ui->tableView->viewport()->mapToGlobal(pos));
}

