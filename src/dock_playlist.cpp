#include "dock_playlist.h"
#include "ui_dock_playlist.h"

#include <QFileSystemModel>
#include <QProcess>

DockPlaylist::DockPlaylist(QWidget *parent) :
    QDockWidget(parent)
  , fileModel(new QFileSystemModel(this))
  , main(qobject_cast<MainWindow*>(parent))
  , ui(new Ui::DockPlaylist)
{
    ui->setupUi(this);
    this->setTitleBarWidget(new QWidget(this));

    // ui header
    auto icon = main->getIcon("view-media-playlist");
    ui->laIcon->setPixmap(icon.pixmap(icon.actualSize(QSize(16, 16))));

    connect(ui->toolReflesh, &QToolButton::clicked, ui->actionReflesh, &QAction::triggered);
    connect(ui->toolOpenFolder, &QToolButton::clicked, ui->actionOpenFolder, &QAction::triggered);

    // action
    needSelectActions << ui->actionOpen << ui->actionPlayOuter
                      << ui->actionRename << ui->actionDelete;

    connect(ui->actionPlay, &QAction::triggered, this, [=] {
        QString file = getSelectedFile();
        if (file.isEmpty()) return;
        main->openPlayerFile(file);
    });
    connect(ui->actionOpen, &QAction::triggered, this, [=] {
        QString file = getSelectedFile();
        if (file.isEmpty()) return;
        main->openPlaylist(file);
    });
    connect(ui->actionPlayOuter, &QAction::triggered, this, [=] {
        QString file = getSelectedFile();
        if (file.isEmpty()) return;
        QProcess::startDetached("xdg-open",QStringList() << file);
    });
    connect(ui->actionRename, &QAction::triggered, this, [=] {
        QModelIndexList indexes = ui->treeView->selectionModel()->selectedRows();
        if (indexes.count() == 0) return;
        ui->treeView->edit(indexes[0]);
    });
    connect(ui->actionDelete, &QAction::triggered, this, [=] {
        if (!main->deleteConfirm(this)) return;
        QModelIndexList indexes = ui->treeView->selectionModel()->selectedRows();
        if (indexes.count() == 0) return;
        fileModel->remove(indexes[0]);
    });
    connect(ui->actionReflesh, &QAction::triggered, this, [=] {
        fileModel->setRootPath("");
        QString dir = main->getSettings()->valueObj(this, "editPlaylistPath").toString();
        fileModel->setNameFilters(QStringList() << "*.m3u8");
        fileModel->setNameFilterDisables(false);
        ui->treeView->setRootIndex(fileModel->setRootPath(dir));
    });
    connect(ui->actionOpenFolder, &QAction::triggered, this, [=] {
        QDir dir = fileModel->rootDirectory();
        if (dir.isEmpty()) {
            QString path = main->getSettings()->valueObj(this, "editPlaylistPath").toString();
            dir.setPath(path);
        }
        QProcess::startDetached("xdg-open", QStringList() << dir.path());
    });
    connect(ui->actionSetting,&QAction::triggered, this, [=] {
        main->openSettings(SettingMenu::Playlist);
    });

    // tree
    fileModel->setReadOnly(false);
    ui->treeView->setModel(fileModel);
    ui->treeView->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    connect(ui->treeView, &QTreeView::doubleClicked, this, [=] (const QModelIndex &index) {
        QString file = fileModel->filePath(index);
        if (file.isEmpty()) return;

        auto btnGrpPlaylistDbClickIndex = main->getSettings()->valueObj(this, "btnGrpPlaylistDbClickIndex").toInt();
        switch (btnGrpPlaylistDbClickIndex) {
        case 0: ui->actionPlay->trigger(); break;
        case 1: ui->actionPlayOuter->trigger(); break;
        case 2: ui->actionOpen->trigger(); break;
        }
    });

    ui->actionReflesh->trigger();
}

DockPlaylist::~DockPlaylist()
{
    delete fileModel;
    delete ui;
}

void DockPlaylist::updatePlaylist(const QString &selectFile)
{
    ui->actionReflesh->trigger();
    if (!selectFile.isEmpty())
        ui->treeView->setCurrentIndex(fileModel->index(QDir(selectFile).path(),0));
}

auto DockPlaylist::getSelectedFile() -> QString
{
    QString result;
    QModelIndexList indexes = ui->treeView->selectionModel()->selectedRows();
    if (indexes.count() == 0) return result;
    QString file = fileModel->filePath(indexes.at(0));
    QFileInfo info(file);
    if (info.suffix() != "m3u8") return result;
    return file;
}

void DockPlaylist::on_treeView_customContextMenuRequested(const QPoint &pos)
{
    Menu menu(this);
    bool isSelected = (!getSelectedFile().isEmpty());

    foreach (auto action, needSelectActions)
        action->setEnabled(isSelected);

    menu.addAction(ui->actionPlay);
    menu.addAction(ui->actionOpen);
    menu.addAction(ui->actionPlayOuter);
    menu.addSeparator();
    menu.addAction(ui->actionRename);
    menu.addAction(ui->actionDelete);
    menu.addSeparator();
    menu.addAction(ui->actionReflesh);
    menu.addAction(ui->actionOpenFolder);
    menu.addSeparator();
    menu.addAction(ui->actionSetting);

    menu.exec(ui->treeView->viewport()->mapToGlobal(pos));
}
