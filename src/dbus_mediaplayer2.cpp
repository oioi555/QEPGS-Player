/**
 * MPRIS D-Bus Interface 2.2
 * https://specifications.freedesktop.org/mpris-spec/latest/index.html
 *
 * Interface MediaPlayer2
 * https://specifications.freedesktop.org/mpris-spec/latest/Media_Player.html
*/

#include "dbus_mediaplayer2.h"
#include "qapplication.h"
#include "widget_player.h"

DBusMediaPlayer2::DBusMediaPlayer2(WidgetPlayer *player, QObject *parent)
    : QDBusAbstractAdaptor(parent),
    main(static_cast<MainWindow*>(parent)),
    widgetPlayer(player)
{
    /**
     * NOTE: KDE Media Playerで、サービスを再構築するとプレイヤーが消えてしまい
     * 再利用できなくなるのでメディアなし状態を維持する
     */
    connect(main, &MainWindow::mprisNoPlayer, this, [=] () {
        widgetPlayer = nullptr;
        main->sendMprisPropertiesChanged(this, getDefaultProperties());
    });
    connect(widgetPlayer, &WidgetPlayer::mprisCurrentItemChanged, this, [=] () {
        main->sendMprisPropertiesChanged(this, getDefaultProperties());
    });
}

// Methods
void DBusMediaPlayer2::Raise()
{
    main->showMaximized();
}

void DBusMediaPlayer2::Quit()
{
    if (widgetPlayer == nullptr) return;
    widgetPlayer->stop();
}

// Properties
bool DBusMediaPlayer2::CanQuit() const
{
    if (widgetPlayer == nullptr) return false;
    return true;
}

bool DBusMediaPlayer2::Fullscreen() const
{
    if (widgetPlayer == nullptr) return false;
    return widgetPlayer->isFullScreen();
}

void DBusMediaPlayer2::setFullscreen(bool isFullscreen) const
{
    if (widgetPlayer == nullptr) return;
    widgetPlayer->setFullscreen(isFullscreen);
}

bool DBusMediaPlayer2::CanSetFullscreen() const
{
    if (widgetPlayer == nullptr) return false;
    return true;
}

bool DBusMediaPlayer2::CanRaise() const
{
    return true;
}

bool DBusMediaPlayer2::HasTrackList() const
{
    return false;
}

QString DBusMediaPlayer2::Identity() const
{
    return qApp->applicationName();
}

QString DBusMediaPlayer2::DesktopEntry() const
{
    return qApp->applicationName();
}

QStringList DBusMediaPlayer2::SupportedUriSchemes() const
{
    return QStringList();
}

QStringList DBusMediaPlayer2::SupportedMimeTypes() const
{
    return QStringList();
}

// Helper
QVariantMap DBusMediaPlayer2::getDefaultProperties()
{
    QVariantMap properties;
    properties["CanQuit"] = CanQuit();
    properties["Fullscreen"] = Fullscreen();
    properties["CanSetFullscreen"] = CanSetFullscreen();
    properties["CanRaise"] = CanRaise();

    return properties;
}

