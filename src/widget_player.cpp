#include "widget_player.h"
#include "ui_widget_player.h"
#include "dialog_screenshot.h"
#include "dock_info.h"
#include "util/playlist.h"
#include "util/tagitem.h"

#include <QDir>
#include <QPainter>
#include <QActionGroup>
#include <QStackedLayout>
#include <QDBusConnection>
#include <QDBusObjectPath>

#define to_uSec(sec) sec * 1000000
#define to_Sec(uSec) uSec / 1000000

WidgetPlayer::WidgetPlayer(QWidget *parent) :
    QWidget(parent)
  , mpv(new MpvWidget(this))
  , skipSec(20)
  , skipBackSec(5)
  , sqlModel(new SqlModel("v_recorded", "id"))
  , sqlModelPlay(new SqlModel("play", "id"))
  , sqlModelTag(new SqlModel("tags", "id"))
  , timerSeekUpdate(new QTimer(this))
  , timerHideControl(new QTimer(this))
  , trackActionGrp(new QActionGroup(this))
  , audioActionGrp(new QActionGroup(this))
  , main(qobject_cast<MainWindow*>(parent))
  , ui(new Ui::WidgetPlayer)
{
    ui->setupUi(this);

    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    // mainwndow
    connect(main, &MainWindow::mainStatusChanged, this, &WidgetPlayer::onMainStatusChanged);
    connect(main, &MainWindow::pinChanged, ui->actionPin, &QAction::setChecked);
    connect(main, &MainWindow::pinChanged, ui->btnPin, &QToolButton::setChecked);

    // setting
    readSetting();  // 設定ダイアログの項目
    ui->btnPlayContinuous->setIconNames("media-playlist-repeat", "media-repeat-none");
    ui->btnMute->setIconNames("audio-volume-muted", "audio-on");
    ui->btnPin->setIconNames("window-pin", "window-unpin");
    ui->btnPin->hide(); // miniScreenのみで表示
    main->setPin(main->getSettings()->valueObj(this, "pin", false).toBool());
    ui->actionSub->setChecked(main->getSettings()->valueObj(this, "sub", false).toBool());

    // shortcut
    this->addActions(this->findChildren<QAction*>());

    // action
    connect(ui->actionPlay, &QAction::triggered, mpv, &MpvWidget::setPlayToggle);
    connect(ui->actionPlayBegin, &QAction::triggered, this, [=] () {
        saveSeek();
        if (!isLive()) main->updateTagTableRecorded(getId());
        if (ui->seekBar->value() > 10)
            mpv->setPlayBegin();
        else
            playlist.setPrevIndex();
    });
    connect(ui->actionStop, &QAction::triggered, this, [=] {
        mpv->setStop();
        if (!main->IsMultiPlayer()) {
            if (main->isMiniScreen()) main->setMiniScreenView(false);
            if (main->isFullScreen()) main->setFullScreenView(false);
        }
        this->parentWidget()->close();
    });
    connect(ui->actionSkip, &QAction::triggered, mpv, &MpvWidget::setSkipNext);
    connect(ui->actionSkipBack, &QAction::triggered, mpv, &MpvWidget::setSkipBack);
    connect(ui->actionPlayContinuous, &QAction::triggered,  mpv, &MpvWidget::setContinuous);
    connect(ui->actionPlayNext, &QAction::triggered, this, [=] {
        if (!isEndFile) {
            saveSeek();
            if (!isLive()) main->updateTagTableRecorded(getId());
        }
        playlist.setNextIndex();
    });
    connect(ui->actionDelete, &QAction::triggered, this, [=] {
        if (this->isLive()) return;
        main->deleteRecorded(QList<int>{getId()});
        this->parentWidget()->close();
    });
    connect(ui->actionSetting, &QAction::triggered, this, [=] {
        main->openSettings(SettingMenu::Player);
    });
    // action info
    actionDockInfo = main->getDockInfoAction();
    ui->btnInfo->setChecked(actionDockInfo->isChecked());

    // volume
    connect(ui->actionMute, &QAction::triggered, mpv, &MpvWidget::setMuteToggle);
    connect(ui->actionVolumeUp, &QAction::triggered, mpv, &MpvWidget::setVolumeUp);
    connect(ui->actionVolumeDown, &QAction::triggered, mpv, &MpvWidget::setVolumeDown);

    // audio track 音声多重放送
    connect(mpv, &MpvWidget::fileLoaded, this, &WidgetPlayer::onFileLoaded);

    // stereo mode ステレオ音声多重放送
    ui->actionAudioStereo->setChecked(true); // default
    audioActionGrp->addAction(ui->actionAudioStereo);
    audioActionGrp->addAction(ui->actionAudioLeft);
    audioActionGrp->addAction(ui->actionAudioRight);
    connect(ui->actionAudioStereo, &QAction::triggered, mpv, &MpvWidget::setAudioStereo);
    connect(ui->actionAudioLeft, &QAction::triggered, mpv, &MpvWidget::setAudioLeft);
    connect(ui->actionAudioRight, &QAction::triggered, mpv, &MpvWidget::setAudioRight);

    // 再生速度
    connect(ui->actionSpeedUp, &QAction::triggered, mpv, &MpvWidget::setSpeedUp);
    connect(ui->actionSpeedDown, &QAction::triggered, mpv, &MpvWidget::setSpeedDown);
    connect(ui->actionSpeedReset, &QAction::triggered, mpv, &MpvWidget::setSpeedReset);

    // 字幕
    connect(ui->actionSub, &QAction::triggered, mpv, &MpvWidget::setSub);

    // インターレース解除
    connect(ui->actionDeinterlace, &QAction::triggered, mpv, &MpvWidget::setDeinterlace);

    // スクリーンショット
    connect(ui->actionScreenshot, &QAction::triggered, this, [=] () {
        mpv->setProperty("screenshot-template", QDir(screenshotsPath).filePath(tr("%1-%n").arg(getIdStr())));
        mpv->command(QVariantList() << "screenshot" );
        mpv->setPlay(false);
        if (isLive()) return;
        if (main->isFullScreen()) main->setFullScreenView(false);
        DialogScreenShot dlg(getId(), main);
        if (dlg.exec() == QDialog::Accepted)
            mpv->setPlay(true);
    });

    // フルスクリーン
    connect(ui->actionFullScreen, &QAction::triggered, this, [=] (bool checked) {
        main->setFullScreenView(checked);
        ui->btnFullScreen->setChecked(checked);
        if (checked) mpv->setOSDText("フルスクリーン", true);
    });
    // ミニスクリーン
    connect(ui->actionMiniScreen, &QAction::triggered, this, [=] (bool checked) {
        main->setMiniScreenView(checked);
        ui->btnMiniScreen->setChecked(checked);
        if (checked) mpv->setOSDText("ミニスクリーン", true);
    });
    // pin
    connect(ui->actionPin, &QAction::triggered, main, &MainWindow::setPin);

    // controlbar
    connect(ui->btnPlay, &QToolButton::clicked, ui->actionPlay, &QAction::triggered);
    connect(ui->btnStop, &QToolButton::clicked, ui->actionStop, &QAction::triggered);
    connect(ui->btnPlayBegin, &QToolButton::clicked, ui->actionPlayBegin, &QAction::triggered);
    connect(ui->btnSkipBack, &QToolButton::clicked, ui->actionSkipBack, &QAction::triggered);
    connect(ui->btnSkip, &QToolButton::clicked, ui->actionSkip, &QAction::triggered);
    connect(ui->btnPlayNext, &QToolButton::clicked, ui->actionPlayNext, &QAction::triggered);
    connect(ui->btnPlayContinuous, &QToolButton::clicked, mpv, &MpvWidget::setContinuous);
    connect(ui->btnMute, &QToolButton::clicked, ui->actionMute, &QAction::triggered);
    connect(ui->volumeSlider, &CustomSlider::valueChanged, mpv, &MpvWidget::setVolume);
    connect(ui->seekBar, &CustomSlider::valueChanged, mpv, &MpvWidget::setSeek);
    connect(ui->spinSpeed, QOverload<double>::of(&QDoubleSpinBox::valueChanged), mpv, &MpvWidget::setSpeed);
    connect(ui->btnInfo, &QToolButton::clicked, actionDockInfo, &QAction::triggered);
    connect(ui->btnScreenShot, &QToolButton::clicked, ui->actionScreenshot, &QAction::triggered);
    connect(ui->btnFullScreen, &QToolButton::clicked, ui->actionFullScreen, &QAction::triggered);
    connect(ui->btnMiniScreen, &QToolButton::clicked, ui->actionMiniScreen, &QAction::triggered);
    connect(ui->btnPin, &QToolButton::clicked, ui->actionPin, &QAction::triggered);

    // mpv
    connect(mpv, &MpvWidget::durationChanged, ui->seekBar, &CustomSlider::setRangeMax);
    connect(mpv, &MpvWidget::durationChanged, this, [=] (qlonglong duration) {
        mprisMetadata["mpris:length"] = to_uSec(duration);
        emit mprisLengthChanged();
    });
    connect(mpv, &MpvWidget::positionChanged, ui->seekBar, &CustomSlider::setValueNoSignal);
    connect(mpv, &MpvWidget::positionChanged, this, [=] (qlonglong value){
        QString text = isLive()? "Streaming..." :
            tr("%1 / %2").arg(mpv->getFormatTime(value), mpv->getFormatTime(ui->seekBar->maximum()));
        ui->laTime->setText(text);
        emit mprisSeeked(to_uSec(value));
    });
    connect(mpv, &MpvWidget::mpvContextMenuEvent, this, &WidgetPlayer::onMpvContextMenuEvent);
    connect(mpv, &MpvWidget::clicked, ui->actionPlay, &QAction::trigger);
    connect(mpv, &MpvWidget::doubleClick, this, [=] {
        if (main->isFullScreen()) main->setFullScreenView(false);
        else if (main->isMiniScreen()) main->setMiniScreenView(false);
        else main->getSimpleViewAction()->trigger();
    });
    connect(mpv, &MpvWidget::endfileEvent, this, [=] {
        isEndFile = true;
        if (ui->actionPlayContinuous->isChecked() && !isLive() && !playlist.isSelectionEnd()) {
            ui->actionPlayNext->trigger();
            return;
        }
        ui->actionStop->trigger();
    });
    connect(mpv, &MpvWidget::playngChanged, this, [=] (bool isPlay) {
        static const QString playIcon = "media-playback-start";
        static const QString pauseIcon = "media-playback-pause";

        ui->btnPlay->setIcon(main->getIcon(isPlay? pauseIcon : playIcon));
        ui->actionPlay->setIcon(main->getIcon(isPlay? pauseIcon : playIcon));
        main->setWindowIcon(isPlay? playIcon : pauseIcon, this);
        main->updateInhibitSleep();
        emit mprisPlayngChanged();
        main->updateTagTableRecorded(getId());
    });

    connect(mpv, &MpvWidget::muteChanged, ui->btnMute, &ToolButtonCheckable::setChecked);
    connect(mpv, &MpvWidget::muteChanged, ui->actionMute, &QAction::setChecked);
    connect(mpv, &MpvWidget::volumeChanged, ui->volumeSlider, &CustomSlider::setValueNoSignal);
    connect(mpv, &MpvWidget::volumeChanged, this, [=] (int /*volume*/) {
        emit mprisVolumeChanged();
    });
    connect(mpv, &MpvWidget::speedChanged, ui->spinSpeed, &CustomSpinBox::setValueNoSignal);
    connect(mpv, &MpvWidget::continuousChanged, this, [=] (bool checked) {
        ui->btnPlayContinuous->setChecked(checked);
        ui->actionPlayContinuous->setChecked(checked);
        emit mprisLoopStatusChanged();
    });

    connect(mpv, &MpvWidget::deinterlaceChanged, ui->actionDeinterlace, &QAction::setChecked);
    // 前回値の復元
    mpv->setMute(main->getSettings()->valueObj(this, "mute" , false).toBool());
    ui->volumeSlider->setValue(main->getSettings()->valueObj(this, "volume", 100).toInt());
    ui->spinSpeed->setValue(main->getSettings()->valueObj(this, "speed", 1.0).toDouble());
    mpv->setContinuous(main->getSettings()->valueObj(this, "playContinuous", true).toBool());
    mpv->setDeinterlace(main->getSettings()->valueObj(this, "deinterlace", false).toBool());

    // プレイリスト
    connect(&playlist, &Playlist::selectChanged, this, [=] (int index) {
        if (index != -1) {
            openPlayItem(playlist.getItems().at(index));
            return;
        }
        ui->actionStop->trigger();
    });

    // seek save timer
    timerSeekUpdate->setInterval(5000);
    timerSeekUpdate->setTimerType(Qt::CoarseTimer);
    connect(timerSeekUpdate, &QTimer::timeout, this, [=] {
        if (!(main->isActiveWindow(this) && mpv->getPlay())) return;
        if (!isLive()) {
            saveSeek();
            static bool IsTagUpdateOver = false;
            static bool IsTagUpdateUnder = false;
            int percent = (ui->seekBar->value() *100) / ui->seekBar->maximum();
            if (percentLimit < percent && !IsTagUpdateOver) {
                IsTagUpdateOver = true;
                IsTagUpdateUnder = false;
                main->updateTagTableRecorded(getId());
            }
            if (percentLimit > percent && !IsTagUpdateUnder) {
                IsTagUpdateUnder = true;
                IsTagUpdateOver = false;
                main->updateTagTableRecorded(getId());
            }
        }
        emit playerStatusChanged(PlayerStatus::PlayerSeek, getId());
    });

    // hide controlbar timer
    timerHideControl->setInterval(3000);
    timerHideControl->setTimerType(Qt::CoarseTimer);
    timerHideControl->setSingleShot(true);
    timerHideControl->start();
    connect(timerHideControl, &QTimer::timeout, this, [=] {
        timerHideControl->stop();
        if (!mpv->getPlay()) return;

        if (autoFullScreen && !main->isFullScreen() && !main->isMiniScreen()) {
            main->setFullScreenView(true);
            mpv->setOSDText("自動フルスクリーン", true);
        }
        if (autoMiniScreen && !main->isMiniScreen() && !main->isFullScreen()) {
            main->setMiniScreenView(true);
            mpv->setOSDText("自動ミニスクリーン", true);
        }
        setCtlBarVisible(false);
        if (main->isMiniScreen()) main->setMiniScreenFrameless(true);
    });
    connect(mpv, &MpvWidget::mouseMove, this, [=] {
        timerHideControlReStart();
        setCtlBarVisible(true);
        if (main->isMiniScreen()) main->setMiniScreenFrameless(false);
    });

    // MpvWidget
    auto *hb = new QHBoxLayout();
    hb->addWidget(mpv);
    hb->setSpacing(0);
    hb->setContentsMargins(0,0,0,0);
    ui->fmMpv->setLayout(hb);

    // コントロールバーの簡易オーバーレイ化でレイアウトから消す
    ui->layoutMain->removeWidget(ui->fmControl);

    // アニメーションの初期化（サイズは、resizeで指定）
    animeCtlBarHide = new QPropertyAnimation(ui->fmControl, "geometry");
    animeCtlBarHide->setDuration(300);
    animeCtlBarShow = new QPropertyAnimation(ui->fmControl, "geometry");
    animeCtlBarShow->setDuration(100);

    // ショートカットが誤動作するので、都度フォーカスをセット
    connect(animeCtlBarHide, &QPropertyAnimation::finished, this, &WidgetPlayer::setFocusDefault);
    connect(animeCtlBarShow, &QPropertyAnimation::finished, this, &WidgetPlayer::setFocusDefault);
    connect(mpv, &MpvWidget::propertyChanged, this, &WidgetPlayer::setFocusDefault);

    // コントロールバーホバーで自動非表示抑制
    connect(ui->fmControl, &CustomFrame::hoverIn, this, [=] () {
        timerHideControl->stop();
    });
    connect(ui->fmControl, &CustomFrame::hoverOut, this, [=] () {
        timerHideControl->start();
    });

    // プレイリストなしで非表示にするコントロール
    playlistActions << ui->actionPlayContinuous << ui->actionPlayBegin << ui->actionPlayNext;
    liveActions << playlistActions << ui->actionDelete;
    playlistButtons << ui->btnPlayContinuous  << ui->btnPlayBegin << ui->btnPlayNext;

    percentLimit = sqlModel->getScholar("select get_var('Threshold')").toInt();
}

WidgetPlayer::~WidgetPlayer()
{
    mpv->setStop();
    main->getSettings()->setValueObj(this, "volume" , ui->volumeSlider->value());
    main->getSettings()->setValueObj(this, "mute" , mpv->getMute());
    main->getSettings()->setValueObj(this, "speed" , ui->spinSpeed->value());
    main->getSettings()->setValueObj(this, "playContinuous" , ui->actionPlayContinuous->isChecked());
    main->getSettings()->setValueObj(this, "pin" , ui->actionPin->isChecked());
    main->getSettings()->setValueObj(this, "deinterlace", ui->actionDeinterlace->isChecked());
    main->getSettings()->setValueObj(this, "sub", ui->actionSub->isChecked());
    saveSeek();

    emit playerStatusChanged(PlayerStatus::PlayerClose, getId());

    foreach (auto action, trackActionGrp->actions())
        trackActionGrp->removeAction(action);
    delete trackActionGrp;
    foreach (auto action, audioActionGrp->actions())
        audioActionGrp->removeAction(action);
    delete audioActionGrp;
    delete timerSeekUpdate;
    delete timerHideControl;
    delete animeCtlBarHide;
    delete animeCtlBarShow;
    delete mpv;
    delete sqlModel;
    delete sqlModelPlay;
    delete sqlModelTag;
    delete ui;
}

void WidgetPlayer::readSetting()
{
    Settings *settings = main->getSettings();
    settings->beginGroupObj(this);
    autoHideControl = settings->value("chkAutoHideControlBar").toBool();
    autoFullScreen = settings->value("chkFullScreen").toBool();
    autoMiniScreen = settings->value("chkMiniScreen").toBool();
    skipSec = settings->value("skipSec").toInt();
    skipBackSec = settings->value("skipBackSec").toInt();
    hwDec = settings->value("HwDec").toString();
    isMultiPlayer = settings->value("chkMultiPlayer").toBool();
    screenshotsPath = settings->value("editScreenshotsPath").toString();
    int spinSeekWheelStepSec = settings->value("spinSeekWheelStepSec").toInt();
    hwDecAutoOff = settings->value("chkHwDecAutoOff").toBool();
    settings->endGroup();

    // set mpv
    mpv->setSkipSec(skipSec, skipBackSec);
    mpv->setSeekWheelStepSec(spinSeekWheelStepSec);
    setHwDec();
}

// 自動ハードウェアデコードOFF
void WidgetPlayer::setHwDec()
{
    QString hwDec = this->hwDec;
    if (hwDecAutoOff && (!playItem.channelId.isEmpty() || playItem.recordedId != -1))
        hwDec = playItem.isHwDecAutoOff()? "no" : this->hwDec;
    mpv->setHwDec(hwDec);
}

// 外部からソースを指定して再生
void WidgetPlayer::openPlayItem(const PlayItem &playItem)
{
    isEndFile = false;
    if (playItem.url.isEmpty()) {
        mpv->setStatusText("プレイリストに情報がありません...");
        return;
    }
    this->playItem = playItem;

    // コントロール非表示
    foreach (auto action, liveActions) action->setVisible(!isLive());
    foreach (auto action, playlistActions) action->setVisible(playlist.hasItem());
    foreach (auto button, playlistButtons) button->setVisible(playlist.hasItem());
    if (isLive()) ui->laTime->setText("Streaming...");

    // 自動ハードウェアデコードOFF
    setHwDec();

    // MPVメディアオープン
    mpv->openPlayMedia(playItem.url, getTitle());
    timerSeekUpdate->start();

    // タブ名、ウィンドウ名変更
    auto tabTitle = getTabTitle();
    setWindowTitle(tabTitle);
    main->setWindowTitle(tr("%1 < %2 >").arg(qApp->applicationName(), tabTitle));

    // dockinfo
    setDockInfo();

    // mpris Metadata
    mprisMetadata = QVariantMap();
    mprisMetadata["mpris:trackid"] = QVariant::fromValue<QDBusObjectPath>(QDBusObjectPath(trackId()));
    mprisMetadata["xesam:title"] = getTitle();
    if (!isLive()) {
        if (playlist.isTag()) {
            int tagId = playlist.getTagId();
            TagItem tagItem(sqlModelTag->getRecord(tagId));
            ThumbnailTag imgTag(main, tagId);
            mprisMetadata["xesam:album"] = tagItem.tag;
            mprisMetadata["xesam:genre"] = tagItem.genre;
            mprisMetadata["mpris:artUrl"] = imgTag.getImageFilePath(true);
        } else {
            mprisMetadata["xesam:album"] = sqlModel->getValue("チャンネル", getId());
            mprisMetadata["xesam:genre"] = sqlModel->getValue("ジャンル", getId());
            mprisMetadata["mpris:artUrl"] = main->getApi()->getThumbnailPath(getIdStr(), true);
        }
    }
    emit mprisCurrentItemChanged();
}

void WidgetPlayer::setDockInfo()
{
    if (isLive()) {
        main->setDockInfoId(InfoType::Live, playItem.channelId);   
    } else {
        auto dockInfo = main->getDockInfo();
        auto infoType = dockInfo->getInfoType();
        if (playlist.isTag()) {
            auto dockTagId = dockInfo->getTagId();
            if (infoType != InfoType::Tag || dockTagId != playlist.getTagId())
                main->setDockInfoId(InfoType::Tag, playlist.getTagIdStr());
        } else if (playlist.hasItem()){
            if (infoType != InfoType::Playlist)
                main->setDockInfoId(InfoType::Playlist, "");
        } else {
            main->setDockInfoId(InfoType::Recorded, getIdStr());
        }
        main->setDcokInfoPlayingId(getId());
    }
}

void WidgetPlayer::onFileLoaded(QList<int> audioIds)
{
    // トラックメニュー作成
    foreach (auto action, trackActionGrp->actions())
        trackActionGrp->removeAction(action);

    foreach (auto id, audioIds) {
        QString name;
        if (id == 1) name = tr("%1 (主音声)").arg(id);
        else if (id == 2) name = tr("%1 (副音声)").arg(id);
        else name = QString::number(id);

        auto action = new QAction(name);
        action->setData(id);
        action->setCheckable(true);
        action->setChecked((id == 1));
        connect(action, &QAction::triggered, this, [=] {
            mpv->setProperty("aid", action->data().toInt());
        });
        trackActionGrp->addAction(action);
    }

    // 字幕
    mpv->setSub(ui->actionSub->isChecked());
    ui->actionSub->setEnabled(mpv->isSubEnabled());

    // シークの復元
    if (!isLive()) {
        QSqlRecord rs = sqlModelPlay->getRecord(getId());
        if (!rs.isEmpty()) {
            int seek = rs.value("seek").toInt();
            int percent = rs.value("percent").toInt();
            // 最後まで見たら最初から再生
            if (percent > 98) seek = 0;
            mpv->setSeek(seek);
        }
    }
}

// DBに再生履歴保存
void WidgetPlayer::saveSeek()
{
    if (isLive()) return;
    if (!mpv->getPlay()) return;    // マウスホイールのシークは除外

    // percent
    if (ui->seekBar->maximum() == 0) return;
    int percent = (ui->seekBar->value() *100) / ui->seekBar->maximum() ;
    sqlModelPlay->setValueClear();
    sqlModelPlay->setValue("seek", ui->seekBar->value());
    sqlModelPlay->setValue("percent", percent);
    sqlModelPlay->saveValues(getId());
}

void WidgetPlayer::setCtlBarVisible(bool visible)
{
    if (visible) {
        animeCtlBarShow->start();
        emit playerStatusChanged(PlayerStatus::ControlBarShow, -1);
        return;
    }

    if (autoHideControl) {
        if (main->isMiniScreen()) {
            if (main->isMiniScreenFrameless()) animeCtlBarHide->start();
        } else {
            animeCtlBarHide->start();
        }
        emit playerStatusChanged(PlayerStatus::ControlBarHide, -1);
    } else {
        animeCtlBarShow->start();
    }
}

// NOTE:コントロールバーのフォーカス位置でショートカットが効かなくなるので
// 都度、デフォルト位置に戻す
// そのまま、setFocusだけ発行すると、WidgetPlayer自体がActiveになるので
// Activeかどうか確認してから発行する
void WidgetPlayer::setFocusDefault()
{
    if (main->isActiveWindow(this)) ui->btnPlay->setFocus();
}

void WidgetPlayer::resizeEvent(QResizeEvent *event)
{
    // コントロールバーの簡易オーバーレイ化アニメーション
    static const int ctlHeight = 52;
    QSize sizeCtl(event->size().width(), ctlHeight);
    QPoint ptShow(0, event->size().height() - ctlHeight);
    QPoint ptHide(0, event->size().height());

    // 初期配置
    ui->fmControl->setGeometry(QRect(ptHide, sizeCtl));
    if (event->size().width() == ui->fmControl->size().width()) {
        // 初期配置済なら、アニメーション
        animeCtlBarHide->setStartValue(QRect(ptShow, sizeCtl));
        animeCtlBarHide->setEndValue(QRect(ptHide, sizeCtl));
        animeCtlBarShow->setEndValue(QRect(ptShow, sizeCtl));

        // 幅が狭い場合の対応
        QList<QWidget*> items = {
            ui->btnPlayBegin, ui->btnPlayContinuous, ui->btnScreenShot, // lv.1
            ui->spinSpeed,  // lv.2 >=3
            ui->laTime      // lv.3 >=4
        };
        for (int i = 0; i < items.count(); i++) {
            if (sizeCtl.width() < 580) {
                items[i]->hide();
            } else if (sizeCtl.width() < 680) {
                if (i >= 3) items[i]->hide();
            } else if (sizeCtl.width() < 780) {
                if (i >= 4) items[i]->hide();
            } else {
                items[i]->show();
            }
        }
        foreach (auto button, playlistButtons) button->setVisible(playlist.hasItem());
    }
}

void WidgetPlayer::onMainStatusChanged(MainStatus status, const QVariant &event)
{
    switch (status){
    case MainStatus::Database:
        readSetting(); return;
    case MainStatus::VideoDeleted: {
        if (isLive()) return;
        if (playlist.hasItem()) {
            playlist.update();
        } else {
            if (sqlModel->getValue("id", getId()).isNull())
                ui->actionStop->trigger();
        }
        return;
    }
    case MainStatus::TagTabel: {
        return;
    }
    case MainStatus::FullScreen: {
        ui->actionFullScreen->setChecked(event.toBool());
        ui->btnFullScreen->setChecked(event.toBool());
        if (event.toBool()) {
            ui->actionMiniScreen->setChecked(false);
            ui->btnMiniScreen->setChecked(false);
        }
        return;
    }
    case MainStatus::MiniScreen: {
        ui->actionMiniScreen->setChecked(event.toBool());
        ui->btnMiniScreen->setChecked(event.toBool());
        if (event.toBool()) {
            ui->actionFullScreen->setChecked(false);
            ui->btnFullScreen->setChecked(false);
            ui->btnPin->show();
        } else {
            ui->btnPin->hide();
        }
        return;
    }
    case MainStatus::DockInfo: {
        ui->btnInfo->setChecked(event.toBool());
        if (event.toBool()) setDockInfo();
        return;
    }
    case MainStatus::Resize: [[fallthrough]];
    case MainStatus::Move: {
        if (!main->isMiniScreenFrameless())
            timerHideControlReStart();
        return;
    }}
}

void WidgetPlayer::onMpvContextMenuEvent(QContextMenuEvent *event)
{
    timerHideControl->stop();

    // サブメニュー
    Menu menuVolume;
    menuVolume.setTitle("音量");
    menuVolume.setIcon(main->getIcon("audio-on"));
    menuVolume.addActions({ui->actionMute, ui->actionVolumeUp, ui->actionVolumeDown});
    Menu menuSpeed;
    menuSpeed.setTitle("再生速度");
    menuSpeed.setIcon(main->getIcon("minuet-scales"));
    menuSpeed.addActions({ui->actionSpeedUp, ui->actionSpeedDown, ui->actionSpeedReset});
    Menu menuTrack;
    menuTrack.setTitle("トラック（音声多重）");
    menuTrack.setIcon(main->getIcon("media-album-track"));
    menuTrack.addActions(trackActionGrp->actions());
    Menu menuAudio;
    menuAudio.setTitle("ステレオ（音声多重）");
    menuAudio.setIcon(main->getIcon("distribute-horizontal-center"));
    menuAudio.addActions(audioActionGrp->actions());

    Menu menu(this);
    menu.addAction(ui->actionPlay);
    menu.addAction(ui->actionStop);
    menu.addAction(ui->actionPlayBegin);
    menu.addSeparator();
    menu.addAction(ui->actionSkip);
    menu.addAction(ui->actionSkipBack);
    menu.addSeparator();
    menu.addAction(ui->actionPlayNext);
    menu.addAction(ui->actionPlayContinuous);
    menu.addSeparator();
    menu.addAction(ui->actionDelete);
    menu.addSeparator();
    menu.addMenu(&menuVolume);
    menu.addMenu(&menuTrack);
    menu.addMenu(&menuAudio);
    menu.addMenu(&menuSpeed);
    menu.addSeparator();
    menu.addAction(ui->actionSub);
    menu.addSeparator();
    menu.addAction(ui->actionDeinterlace);
    menu.addAction(ui->actionScreenshot);
    menu.addSeparator();
    menu.addAction(main->getDockNaviAction());
    menu.addAction(actionDockInfo);
    menu.addSeparator();
    menu.addAction(main->getSimpleViewAction());
    menu.addAction(ui->actionMiniScreen);
    menu.addAction(ui->actionFullScreen);
    if (ui->actionMiniScreen->isChecked())
        menu.addAction(ui->actionPin);
    menu.addSeparator();
    menu.addAction(ui->actionSetting);

    menu.exec(this->mapToGlobal(event->pos()));
}

/*---------------------------------------------------
 * MPRIS D-Bus Interface
 * https://specifications.freedesktop.org/mpris-spec/latest/index.html
----------------------------------------------------*/

void WidgetPlayer::setFullscreen(bool isFullscreen)
{
    if (isFullscreen != ui->actionFullScreen->isChecked())
        ui->actionFullScreen->trigger();
}

void WidgetPlayer::next()
{
    ui->actionPlayNext->trigger();
}

void WidgetPlayer::previous()
{
    ui->actionPlayBegin->trigger();
}

void WidgetPlayer::pause()
{
    if (mpv->getPlay()) mpv->setPlay(false);
}

void WidgetPlayer::playPause()
{
    mpv->setPlay(!mpv->getPlay());
}

void WidgetPlayer::stop()
{
    ui->actionStop->trigger();
}

void WidgetPlayer::play()
{
    if (!mpv->getPlay()) mpv->setPlay(true);
}

void WidgetPlayer::seek(qlonglong Offset)
{
    auto seekPosition = position() + Offset;
    mpv->setSeek(to_Sec(seekPosition));
}

void WidgetPlayer::setPosition(const QDBusObjectPath &TrackId, qlonglong Position)
{
    if (TrackId.path() == trackId())
        mpv->setSeek(to_Sec(Position));
}

void WidgetPlayer::openUri(QString /*uri*/)
{
    ;// 使いようがない
}

QString WidgetPlayer::playbackStatus()
{
    return mpv->getPlay()? "Playing" : "Paused";
}

QString WidgetPlayer::loopStatus()
{
    if (playItem.live) return "None";
    return ui->actionPlayContinuous->isChecked()? "Playlist" : "None";
}

void WidgetPlayer::setLoopStatus(const QString& /*loopStatus*/)
{
    // loopStatusが、Track以外投げてこないのでトグルにする
    if (!playItem.live)
        mpv->setContinuous(!ui->actionPlayContinuous->isChecked());
}

double WidgetPlayer::rate()
{
    return ui->spinSpeed->value();
}

void WidgetPlayer::setRate(double rate)
{
    mpv->setSpeed(rate);
}

QString WidgetPlayer::trackId()
{
    // NOTE: パスに「-」が使えない
    return QString("/org/QEPGS_Player/QEPGS_Player/Track/%1")
            .arg((playItem.live)? playItem.channelId : getIdStr());
}

QVariantMap WidgetPlayer::metaData()
{
    return mprisMetadata;
}

double WidgetPlayer::volume()
{
    return static_cast<double>(mpv->getVolume() / 100.0);
}

void WidgetPlayer::setVolume(double volume)
{
    ui->volumeSlider->setValue(static_cast<int>(volume * 100));
}

qlonglong WidgetPlayer::position()
{
    return to_uSec(mpv->getSeek());
}

bool WidgetPlayer::canGoNext()
{
    return playlist.canGoNext();
}

bool WidgetPlayer::canGoPrevious()
{
    // NOTE: 最初から再生とミックス
    return (playlist.canGoPrev() || (to_Sec(position()) > 10));
}
