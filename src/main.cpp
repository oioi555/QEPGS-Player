#include "mainwindow.h"

#include <QApplication>
#include <QFile>
#include <QDebug>
#include <QStyleFactory>
#include <QCommandLineParser>
#include "util/settings.h"

void setStyleSheet(const QStringList &cssNames) {
    QString cssText;
    foreach (auto cssName, cssNames) {
        QFile file(QString(":/css/%1.css").arg(cssName));
        if (file.open(QFile::ReadOnly)) {
            cssText += QLatin1String(file.readAll());
            file.close();
        }
    }
    qApp->setStyleSheet(cssText);
}

/***
 * NOTE: v1 v2 など複数の接続先設定でテスト運用するために
 * profileオプションを追加した
 * (appName-profile)をApplicationNameにすることで
 * .config の設定と、.local/shaer/ のアプリケーションデータを
 * まとめて個別に管理できる
 * */
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    setlocale(LC_NUMERIC, "C"); // for mpv

    QString appName = "QEPGS-Player";
    app.setApplicationName(appName);
    app.setApplicationVersion(VERSION);

    QCommandLineParser parser;
    parser.setApplicationDescription("EPGStation専用視聴アプリケーション");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption optionP({"p", "profile"},
        "設定やローカルアプリケーションデータをプロファイルごとに個別に管理する", "Profile");
    parser.addOption(optionP);
    parser.process(QCoreApplication::arguments());

    if (parser.isSet(optionP))
        app.setApplicationName(appName + "-" + parser.value(optionP));

    // icon load
    Settings setting;
    bool chkDarkIcon = setting.value("chkDarkIcon", false).toBool();
    QIcon::setThemeName(chkDarkIcon? "breeze-dark" : "breeze");

    // css style
    setStyleSheet({"common", chkDarkIcon? "dark" : "light"});

    MainWindow main;
    main.show();
    return app.exec();
}
