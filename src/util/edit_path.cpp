#include "edit_path.h"
#include <QApplication>
#include <QAction>
#include <QFileDialog>

EditPath::EditPath(QWidget *parent) : QLineEdit(parent)
{
    QAction *action = this->addAction(QIcon::fromTheme("folder"), QLineEdit::TrailingPosition);
    connect(action, &QAction::triggered, this, &EditPath::on_action_clicked);
}

void EditPath::on_action_clicked()
{
    QFileDialog::Options options = QFileDialog::DontResolveSymlinks | QFileDialog::ShowDirsOnly
                                    | QFileDialog::DontUseNativeDialog;
    QString dirName = QFileDialog::getExistingDirectory(this, tr("フォルダ選択"), this->text(), options);
    if (!dirName.isEmpty())
        this->setText(dirName);
}
