#include "combo_sort.h"

ComboSort::ComboSort(QWidget *parent) : QComboBox(parent)
{
    QIcon iconSortAsc = QIcon::fromTheme("view-sort-ascending");
    QIcon iconSortDesc = QIcon::fromTheme("view-sort-descending");

    QList<SortElement> elements = {
        { "新着順", false, "tag_last_update DESC" },
        { "古い順", true, "tag_last_update" },
        { "名前順", true, "tag" },
        { "視聴済", true, "tag_watched_count" },
        { "視聴済", false, "tag_watched_count DESC" },
        { "録画数", true, "tag_video_count" },
        { "録画数", false, "tag_video_count DESC" }
    };
    for (int i = 0; i < elements.count(); i++) {
        addItem(elements[i].name, elements[i].data);
        setItemIcon(i, elements[i].oderAsc? iconSortAsc : iconSortDesc);
    }
    setToolTip("並び順");
}

