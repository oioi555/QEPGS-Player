#include "combo_limit.h"

ComboLimit::ComboLimit(const QList<int> &itemDatas, QWidget *parent) : QComboBox(parent)
{
    setToolTip("表示件数");
    QIcon iconFilter = QIcon::fromTheme("view-filter");
    QIcon iconNotFilter = QIcon::fromTheme("gnumeric-autofilter-delete");
    for (int i = 0; i < itemDatas.count(); i++) {
        if (i == 0) {
            addItem(iconNotFilter, "全て", QVariant(itemDatas[i]));
        } else {
            addItem(iconFilter, tr("%1件").arg(itemDatas[i]), QVariant(itemDatas[i]));
        }
    }
}
