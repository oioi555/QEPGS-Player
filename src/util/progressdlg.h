#ifndef PROGRESSDLG_H
#define PROGRESSDLG_H

#include <QProgressDialog>

class ProgressDlg : public QProgressDialog
{
public:
    ProgressDlg(QWidget *parent, const QString &title, const QString &text, int max);
    void setValueInc();
    bool setValueIncWasCanceled();
private:
    QString labelText;
};

#endif // PROGRESSDLG_H
