#include "base_mainwindow.h"

#include <QApplication>
#include <QMdiArea>
#include <QAction>
#include <QIcon>
#include <QMdiSubWindow>
#include <QMenu>
#include <QTabBar>
#include <QToolBar>
#include <QStandardPaths>
#include <QMessageBox>
#include <QStyle>
#include <QActionGroup>

BaseMainWindow::BaseMainWindow(QWidget *parent) : QMainWindow(parent)
    , settings(new Settings())
    , appDataPath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation))
    , toolBarStyleGrp(new QActionGroup(this))
{
    // action sub window menu custom
    actionCloseOther = new QAction(getIcon("view-close"), "他のウィンドウをすべて閉じる");
    connect(actionCloseOther,&QAction::triggered, this, [=] { closeSubWindowOther(); });
    actionCloseAll = new QAction(getIcon("window-close"), "すべてのウィンドウを閉じる");
    connect(actionCloseAll, &QAction::triggered, this, [=] { mdiArea->closeAllSubWindows(); });

    // action toolbar syle custom
    actionToolBarIconTextV = new QAction("アイコンと文字（縦）");
    actionToolBarIconTextH = new QAction("アイコンと文字（横）");
    actionToolBarIcon = new QAction("アイコンのみ");
    actionToolBarText = new QAction("文字のみ");

    QList<toolBarStyleItem> styleList = {
        {Qt::ToolButtonTextUnderIcon, actionToolBarIconTextV},
        {Qt::ToolButtonTextBesideIcon, actionToolBarIconTextH},
        {Qt::ToolButtonIconOnly, actionToolBarIcon},
        {Qt::ToolButtonTextOnly, actionToolBarText}
    };
    foreach (auto item, styleList) {
        item.action->setCheckable(true);
        item.action->setData(item.style);
        toolBarStyleGrp->addAction(item.action);
        connect(item.action, &QAction::triggered, this, [=] {
            setToolBarStyle(item.style);
        });
    }
}

BaseMainWindow::~BaseMainWindow()
{
    delete settings;
    delete actionCloseOther;
    delete actionCloseAll;
    delete toolBarStyleGrp;
    delete actionToolBarIconTextV;
    delete actionToolBarIconTextH;
    delete actionToolBarIcon;
    delete actionToolBarText;
}

/*---------------------------------------------------
 * MDI TabMode
----------------------------------------------------*/
void BaseMainWindow::setupMdiAreaTabMode(QMdiArea *mdiArea, bool enable /*=true*/)
{
    this->mdiArea = mdiArea;
    setMdiAreaTabMode(enable);
    mdiArea->setTabsClosable(true);
    mdiArea->setTabsMovable(true);
    mdiArea->setContextMenuPolicy(Qt::CustomContextMenu);
}

void BaseMainWindow::setMdiAreaTabMode(bool enable)
{
    mdiArea->setOption(QMdiArea::DontMaximizeSubWindowOnActivation, true);  // 勝手に違うWindowがActiveになるのを防ぐ
    if (enable) {
        mdiArea->setViewMode(QMdiArea::TabbedView);
        QList<QTabBar*> tabBarList = mdiArea->findChildren<QTabBar*>();
        tabMdiBar = tabBarList.at(0);
        tabMdiBar->setExpanding(false);
        tabMdiBar->setIconSize(QSize(22, 22));
        tabMdiBar->setContextMenuPolicy(Qt::CustomContextMenu);
        // デフォルトだと、右クリックしたタブがActiveにならないので、他のWindowを閉じる処理が実現できない
        connect(tabMdiBar, &QTabBar::customContextMenuRequested, this, [=] (const QPoint &pos) {
            tabMdiBar->setCurrentIndex(tabMdiBar->tabAt(pos));  // 選択TabをActiveにする
            QPoint position(pos.x(), 0);    // 位置の調整
            auto win = mdiArea->activeSubWindow();
            if (win) win->systemMenu()->exec(mdiArea->viewport()->mapToGlobal(position));
        });
    } else {
        mdiArea->setViewMode(QMdiArea::SubWindowView);
    }
}

/*---------------------------------------------------
 * MDI SubWindow
----------------------------------------------------*/
void BaseMainWindow::loadSubWindows(QWidget *widget, const QList<QAction*> &tabMenuActions/* = QList<QAction*>{}*/)
{
    auto window = mdiArea->addSubWindow(widget);
    window->setWindowTitle(widget->windowTitle());
    window->setWindowIcon(widget->windowIcon());
    window->setWindowState(windowState() ^ Qt::WindowMaximized);
    QMenu *menu = window->systemMenu();
    menu->clear();  // デフォルトだと、戻る、最大化などフルスペックになってしまうので消す
    if (tabMenuActions.count()) {
        menu->addActions(tabMenuActions);
        menu->addSeparator();
    }
    menu->addAction(actionCloseOther);
    menu->addAction(actionCloseAll);
    window->showMaximized();
}

auto BaseMainWindow::isActiveWindow(QWidget *widget) -> bool
{
    QMdiSubWindow* active = getActiveWindow();
    if (active)
        if (active->widget() == widget) return true;
    return false;
}

bool BaseMainWindow::isActiveWindow(const QString &className)
{
    QMdiSubWindow* active = getActiveWindow();
    if (active)
        return (getSubWindowClassName(active) == className);
    return false;
}

void BaseMainWindow::closeSubWindowOther(const QString &className /*= ""*/)
{
    auto active = mdiArea->currentSubWindow();
    auto subwindows = className.isEmpty()? mdiArea->subWindowList() : getSubWindowInstanceAll(className);
    foreach (QMdiSubWindow* win, subwindows)
        if (win != active) win->close();
}

void BaseMainWindow::closeSubWindowAll(const QString &className)
{
    foreach (auto win, getSubWindowInstanceAll(className))
        win->close();
}

// ウィジェットのタブアイコン動的変更
void BaseMainWindow::setWindowIcon(const QString &iconName, QWidget *widget)
{
    foreach (auto win, mdiArea->subWindowList(QMdiArea::ActivationHistoryOrder)) {
        if (win->widget() == widget) {
            win->setWindowIcon(getIcon(iconName)); return;
        }
    }
}

// MdiAreaのアクティブサブウィンドウを取得
// QMdiArea::currentSubWindow QMdiArea::activeSubWindow
// では、他のWindowの影響を受けて正確に判定できない
QMdiSubWindow *BaseMainWindow::getActiveWindow()
{
    QMdiSubWindow* result = nullptr;
    QList<QMdiSubWindow*> subwindows = mdiArea->subWindowList(QMdiArea::StackingOrder);
    if (!subwindows.empty())
        result = subwindows.last();
    return result;
}

// QMdiSubWindowのwidgetのクラス名を取得
QString BaseMainWindow::getSubWindowClassName(QMdiSubWindow *subWin)
{
    if (subWin)
        return QString(subWin->widget()->metaObject()->className());
    return QString();
}

// 現在開いているサブウィンドウで、クラス名からInstanceを取得する
// (複数ある場合は、一番最後に開かれた物)
QMdiSubWindow *BaseMainWindow::getSubWindowInstance(const QString &className)
{
    QMdiSubWindow* result = nullptr;
    foreach (QMdiSubWindow* subWin, mdiArea->subWindowList(QMdiArea::StackingOrder)) {
        if (getSubWindowClassName(subWin) == className) return subWin;
    }
    return result;
}

QList<QMdiSubWindow *> BaseMainWindow::getSubWindowInstanceAll(const QString &className)
{
    QList<QMdiSubWindow*> result = QList<QMdiSubWindow*>();
    foreach (QMdiSubWindow* subWin, mdiArea->subWindowList(QMdiArea::StackingOrder)) {
        if (getSubWindowClassName(subWin) == className) result << subWin;
    }
    return result;
}

/*---------------------------------------------------
 * ToolBar
----------------------------------------------------*/
void BaseMainWindow::setupToolBar(QToolBar *toolBar)
{
    this->toolBar = toolBar;

    uint styleint = settings->value("setToolBarStyle", Qt::ToolButtonTextUnderIcon).toUInt();
    auto selectStyle = static_cast<Qt::ToolButtonStyle>(styleint);
    setToolBarStyle(selectStyle);
    foreach (auto action, toolBarStyleGrp->actions()) {
        if (action->data() == selectStyle) {
            action->setChecked(true);
            break;
        }
    }
}

void BaseMainWindow::setToolBarStyle(Qt::ToolButtonStyle style)
{
    if (style == Qt::ToolButtonTextUnderIcon)
        toolBar->setIconSize(QSize(70, 22));
    else
        toolBar->setIconSize(QSize(22, 22));

    toolBar->setToolButtonStyle(style);
    settings->setValue("setToolBarStyle", style);
}

QMenu *BaseMainWindow::createPopupMenu()
{
    QMenu* menu= QMainWindow::createPopupMenu();
    menu->clear();  // デフォルトメニューの無効化
    menu->addSection("ツールバー設定");
    menu->addActions(toolBarStyleGrp->actions());
    return menu;
}

/*---------------------------------------------------
 * screen
----------------------------------------------------*/
// NOTE:タイトルバーの高さを正確に取得する方法がない
// pos()やgeometryFrame()などは正確性を保証されていないので
// 実物から取得して保存する
// lastTitlebarHeightの初期値は、一番確実な、MainWindowのActiveイベントで取得する
int BaseMainWindow::getTitlebarHeight()
{
    int result = geometry().y() - this->pos().y();
    if (result > 0) lastTitlebarHeight = result;
    return result;
}

void BaseMainWindow::saveNormalGeometry()
{
    isPrevMaximized = isMaximized();
    showNormal();
    if (!isPrevMaximized) normalGeometry = geometry();
}

void BaseMainWindow::restoreNormalGeometry()
{
    showNormal();
    setGeometry(normalGeometry);
    if (isPrevMaximized) showMaximized();
}

QRect BaseMainWindow::getRectFrameless()
{
    QRect rect = geometry();
    rect.setY(geometry().y() - lastTitlebarHeight);
    rect.setHeight(geometry().height() + lastTitlebarHeight);
    return rect;
}

QRect BaseMainWindow::getRectNotFrameless()
{
    QRect rect =  geometry();
    rect.setY(geometry().y() + lastTitlebarHeight);
    rect.setHeight(geometry().height() - lastTitlebarHeight);
    return rect;
}

/*---------------------------------------------------
 * Other
----------------------------------------------------*/
bool BaseMainWindow::deleteConfirm(QWidget *parent)
{
    QWidget *widget = parent == nullptr? qobject_cast<QWidget*>(this->window()) : parent;
    QMessageBox msgBox(widget);
    msgBox.setWindowTitle("削除確認");
    msgBox.setText("本当に削除しますか？");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    msgBox.setIcon(QMessageBox::Question);
    return (msgBox.exec() == QMessageBox::Yes);
}




