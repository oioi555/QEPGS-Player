#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDir>
#include <QObject>
#include <QSettings>
#include <QStandardPaths>

class Settings : public QSettings
{
    Q_OBJECT

public:
    explicit Settings() {
        QString appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
        defaultValues = {
            // epgstation
            {"url", "http://localhost:8888"},
            {"chkInnerPlayerEPG", true},
            {"chkStartEPG", false},
            {"chkStartRecorded", true},
            {"chkStartTagWidget", false},

            // database
            {"DB/dbType", "QMYSQL"},
            {"DB/chkDbPortDefault", getDbPortDefault(value("dbType").toString() == "QPSQL")},
            {"DB/host", "localhost"},
            {"DB/dbName", "epgstation"},
            {"DB/user", "epgstation"},
            {"DB/pass", "epgstation"},
            {"DB/editDbBackupPath", appDataPath + "/backup" },

            // style
            {"toolTipOpacity", 90},
            {"chkDarkIcon", false},
            {"style", "Fusion"},

            // Player
            {"WidgetPlayer/chkMultiPlayer", false},
            {"WidgetPlayer/chkAutoHideControlBar", true},
            {"WidgetPlayer/chkFullScreen", false},
            {"WidgetPlayer/chkMiniScreen", false},
            {"WidgetPlayer/skipSec", 15},
            {"WidgetPlayer/skipBackSec", 5},
            {"WidgetPlayer/chkInhibitSleep", true},
            {"WidgetPlayer/chkMPRIS", true},
            {"WidgetPlayer/HwDec", "no"},
            {"WidgetPlayer/chkHwDecAutoOff", true},
            {"WidgetPlayer/spinSeekWheelStepSec", 5},
            {"WidgetPlayer/editScreenshotsPath", appDataPath + "/screenshots"},

            // Recorded
            {"WidgetRecorded/chkInnerPlayerRecorded", true},
            {"WidgetRecorded/editMaxViewHistory", 30},
            {"WidgetRecorded/chkInnerPlayerRecorded", true},
            {"WidgetRecorded/chkViewThumbnail", true},
            {"WidgetRecorded/spinThunmbnailWidth", 120},
            {"WidgetRecorded/editThumbnailPath", appDataPath + "/thumbnail"},
            {"WidgetRecorded/columnsState",
                QVariantList {true, false, true, false, false, true, false, true, true, false, false}
            },

            // DockTag
            {"DockTag/btnGrpTagDbClickIndex", 1},
            {"DockTag/chkDefaultTag", true},
            {"DockTag/chkAutoNewTag", true},
            {"DockTag/listRegForward", QStringList {
                    "^日5",
                    "(テレビ|TV|ミニ|プチプチ・|新)アニメ",
                    "アニメ(\\s|A・|の神様|傑作選|)",
                    "＜木曜劇場＞",
                    "金曜8時のドラマ",
                    "treatment ★",
                    "ドラマ(\\s|・|Paravi|ストリーム|NEXT|\\d+)",
                    ".曜ドラマ(\\d+・|\\d+)",
                    "(|SP|.曜|ミニ|大河|韓国|真夜中)ドラマ",
                    ".*原作",
                    "シンドラ",
                    "日曜劇場",
                    "月曜プレミア8",
                    "土曜プレミアム・",
                    ".*ドラマW"
                }
            },
            {"DockTag/listRegBackward", QStringList {
                    "(▼|▽|★|☆).*(話|頭目)",
                    "第.*(話|夜|幕|回|週)",   // 漢字の場合がある
                    "第\\d+(R|服)",
                    "(#|＃|♯|ep|episode|stage|spage|PHASE|plan|page|file|life|Turn|Rd)(|\\s|\\.|-)\\d+",
                    "(|\\s)\\d+(|\\s)(話|頭目)",
                    "\\(\\d+\\)",
                    "(▼|▽|★|☆).*",
                    "\\s主演",
                    "\\d+$"
                }
            },
            {"DockTag/editThumbnailTagPath", appDataPath + "/thumbnailTag"},

            // DockPlaylist
            {"DockPlaylist/btnGrpPlaylistDbClickIndex", 0},
            {"DockPlaylist/editPlaylistPath", appDataPath + "/playlist"},

            // DockLive
            {"DockLive/btnGrpLiveDefault", 0},

            // SimpleView
            {"SimpleView/chkEnableToolBar", true},
            {"SimpleView/chkEnableDock", true},
            {"SimpleView/chkEnableInfo", true},
            {"SimpleView/chkAutoEPG", true},
            {"SimpleView/chkAutoRecorded", true},
            {"SimpleView/chkAutoPlayer", true},
            {"SimpleView/chkAutoTag", true},

            // about
            {"chkVersionCheck", true},
        };
        makePathKeys << "DB/editDbBackupPath"
                     << "DockPlaylist/editPlaylistPath"
                     << "WidgetRecorded/editThumbnailPath"
                     << "DockTag/editThumbnailTagPath"
                     << "WidgetPlayer/editScreenshotsPath";
    }
    ~Settings() {}
    QVariant value(const QString &key) const {
        QString defalutKey = group().isEmpty()? key : group() + "/" + key;
        return QSettings::value(key, defaultValues[defalutKey]);
    }
    QVariant value(const QString &key, const QVariant &defaultValue) const {
        return QSettings::value(key, defaultValue);
    }

    QVariant valueObj(QObject *object, const QString &key) const {
        return value(tr("%1/%2").arg(object->metaObject()->className(), key));
    }
    QVariant valueObj(QObject *object, const QString &key, const QVariant &defaultValue) const {
        return QSettings::value(tr("%1/%2").arg(object->metaObject()->className(), key), defaultValue);
    }

    void setValueObj(QObject *object, const QString &key, const QVariant &value) {
        setValue(tr("%1/%2").arg(object->metaObject()->className(), key), value);
    }
    void beginGroupObj(QObject *object) {
        beginGroup(object->metaObject()->className());
    }
    QString getDbPortDefault(bool isPgsql) {
        return isPgsql? "5432" : "3306";
    };

    void makePaths() {
        foreach (auto key, makePathKeys) makePath(key);
    }
    void makePath(const QString &key) {
        QDir dir(value(key).toString());
        if (!dir.mkpath(dir.absolutePath())) {
            auto defPath = defaultValues[key].toString();
            if (value(key).toString() != defPath) {
                if (dir.mkpath(defPath)) setValue(key, defPath);
            }
        }
    }
    QHash<QString, QVariant> defaultValues;
    QStringList makePathKeys;
};

#endif // SETTINGS_H
