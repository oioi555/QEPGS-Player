#ifndef RECMODEL_H
#define RECMODEL_H

#include <QMetaObject>
#include <QMetaProperty>
#include <QAbstractListModel>
#include <QDateTime>

struct RecElement {
    Q_GADGET
    Q_PROPERTY(int id MEMBER id)
    Q_PROPERTY(QString name MEMBER name)
    Q_PROPERTY(QString date MEMBER date)
    Q_PROPERTY(int percent MEMBER percent)
    Q_PROPERTY(bool isRecording MEMBER isRecording)
    Q_PROPERTY(bool isEncording MEMBER isEncording)
    Q_PROPERTY(int encId MEMBER encId)
    Q_PROPERTY(int encPercent MEMBER encPercent)
    Q_PROPERTY(QString encMode MEMBER encMode)
    Q_PROPERTY(QDateTime startTime MEMBER startTime)
    Q_PROPERTY(QDateTime endTime MEMBER startTime)
    Q_PROPERTY(bool isPlay MEMBER isPlay)

public:
    int id = -1;
    QString name = "";
    QString date = "";
    int percent = 0;
    bool isRecording = false;
    bool isEncording = false;
    int encId = -1;
    int encPercent = 0;
    QString encMode = "";
    QDateTime startTime;
    QDateTime endTime;
    bool isPlay = false;
};
Q_DECLARE_METATYPE(RecElement)

class RecModel : public QAbstractListModel {
    Q_OBJECT
public:
    RecModel(QObject *parent = nullptr)
        :QAbstractListModel(parent){}

    QHash<int, QByteArray> roleNames() const override {
        QHash<int, QByteArray> result;
        const auto obj = &RecElement::staticMetaObject;
        for (int i = 0; i < obj->propertyCount(); i++)
            result.insert(Qt::UserRole + i, obj->property(i).name());
        return result;
    }
    int rowCount(const QModelIndex & parent = QModelIndex()) const override {
        if (parent.isValid()) return 0;
        return elements.count();
    }
    QVariant data(const QModelIndex &index, int role) const override {
        if (!hasIndex(index.row(), index.column(), index.parent()))
            return {};
        const auto obj = &RecElement::staticMetaObject;
        const auto prop = obj->property(role - Qt::UserRole);
        const auto element = elements[index.row()];
        return prop.readOnGadget(&element);
    }
    void append(const RecElement &element){
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        elements << element;
        endInsertRows();
    }
    void resetModel() {
        beginResetModel(); elements.clear(); endResetModel();
    }
    RecElement getRow(const int &row) { return elements[row]; };
    void replace (const int &row, RecElement element) {
        if (row >= 0 && row < elements.size()) {
            elements.replace(row, element);
            QModelIndex modelIndex = index(row);
            dataChanged(modelIndex, modelIndex, roleNames().keys().toVector());
        }
    }
    Q_INVOKABLE QVariant get(const int &row, const int &col) {
        auto modelIndex = this->index(row, 0, QModelIndex());
        return data(modelIndex, Qt::UserRole + col);
    }
    Q_INVOKABLE QVariant get(const int &row, const QString &roleName) {
        QVariant result = QVariant();
        const auto obj = &RecElement::staticMetaObject;

        QByteArray ba = roleName.toLatin1();
        const char *c_str = ba.data();
        const auto col = obj->indexOfProperty(c_str);
        if (col != -1) result = get(row, col);
        return result;
    }
    QList<int> getIds() {
        QList<int> result;
        foreach (auto element, elements)
            result << element.id;
        return result;
    }

private:
    QList<RecElement> elements;
};

#endif // RECMODEL_H
