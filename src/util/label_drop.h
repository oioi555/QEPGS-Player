#ifndef LABELDROP_H
#define LABELDROP_H

#include <QLabel>

class QMimeData;

class LabelDrop : public QLabel {

    Q_OBJECT

public:
    explicit LabelDrop(QWidget *parent = nullptr);

public slots:
    void clear();

signals:
    void changed(const QMimeData *mimeData = nullptr);

protected:
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dragMoveEvent(QDragMoveEvent *event) override;
    void dragLeaveEvent(QDragLeaveEvent *event) override;
    void dropEvent(QDropEvent *event) override;

private:
    QLabel *label;
};

#endif // LABELDROP_H
