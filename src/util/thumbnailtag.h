#ifndef THUMBNAILTAG_H
#define THUMBNAILTAG_H



#include <QFileInfo>
#include <QObject>

class MainWindow;
class SqlModel;

class ThumbnailTag : public QObject {
    Q_OBJECT
public:
    explicit ThumbnailTag(QObject *parent, int id = -1);
    ~ThumbnailTag();

    void setId(int id);
    bool saveDropUrlFile(const QUrl &url, int id = -1);
    bool saveImageFile(const QString &imageFilePath);
    QString getImageFilePath( bool isQML = true);
    bool isFileExist() {
        return QFileInfo::exists(tagFileInfo.absoluteFilePath());
    }
    bool remove(int id = -1, bool confirm = true);
    bool openFileSelectDlg(int id = -1);
    void searchWebImage(const QString &word);

signals:

private:
    int id;
    QString thumbnailTagPath;
    QString screenshotsPath;
    QFileInfo tagFileInfo;
    SqlModel *sqlModel;
    SqlModel *sqlModelTag;
    MainWindow* main;
};

#endif // THUMBNAILTAG_H
