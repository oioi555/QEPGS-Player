#ifndef TABLEVIEW_STYLEDITEMDELEGATE_H
#define TABLEVIEW_STYLEDITEMDELEGATE_H

#include "../mainwindow.h"

#include <QApplication>
#include <QStyledItemDelegate>
#include <QPainter>
#include <QTextDocument>
#include <QAbstractTextDocumentLayout>
#include <QProgressBar>
#include <QRegularExpression>

class TableViewQStyledItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    explicit TableViewQStyledItemDelegate(QObject *parent = nullptr): QStyledItemDelegate(parent)
        , sqlModel(new SqlModel("tags", "id"))
        , main(qobject_cast<MainWindow*>(parent))
    {
        updateData();
    }
    ~TableViewQStyledItemDelegate(){delete sqlModel;}

    void updateData(){
        // getNameHtmlで、繰り返し利用するので、変数に格納しておく
        QString sql = "select tag, CHAR_LENGTH(tag) as taglength from tags where endflag=true and field='番組タイトル' order by taglength desc";
        tags = sqlModel->getOneFieldValues(sql);

        // 録画中判定
        QDateTime now = QDateTime::currentDateTime();
        sql = tr("select id from v_recorded_detail where datestop > '%1'").arg(now.toString("yyyy-MM-dd hh:mm:ss"));
        recordingIds = sqlModel->getOneFieldValues(sql);

        // サムネイル表示の設定
        chkViewThumbnail = main->getSettings()->value("WidgetRecorded/chkViewThumbnail").toBool();
    }
    QString anchorAt(QString text, const QPoint &point) const {
        QTextDocument doc;
        doc.setHtml(getNameHtml(text));
        auto textLayout = doc.documentLayout();
        Q_ASSERT(textLayout != nullptr);
        return textLayout->anchorAt(point);
    }

protected:
    bool paintThumbnaill(const QString &idstr, QPainter *painter, const QStyleOptionViewItem &option) const {
        if (!chkViewThumbnail) return false;

        QStyleOptionViewItem optCopy = option;
        QPixmap pixmap = main->getApi()->getThumbnail(idstr);
        pixmap = pixmap.scaled(option.rect.width(), option.rect.height(), Qt::KeepAspectRatio);
        const int x = optCopy.rect.center().x() - pixmap.rect().width() / 2;
        const int y = optCopy.rect.center().y() - pixmap.rect().height() / 2;
        optCopy.widget->style()->drawControl(QStyle::CE_ItemViewItem, &option, painter);

        QApplication::style()->drawItemPixmap(painter, QRect(x, y, pixmap.rect().width(), pixmap.rect().height()), Qt::AlignCenter, pixmap);
        return true;
    }
    void paintProgress(int percent, QPainter *painter, const QStyleOptionViewItem &option) const {
        QStyleOptionViewItem optCopy = option;

        QRect rect;
        rect.setWidth(optCopy.rect.size().width()-5);
        rect.setHeight(15);

        QProgressBar progress(main);
        progress.resize(rect.size());
        progress.setMinimum(0);
        progress.setMaximum(100);
        progress.setValue(percent);
        progress.setTextVisible(false);
        optCopy.widget->style()->drawControl(QStyle::CE_ItemViewItem, &option, painter);

        painter->save();
        painter->translate(optCopy.rect.center()-rect.center());
        progress.render(painter);
        painter->restore();
    }

    bool paintRecordingDateTime(const QString &id, const QString &text, QPainter *painter, const QStyleOptionViewItem &option) const{
        QStyleOptionViewItem optCopy = option;
        if (recordingIds.contains(id)) {
            QString htmltext = tr("<span style='color:red;'>録画中</span><br>%1").arg(text);
            paintTextToHtml(htmltext, painter, option);
            return true;
        }
        return false;
    }

    void paintVideoType(const QString &text, QPainter *painter, const QStyleOptionViewItem &option) const{
        QStyleOptionViewItem optCopy = option;
        QStringList types;
        foreach (auto type, text.split(", ")) {
            types << tr("<span style='font:bold;'>&nbsp;%2&nbsp;</span>").arg(type);
        }
        paintTextToHtml(types.join(",&nbsp;"), painter, option);
    }

    void paintTextToHtml(const QString &text, QPainter *painter, const QStyleOptionViewItem &option) const{
        QStyleOptionViewItem optCopy = option;
        painter->save();

        QTextDocument doc;
        doc.setHtml(text);
        optCopy.text = "";
        optCopy.widget->style()->drawControl(QStyle::CE_ItemViewItem, &optCopy, painter);

        painter->translate(optCopy.rect.left(), optCopy.rect.top());
        //painter->translate(optCopy.rect.left(), optCopy.rect.height()/2);
        QRect clip(0, 0, optCopy.rect.width(), optCopy.rect.height());
        doc.setTextWidth(optCopy.rect.width());
        doc.drawContents(painter, clip);

        painter->restore();
    }
    QString getNameHtml(const QString &text, const QString &assignTag = "") const {
        QString html = text.toHtmlEscaped();
        QString htmlTag;
        if (assignTag.isEmpty()) {
            foreach (auto tag, tags) {
                if (html.contains(tag)) { htmlTag = tag.toHtmlEscaped(); break; }
            }
        } else {
            htmlTag = assignTag.toHtmlEscaped();
        }
        if (!htmlTag.isEmpty())
            html.replace(QRegularExpression(tr("(%1)").arg(getRegTagEscaped(htmlTag))), tr("<a href='%1'>\\1</a>").arg(htmlTag));

        QString style = "color:#fff; background-color:rgba(63, 81, 181, 0.9);";
        html.replace(QRegularExpression("\\[(.)\\]"),tr("&nbsp;<span style='%1'>&nbsp;\\1&nbsp;</span>&nbsp;").arg(style));
        return html;
    }

private:

    QString getRegTagEscaped(const QString &tag) const {
        auto result = tag;
        const static QStringList words =
            {"(", ")", "[", "]",  "{", "}", "*", "+", ".", "?", "$", "^", "|", "/"};
        foreach (auto word, words) result.replace(word, "\\" + word);
        return result;
    }

private:
    bool chkViewThumbnail = false;
    QStringList tags;
    QStringList recordingIds;
    SqlModel *sqlModel;
    MainWindow* main;
};

#endif // TABLEVIEW_STYLEDITEMDELEGATE_H
