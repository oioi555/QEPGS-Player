#ifndef CUSTOMSPINBOX_H
#define CUSTOMSPINBOX_H

#include <QDoubleSpinBox>

class CustomSpinBox : public QDoubleSpinBox
{
    Q_OBJECT

public:
    CustomSpinBox(QWidget *parent = nullptr);

public slots:
    void setValueNoSignal(double value);
};

#endif // CUSTOMSPINBOX_H
