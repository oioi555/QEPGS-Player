#ifndef PLAYITEM_H
#define PLAYITEM_H

#include <QSettings>

class SqlModel;

class PlayItem {
public:
    explicit PlayItem();
    ~PlayItem();

    void setValueRecorded(int recordedId);
    void setValueVideoId(int videoId); // v1 v2
    void setValueLive(const QString &channelId, const QString &title);
    bool parseUrl(const QString &url);
    // hwDec判定用
    // 衛星放送のmpeg2をハードウェアデコードをしないようにする判定
    bool isHwDecAutoOff() {
        return (channelType != "GR" && videoType == "TS");
    };

    int recordedId = -1;
    int videoId = -1;       // v2 only
    QString channelId = "";
    QString url;
    QString title;
    bool live;
    QString channelType = "";
    QString videoType = "TS";

private:
    void createUrl();
    int getVideoId();
    int getVideoIdToRecordedId();
    void getTitile();
    void setChannelVideoType();

    QString formatVideo;
    QString formatLive;
    QString serverUrl;
    bool chkVideoEncPlay;
    bool isV2;
};

#endif // PLAYITEM_H
