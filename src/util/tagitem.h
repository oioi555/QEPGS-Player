#ifndef TAGITEM_H
#define TAGITEM_H

#include <QListWidgetItem>
#include <QObject>
#include <QTreeWidget>
#include "../mainwindow.h"
#include "tagmodel.h"

class QStandardItem;

class TagItem : public QObject
{
    Q_OBJECT

public:
    // parent にMainWindow指定で、ツールチップにサムネイル表示
    explicit TagItem(QObject *parent = nullptr);

    // recordsetからデータを取得
    // parent にMainWindow指定で、ツールチップにサムネイル表示
    explicit TagItem(const QSqlRecord &rs, QObject *parent = nullptr)
    :main(qobject_cast<MainWindow*>(parent)){
        setData(rs);
    }

    // QVariantMapからデータを取得
    // parent にMainWindow指定で、ツールチップにサムネイル表示
    explicit TagItem(const QVariantMap &rs, QObject *parent = nullptr)
        :main(qobject_cast<MainWindow*>(parent)){
        setData(rs);
    }

    // treeItemGroup 個別にデータ指定
    explicit TagItem(int id,
                     const QString &tag,
                     const QString &field,
                     const QString &genre = "",
                     const QString &tooltip = ""){
        setData(id, tag, field, genre, tooltip);
    }
    ~TagItem();

    // recordsetからデータを取得
    void setData(const QSqlRecord &rs);
    // QVariantMapからデータを取得
    void setData(const QVariantMap &rs);
    // treeItemGroup 個別にデータ指定
    void setData(int id,
                 const QString &tag,
                 const QString &field,
                 const QString &genre = "",
                 const QString &tooltip = "");

    // TreeItemに変換
    QList<QStandardItem *> getTreeItem();
    QStandardItem* getTreeItemGroup();
    bool isTag() { return (id > 0); }

    // qmlで利用する、modelに変換
    TagElement getElement();
    TagElement getElement(const QSqlRecord &rs) {
        setData(rs); return getElement();
    }

    // screen shot で検索タグ選択に利用するListItemに変換
    QListWidgetItem* getListItem(bool checkable = false);
    QListWidgetItem* getListItem(const QSqlRecord &rs, bool checkable = false) {
        setData(rs); return getListItem(checkable);
    }

    int id;
    QString tag = "";
    QString field = "";
    QString genre = "";
    QDateTime date;
    QIcon icon;
    bool isEndProgram = false;
    int watched = 0;
    int count = 0;
    bool endflag = false;
    bool newflag = false;
    bool defaultflag = false;
    bool blackflag = false;
    QString tooltip = "";
    QDateTime lastUpdate;

private:
    const QString getDateStr(const QString &format = "") {
        return date.toString(format.isEmpty()? dateShortFormat : format);
    }
    const QString getLastUpdateStr(const QString &format = "") {
        return lastUpdate.toString(format.isEmpty()? dateShortFormat : format);
    }
    QString getTooltip();
    QString getTooltipInner();

    MainWindow* main;

};
#endif // TAGITEM_H
