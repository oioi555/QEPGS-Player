#ifndef QLINEEDIT_PATH_H
#define QLINEEDIT_PATH_H

#include <QLineEdit>

class EditPath : public QLineEdit
{
    Q_OBJECT

public:
    explicit EditPath(QWidget *parent = nullptr);

private slots:
    void on_action_clicked();
};

#endif // QLINEEDIT_PATH_H
