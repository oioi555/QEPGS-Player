#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <QObject>
#include <QDebug>

#include "playitem.h"
class SqlModel;

class Playlist : public QObject {
    Q_OBJECT
public:
    explicit Playlist();
    ~Playlist();

    bool hasTagId(int tagId, int recordedId = -1);
    bool hasRecordedId(int recordedId);
    void reset();
    void setTagId(int tagId, int selected = -1);
    void setRecordedIds(QList<int> ids, int selected = -1);
    void update();
    void setSelectedId(int recordedId);
    int getSelectedId() {
        return items.at(selectedIndex).recordedId;
    }
    int getRecordedIdIndex(int recordedId);
    void removeRecordedId(int recordedId);
    bool canGoNext() { return selectedIndex > 0; };
    bool canGoPrev() { return selectedIndex < (items.length() -1); };
    void setPrevIndex();
    void setNextIndex();
    bool isSelectionEnd() { return (selectedIndex == 0); }

    PlayItem getRecordedId(int recordedId);

    bool isTag() { return (tagId != -1); }
    int getTagId() { return tagId; };
    QString getTagIdStr() { return QString::number(tagId); }

    void addItem(PlayItem item) {items << item;};
    void addItemLive(const QString &channelId, const QString &title) {
        PlayItem item; item.setValueLive(channelId, title); items << item;
    }
    QList<PlayItem> getItems() const { return items; }
    bool hasItem() { return (items.length() > 0); }

    bool readPlayFile(const QString &playListFile);
    QStringList getIdStrs();
    QStringList getPlaylistText();
    static QString getSaveFileName(const QString &dir, const QString &title, QWidget *parent);
    bool saveFile(QString &fileName);

signals:
    void selectChanged(int index);

private:
    void addRecordedIds(QList<int> ids, int selected = -1);
    int tagId = -1;
    int selectedIndex = -1;
    int threshold = 90;
    QList<PlayItem> items;

    SqlModel *sqlModel;
    SqlModel *sqlModelTag;
    SqlModel *sqlModelDetail;

};

#endif // PLAYLIST_H
