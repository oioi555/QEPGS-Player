#include "label_drop.h"
#include <QDragEnterEvent>
#include <QMimeData>

LabelDrop::LabelDrop(QWidget *parent)
    : QLabel(parent)
{
    setMinimumSize(200, 200);
    setFrameStyle(QFrame::Sunken | QFrame::StyledPanel);
    setAlignment(Qt::AlignCenter);
    setAcceptDrops(true);
    setAutoFillBackground(true);
    clear();
}

void LabelDrop::clear()
{
    setBackgroundRole(QPalette::Dark);

    emit changed();
}

void LabelDrop::dragEnterEvent(QDragEnterEvent *event)
{
    setBackgroundRole(QPalette::Highlight);

    event->acceptProposedAction();
}

void LabelDrop::dragMoveEvent(QDragMoveEvent *event)
{
    event->acceptProposedAction();
}

void LabelDrop::dragLeaveEvent(QDragLeaveEvent *event)
{
    clear();
    event->accept();
}

void LabelDrop::dropEvent(QDropEvent *event)
{
    setBackgroundRole(QPalette::Dark);
    emit changed(event->mimeData());

    event->acceptProposedAction();
}
