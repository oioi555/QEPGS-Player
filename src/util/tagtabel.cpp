#include "tagtabel.h"

#include "../mainwindow.h"
#include "util/progressdlg.h"

#include <QApplication>
#include <QElapsedTimer>

TagTable::TagTable(QWidget *parent): QObject(parent)
    , sqlModel(new SqlModel("recorded", "id", "v_recorded"))
    , sqlModelTag(new SqlModel("tags", "id"))
    , main(qobject_cast<MainWindow*>(parent))
{
    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);
    connect(sqlModelTag, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    // mainwindow
    connect(main, &MainWindow::mainStatusChanged, this, [=] (MainStatus status) {
        if (status == MainStatus::VideoDeleted) {
            updateTagFull();
            emit tagTableUpdated();
        }
        if (status == MainStatus::Database && initUpdate) {
            updateTable();
            emit tagTableUpdated();
        }
    });
}

TagTable::~TagTable()
{
    delete sqlModel;
    delete sqlModelTag;
}

void TagTable::updateTable()
{
    QElapsedTimer timer;
    timer.start();

    main->getSettings()->beginGroup("DockTag");
    bool chkAutoNewTag = main->getSettings()->value("chkAutoNewTag").toBool();
    bool chkDefaultTag = main->getSettings()->value("chkDefaultTag").toBool();
    QStringList listDefaultTags = main->getSettings()->value("listDefaultTags").toStringList();
    main->getSettings()->endGroup();

    if (chkAutoNewTag) mergeNewTag();       // 新番組タグ追加

    // デフォルトタグ V2対応
    // NOTE: v2でテーブル名が変わっている
    // rules → rule , services → channel
    static const QList<tableItem> defItems = {
        {"ルール", "rules", "id", "keyword"},
        {"ジャンル", "genre1", "id", "name"},
        {"サブジャンル", "genre2", "code", "name"},
        {"チャンネル", "services", "id", "name"}
    };
    static const QList<tableItem> defItemsV2 = {
        {"ルール", "rule", "id", "keyword"},
        {"ジャンル", "genre1", "id", "name"},
        {"サブジャンル", "genre2", "code", "name"},
        {"チャンネル", "channel", "id", "halfWidthName"}
    };

    if (chkDefaultTag) {
        foreach (auto item, main->getApi()->isV2? defItemsV2 : defItems) {
            if (listDefaultTags.contains(item.field)) {
                sqlModel->setInit(item.tabel, item.idName);
                mergeDefaultList(item.field, sqlModel->getGroupByValues(item.group));
            } else {
                // 削除
                sqlModelTag->setInit("tags", "id");
                sqlModelTag->deleteRecord(tr("field='%2'").arg(item.field));
            }
        }
    }

    // 新しい録画の集計を更新する
    sqlModel->setInit("recorded", "id", "v_recorded");
    auto tagLastUpdateValue = sqlModelTag->getScholar("select tag_last_update from tags order by tag_last_update desc limit 1 OFFSET 0;");
    if (!tagLastUpdateValue.isNull()) {
        auto dateType = sqlModel->isPgsql()? "timestamp" : "DATETIME";
        auto tagLastUpdate = tagLastUpdateValue.toDateTime().toString("yyyy-MM-dd hh:mm:ss");
        auto recordedIds = sqlModel->getValuesId(tr("CAST(録画日時 AS %1) > '%2'").arg(dateType, tagLastUpdate));

        qDebug() << "TagTable::updateTable() tagLastUpdate:" << tagLastUpdate << " Update RecodedIds" << recordedIds;
        updateRecorded(recordedIds);
    }

    initUpdate = true;

    qDebug() << tr("TagTable::updateTable() 実行時間: %1秒").arg(static_cast<double>(timer.elapsed()) / 1000, 0, 'f', 2);
}

void TagTable::updateRecorded(const QList<int> &ids)
{
    foreach (auto id, ids) {
        qDebug() << "TagTable::updateRecorded() id:" << id;
        sqlModel->setInit("recorded", "id", "v_recorded");
        sqlModel->setWhere(tr("id=%1").arg(id));
        auto recorded = sqlModel->getModelList();
        if (!recorded.count()) return;

        QList<int> tagIds;  // 更新対象のTagId
        sqlModelTag->setInit("tags", "id", "", "id, tag", "field='番組タイトル'");
        auto nameModelTags = sqlModelTag->getModelList();
        foreach (auto rsTag, nameModelTags) {
            if (recorded[0]["番組タイトル"].toString().contains(rsTag["tag"].toString()))
                tagIds << rsTag["id"].toInt();
        }

        QStringList sqls;
        sqls << tr("(field='チャンネル' AND tag='%1')").arg(recorded[0]["チャンネル"].toString());
        sqls << tr("(field='ルール' AND tag='%1')").arg(recorded[0]["ルール"].toString());
        sqls << tr("(field='ジャンル' AND tag='%1')").arg(recorded[0]["ジャンル"].toString());
        sqls << tr("(field='サブジャンル' AND tag='%1')").arg(recorded[0]["サブジャンル"].toString());
        tagIds << sqlModelTag->getValuesId(sqls.join(" OR "));
        foreach (auto tagId, tagIds) updateTag(tagId);
    }
}

void TagTable::updateTag(int tagId)
{
    updateTag(QList<int>{tagId});
}

void TagTable::updateTag(const QList<int> tagIds)
{
    if (tagIds.count()) qDebug() << "TagTempDb::updateTag()" << tagIds;
    foreach (auto tagId, tagIds) {
        if (tagId == -1) continue;

        sqlModelTag->setInit("tags", "id");
        auto rs = sqlModelTag->getRecord(tagId);

        auto field = sqlModelTag->escape(rs.value("field").toString());
        auto tag = sqlModelTag->escape(rs.value("tag").toString());
        bool endflag = rs.value("endflag").toBool();

        int tag_watched_count = sqlModelTag->getScholar(tr("select tag_watched_count('%1', '%2')").arg(field, tag)).toInt();
        int tag_video_count = sqlModelTag->getScholar(tr("select tag_video_count('%1', '%2')").arg(field, tag)).toInt();
        int tag_is_endprogram = sqlModelTag->getScholar(tr("select tag_is_endprogram('%1', '%2', '%3')").arg(field, tag).arg(endflag)).toInt();
        QDateTime tag_last_update = QDateTime::currentDateTime();
        QVariant tag_last_update_value = sqlModelTag->getScholar(tr("select tag_last_update('%1', '%2')").arg(field, tag));
        if (tag_last_update_value != "")
            tag_last_update = tag_last_update_value.toDateTime();

        sqlModelTag->setValueClear();
        sqlModelTag->setValue("tag_watched_count", tag_watched_count);
        sqlModelTag->setValue("tag_video_count", tag_video_count);
        sqlModelTag->setValue("tag_is_endprogram", tag_is_endprogram);
        sqlModelTag->setValue("tag_last_update", tag_last_update);
        sqlModelTag->saveValues(tagId);
    }
}

void TagTable::updateTagFull()
{
    sqlModelTag->setInit("tags", "id");
    QList<QVariantMap> modelTags = sqlModelTag->getModelList();
    ProgressDlg progress(main, "タグ", "タグ統計の同期中...", modelTags.count());
    foreach (auto rs, modelTags) {
        if (progress.setValueIncWasCanceled()) break;
        updateTag(rs["id"].toInt());
    }
}

QList<int> TagTable::addTagEndFlag(const QList<QVariantMap> &modelList)
{
    QList<int> tagIds;
    QString field = "番組タイトル";
    foreach (auto rs, modelList) {
        auto tag = sqlModelTag->escape(main->parseStrTitle(rs[field].toString()));
        if (!tag.isEmpty()) {
            int tagsCount = sqlModelTag->getScholar(tr("select count(*) from tags where field='%1' and tag='%2'").arg(field, tag)).toInt();
            if (!tagsCount) {
                sqlModelTag->setValueClear();
                sqlModelTag->setValue("field", field);
                sqlModelTag->setValue("tag", tag);
                sqlModelTag->setValue("genre", rs["ジャンル"].toString());
                sqlModelTag->setValue("newflag", true);
                sqlModelTag->setValue("endflag", true);
                tagIds << sqlModelTag->saveValues();
            }
        }
    }
    updateTag(tagIds);
    return tagIds;
}

void TagTable::mergeNewTag()
{
    QString field = "番組タイトル";
    sqlModel->setInit("recorded", "id", "v_recorded", field + ", ジャンル" , tr("%1 like '%[新]%'").arg(field));
    addTagEndFlag(sqlModel->getModelList());
}

void TagTable::mergeDefaultList(const QString &field, const QStringList &list)
{
    QList<int> tagIds;
    foreach (QString tag, list) {
        if (!tag.isEmpty()) {
            QString escapedTag = sqlModelTag->escape(tag);
            int tagsCount = sqlModelTag->getScholar(tr("select count(*) from tags where field='%1' and tag='%2'").arg(field, escapedTag)).toInt();
            if (!tagsCount) {
                int recordedCount = sqlModelTag->getScholar(tr("select tag_video_count('%1', '%2')").arg(field, escapedTag)).toInt();
                if (recordedCount > 0) {
                    // 該当動画があり、タグに登録が無いものを追加
                    sqlModelTag->setValueClear();
                    sqlModelTag->setValue("field", field);
                    sqlModelTag->setValue("tag", tag);
                    sqlModelTag->setValue("defaultflag", true);
                    tagIds << sqlModelTag->saveValues();
                }
            }
        }
    }
    updateTag(tagIds);
}
