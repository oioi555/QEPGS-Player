#include "toolbutton_checkable.h"

#include <QMouseEvent>
#include <QDebug>

ToolButtonCheckable::ToolButtonCheckable(QWidget *parent): QToolButton(parent)
{
    setCheckable(true);
}

void ToolButtonCheckable::setIconNames(const QString &onName, const QString &offName)
{
    iconOn = QIcon::fromTheme(onName);
    iconOff = QIcon::fromTheme(offName);
}

void ToolButtonCheckable::checkStateSet()
{
    setIcon(isChecked()? iconOn : iconOff);
}

