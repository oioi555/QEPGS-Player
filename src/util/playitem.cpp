#include "playitem.h"
#include "sqlmodel.h"

#include <QRegularExpression>
#include <QDebug>

PlayItem::PlayItem()
{
    QSettings settings;
    serverUrl = settings.value("url").toString();
    formatVideo = settings.value("formatVideo").toString();
    formatLive = settings.value("formatLive").toString();
    chkVideoEncPlay = settings.value("chkVideoEncPlay").toBool();
    isV2 = settings.value("isV2").toBool();
}

PlayItem::~PlayItem()
{
}

void PlayItem::setValueRecorded(int recordedId)
{
    this->recordedId = recordedId;
    this->videoId = getVideoId();
    getTitile();
    live = false;
    createUrl();
    setChannelVideoType();  // hwDec判定用
}

void PlayItem::setValueVideoId(int videoId)
{
    if (isV2) {
        this->videoId = videoId;
        this->recordedId = getVideoIdToRecordedId();
    } else { // v1
        this->recordedId = videoId;
    }
    getTitile();
    live = false;
    createUrl();
    setChannelVideoType();  // hwDec判定用
}

void PlayItem::setValueLive(const QString &channelId, const QString &title)
{
    this->channelId = channelId;
    this->title = title;  // v1 v2 で取得テーブルが違うので、呼び出し元に任せる
    live = true;
    createUrl();
    setChannelVideoType();  // hwDec判定用
}

void PlayItem::createUrl()
{
    if (live)
        url = QString(formatLive).arg(serverUrl).arg(channelId);
    else
        url = QString(formatVideo).arg(serverUrl).arg((videoId == -1)? recordedId: videoId);
}

auto PlayItem::parseUrl(const QString &url) -> bool
{
    this->url = url;

    QString tmpUrl = url;
    tmpUrl.replace(serverUrl, "");

    live = url.contains("/api/streams/live/");
    if (live) {
        tmpUrl.replace("/api/streams/live/", "");
        auto values = tmpUrl.split("/");
        channelId = values[0];
        setChannelVideoType();  // hwDec判定用
        return true;
    } else {
        if (tmpUrl.contains("/api/videos/")) {
            // v2
            tmpUrl.replace(QRegularExpression(".*\\/api\\/videos\\/"), "");
            videoId = tmpUrl.toInt();
            recordedId = getVideoIdToRecordedId();
            setChannelVideoType();  // hwDec判定用
            return true;
        } else {
            // v1
            tmpUrl.replace(QRegularExpression(".*\\/api\\/recorded\\/|\\/file"), "");
            videoId = getVideoId();
            recordedId = tmpUrl.toInt();
            setChannelVideoType();  // hwDec判定用
            return true;
        }
    }
    return false;
}

int PlayItem::getVideoId()
{
    if (!isV2) return -1;
    auto order = chkVideoEncPlay? "" : "desc";
    SqlModel sqlModel;
    auto sql = QString("select id from video_file where recordedId=%1 order by `type` %2").arg(recordedId).arg(order);
    return sqlModel.getScholar(sql).toInt();
}

int PlayItem::getVideoIdToRecordedId()
{
    if (!isV2) return -1;
    SqlModel sqlModel;
    auto sql = QString("select recordedId from video_file where id=%1").arg(videoId);
    return sqlModel.getScholar(sql).toInt();
}

void PlayItem::getTitile()
{
    SqlModel sqlModel("recorded", "id", "v_recorded");
    this->title = sqlModel.getValue("番組タイトル", this->recordedId).toString();
}

void PlayItem::setChannelVideoType()
{
    SqlModel sqlModel;
    videoType = "TS";
    if (live) {
        auto sql = QString("select channelType from %1 where id=%2")
                       .arg(isV2? "channel" : "services", channelId);
        channelType = sqlModel.getScholar(sql).toString();
    } else {
        sqlModel.setInit("recorded", "id", "v_recorded_detail");
        channelType = sqlModel.getValue("type", this->recordedId).toString();
        if (isV2) {
            sqlModel.setInit("video_file", "id");
            videoType = sqlModel.getValue("name", videoId).toString();
        }
    }
}
