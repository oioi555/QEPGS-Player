#ifndef CUSTOMFRAME_H
#define CUSTOMFRAME_H

#include <QEvent>
#include <QFrame>

class CustomFrame : public QFrame
{
    Q_OBJECT

public:
    explicit CustomFrame(QWidget *parent = nullptr);

signals:
    void hoverIn();
    void hoverOut();

protected:
    #if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)
    virtual void enterEvent(QEnterEvent *event) override;
    #else
    virtual void enterEvent(QEvent *event) override;
    #endif
    virtual void leaveEvent(QEvent *event) override;
};

#endif // CUSTOMFRAME_H
