#ifndef TAGTABEL_H
#define TAGTABEL_H

#include "sqlmodel.h"
#include <QObject>
#include <QTimer>


class MainWindow;

class TagTable  : public QObject {
    Q_OBJECT

public:
    TagTable(QWidget *parent = nullptr);
    ~TagTable();

    void updateTable();
    void updateRecorded(int id) { if (id != -1) updateRecorded(QList<int>{id}); };
    void updateRecorded(const QList<int> &ids);
    void updateTag(int tagId);
    void updateTag(const QList<int> tagIds);
    void updateTagFull();
    QList<int> addTagEndFlag(const QList<QVariantMap> &modelList);
    bool isInitUpdate(){ return initUpdate; }

signals:
    void tagTableUpdated();

private:
    void mergeNewTag();
    void mergeDefaultList(const QString &field, const QStringList &list);
    struct tableItem {
        QString field;
        QString tabel;
        QString idName;
        QString group;
    };
    SqlModel *sqlModel;
    SqlModel *sqlModelTag;
    bool initUpdate = false;
    MainWindow* main;

};

#endif // TAGTABEL_H
