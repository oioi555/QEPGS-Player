#include "customspinbox.h"

CustomSpinBox::CustomSpinBox(QWidget *parent): QDoubleSpinBox(parent)
{

}

void CustomSpinBox::setValueNoSignal(double value)
{
    this->blockSignals(true);
    this->setValue(value);
    this->blockSignals(false);
}
