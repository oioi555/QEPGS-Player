#ifndef TAGMODEL_H
#define TAGMODEL_H

#include <QMetaObject>
#include <QMetaProperty>
#include <QAbstractListModel>

struct TagElement {
    Q_GADGET
    Q_PROPERTY(int id MEMBER id)
    Q_PROPERTY(QString thumbnail MEMBER thumbnail)
    Q_PROPERTY(QString tag MEMBER tag)
    Q_PROPERTY(QString field MEMBER field)
    Q_PROPERTY(QString genre MEMBER genre)
    Q_PROPERTY(QString date MEMBER date)
    Q_PROPERTY(bool isEndProgram MEMBER isEndProgram)
    Q_PROPERTY(int watched MEMBER watched)
    Q_PROPERTY(int count MEMBER count)
    Q_PROPERTY(bool endflag MEMBER endflag)
    Q_PROPERTY(bool newflag MEMBER newflag)
    Q_PROPERTY(bool defaultflag MEMBER defaultflag)
    Q_PROPERTY(bool blackflag MEMBER blackflag)
    Q_PROPERTY(QString lastUpdate MEMBER lastUpdate)
    Q_PROPERTY(bool hasThumbnail MEMBER hasThumbnail)

public:
    int id;
    QString thumbnail;
    QString tag;
    QString field;
    QString genre;
    QString date;
    bool isEndProgram;
    int watched;
    int count;
    bool endflag;
    bool newflag;
    bool defaultflag;
    bool blackflag;
    QString lastUpdate;
    bool hasThumbnail;
};
Q_DECLARE_METATYPE(TagElement)

class TagModel : public QAbstractListModel {
    Q_OBJECT

public:
    TagModel(QObject *parent = nullptr)
        :QAbstractListModel(parent){}

    QHash<int, QByteArray> roleNames() const override {
        QHash<int, QByteArray> result;
        const auto obj = &TagElement::staticMetaObject;
        for (int i = 0; i < obj->propertyCount(); i++)
            result.insert(Qt::UserRole + i, obj->property(i).name());
        return result;
    }
    Q_INVOKABLE int rowCount(const QModelIndex & parent = QModelIndex()) const override {
        if (parent.isValid()) return 0;
        return elements.count();
    }
    QVariant data(const QModelIndex &index, int role) const override {
        if (!hasIndex(index.row(), index.column(), index.parent()))
            return {};
        const auto obj = &TagElement::staticMetaObject;
        const auto prop = obj->property(role - Qt::UserRole);
        const auto item = elements[index.row()];
        return prop.readOnGadget(&item);
    }
    void append(const TagElement &element) {
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        elements << element;
        endInsertRows();
    }
    void resetModel() {
        beginResetModel();
        elements.clear();
        endResetModel();
    }
    TagElement getRow(const int &row) { return elements[row]; };
    void replace (const int &row, TagElement element) {
        if (row >= 0 && row < elements.size()) {
            elements.replace(row, element);
            QModelIndex modelIndex = index(row);
            dataChanged(modelIndex, modelIndex, roleNames().keys().toVector());
        }
    }
    Q_INVOKABLE QVariant get(const int &row, const int &col) {
        auto modelIndex = this->index(row, 0, QModelIndex());
        return data(modelIndex, Qt::UserRole + col);
    }
    Q_INVOKABLE QVariant get(const int &row, const QString &roleName) {
        QVariant result = QVariant();
        const auto obj = &TagElement::staticMetaObject;

        QByteArray ba = roleName.toLatin1();
        const char *c_str = ba.data();
        const auto col = obj->indexOfProperty(c_str);
        if (col != -1) result = get(row, col);
        return result;
    }

private:
    QList<TagElement> elements;
};

#endif // TAGMODEL_H
