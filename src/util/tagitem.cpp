#include "util/tagitem.h"
#include "thumbnailtag.h"

#include <QStandardItemModel>

static const QStringList toolTipStyle = QStringList()
    << "h3 {white-space: nowrap;}"
    << "img {background-color: #2d2d2d}"
    << "#outer td{padding: 0px;}"
    << "#inner td{padding: 0px;}";

TagItem::TagItem(QObject *parent):main(qobject_cast<MainWindow*>(parent)){}
TagItem::~TagItem()
= default;

void TagItem::setData(const QSqlRecord &rs)
{
    id = rs.value("id").toInt();
    tag = rs.value("tag").toString();
    field = rs.value("field").toString();
    genre = rs.value("genre").toString();
    date = rs.value("date").toDateTime();
    endflag = rs.value("endflag").toBool();
    newflag = rs.value("newflag").toBool();
    defaultflag = rs.value("defaultflag").toBool();
    blackflag = rs.value("blackflag").toBool();
    watched = rs.value("tag_watched_count").toInt();
    count = rs.value("tag_video_count").toInt();
    isEndProgram = rs.value("tag_is_endprogram").toBool();
    lastUpdate = rs.value("tag_last_update").toDateTime();
    QString iconName = defaultflag? "bookmark-new" : "tag";
    if (endflag) iconName = "rating-unrated";
    if (newflag) iconName = "rating";
    if (endflag)
        if (isEndProgram) iconName = "checkmark";
    icon = QIcon::fromTheme(iconName);
}

void TagItem::setData(const QVariantMap &rs)
{
    id = rs["id"].toInt();
    tag = rs["tag"].toString();
    field = rs["field"].toString();
    genre = rs["genre"].toString();
    date = rs["date"].toDateTime();
    endflag = rs["endflag"].toBool();
    newflag = rs["newflag"].toBool();
    defaultflag = rs["defaultflag"].toBool();
    blackflag = rs["blackflag"].toBool();
    watched = rs["tag_watched_count"].toInt();
    count = rs["tag_video_count"].toInt();
    isEndProgram = rs["tag_is_endprogram"].toBool();
    lastUpdate = rs["tag_last_update"].toDateTime();
    QString iconName = defaultflag? "bookmark-new" : "tag";
    if (endflag) iconName = "rating-unrated";
    if (newflag) iconName = "rating";
    if (endflag)
        if (isEndProgram) iconName = "checkmark";
    icon = QIcon::fromTheme(iconName);
}

void TagItem::setData(int id, const QString &tag, const QString &field, const QString &genre, const QString &tooltip)
{
    this->id = id;
    this->tag = tag;
    this->field = field;
    this->genre = genre;
    this->tooltip = tooltip;
    QString iconName = (genre == "もう見ないリスト")? "view-hidden" : "object-columns";
    icon = QIcon::fromTheme(iconName);
}

QList<QStandardItem*> TagItem::getTreeItem()
{
    auto item = new QStandardItem(icon, tag);
    QVariant stored = QVariant::fromValue(static_cast<QObject *>(this));
    item->setData(stored, Qt::UserRole);
    item->setEditable(false);
    if (watched == count) item->setForeground(QColor("silver"));
    // tooltip
    item->setToolTip(getTooltip());

    auto itemSub = new QStandardItem(tr("%1 /%2").arg(
        QString::number(count - watched).rightJustified(2, ' '),
        QString::number(count).rightJustified(3, ' ')));
    itemSub->setForeground((count == 0)? QColor("orangered") : QColor("darkgreen") );
    itemSub->setEditable(false);

    return {item, itemSub};
}

QStandardItem *TagItem::getTreeItemGroup()
{
    auto item = new QStandardItem(icon, tag);
    QVariant stored = QVariant::fromValue(static_cast<QObject *>(this));
    item->setData(stored, Qt::UserRole);
    item->setEditable(false);
    if (!tooltip.isEmpty())
        item->setToolTip(tooltip);

    return item;
}

QString TagItem::getTooltip()
{
    QStringList htmlBody;
    htmlBody << tr("<h3>%1</h3>").arg(tag);
    if (this->main != nullptr && isTag()) {
        // サムネイルつき
        ThumbnailTag tagImage(main, id);
        auto imgaeSrc = tagImage.getImageFilePath(false);
        htmlBody << "<table id='outer'><tr>";
        htmlBody << tr("<td>%1</td>").arg(getTooltipInner());
        htmlBody << tr("<td><img src='%1' width='240' alt='noimage'/></td>").arg(imgaeSrc);
        htmlBody << "</tr></table>";
    } else {
        // サムネイルなし
        htmlBody << getTooltipInner();
    }
    tooltip = QString(htmlFormat).arg(toolTipStyle.join("\n"), htmlBody.join("\n"));;
    return tooltip;
}

QString TagItem::getTooltipInner()
{
    QStringList htmlTable;
    htmlTable << "<table id='inner'>";
    htmlTable << tr("<tr><td>ジャンル：</td><td>%1</td></tr>").arg(genre.isEmpty()? "任意" : genre);
    htmlTable << tr("<tr><td>登録日：</td><td>%1</td></tr>").arg(getDateStr());
    htmlTable << tr("<tr><td>録画日：</td><td>%1</td></tr>").arg(getLastUpdateStr());
    htmlTable << tr("<tr><td>未視聴：</td><td>%1</td></tr>").arg(QString::number(count - watched));
    htmlTable << tr("<tr><td>視聴済：</td><td>%1</td></tr>").arg(QString::number(watched));
    htmlTable << tr("<tr><td>録画数：</td><td>%1</td></tr>").arg(QString::number(count));
    if (endflag)
        htmlTable << "<tr><td>種別：</td><td>連続番組</td></tr>";
    if (newflag)
        htmlTable << "<tr><td>種別：</td><td>新番組検出</td></tr>";
    if (defaultflag)
        htmlTable << "<tr><td>種別：</td><td>デフォルト</td></tr>";
    if (blackflag)
        htmlTable << "<tr><td>種別：</td><td>もう見ないリスト</td></tr>";
    htmlTable << "</table>";
    return htmlTable.join("\n");
}

// qmlで利用する、modelに変換
TagElement TagItem::getElement()
{
    TagElement element;
    element.id = this->id;
    element.tag = this->tag;
    element.field = this->field;
    element.genre = this->genre;
    element.date = this->date.toString(dateShortFormat);
    element.lastUpdate = this->lastUpdate.toString(dateShortFormat);
    element.isEndProgram = this->isEndProgram;
    element.watched = this->watched;
    element.count = this->count;
    element.endflag = this->endflag;
    element.newflag = this->newflag;
    element.defaultflag = this->defaultflag;
    element.blackflag = this->blackflag;
    return element;
}
// screen shot で検索タグ選択に利用するListItemに変換
QListWidgetItem *TagItem::getListItem(bool checkable /*= false*/)
{
    auto item = new QListWidgetItem();
    item->setText(tag);
    item->setData(Qt::UserRole, id);
    item->setIcon(icon);
    item->setToolTip(getTooltip());
    if (checkable) {
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable );
        item->setCheckState(Qt::CheckState::Unchecked);
    }
    return item;
}
