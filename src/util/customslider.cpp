#include "customslider.h"

#include <QStyle>

CustomSlider::CustomSlider(QWidget *parent): QSlider(parent)
{
}

void CustomSlider::setValueNoSignal(int value)
{
    this->blockSignals(true);
    this->setValue(value);
    this->blockSignals(false);
}

void CustomSlider::setRangeMax(int value)
{
    setRange(0, value);
}

void CustomSlider::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
        int pos = event->position().x();
#else
        int pos = event->x();
#endif
        setValue(QStyle::sliderValueFromPosition(minimum(), maximum(), pos, width()));
        event->accept();
        emit click();
    }
    QSlider::mousePressEvent(event);
}
