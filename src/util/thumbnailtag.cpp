#include "thumbnailtag.h"
#include "mainwindow.h"
#include "sqlmodel.h"

#include <QDir>
#include <QFileDialog>
#include <QPixmap>
#include <QProcess>
#include <QStandardPaths>
#include <QRegularExpression>

ThumbnailTag::ThumbnailTag(QObject *parent, int id) : QObject(parent)
    , id (id)
    , sqlModel(new SqlModel("recorded", "id", "v_recorded", "id"))
    , sqlModelTag(new SqlModel("tags", "id"))
    , main(qobject_cast<MainWindow*>(parent))
{
    QSettings setting;
    thumbnailTagPath = main->getSettings()->value("DockTag/editThumbnailTagPath").toString();
    screenshotsPath = main->getSettings()->value("WidgetPlayer/editScreenshotsPath").toString();
    setId(id);
}

ThumbnailTag::~ThumbnailTag()
{
    delete sqlModel;
    delete sqlModelTag;
}

void ThumbnailTag::setId(int id)
{
    this->id = id;
    tagFileInfo = QFileInfo(QDir(thumbnailTagPath), tr("%1.jpg").arg(id));
}

bool ThumbnailTag::saveDropUrlFile(const QUrl &url, int id /*= -1*/)
{
    if (id != -1) setId(id);

    // 画像リンク
    if (url.scheme().contains("http")) {
        QImage img = main->getApi()->getThumbnailTagImage(url);
        if (!img.save(tagFileInfo.absoluteFilePath())) return false;
        return saveImageFile(tagFileInfo.absoluteFilePath());
    }
    // ローカルファイル
    if (url.scheme().contains("file"))
        return saveImageFile(url.toLocalFile());

    return false;
}

bool ThumbnailTag::saveImageFile(const QString &imageFilePath)
{
    QFileInfo imageInfo(imageFilePath);
    if (!QFileInfo::exists(imageInfo.absoluteFilePath())) return false;

    QPixmap image = QPixmap(imageInfo.absoluteFilePath());
    QPixmap saveImage = image.scaled(480,270, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
    return saveImage.save(tagFileInfo.absoluteFilePath());
}

QString ThumbnailTag::getImageFilePath(bool isQML)
{
    // tag用に保存したサムネイル
    if (QFileInfo::exists(tagFileInfo.absoluteFilePath())) {
        auto result = tagFileInfo.absoluteFilePath();
        return isQML? "file:///" + result : result;
    }
    return  isQML? "qrc:/images/noimage.png" : ":/images/noimage.png";
}

bool ThumbnailTag::remove(int id /*= -1*/, bool confirm /*= true*/)
{
    if (id != -1) setId(id);
    if (confirm && !main->deleteConfirm(nullptr)) return false;
    return QFile::remove(tagFileInfo.absoluteFilePath());
}

bool ThumbnailTag::openFileSelectDlg(int id /*= -1*/)
{
    if (id != -1) setId(id);

    QString filter = tr("画像(*.jpg *.png)");
    auto file = QFileDialog::getOpenFileName( qobject_cast<QWidget*>(main),
        tr("ファイルを開く"), screenshotsPath,
        tr("すべて(*.*);;画像(*.jpg *.png)"),
        &filter,
        QFileDialog::DontUseCustomDirectoryIcons
        );
    if (!file.isEmpty())
        return saveImageFile(file);

    return false;
}

void ThumbnailTag::searchWebImage(const QString &word)
{
    QString text = word;
    if (text.isEmpty()) return;
    text.truncate(30);
    text.replace(QRegularExpression("#"), "");
    text = text.trimmed();
    QString value = tr("https://duckduckgo.com/?q=%1&iar=images&iaf=layout%3AWide").arg(text);
    QProcess::startDetached("xdg-open", QStringList() << QUrl(value).toString());
}
