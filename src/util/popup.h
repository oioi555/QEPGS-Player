#ifndef POPUP_H
#define POPUP_H

#include <QDialog>
#include <QLabel>
#include <QGridLayout>
#include <QPropertyAnimation>
#include <QTimer>

class PopUp : public QDialog
{
    Q_OBJECT

public:
    explicit PopUp(QWidget *parent = nullptr);
    ~PopUp();

    Q_PROPERTY(double popupOpacity READ getPopupOpacity WRITE setPopupOpacity)

    void show(bool top, const QString &text = "", QWidget* host = nullptr);
    void show(bool top, const QStringList &value, QWidget* host = nullptr) {
        show(top, value.join("\n"),host);
    }
    void show(const QString &text = "", QWidget* host = nullptr) {
        show(false, text, host);
    }
    void show(const QStringList &value, QWidget* host = nullptr) {
        show(false, value.join("\n"), host);
    }
    void setPopupOpacity(double opacity);
    double getPopupOpacity() const;

protected:
    void paintEvent(QPaintEvent *event);    // The background will be drawn through the redraw method

private slots:
    void hideAnimation();                   // Slot start the animation hide
    void hide();                            /* At the end of the animation, it is checked in a given slot,
                                             * Does the widget visible, or to hide
                                             * */
private:
    QLabel label;
    QGridLayout layout;
    QPropertyAnimation animation;
    double popupOpacity;
    QTimer *timerClose;
};

#endif // POPUP_H
