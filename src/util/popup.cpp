#include "popup.h"

#include <QPainter>
#include <QApplication>
#include <QDebug>

PopUp::PopUp(QWidget *parent) : QDialog(parent)
  , timerClose(new QTimer())
{
    setWindowFlags(Qt::FramelessWindowHint |        // Disable window decoration
                   Qt::Tool);                       // Discard display in a separate window
    setAttribute(Qt::WA_TranslucentBackground);     // Indicates that the background will be transparent
    setAttribute(Qt::WA_ShowWithoutActivating);     // At the show, the widget does not get the focus automatically

    animation.setTargetObject(this);                // Set the target animation
    animation.setPropertyName("popupOpacity");
    connect(&animation, &QAbstractAnimation::finished, this, &PopUp::hide);
    label.setObjectName("popLabel");
    label.setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    layout.addWidget(&label, 0, 0);
    setLayout(&layout);

    connect(timerClose, &QTimer::timeout, this, &PopUp::hideAnimation);
}

PopUp::~PopUp()
{
    delete timerClose;
}

void PopUp::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    QRect roundedRect;
    roundedRect.setX(rect().x() + 5);
    roundedRect.setY(rect().y() + 5);
    roundedRect.setWidth(rect().width() - 10);
    roundedRect.setHeight(rect().height() - 10);

    painter.setBrush(QBrush(QColor(0,0,0,120)));
    painter.setPen(Qt::NoPen);

    painter.drawRoundedRect(roundedRect, 10, 10);
}

void PopUp::show(bool top, const QString &text/* = ""*/, QWidget* host /*= nullptr*/)
{
    label.setWordWrap(true);
    if (!text.isEmpty()) {
        label.setText(text);        // Set the text in the Label
        adjustSize();
    }

    setWindowOpacity(0.0);          // Set the transparency to zero

    animation.setDuration(100);     // Configuring the duration of the animation
    animation.setStartValue(0.0);   // The start value is 0 (fully transparent widget)
    animation.setEndValue(1.0);     // End - completely opaque widget

    if (host != nullptr && host != parent()) {
        QRect rect = host->geometry();
        if (this->rect().width() > (rect.width() -5 ))
            this->setGeometry(0,0,rect.width() -5,this->height());
        else
            label.setWordWrap(false);

        QPoint pos;
        if (top)
            pos = host->mapToGlobal(rect.topLeft() - this->rect().topLeft());
        else
            pos = host->mapToGlobal(rect.center() - this->rect().center());
        this->move(pos);
    }

    QDialog::show();
    animation.start();
    timerClose->start(3000);
}

void PopUp::hideAnimation()
{
    timerClose->stop();
    animation.setDuration(500);
    animation.setStartValue(1.0);
    animation.setEndValue(0.0);
    animation.start();
}

void PopUp::hide()
{
    // If the widget is transparent, then hide it
    if (getPopupOpacity() == 0.0)
        QDialog::hide();
}

void PopUp::setPopupOpacity(double opacity)
{
    popupOpacity = opacity;
    setWindowOpacity(opacity);
}

auto PopUp::getPopupOpacity() const -> double
{
    return popupOpacity;
}

