#ifndef BASE_MAINWINDOW_H
#define BASE_MAINWINDOW_H

#include <QMainWindow>
#include <QtDebug>
#include "settings.h"

class QMdiArea;
class QMdiSubWindow;
class QAction;
class QActionGroup;
class QToolBar;

class BaseMainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit BaseMainWindow(QWidget *parent = nullptr);
    virtual ~BaseMainWindow() override;

    // settings helper
    Settings* getSettings() {return settings;}
    QString getAppDataPath() const {return appDataPath;}
    bool deleteConfirm(QWidget *parent = nullptr);

    // MdiWindow
    bool isActiveWindow(QWidget *widget);
    bool isActiveWindow(const QString &className);
    void closeSubWindowOther(const QString &className = "");
    void closeSubWindowAll(const QString &className);

    // icon
    QIcon getIcon(const QString &iconName) const {
        return QIcon::fromTheme(iconName);
    }
    void setWindowIcon(const QString &iconName, QWidget *widget);   // 再生アイコン変更に使用

protected:
    Settings *settings;
    QString appDataPath;

    // MdiWindow
    void setupMdiAreaTabMode(QMdiArea *mdiArea, bool enable = true);
    void setMdiAreaTabMode(bool enable);
    void loadSubWindows(QWidget *widget, const QList<QAction*> &tabMenuActions = QList<QAction*>{});
    QMdiSubWindow *getActiveWindow();
    QString getSubWindowClassName(QMdiSubWindow *subWin);
    QMdiSubWindow *getSubWindowInstance(const QString &className);
    QList<QMdiSubWindow*> getSubWindowInstanceAll(const QString &className);

    // toolBar
    void setupToolBar(QToolBar *toolBar);

    // screen
    int getTitlebarHeight();
    void saveNormalGeometry();
    void restoreNormalGeometry();
    QRect getRectFrameless();
    QRect getRectNotFrameless();
    QRect normalGeometry;
    QRect miniGeometry;
private:
    // mdiarea
    QMdiArea *mdiArea;
    QTabBar *tabMdiBar;
    QAction *actionCloseOther;
    QAction *actionCloseAll;

    // toolBar
    void setToolBarStyle(Qt::ToolButtonStyle style);
    QToolBar *toolBar;
    QAction *actionToolBarIconTextV;
    QAction *actionToolBarIconTextH;
    QAction *actionToolBarIcon;
    QAction *actionToolBarText;
    struct toolBarStyleItem  {
        Qt::ToolButtonStyle style;
        QAction* action;
    };
    QActionGroup *toolBarStyleGrp;

    // screen
    int lastTitlebarHeight = 0;
    bool isPrevMaximized = false;

signals:

    // QMainWindow interface
public:
    virtual QMenu *createPopupMenu() override;
};

#endif // BASE_MAINWINDOW_H
