#ifndef TOOLBUTTONCHECKABLE_H
#define TOOLBUTTONCHECKABLE_H

#include <QToolButton>

class ToolButtonCheckable : public QToolButton
{
    Q_OBJECT

public:
    ToolButtonCheckable(QWidget *parent = nullptr);

    void setIconNames(const QString &onName, const QString &offName);
    void setIconNames(bool initChecked, const QString &onName, const QString &offName) {
        setIconNames(onName, offName); setChecked(initChecked);
    }

private:
    QIcon iconOn;
    QIcon iconOff;

    // QAbstractButton interface
protected:
    virtual void checkStateSet() override;
};

#endif // TOOLBUTTONCHECKABLE_H
