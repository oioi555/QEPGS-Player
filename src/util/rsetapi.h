#ifndef RSETAPI_H
#define RSETAPI_H

#include "settings.h"

#include <QUrl>
#include <QObject>
#include <QJsonObject>
#include <QJsonArray>

class QNetworkAccessManager;
class SqlModel;
class QNetworkReply;

class RsetApi : public QObject
{
    Q_OBJECT

public:
    explicit RsetApi(Settings *settings);
    ~RsetApi();

    bool readEpgstation(const QString &server = "");
    QString readEpgstationTest(const QString &server, QString &version);
    QStringList readRssTag();
    QUrl getUrl(const QString &uri) const;

    QJsonObject get(const QString &uri);
    QIcon getIcon(const QString &uri);
    QJsonArray gets(const QString &uri);
    QJsonObject post(const QString &uri, const QJsonObject &jsonObj);
    QJsonObject put(const QString &uri, const QJsonObject &jsonObj);
    QJsonObject deleteResource(const QString &uri);
    QPixmap getThumbnail(const QString &id);
    QString getThumbnailPath(const QString &id, bool isQML = false);
    QImage getThumbnailTagImage(const QUrl &uri);
    void updateStorage();
    QJsonValue getStorage() { return jsonStorage; }
    void deleteRecorded(int id);
    void deleteVideoFile(int videoId);
    QString epgsVersion;
    bool isV2;

signals:
    void netError(const QString &lastError);
    void updatedStorage(const QJsonValue &json);

private:
    QNetworkReply* getReplyAwait(const QString &uri);
    Settings *settings;
    QString thumbnailPath;
    QNetworkAccessManager *managerAsyncThumbnail;
    QNetworkAccessManager *managerAwait;
    QNetworkAccessManager *managerAsyncStorage;
    SqlModel *sqlModel;
    QStringList requestThumbnail;
    QList<int> deleteIds;
    QString serverUrl;
    QJsonValue jsonStorage;
};

#endif // RSETAPI_H
