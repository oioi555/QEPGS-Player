#include "edit_search.h"
#include <QApplication>
#include <QAction>

EditSearch::EditSearch(QWidget *parent) : QLineEdit(parent)
{
    this->setClearButtonEnabled(true);

    QAction *action = this->addAction(QIcon::fromTheme("search"), QLineEdit::TrailingPosition);
    connect(action, &QAction::triggered,[=] {
        emit searchClicked();
    });
    setToolTip("検索キーワード");
}
