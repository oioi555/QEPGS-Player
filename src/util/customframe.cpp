#include "customframe.h"
#include <QDebug>

CustomFrame::CustomFrame(QWidget *parent) : QFrame(parent)
{
    this->setAttribute(Qt::WA_Hover, true);
}

#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)
void CustomFrame::enterEvent(QEnterEvent *event)
#else
void CustomFrame::enterEvent(QEvent *event)
#endif
{
//    qDebug() << Q_FUNC_INFO << this->objectName();
    QWidget::enterEvent(event);
    emit hoverIn();
}

void CustomFrame::leaveEvent(QEvent *event)
{
//    qDebug() << Q_FUNC_INFO << this->objectName();
    QWidget::leaveEvent(event);
    emit hoverOut();
}
