#include "playlist.h"
#include "sqlmodel.h"

#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QProcess>
#include <QStandardPaths>
#include <QTextStream>

Playlist::Playlist()
    : sqlModel(new SqlModel("recorded", "id", "v_recorded"))
    , sqlModelTag(new SqlModel("tags", "id"))
    , sqlModelDetail(new SqlModel("recorded", "id", "v_recorded_detail"))
{
    threshold = sqlModel->getScholar("select get_var('Threshold')").toInt();
}

Playlist::~Playlist()
{
    delete sqlModel;
    delete sqlModelTag;
}

bool Playlist::hasTagId(int tagId, int recordedId)
{
    if (this->tagId == tagId)
        return hasRecordedId(recordedId);

    return false;
}

bool Playlist::hasRecordedId(int recordedId)
{
    auto item = getRecordedId(recordedId);
    return (item.recordedId != -1);
}

void Playlist::reset()
{
    tagId = -1;
    selectedIndex = -1;
    items.clear();
}

void Playlist::setTagId(int tagId, int selected /*= -1*/)
{
    this->tagId = tagId;
    auto rs = sqlModelTag->getRecord(tagId);
    auto escapedTag = sqlModel->escape(rs.value("tag").toString());
    auto where = QString("%1 like '%%2%'").arg(rs.value("field").toString(), escapedTag);
    auto ids = sqlModel->getValuesId(where, "録画日時 DESC");
    addRecordedIds(ids, selected);
}

void Playlist::setRecordedIds(QList<int> ids, int selected)
{
    tagId = -1;
    addRecordedIds(ids, selected);
}

// video削除後に呼出す
void Playlist::update()
{
    auto selectedId = items[selectedIndex].recordedId;
    QList<int> ids;
    foreach (auto item, items) {
        if (!sqlModel->getValue("id", item.recordedId).isNull())
            ids << item.recordedId;
    }
    addRecordedIds(ids, selectedId);
}

void Playlist::addRecordedIds(QList<int> ids, int selected /*= -1*/)
{
    items.clear();
    selectedIndex = -1;
    for (int i = 0; i < ids.count(); i++) {
        int id = ids[i];
        PlayItem item;
        item.setValueRecorded(id);
        items << item;
        auto percent = sqlModel->getValue("再生", id).toInt();
        if (selected == ids[i] || (selected == -1 && threshold > percent))
            selectedIndex = i;
    }
    if (selectedIndex == -1 && hasItem())
        selectedIndex = items.count() -1;
    if (hasItem())
        emit selectChanged(selectedIndex);
}

void Playlist::setSelectedId(int recordedId)
{
    for (int i = 0; i < items.count(); i++) {
        if (items[i].recordedId == recordedId) {
            selectedIndex = i;
            emit selectChanged(selectedIndex);
        }
    }
}

int Playlist::getRecordedIdIndex(int recordedId)
{
    for (int i = 0; i < items.count(); i++) {
        if (items[i].recordedId == recordedId) return i;
    }
    return -1;
}

void Playlist::removeRecordedId(int recordedId)
{
    int removeIndex = getRecordedIdIndex(recordedId);
    if (removeIndex == -1) return;
    items.removeAt(removeIndex);
    selectedIndex--;
    emit selectChanged(selectedIndex);
}

void Playlist::setPrevIndex()
{
    if (canGoPrev()) {
        selectedIndex++;
        emit selectChanged(selectedIndex);
    }
}

void Playlist::setNextIndex()
{
    if (canGoNext()) {
        selectedIndex--;
        emit selectChanged(selectedIndex);
    }
}

PlayItem Playlist::getRecordedId(int recordedId)
{
    foreach (auto item, items) {
        if (item.recordedId == recordedId)
            return item;
    }
    return PlayItem();
}

QStringList Playlist::getIdStrs()
{
    QStringList result;
    foreach (auto item, items)
        result << QString::number(item.recordedId);
    return result;
}

auto Playlist::readPlayFile(const QString &playList) -> bool
{
    QFile file(playList);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    QTextStream stream( &file );
    while (!stream.atEnd()) {
        QString line = stream.readLine();
        // 開始タグを検出
        if (line.contains("#EXTINF:")) {
            QString title;
            QStringList head = line.split(",");
            if (head.length() > 1) title = head[1];
            //2行目のurl
            QString secondline = stream.readLine();
            // urlから、取得可能なプロパティは
            // live, id or videoid
            PlayItem item;
            item.parseUrl(secondline);
            item.title = title;
            items << item;
        }
    }
    file.close();
    if (hasItem()) {
        selectedIndex = items.count() -1;
        emit selectChanged(selectedIndex);
    }
    return true;
}

auto Playlist::getPlaylistText() -> QStringList
{
    QStringList result;
    result << "#EXTM3U";
    foreach (auto item, items) {
        uint time = 0;
        if (!item.live) {
            auto rs = sqlModelDetail->getRecord(item.recordedId);
            auto startDateTime = rs.value("datestart").toDateTime();
            auto endDateTime = rs.value("datestop").toDateTime();
            time = (endDateTime.toSecsSinceEpoch() - startDateTime.toSecsSinceEpoch());
        }
        result << tr("#EXTINF:%1, %2").arg(time).arg(item.title);
        result << item.url;
    }
    return result;
}

auto Playlist::getSaveFileName(const QString &dir, const QString &title, QWidget *parent) -> QString
{
    QDateTime now = QDateTime::currentDateTime();
    QString fileName = QString("%1_%2.m3u8").arg(now.toString("yyyy-MM-dd_hh:mm"), title);
    QString filePath = QDir(dir).filePath(fileName);
    return QFileDialog::getSaveFileName(parent, "プレイリストの保存", filePath, "プレイリスト(*.m3u8)");
}

auto Playlist::saveFile(QString &fileName) -> bool
{
    QString appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QFileInfo info(fileName);
    if (!info.isDir())
        fileName = QDir(appDataPath).filePath(fileName);
    QFile file(fileName);    // 保存するファイル
    QStringList writeList = getPlaylistText();

    if (file.open(QIODevice::WriteOnly| QIODevice::Text)) {
        QTextStream out(&file);
        foreach (QString value, writeList)
            out << value + '\n';
        file.close();
        return true;
    }
    return false;
}
