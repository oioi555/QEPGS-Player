#ifndef CUSTOMSLIDER_H
#define CUSTOMSLIDER_H

#include <QSlider>
#include <QMouseEvent>

class CustomSlider : public QSlider
{
    Q_OBJECT

public:
    explicit CustomSlider(QWidget *parent = nullptr);

signals:
    void click();

public slots:
    void setValueNoSignal(int value);
    void setRangeMax(int value);

protected:
    void mousePressEvent(QMouseEvent *event);
};

#endif // CUSTOMSLIDER_H
