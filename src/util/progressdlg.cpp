#include "progressdlg.h"
#include "qicon.h"

ProgressDlg::ProgressDlg(QWidget *parent, const QString &title, const QString &text, int max)
    : QProgressDialog(parent)
    , labelText(text)
{
    QString iconName = "delete";
    if (title.contains("エンコード")) iconName = "tools-media-optical-burn-image";
    else if (title.contains("タグ")) iconName = "tag";
    setWindowIcon(QIcon::fromTheme(iconName));
    setWindowTitle(title);
    setLabelText(labelText);
    setMinimumSize(300, 150);
    setMinimum(0);
    setMaximum(max);
    setWindowModality(Qt::WindowModal);
    setMinimumDuration(1000);
}

void ProgressDlg::setValueInc()
{
    setValue(value() + 1);
}

bool ProgressDlg::setValueIncWasCanceled()
{
    setValueInc();
    setLabelText(tr("%1　%2/%3").arg(labelText).arg(value()).arg(maximum()));
    return wasCanceled();
}
