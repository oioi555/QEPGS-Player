#ifndef COMBOLIMIT_H
#define COMBOLIMIT_H

#include <QComboBox>

class ComboLimit : public QComboBox
{
public:
    ComboLimit(const QList<int> &itemDatas, QWidget *parent = nullptr);
};

#endif // COMBOLIMIT_H
