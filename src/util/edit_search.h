#ifndef EDITSEARCH_H
#define EDITSEARCH_H

#include <QLineEdit>

class EditSearch : public QLineEdit
{
    Q_OBJECT

public:
    explicit EditSearch(QWidget *parent = nullptr);

signals:
    void searchClicked();
};

#endif // EDITSEARCH_H
