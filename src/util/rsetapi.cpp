#include "rsetapi.h"
#include "util/sqlmodel.h"

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>

#include <QJsonDocument>
#include <QNetworkRequest>
#include <QIcon>
#include <QFile>
#include <QDebug>
#include <QtXml>

const static QString contentTypeJson(QLatin1String("application/json"));

RsetApi::RsetApi(Settings *settings)
    : epgsVersion("unknown")
    , isV2(false)
    , settings(settings)
    , managerAsyncThumbnail(new QNetworkAccessManager(this))
    , managerAwait(new QNetworkAccessManager(this))
    , managerAsyncStorage(new QNetworkAccessManager(this))
    , sqlModel(new SqlModel("thumbnail", "id"))
{
    // サムネイルの保存(非同期)
    connect(managerAsyncThumbnail, &QNetworkAccessManager::finished, this, [=] (QNetworkReply *reply) {
        if (reply->error() == QNetworkReply::NoError) {
            // urlからIDを拾う
            QString idStr = reply->url().toString();
            idStr.replace(serverUrl, "");
            if (isV2) {
                idStr.replace("/api/thumbnails/", "");
                idStr = sqlModel->getValue("recordedId", idStr.toInt()).toString();
            } else {
                idStr.replace("/api/recorded/", "");
                idStr.replace("/thumbnail", "");
            }
            qDebug() << "thumbnail save:" << reply->url().toString() << "recordedId:" << idStr;
            if (!idStr.isEmpty()) {
                requestThumbnail.removeOne(idStr);
                QImage img = QImage::fromData(reply->readAll(), "JPG");
                img.save(tr("%1/%2.jpg").arg(thumbnailPath, idStr));
            }
        } else {
            // Note: 録画ファイルの破損なので、サムネイル生成できないケースで
            // 永遠にUIに繰り返しエラーメッセージが表示されないようにする
            // この場合、replyのエラーメッセージのIDが実際に取得できなかったIDと異なるので
            // requestThumbnailもログに出力する
            // 視覚的には、リスト上でサムネイルが取得できていないので判別可能
            //emit netError(reply->errorString());
            qDebug() << "requestThumbnail: " << requestThumbnail << " thumbnail errMassage:" << reply->errorString();
            requestThumbnail.clear();
        }
    });
    connect(managerAsyncStorage, &QNetworkAccessManager::finished, this, [=] (QNetworkReply *reply) {
        if (reply->error() == QNetworkReply::NoError) {
            auto json = QJsonDocument::fromJson(reply->readAll()).object();
            qDebug() << json;
            if (isV2) {
                auto item = json.value("items").toArray().at(0);
                QVariantMap result;
                result["used"] = item["used"];
                result["total"] = item["total"];
                result["free"] = item["available"];
                jsonStorage = QJsonValue::fromVariant(result);
            } else {
                jsonStorage = QJsonValue::fromVariant(json);
            }
            emit updatedStorage(jsonStorage);
        }
    });
}

RsetApi::~RsetApi()
{
    delete managerAsyncThumbnail;
    delete managerAwait;
    delete managerAsyncStorage;
    delete sqlModel;
}

auto RsetApi::readEpgstation(const QString &server /*= ""*/) -> bool
{
    // 設定変更時に呼び出されるので、ここで取得する
    thumbnailPath = settings->value("WidgetRecorded/editThumbnailPath").toString();

    // サーバーへの接続確認
    auto url = server.isEmpty()? settings->value("url").toString() : server;
    auto errMsg = readEpgstationTest(url, epgsVersion);
    if (!errMsg.isEmpty())
        return false;

    // EPGStationのバージョンチェック
    // v1には、パージョンを調べるAPIが無いので、それで判定する
    isV2 = (epgsVersion != "unknown");
    settings->setValue("isV2", isV2);

    auto config = get("/api/config");
    int index = 0;
    if (isV2) {
        auto m2ts = config["streamConfig"].toObject()["live"].toObject()["ts"].toObject()["m2ts"].toArray();
        foreach (auto value, m2ts) {
            if (value.toObject()["name"] == "無変換") break;
            index++;
        }
        settings->setValue("formatVideo", "%1/api/videos/%2");
        settings->setValue("formatLive", "%1/api/streams/live/%2/m2ts?mode=" + QString::number(index));
    } else {
        foreach (auto value, config["mpegTsStreaming"].toArray()) {
            if (value.toString() == "無変換") break;
            index++;
        }
        settings->setValue("formatVideo", "%1/api/recorded/%2/file");
        settings->setValue("formatLive", "%1/api/streams/live/%2/mpegts?mode=" + QString::number(index));
    }
    return true;
}

QString RsetApi::readEpgstationTest(const QString &server, QString &version)
{
    QString result = "";
    version = "unknown";

    serverUrl = server;
    auto reply = getReplyAwait(server);
    if (reply->error() == QNetworkReply::NoError) {
        auto json = get("/api/version");
        version = json["version"].toString();
    } else {
        result = reply->errorString();
    }

    return result;
}

QStringList RsetApi::readRssTag()
{
    QStringList result;
    QString url = "https://gitlab.com/oioi555/QEPGS-Player/-/tags?format=atom";
    auto reply = getReplyAwait(url);
    QDomDocument xml;
    xml.setContent(reply->readAll());
    QDomElement entry = xml.documentElement().firstChild().toElement();
    while(!entry.isNull()) {
        if (entry.tagName() == "entry") {
            QDomElement child = entry.firstChild().toElement();
            while (!child.isNull()) {
                if (child.tagName() == "title")
                    result << child.text();
                child = child.nextSibling().toElement();
            }
        }
        entry = entry.nextSibling().toElement();
    }
    return result;
}

auto RsetApi::getUrl(const QString &uri) const -> QUrl
{
    QUrl url = QUrl(serverUrl);
    if (uri.contains("?")) {
        auto uris = uri.split("?");
        url.setPath(uris[0]);
        url.setQuery(uris[1]);
    } else {
        url.setPath(uri);
    }
    return url;
}

auto RsetApi::post(const QString &uri, const QJsonObject &jsonObj) -> QJsonObject
{
    QNetworkRequest request(getUrl(uri));
    QNetworkAccessManager manager;
    QEventLoop event;
    request.setHeader(QNetworkRequest::ContentTypeHeader, contentTypeJson);
    QNetworkReply *reply = manager.post(request, QJsonDocument(jsonObj).toJson());
    connect(reply, &QNetworkReply::finished, &event, &QEventLoop::quit);
    event.exec();

    bool error = (reply->error() != QNetworkReply::NoError);
    if (error) emit netError(reply->errorString());

    return QJsonDocument::fromJson(reply->readAll()).object();
}

auto RsetApi::put(const QString &uri, const QJsonObject &jsonObj) -> QJsonObject
{
    QNetworkRequest request(getUrl(uri));
    QNetworkAccessManager manager;
    QEventLoop event;
    request.setHeader(QNetworkRequest::ContentTypeHeader, contentTypeJson);
    QNetworkReply *reply = manager.put(request, QJsonDocument(jsonObj).toJson());
    connect(reply, &QNetworkReply::finished, &event, &QEventLoop::quit);
    event.exec();

    bool error = (reply->error() != QNetworkReply::NoError);
    if (error) emit netError(reply->errorString());

    return QJsonDocument::fromJson(reply->readAll()).object();
}

auto RsetApi::deleteResource(const QString &uri) -> QJsonObject
{
    QNetworkRequest request(getUrl(uri));
    QNetworkAccessManager manager;
    QEventLoop event;
    QNetworkReply *reply = manager.deleteResource(request);
    connect(reply, &QNetworkReply::finished, &event, &QEventLoop::quit);
    event.exec();

    bool error = (reply->error() != QNetworkReply::NoError);
    if (error) emit netError(reply->errorString());

    return QJsonDocument::fromJson(reply->readAll()).object();
}

QNetworkReply *RsetApi::getReplyAwait(const QString &uri)
{
    QUrl url = uri.contains("http")? QUrl(uri) : getUrl(uri);
    QNetworkRequest request(url);
    QEventLoop event;
    QNetworkReply *reply = managerAwait->get(request);
    connect(reply, &QNetworkReply::finished, &event, &QEventLoop::quit);
    event.exec();

    bool error = (reply->error() != QNetworkReply::NoError);
    if (error) emit netError(reply->errorString());
    return reply;
}

auto RsetApi::get(const QString &uri) -> QJsonObject
{
    auto reply = getReplyAwait(uri);
    return QJsonDocument::fromJson(reply->readAll()).object();
}

auto RsetApi::getIcon(const QString &uri) -> QIcon
{
    auto reply = getReplyAwait(uri);
    QImage img = QImage::fromData(reply->readAll(), "PNG");
    return QIcon(QPixmap::fromImage(img.convertToFormat(QImage::Format_ARGB32)));
}

auto RsetApi::gets(const QString &uri) -> QJsonArray
{
    auto reply = getReplyAwait(uri);
    return QJsonDocument::fromJson(reply->readAll()).array();
}

auto RsetApi::getThumbnail(const QString &id) -> QPixmap
{
    QPixmap pixmap;
    pixmap.load(getThumbnailPath(id));
    return pixmap;
}

auto RsetApi::getThumbnailPath(const QString &id, bool isQML /*= false*/) -> QString
{
    auto file = tr("%1/%2.jpg").arg(thumbnailPath, id);
    bool exists = QFile::exists(file);
    if (!exists) {
        // 録画リストのListViewでは背景が無いので、透過なしのjpgにする
        file = isQML? ":/images/noimage.png" : ":/images/noimage.jpg";
    }

    if (!requestThumbnail.contains(id) && !exists) {
        if (isV2) {
            auto thumbnailIds = sqlModel->getValuesIdStrs("recordedId=" + id, "id desc");
            if (thumbnailIds.count()) {
                auto uri = tr("/api/thumbnails/%1").arg(thumbnailIds[0]);
                requestThumbnail << id;
                QNetworkRequest request(getUrl(uri));
                managerAsyncThumbnail->get(request);
            }
        } else {
            auto uri = tr("/api/recorded/%1/thumbnail").arg(id);
            requestThumbnail << id;
            QNetworkRequest request(getUrl(uri));
            managerAsyncThumbnail->get(request);
        }
    }

    // QMLならプレフィックスを変える
    if (isQML) file = exists? "file:///" + file : "qrc" + file;

    // qDebug() << "isQML:" << isQML << " file:" << file;
    return file;
}

QImage RsetApi::getThumbnailTagImage(const QUrl &url)
{
    auto reply = getReplyAwait(url.toString());
    return QImage::fromData(reply->readAll()); // フォーマットは自動変換
}

void RsetApi::updateStorage()
{
    QString apiUrl = isV2? "/api/storages" : "/api/storage";
    QNetworkRequest request(getUrl(apiUrl));
    managerAsyncStorage->get(request);
}

void RsetApi::deleteRecorded(int id)
{
    deleteResource(tr("/api/recorded/%1").arg(id));
}

// v2 限定
void RsetApi::deleteVideoFile(int videoId)
{
    deleteResource(tr("/api/videos/%1").arg(videoId));
}


