#ifndef VIDEOMODEL_H
#define VIDEOMODEL_H

#include <QMetaObject>
#include <QMetaProperty>
#include <QAbstractListModel>

struct VideoElement {
    Q_GADGET
    Q_PROPERTY(int id MEMBER id)
    Q_PROPERTY(QString name MEMBER name)
    Q_PROPERTY(int recordedId MEMBER recordedId)

public:
    int id = -1;
    QString name = "";
    int recordedId = -1;
};
Q_DECLARE_METATYPE(VideoElement)

class VideoModel : public QAbstractListModel {
    Q_OBJECT
public:
    VideoModel(QObject *parent = nullptr)
        :QAbstractListModel(parent){}

    QHash<int, QByteArray> roleNames() const override {
        QHash<int, QByteArray> result;
        const auto obj = &VideoElement::staticMetaObject;
        for (int i = 0; i < obj->propertyCount(); i++)
            result.insert(Qt::UserRole + i, obj->property(i).name());
        return result;
    }
    Q_INVOKABLE int rowCount(const QModelIndex & parent = QModelIndex()) const override {
        if (parent.isValid()) return 0;
        return elements.count();
    }
    QVariant data(const QModelIndex &index, int role) const override {
        if (!hasIndex(index.row(), index.column(), index.parent()))
            return {};
        const auto obj = &VideoElement::staticMetaObject;
        const auto prop = obj->property(role - Qt::UserRole);
        const auto element = elements[index.row()];
        return prop.readOnGadget(&element);
    }
    void append(const VideoElement &element){
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        elements << element;
        endInsertRows();
    }
    void resetModel() {
        beginResetModel(); elements.clear(); endResetModel();
    }
    VideoElement getRow(const int &row) { return elements[row]; };
    void replace (const int &row, VideoElement element) {
        if (row >= 0 && row < elements.size()) {
            elements.replace(row, element);
            QModelIndex modelIndex = index(row);
            dataChanged(modelIndex, modelIndex, roleNames().keys().toVector());
        }
    }
    Q_INVOKABLE QVariant get(const int &row, const int &col) {
        auto modelIndex = this->index(row, 0, QModelIndex());
        return data(modelIndex, Qt::UserRole + col);
    }
    Q_INVOKABLE QVariant get(const int &row, const QString &roleName) {
        QVariant result = QVariant();
        const auto obj = &VideoElement::staticMetaObject;

        QByteArray ba = roleName.toLatin1();
        const char *c_str = ba.data();
        const auto col = obj->indexOfProperty(c_str);
        if (col != -1) result = get(row, col);
        return result;
    }

private:
    QList<VideoElement> elements;
};

#endif // VIDEOMODEL_H
