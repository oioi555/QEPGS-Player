﻿#include "sqlmodel.h"
#include <QDebug>
#include <QSqlTableModel>

auto SqlModel::getFrom() -> QString
{
    select = select.isEmpty() ? "*" : select;
    return hasView()? viewName : tableName;
}

auto SqlModel::getSqlSelectWhere(bool bOrderBy) -> QString
{
    QStringList sqls;
    if (group.isEmpty()) {
        sqls << getSqlSelect(false);
        sqls << getSqlWhere();
        if (bOrderBy) sqls << getSqlOrderBy();
    } else {
        // グループ指定時
        sqls << tr("SELECT %1 FROM %2 ").arg(group , getFrom());
        sqls << getSqlWhere();
        sqls << "GROUP BY " + group;
    }

    // Limit指定
    if (limitTypeData != LimitType::limitAll) {
        int offset = 0;
        if (this->limitTypeData == LimitType::limitLast) {
            int offsetLast = getRecordCount() - limitNumber;
            offset = (offsetLast)? offsetLast : 0;
        }
        sqls << tr("LIMIT %1 OFFSET %2").arg(limitNumber).arg(offset);
    }
    sqlLast = sqls.join(" ");
    return sqlLast;
}

auto SqlModel::getSqlSelect(bool bOrderBy) -> QString
{
    QStringList sqls = {tr("SELECT %1 FROM %2 ").arg(select, getFrom())};
    if (bOrderBy) sqls << getSqlOrderBy();
    sqlLast = sqls.join(" ");
    return sqlLast;
}

auto SqlModel::getSqlDelete(const QList<int> &ids) -> QString
{
    QStringList idsStr;
    foreach (auto id, ids) idsStr << QString::number(id);
    return tr("DELETE FROM %1 WHERE %2 in (%3);")
        .arg(tableName, idName, idsStr.join(","));
}

auto SqlModel::getSqlDelete(const QString &where) -> QString
{
    QString sql;
    sql = tr("DELETE FROM %1 WHERE %2;").arg(tableName, where);
    return sql;
}

auto SqlModel::getModel(SelectType type) -> QSqlQueryModel *
{
    QString sql;
    switch (type) {
    case SelectType::select:
        sql = getSqlSelect(isOrderBy()); break;
    case SelectType::selectWhere:
        sql = getSqlSelectWhere(isOrderBy()); break;
    case SelectType::lastSQL:
        sql = getSqlLast(); break;
    }

    return getModel(sql);
}

auto SqlModel::getSqlFilterGroup(const QString &sFieldName, OrderType order ,bool bColumnFilter) -> QString
{
    QStringList sqls = {tr("SELECT %1 FROM %2 ").arg(sFieldName, getFrom())};
    if (bColumnFilter) sqls << getSqlWhere();
    sqls << tr("GROUP BY %1 ORDER BY %1 %2")
            .arg(sFieldName, (order==OrderType::asc)? "ASC" : "DESC");
    return sqls.join(" ");
}

auto SqlModel::getRecordCount() -> int
{
    return getRecordCountWhere(where);
}

auto SqlModel::getRecordCountWhere(const QString &where) -> int
{
    QStringList sqls = {tr("SELECT count(*) AS records FROM %1").arg(getFrom())};
    if (!where.isEmpty()) sqls << "WHERE " + where;
    auto value = getScholar(sqls.join(" "));
    return (!value.isNull())? value.toInt() : 0;
}

auto SqlModel::getFieldNames() -> QStringList
{
    QStringList result;
    QSqlQuery query(getSqlSelect(false) + " LIMIT 1 OFFSET 0;");
    if (query.next()) {
        QSqlRecord rs = query.record();
        for (int i = 0; i < rs.count(); ++i)
            result << rs.fieldName(i);
    }
    if (query.lastError().isValid())
        emit sqlError(query.lastQuery(), query.lastError());
    return result;
}

auto SqlModel::getOneFieldValues(const QString &sql) -> QStringList
{
    QStringList result;
    QSqlQuery query(sql);
    while (query.next())
        result << query.value(0).toString();
    if (query.lastError().isValid())
        emit sqlError(query.lastQuery(), query.lastError());
    return result;
}

auto SqlModel::getScholar(const QString &sql) -> QVariant
{
    QVariant result;
    QSqlQuery query(sql);
    if (query.next())
        result = query.value(0);
    if (query.lastError().isValid())
        emit sqlError(query.lastQuery(), query.lastError());
    return result;
}

auto SqlModel::getSqlUpdateFieldsValues(int id /* =-1 */) -> QString
{
    QStringList keysValues;
    if (!fieldItems.keys().contains(idName))
        this->setValue(idName , id);
    foreach (const QString &key, fieldItems.keys())
        keysValues << key + "=" + fieldItems[key];

    QString sWhere = (id == -1)? where : tr("%1=%2").arg(idName).arg(id);
    QString result = tr("UPDATE %1 SET %2 WHERE %3")
                     .arg(tableName, keysValues.join(","), sWhere);
    return result;
}

auto SqlModel::getSqlInsertFieldsValues(int id /* =-1 */) -> QString
{
    if (id != -1 && !fieldItems.keys().contains(idName))
        this->setValue(idName , id);

    QString result = tr("INSERT INTO %1 (%2) VALUES(%3)")
                     .arg(tableName, fieldItems.keys().join(","), fieldItems.values().join(","));
    return result;
}

auto SqlModel::saveValues(int id /* =-1*/) -> int
{
    if (id == -1 || getRecordCountWhere(tr("%1=%2").arg(idName).arg(id)) == 0) {
        if (execSql(getSqlInsertFieldsValues(id)) && id == -1)
            return getInsertedId();   //登録されたIDを取得
    } else {
        execSql(getSqlUpdateFieldsValues(id));
    }
    return id;
}

auto SqlModel::getInsertedId() -> int
{
    auto sql = isPgsql()? tr("SELECT currval('%1_%2_seq')").arg(tableName, idName) : "SELECT LAST_INSERT_ID()";
    QVariant vValue = SqlModel::getScholar(sql);
    return vValue.isNull()? -1: vValue.toInt();
}

auto SqlModel::getRecord(const int id) -> QSqlRecord
{
    QSqlRecord result;
    QString sql = tr("SELECT * FROM %1 WHERE %2=%3;").arg(getFrom(), idName).arg(id);
    QSqlQuery query(sql);
    if (query.next())
        result = query.record();
    if (query.lastError().isValid())
        emit sqlError(query.lastQuery(), query.lastError());
    return result;
}

auto SqlModel::getValues(const QString &fieldName, const QString &where, const QString &order /*= ""*/) -> QStringList
{
    QStringList sqls = {tr("SELECT %1 FROM %2").arg(fieldName, getFrom())};
    if (!where.isEmpty()) sqls << "WHERE " + where;
    if (!order.isEmpty()) sqls << "ORDER BY " + order;
    return getOneFieldValues(sqls.join(" "));
}

auto SqlModel::getValuesId(const QString &where, const QString &order /*= ""*/) -> QList<int> {
    QList<int> result;
    QStringList ids = getValues(idName, where, order);
    foreach (auto idStr, ids)
        result << idStr.toInt();
    return result;
}

auto SqlModel::getValuesIdStrs(const QString &where, const QString &order /*= ""*/) -> QStringList
{
    return getValues(idName, where, order);
}

auto SqlModel::execSql(const QString &sql) -> bool
{
    QSqlQuery query;
    bool result = query.exec(sql);
    if (query.lastError().isValid())
        emit sqlError(query.lastQuery(), query.lastError());
    //qDebug() << sql;

    return result;
}

auto SqlModel::execSqlFile(const QString &filePath) -> bool {
    QFile file(filePath);
    if (file.open(QFile::ReadOnly)) {
        QString sql = QString(file.readAll());
        file.close();
        return execSql(sql);
    }
    return false;
}

void SqlModel::deleteTable(const QString &table)
{
    auto deleteName = table.isEmpty()? tableName : table;
    auto fix = isPgsql()? "RESTART IDENTITY" : "";
    execSql(tr("TRUNCATE TABLE %1 %2;").arg(deleteName, fix));
}

auto SqlModel::getSqlTableDump(const QString &table) -> QStringList
{
    auto result = QStringList();
    auto tableNameOutput = table.isEmpty()? tableName : table;
    auto model = getModel(tr("select * from %1").arg(tableNameOutput));
    if (!model->rowCount()) return result;
    setValueClear();
    for (int i = 0; i < model->rowCount(); i++) {
        auto rs = model->record(i);
        for (int col = 0; col < model->columnCount(); col++)
            setValue(rs.fieldName(col), rs.value(col));
        auto insert = tr("INSERT INTO %1 (%2) VALUES(%3);").arg(tableNameOutput,
                              fieldItems.keys().join(","),
                              fieldItems.values().join(","));
        //qDebug() << insert;
        result << insert;
    }
    return result;
}

auto SqlModel::getModel(const QString &sql) -> QSqlQueryModel *
{
    queryModel->setQuery(sql);
    if (queryModel->lastError().isValid())
        emit sqlError(sql, queryModel->lastError());
    return queryModel;
}

QList<QVariantMap> SqlModel::getModelToList(QSqlQueryModel* model)
{
    QList<QVariantMap> result;
    for (int i = 0; i < model->rowCount(); i++) {
        auto rs = model->record(i);
        QVariantMap map;
        for (int col = 0; col < model->columnCount(); col++)
            map[rs.fieldName(col)] = rs.value(col);
        result << map;
    }
    return result;
}

