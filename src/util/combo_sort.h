#ifndef COMBOSORT_H
#define COMBOSORT_H

#include <QComboBox>

struct SortElement {
public:
    QString name;
    bool oderAsc;
    QVariant data;
};

class ComboSort : public QComboBox
{
public:
    ComboSort(QWidget *parent = nullptr);

};

#endif // COMBOSORT_H
