#include "dialog_screenshot.h"
#include "ui_dialog_screenshot.h"
#include "util/tagitem.h"
#include "util/thumbnailtag.h"

#include <QDir>
#include <QFileDialog>
#include <QMimeData>
#include <QProcess>

DialogScreenShot::DialogScreenShot(int id, QWidget *parent) :
    QDialog(parent)
    , id(id)
    , imageFilePath("")
    , sqlModel(new SqlModel("recorded", "id", "v_recorded"))
    , sqlModelTag(new SqlModel("tags", "id")) // tag登録の再表示でリアルタイム参照が必要
    , main(qobject_cast<MainWindow*>(parent))
    , ui(new Ui::DialogScreenShot)
{
    ui->setupUi(this);
    fileFilterId = tr("%1-*.jpg").arg(id);
    screenshotsPath = main->getSettings()->value("WidgetPlayer/editScreenshotsPath").toString();
    loadNewestScreenShot();

    // tagの検出
    updateTagListItem();

    // ListWidgetのシングル選択
    connect(ui->listWidget->model(), &QAbstractItemModel::dataChanged, this, [=]
        (const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles) {
        Q_ASSERT(topLeft == bottomRight);
        if (!roles.contains(Qt::CheckStateRole)) return;
        if (topLeft.data(Qt::CheckStateRole) != Qt::Checked) return;
        for (int i = 0; i < ui->listWidget->model()->rowCount(); i++){
            if (i == topLeft.row())
                continue;
            ui->listWidget->item(i)->setData(Qt::CheckStateRole, Qt::Unchecked);
        }
    });

    connect(ui->btnDelete, &QPushButton::clicked, this, [=] () {
        if (!main->deleteConfirm(main)) return;
        QFile::remove(imageFilePath);
        updateImageFile();
    });
    connect(ui->btnSelectFile, &QPushButton::clicked, this, [=] () {
        QString filter = tr("この動画の画像(%1)").arg(fileFilterId);
        auto file = QFileDialog::getOpenFileName( qobject_cast<QWidget*>(main),
            tr("ファイルを開く"), screenshotsPath,
            tr("すべて(*.*);;%1").arg(filter),
            &filter,
            QFileDialog::DontUseCustomDirectoryIcons
            );
        if (!file.isEmpty()) {
            imageFilePath = file;
            updateImageFile();
        }
    });
    connect(ui->btnOpenFolder, &QPushButton::clicked, this, [=] () {
        QProcess::startDetached("xdg-open", QStringList() << screenshotsPath);
    });
    connect(ui->btnReflesh, &QPushButton::clicked, this, &DialogScreenShot::loadNewestScreenShot);

    connect(ui->btnNewTag, &QPushButton::clicked, this, [=] () {
        main->addTagEndFlag({id});
        updateTagListItem();
    });

    // 画面サイズの復元
    restoreGeometry( main->getSettings()->valueObj(this, "WindowSize").toByteArray());
}

DialogScreenShot::~DialogScreenShot()
{
    // 画面サイズの保存
    main->getSettings()->setValueObj(this, "WindowSize", saveGeometry());

    delete sqlModel;
    delete sqlModelTag;
    delete ui;
}

void DialogScreenShot::loadNewestScreenShot()
{
    // 最新のスクリーンショットを取得する
    QDir dir(screenshotsPath);
    auto files = dir.entryList({fileFilterId}, QDir::Files, QDir::Name | QDir::Reversed);
    imageFilePath = files.count()? dir.filePath(files[0]) : "";
    updateImageFile();
}

void DialogScreenShot::updateTagListItem()
{
    ui->listWidget->clear();
    QSqlRecord rs = sqlModel->getRecord(id);
    sqlModelTag->setWhere("");
    sqlModelTag->setOrderBy("field desc");
    TagItem tagItem(main);
    auto model = sqlModelTag->getModel(SelectType::selectWhere);
    for (int i = 0; i < model->rowCount(); i++) {
        const auto rsTag = model->record(i);
        auto field = rsTag.value("field").toString();
        if (rs.value(field).toString().contains(rsTag.value("tag").toString())) {
            ui->listWidget->addItem(tagItem.getListItem(rsTag, true));
        }
    }
}

void DialogScreenShot::updateImageFile()
{
    QPixmap pix = QPixmap(imageFilePath);
    ui->laImage->setPixmap(pix.scaled(ui->laImage->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
}

void DialogScreenShot::on_buttonBox_accepted()
{
    // 指定元ファイルの検査
    QFileInfo imageInfo(imageFilePath);
    if (!QFileInfo::exists(imageInfo.absoluteFilePath())) {
        QMessageBox::warning(this, "確認", "ファイルを指定してください");
        return;
    }

    // recorded キャッシュ差し替え
    if (ui->chkChangeRecordedImage->isChecked()) {
        auto thumbnailPath = main->getSettings()->value("WidgetRecorded/editThumbnailPath").toString();
        QPixmap image = QPixmap(imageInfo.absoluteFilePath());
        QPixmap saveImage = image.scaled(480,270, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
        QDir dir(thumbnailPath);
        if (!saveImage.save(dir.filePath(tr("%1.jpg").arg(id))))
            QMessageBox::warning(this, "エラー", "キャッシュファイルの差し替えに失敗しました");
    }

    // TagImage登録
    for (int i = 0; i < ui->listWidget->model()->rowCount(); i++){
        auto item = ui->listWidget->item(i);
        if (item->data(Qt::CheckStateRole) == Qt::Checked) {
            int tagid = item->data(Qt::UserRole).toInt();
            ThumbnailTag imgTag(main, tagid);
            if (!imgTag.saveImageFile(imageFilePath))
                QMessageBox::warning(this, "エラー", "検索Tagイメージ保存に失敗しました");
        }
    }
    main->setDcokInfoPlayingId(id);
    accept();
}

void DialogScreenShot::resizeEvent(QResizeEvent */*event*/)
{
    updateImageFile();
}

void DialogScreenShot::showEvent(QShowEvent */*event*/)
{
    updateImageFile();
}
