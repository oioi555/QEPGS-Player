#ifndef WIDGET_TAG_H
#define WIDGET_TAG_H

#include <QWidget>
#include "mainwindow.h"

class TagItem;
class QComboBox;
class ThumbnailTag;
class TagModel;
class QGridLayout;
class EditSearch;
class QQuickWidget;
class QToolButton;
class QLabel;
class ComboLimit;
class ComboSort;

class WidgetTag : public QWidget {
    Q_OBJECT
    Q_PROPERTY(int currentIndex MEMBER currentIndex NOTIFY currentIndexChanged)

signals:
    void currentIndexChanged(int currentIndex);
    void updateTagTable();

public:
    explicit WidgetTag(QWidget *parent = nullptr);
    ~WidgetTag();

    Q_INVOKABLE void setTagID(int id, bool reload = false);
    void setFilter(int id = -1 , const QString &field = "", const QString &genre = "");

    Q_INVOKABLE void openPlayerTag(int id) { main->openPlayerTag(id); };
    Q_INVOKABLE void actionSearchNewTab(const QString &word);
    Q_INVOKABLE void actionSearch(const QString &word);
    Q_INVOKABLE bool actionFilter(const QString &field, const QString &tag);
    Q_INVOKABLE void actionFilterNewTab(const QString &field, const QString &tag);
    Q_INVOKABLE void actionSearchWeb(const QString &word);
    Q_INVOKABLE void openTagEdit(int id);
    Q_INVOKABLE void openTagDelete(int id);
    Q_INVOKABLE void setBlackList(int id, bool checked);
    Q_INVOKABLE void openSearchWebImageDlg(int id);
    Q_INVOKABLE void openImageFileSelectDlg(int id);
    Q_INVOKABLE void deleteImageFile(int id);
    Q_INVOKABLE void saveDropUrlFile(const QString &url, int id = -1);

private slots:

private:
    void loadFilterSettings();
    void saveFilterSettings();
    void setupComboBox();
    void setCurrentGenreFlags(const QString &genre = "");
    void updateGridView();
    void blockSignals(QList<QComboBox*> &comboBoxs, bool block);
    int getIdToIndex(int id);

    SqlModel *sqlModel;
    SqlModel *sqlModelTag;
    TagModel *tagModel;
    TagItem *tagItem;
    ThumbnailTag *imgTag;

    int selectedId = -1;
    int currentIndex = -1;
    QString saveGenre = "";
    QString saveFlags = "";

    // ui
    QFrame *frameHead;
    QGridLayout* layoutHeader;
    QHBoxLayout *layoutLeft;
    QComboBox *cmbGenre;
    QComboBox *cmbField;
    QComboBox *cmbFlags;
    QHBoxLayout *layoutRight;
    EditSearch *editSearch;
    ComboSort *cmbSort;
    QLabel *laViewCount;
    ComboLimit *cmbLimit;
    QToolButton *toolReflesh;
    QQuickWidget *quickWidget;

    MainWindow* main;

    // QWidget interface
protected:
    virtual void resizeEvent(QResizeEvent *event) override;
};

#endif // WIDGET_TAG_H
