#ifndef DOCK_LIVE_H
#define DOCK_LIVE_H

#include <QDockWidget>
#include "mainwindow.h"

namespace Ui {class DockLiveTv;}
class QStandardItemModel;

class DockLive : public QDockWidget
{
    Q_OBJECT

public:
    explicit DockLive(QWidget *parent = nullptr);
    ~DockLive();

private slots:
    void on_treeView_customContextMenuRequested(const QPoint &pos);

private:
    SqlModel *sqlModel;
    QStringList checkedIds;
    QList<QAction*> needSelectActions;
    Menu *menuPrograms;
    QList<QAction*> actionPrograms;
    QStandardItemModel* treeModel;

    void loadTree(bool checked);
    void setChilds(const QString &channelType, const QString &channelName,  bool checked);
    QModelIndex getSelectedIndex();
    QString getSelectedId();
    QString getSelectedText();
    QString getId(const QModelIndex &index);


    MainWindow* main;
    Ui::DockLiveTv *ui;

    // QWidget interface
protected:
    virtual void closeEvent(QCloseEvent *event) override;
};

#endif // DOCK_LIVE_H
