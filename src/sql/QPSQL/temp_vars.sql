CREATE TABLE temp_vars (name TEXT PRIMARY KEY, value TEXT);

CREATE OR REPLACE FUNCTION put_var(key TEXT, data TEXT) RETURNS VOID AS $$
  BEGIN
    LOOP
        UPDATE temp_vars SET value = data WHERE name = key;
        IF found THEN
            RETURN;
        END IF;
        BEGIN
            INSERT INTO temp_vars(name,value) VALUES (key, data);
            RETURN;
        EXCEPTION WHEN unique_violation THEN
            -- do nothing, and loop to try the UPDATE again
        END;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_var(key TEXT) RETURNS TEXT AS $$
  DECLARE
    result TEXT;
  BEGIN
    SELECT value FROM temp_vars where name = key INTO result;
    RETURN result;
  END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION del_var(key TEXT) RETURNS VOID AS $$
  BEGIN
    DELETE FROM temp_vars WHERE name = key;
  END;
$$ LANGUAGE plpgsql;

