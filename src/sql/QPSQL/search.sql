CREATE TABLE public.search (
    id bigserial,
    date timestamp with time zone DEFAULT now() NOT NULL,
    word text NOT NULL,
    primary key(id)
);
