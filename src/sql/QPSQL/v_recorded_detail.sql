CREATE VIEW public.v_recorded_detail AS
 SELECT recorded.id,
    recorded.channeltype AS type,
    to_char(to_timestamp(((recorded.startat / 1000))::double precision), 'YYYY-MM-DD HH24:MI'::text) AS datestart,
    to_char(to_timestamp(((recorded.endat / 1000))::double precision), 'YYYY-MM-DD HH24:MI'::text) AS datestop,
    services.name AS channel,
    rules.keyword AS rules,
    recorded.name,
    recorded.description,
    recorded.extended,
    genre1.name AS genre1_name,
    genre2.name AS genre2_name,
    play.percent,
    recorded.videotype,
    recorded.filesize
    FROM public.recorded
      LEFT JOIN public.services ON recorded.channelid = services.id
      LEFT JOIN public.rules ON recorded.ruleid = rules.id
      LEFT JOIN public.play ON recorded.id = play.id
      LEFT JOIN public.genre1 ON recorded.genre1 = genre1.id
      LEFT JOIN public.genre2 ON concat(lpad(recorded.genre1::character(1)::text, 2, '0'::text), lpad(recorded.genre2::character(1)::text, 2, '0'::text)) = genre2.code::text;
