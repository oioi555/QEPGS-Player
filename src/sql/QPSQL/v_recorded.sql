CREATE VIEW public.v_recorded AS
 SELECT recorded.id,
    recorded.channeltype AS "種別",
    to_char(to_timestamp(((recorded.startat / 1000))::double precision), 'YYYY-MM-DD HH24:MI'::text) AS "録画日時",
    services.name AS "チャンネル",
    rules.keyword AS "ルール",
    genre1.name AS "ジャンル",
    genre2.name AS "サブジャンル",
    play.percent AS "再生",
    recorded.name AS "番組タイトル",
    recorded.description AS "番組詳細",
    recorded.extended AS "番組情報"
   FROM public.recorded
     LEFT JOIN public.services ON recorded.channelid = services.id
     LEFT JOIN public.rules ON recorded.ruleid = rules.id
     LEFT JOIN public.play ON recorded.id = play.id
     LEFT JOIN public.genre1 ON recorded.genre1 = genre1.id
     LEFT JOIN public.genre2 ON concat(lpad(recorded.genre1::character(1)::text, 2, '0'::text), lpad(recorded.genre2::character(1)::text, 2, '0'::text)) = genre2.code::text;
