CREATE OR REPLACE VIEW public.v_programs_detail
AS SELECT programs.id,
    programs.channelid,
    programs.channeltype AS type,
    to_char(to_timestamp((programs.startat / 1000)::double precision), 'YYYY-MM-DD HH24:MI'::text) AS datestart,
    to_char(to_timestamp((programs.endat / 1000)::double precision), 'YYYY-MM-DD HH24:MI'::text) AS datestop,
    services.name AS channel,
    programs.name,
    programs.description,
    programs.extended,
    genre1.name AS genre1_name,
    genre2.name AS genre2_name
   FROM programs
     LEFT JOIN services ON programs.channelid = services.id
     LEFT JOIN genre1 ON programs.genre1 = genre1.id
     LEFT JOIN genre2 ON concat(lpad(programs.genre1::character(1)::text, 2, '0'::text), lpad(programs.genre2::character(1)::text, 2, '0'::text)) = genre2.code::text;
