CREATE TABLE public.genre1 (
    id integer NOT NULL,
    name text,
    primary key(id)
);

INSERT INTO public.genre1(id, name)VALUES (0, 'ニュース・報道');
INSERT INTO public.genre1(id, name)VALUES (1, 'スポーツ');
INSERT INTO public.genre1(id, name)VALUES (2, '情報・ワイドショー');
INSERT INTO public.genre1(id, name)VALUES (3, 'ドラマ');
INSERT INTO public.genre1(id, name)VALUES (4, '音楽');
INSERT INTO public.genre1(id, name)VALUES (5, 'バラエティ');
INSERT INTO public.genre1(id, name)VALUES (6, '映画');
INSERT INTO public.genre1(id, name)VALUES (7, 'アニメ・特撮');
INSERT INTO public.genre1(id, name)VALUES (8, 'ドキュメンタリー・教養');
INSERT INTO public.genre1(id, name)VALUES (9, '劇場・公演');
INSERT INTO public.genre1(id, name)VALUES (10, '趣味・教育');
INSERT INTO public.genre1(id, name)VALUES (11, '福祉');
INSERT INTO public.genre1(id, name)VALUES (12, '予備');
INSERT INTO public.genre1(id, name)VALUES (13, '予備');
INSERT INTO public.genre1(id, name)VALUES (14, '拡張');
INSERT INTO public.genre1(id, name)VALUES (15, 'その他');
