CREATE TABLE public.tags
(
    id bigserial,
    tag text,
    field text,
    genre text,
    date timestamp with time zone NOT NULL DEFAULT now(),
    defaultflag boolean DEFAULT false,
    newflag boolean DEFAULT false,
    endflag boolean DEFAULT false,
    blackflag boolean DEFAULT false,
    tag_watched_count integer DEFAULT 0,
    tag_video_count integer DEFAULT 0,
    tag_is_endprogram integer DEFAULT 0,
    tag_last_update timestamp NOT NULL DEFAULT now(),
    primary key(id)
)
