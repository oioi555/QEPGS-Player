CREATE OR REPLACE FUNCTION tag_video_count(field TEXT, tag TEXT)
RETURNS INTEGER AS $$
  BEGIN
    CASE field
        WHEN '番組タイトル' THEN RETURN (SELECT count(*) FROM recorded WHERE name like '%'||tag||'%');
        WHEN 'チャンネル' THEN RETURN (SELECT count(*) FROM v_recorded WHERE チャンネル like '%'||tag||'%');
        WHEN 'ルール' THEN RETURN (SELECT count(*) FROM v_recorded WHERE ルール like '%'||tag||'%');
        WHEN 'ジャンル' THEN RETURN (SELECT count(*) FROM v_recorded WHERE ジャンル like '%'||tag||'%');
        WHEN 'サブジャンル' THEN RETURN (SELECT count(*) FROM v_recorded WHERE サブジャンル like '%'||tag||'%');
                ELSE RETURN 0;
    END CASE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION tag_watched_count(field TEXT, tag TEXT)
RETURNS INTEGER AS $$
  BEGIN
    CASE field
        WHEN '番組タイトル' THEN RETURN (SELECT count(*) FROM v_recorded WHERE 番組タイトル like '%'||tag||'%' AND 再生 > get_var('Threshold')::numeric);
        WHEN 'チャンネル' THEN RETURN (SELECT count(*) FROM v_recorded WHERE チャンネル like '%'||tag||'%' AND 再生 > get_var('Threshold')::numeric);
        WHEN 'ルール' THEN RETURN (SELECT count(*) FROM v_recorded WHERE ルール like '%'||tag||'%' AND 再生 > get_var('Threshold')::numeric);
        WHEN 'ジャンル' THEN RETURN (SELECT count(*) FROM v_recorded WHERE ジャンル like '%'||tag||'%' AND 再生 > get_var('Threshold')::numeric);
        WHEN 'サブジャンル' THEN RETURN (SELECT count(*) FROM v_recorded WHERE サブジャンル like '%'||tag||'%' AND 再生 > get_var('Threshold')::numeric);
                ELSE RETURN 0;
    END CASE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION tag_is_endprogram(field TEXT, tag TEXT, endflag BOOLEAN)
RETURNS BOOLEAN AS $$
  DECLARE
    rs_count INTEGER := 0;
  BEGIN
    IF endflag IS FALSE THEN
        RETURN FALSE;
    END IF;
    CASE field
        WHEN '番組タイトル' THEN rs_count := (SELECT count(*) FROM v_recorded WHERE 番組タイトル like '%'||tag||'%' AND 番組タイトル like '%[終]%');
        ELSE RETURN FALSE;
    END CASE;
    IF rs_count > 0 THEN
        RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION tag_last_update(field TEXT, tag TEXT)
RETURNS TEXT AS $$
  BEGIN
    CASE field
        WHEN '番組タイトル' THEN RETURN (SELECT max(録画日時) FROM v_recorded WHERE 番組タイトル like '%'||tag||'%');
        WHEN 'チャンネル' THEN RETURN (SELECT max(録画日時) FROM v_recorded WHERE チャンネル like '%'||tag||'%');
        WHEN 'ルール' THEN RETURN (SELECT max(録画日時) FROM v_recorded WHERE ルール like '%'||tag||'%');
        WHEN 'ジャンル' THEN RETURN (SELECT max(録画日時) FROM v_recorded WHERE ジャンル like '%'||tag||'%');
        WHEN 'サブジャンル' THEN RETURN (SELECT max(録画日時) FROM v_recorded WHERE サブジャンル like '%'||tag||'%');
    END CASE;
        RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

-- QEPGS-Player v1.3
-- 録画やタグが増えると大幅にパフォーマンスが落ちるので、tagsテーブル集計情報を統合
-- 都度プログラム側で集計を更新するように修正
-- なので、v_tagsは廃止にする
CREATE VIEW public.v_tags AS
SELECT id, tag, field, genre, date, defaultflag, newflag, endflag, blackflag, tag_watched_count(field, tag), tag_video_count(field, tag), tag_is_endprogram(field, tag, endflag), tag_last_update(field, tag) FROM tags
