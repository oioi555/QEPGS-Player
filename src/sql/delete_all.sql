DROP VIEW IF EXISTS v_tags;
DROP FUNCTION IF EXISTS tag_video_count;
DROP FUNCTION IF EXISTS tag_watched_count;
DROP FUNCTION IF EXISTS tag_last_update;
DROP FUNCTION IF EXISTS tag_is_endprogram;

DROP VIEW IF EXISTS v_programs_detail;
DROP VIEW IF EXISTS v_recorded_detail;
DROP VIEW IF EXISTS v_recorded;
DROP VIEW IF EXISTS v_recorded_light;

DROP FUNCTION IF EXISTS get_var;
DROP FUNCTION IF EXISTS put_var;
DROP FUNCTION IF EXISTS del_var;

DROP TABLE IF EXISTS temp_vars;
DROP TABLE IF EXISTS tags;
DROP TABLE IF EXISTS search;
DROP TABLE IF EXISTS play;
DROP TABLE IF EXISTS genre2;
DROP TABLE IF EXISTS genre1;
