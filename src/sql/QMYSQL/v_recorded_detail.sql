CREATE OR REPLACE VIEW v_recorded_detail AS
 SELECT recorded.id,
    recorded.channeltype AS type,
    DATE_FORMAT(FROM_UNIXTIME(recorded.startAt / 1000), '%Y-%m-%d %H:%i') AS datestart,
    DATE_FORMAT(FROM_UNIXTIME(recorded.endat / 1000), '%Y-%m-%d %H:%i') AS datestop,
    services.name AS channel,
    rules.keyword AS rules,
    recorded.name,
    recorded.description,
    recorded.extended,
    genre1.name AS genre1_name,
    genre2.name AS genre2_name,
    play.percent,
    recorded.videotype,
    recorded.filesize
   FROM recorded
     LEFT JOIN services ON recorded.channelid = services.id
     LEFT JOIN rules ON recorded.ruleid = rules.id
     LEFT JOIN play ON recorded.id = play.id
     LEFT JOIN genre1 ON recorded.genre1 = genre1.id
     LEFT JOIN genre2 ON concat(lpad(recorded.genre1, 2,'0'), lpad(recorded.genre2, 2, '0')) = genre2.code;
