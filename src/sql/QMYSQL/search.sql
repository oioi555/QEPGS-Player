CREATE TABLE search (
    id bigint NOT NULL AUTO_INCREMENT,
    `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    word varchar(255) NOT NULL,
    primary key(id)
);
