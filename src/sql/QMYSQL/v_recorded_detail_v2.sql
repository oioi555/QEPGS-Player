-- v1 v2 diff
-- services → channel
-- rules → rule
-- recorded.channeltype → channel.channeltype

-- QEPGS-Player v1.3
-- recorded.name を halfWidthNameへ変更
CREATE OR REPLACE VIEW v_recorded_detail AS
 SELECT recorded.id,
    channel.channeltype AS type,
    DATE_FORMAT(FROM_UNIXTIME(recorded.startAt / 1000), '%Y-%m-%d %H:%i') AS datestart,
    DATE_FORMAT(FROM_UNIXTIME(recorded.endat / 1000), '%Y-%m-%d %H:%i') AS datestop,
    channel.halfWidthName AS channel,
    rule.keyword AS rules,
    recorded.halfWidthName AS name,
    recorded.halfWidthDescription AS description,
    recorded.halfWidthExtended AS extended,
    genre1.name AS genre1_name,
    genre2.name AS genre2_name,
    play.percent,
    recorded.videotype
   FROM recorded
     LEFT JOIN channel ON recorded.channelid = channel.id
     LEFT JOIN rule ON recorded.ruleid = rule.id
     LEFT JOIN play ON recorded.id = play.id
     LEFT JOIN genre1 ON recorded.genre1 = genre1.id
     LEFT JOIN genre2 ON concat(lpad(recorded.genre1, 2, '0'), lpad(recorded.genre2, 2, '0')) = genre2.code;

