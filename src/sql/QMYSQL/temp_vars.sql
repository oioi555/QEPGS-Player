CREATE TABLE temp_vars (
    name varchar(50) NOT NULL,
    value varchar(50),
    primary key(name)
);

-- MySQLでは、PROCEDUREでないと、戻り値なしにできないが
-- Posgreでは、Functionで実装してしまった為、ダミーの戻り値を使う
-- QSqlQueryでは、DELIMITERは必要ない、付けるとエラーになる
CREATE OR REPLACE FUNCTION put_var(`key` varchar(50), `data` varchar(50)) RETURNS INT
BEGIN
    INSERT INTO temp_vars (name, value) VALUES (`key`, `data`)
    ON DUPLICATE KEY UPDATE value = VALUES (value);
    RETURN 0;
END;

CREATE OR REPLACE FUNCTION get_var(`key` varchar(50)) RETURNS varchar(50) NOT DETERMINISTIC
RETURN (SELECT value FROM temp_vars where name = `key`);

-- MySQLでは、PROCEDUREでないと、戻り値なしにできないが
-- Posgreでは、Functionで実装してしまった為、ダミーの戻り値を使う

CREATE OR REPLACE FUNCTION del_var(`key` varchar(50)) RETURNS INT
BEGIN
    DELETE FROM temp_vars WHERE name = `key`;
    RETURN 0;
END;
