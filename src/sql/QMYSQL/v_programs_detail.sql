CREATE OR REPLACE VIEW v_programs_detail
AS SELECT programs.id,
    programs.channelid,
    programs.channeltype AS type,
    DATE_FORMAT(FROM_UNIXTIME(programs.startAt / 1000), '%Y-%m-%d %H:%i') AS datestart,
    DATE_FORMAT(FROM_UNIXTIME(programs.endat / 1000), '%Y-%m-%d %H:%i') AS datestop,
    services.name AS channel,
    programs.name,
    programs.description,
    programs.extended,
    genre1.name AS genre1_name,
    genre2.name AS genre2_name
   FROM programs
     LEFT JOIN services ON programs.channelid = services.id
     LEFT JOIN genre1 ON programs.genre1 = genre1.id
     LEFT JOIN genre2 ON concat(lpad(programs.genre1, 2,'0'), lpad(programs.genre2, 2, '0')) = genre2.code;
