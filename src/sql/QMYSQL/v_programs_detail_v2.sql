-- v1 v2 diff
-- programs → program

CREATE OR REPLACE VIEW v_programs_detail
AS SELECT program.id,
    program.channelid,
    program.channeltype AS type,
    DATE_FORMAT(FROM_UNIXTIME(program.startAt / 1000), '%Y-%m-%d %H:%i') AS datestart,
    DATE_FORMAT(FROM_UNIXTIME(program.endat / 1000), '%Y-%m-%d %H:%i') AS datestop,
    channel.name AS channel,
    program.name,
    program.description,
    program.extended,
    genre1.name AS genre1_name,
    genre2.name AS genre2_name
   FROM program
     LEFT JOIN channel ON program.channelid = channel.id
     LEFT JOIN genre1 ON program.genre1 = genre1.id
     LEFT JOIN genre2 ON concat(lpad(program.genre1, 2,'0'), lpad(program.genre2, 2, '0')) = genre2.code;
