CREATE OR REPLACE VIEW v_recorded AS
 SELECT recorded.id,
    recorded.channeltype AS "種別",
    DATE_FORMAT(FROM_UNIXTIME(recorded.startAt / 1000), '%Y-%m-%d %H:%i') AS 録画日時,
    services.name AS "チャンネル",
    rules.keyword AS "ルール",
    genre1.name AS "ジャンル",
    genre2.name AS "サブジャンル",
    play.percent AS "再生",
    recorded.name AS "番組タイトル",
    recorded.description AS "番組詳細",
    recorded.extended AS "番組情報"
   FROM recorded
     LEFT JOIN services ON recorded.channelid = services.id
     LEFT JOIN rules ON recorded.ruleid = rules.id
     LEFT JOIN play ON recorded.id = play.id
     LEFT JOIN genre1 ON recorded.genre1 = genre1.id
     LEFT JOIN genre2 ON concat(lpad(recorded.genre1, 2,'0'), lpad(recorded.genre2, 2, '0')) = genre2.code;

-- v1.2 tagの計算用に新たに追加
CREATE OR REPLACE VIEW v_recorded AS
 SELECT recorded.id,
    recorded.startAt,
    services.name AS "チャンネル",
    rules.keyword AS "ルール",
    genre1.name AS "ジャンル",
    genre2.name AS "サブジャンル",
    play.percent AS "再生",
    recorded.name AS "番組タイトル"
   FROM recorded
     LEFT JOIN services ON recorded.channelid = services.id
     LEFT JOIN rules ON recorded.ruleid = rules.id
     LEFT JOIN play ON recorded.id = play.id
     LEFT JOIN genre1 ON recorded.genre1 = genre1.id
     LEFT JOIN genre2 ON concat(lpad(recorded.genre1, 2,'0'), lpad(recorded.genre2, 2, '0')) = genre2.code;
