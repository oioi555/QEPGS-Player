-- QEPGS-Player v1.2 v_recoredのビデオファイルフィールド追加に伴い負荷軽減のための v_recorded_light 変更

CREATE OR REPLACE FUNCTION tag_watched_count(field TEXT, tag TEXT) RETURNS INT NOT DETERMINISTIC
RETURN CASE field
    WHEN '番組タイトル' THEN (SELECT count(id) FROM v_recorded_light WHERE 番組タイトル like CONCAT('%', tag, '%') AND 再生 > CAST(get_var('Threshold') AS INT))
    WHEN 'チャンネル' THEN (SELECT count(id) FROM v_recorded_light WHERE チャンネル like CONCAT('%', tag, '%') AND 再生 > CAST(get_var('Threshold') AS INT))
    WHEN 'ルール' THEN (SELECT count(id) FROM v_recorded_light WHERE ルール like CONCAT('%', tag, '%') AND 再生 > CAST(get_var('Threshold') AS INT))
    WHEN 'ジャンル' THEN (SELECT count(id) FROM v_recorded_light WHERE ジャンル like CONCAT('%', tag, '%') AND 再生 > CAST(get_var('Threshold') AS INT))
    WHEN 'サブジャンル' THEN (SELECT count(id) FROM v_recorded_light WHERE サブジャンル like CONCAT('%', tag, '%') AND 再生 > CAST(get_var('Threshold') AS INT))
    ELSE 0
END;

CREATE OR REPLACE FUNCTION tag_video_count(field TEXT, tag TEXT) RETURNS INT NOT DETERMINISTIC
RETURN CASE field
    WHEN '番組タイトル' THEN (SELECT count(id) FROM v_recorded_light WHERE 番組タイトル like CONCAT('%', tag, '%'))
    WHEN 'チャンネル' THEN (SELECT count(id) FROM v_recorded_light WHERE チャンネル like CONCAT('%', tag, '%'))
    WHEN 'ルール' THEN (SELECT count(id) FROM v_recorded_light WHERE ルール like CONCAT('%', tag, '%'))
    WHEN 'ジャンル' THEN (SELECT count(id) FROM v_recorded_light WHERE ジャンル like CONCAT('%', tag, '%'))
    WHEN 'サブジャンル' THEN (SELECT count(id) FROM v_recorded_light WHERE サブジャンル like CONCAT('%', tag, '%'))
    ELSE 0
END;

-- max()をODER BY LIMITへ若干負荷軽減
CREATE OR REPLACE FUNCTION tag_last_update(field TEXT, tag TEXT) RETURNS varchar(50) NOT DETERMINISTIC
RETURN CASE field
    WHEN '番組タイトル' THEN (SELECT date_format(from_unixtime(startAt / 1000), '%Y-%m-%d %H:%i') FROM v_recorded_light WHERE 番組タイトル like CONCAT('%', tag, '%') ORDER BY startAt DESC LIMIT 1)
    WHEN 'チャンネル' THEN (SELECT date_format(from_unixtime(startAt / 1000), '%Y-%m-%d %H:%i') FROM v_recorded_light WHERE チャンネル like CONCAT('%', tag, '%') ORDER BY startAt DESC LIMIT 1)
    WHEN 'ルール' THEN (SELECT date_format(from_unixtime(startAt / 1000), '%Y-%m-%d %H:%i') FROM v_recorded_light WHERE ルール like CONCAT('%', tag, '%') ORDER BY startAt DESC LIMIT 1)
    WHEN 'ジャンル' THEN (SELECT date_format(from_unixtime(startAt / 1000), '%Y-%m-%d %H:%i') FROM v_recorded_light WHERE ジャンル like CONCAT('%', tag, '%') ORDER BY startAt DESC LIMIT 1)
    WHEN 'サブジャンル' THEN (SELECT date_format(from_unixtime(startAt / 1000), '%Y-%m-%d %H:%i') FROM v_recorded_light WHERE サブジャンル like CONCAT('%', tag, '%') ORDER BY startAt DESC LIMIT 1)
    ELSE ""
END;

-- QSqlQueryでは、DELIMITERは必要ない、付けるとエラーになる
CREATE OR REPLACE FUNCTION tag_is_endprogram(field TEXT, tag TEXT, endflag BOOLEAN) RETURNS BOOLEAN  NOT DETERMINISTIC
BEGIN
    DECLARE rs_count INTEGER DEFAULT 0;
    IF endflag IS FALSE THEN
        RETURN FALSE;
    END IF;
    CASE field
        WHEN '番組タイトル' THEN
            SELECT count(id) INTO rs_count FROM v_recorded_light WHERE 番組タイトル like CONCAT('%', tag, '%') AND 番組タイトル like '%[終]%';
        ELSE RETURN FALSE;
    END CASE;
    IF rs_count > 0 THEN
        RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;
END;

-- QEPGS-Player v1.3
-- 録画やタグが増えると大幅にパフォーマンスが落ちるので、tagsテーブル集計情報を統合
-- 都度プログラム側で集計を更新するように修正
-- なので、v_tagsは廃止にする
--CREATE OR REPLACE VIEW v_tags AS
--SELECT id, tag, field, genre, date, defaultflag, newflag, endflag, blackflag,
--    tag_watched_count(field, tag) AS tag_watched_count,
--    tag_video_count(field, tag) AS tag_video_count,
--    tag_is_endprogram(field, tag, endflag) AS tag_is_endprogram,
--    tag_last_update(field, tag) AS tag_last_update
--FROM tags
