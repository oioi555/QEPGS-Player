-- v1 v2 diff
-- services → channel
-- rules → rule
-- recorded.channeltype → channel.channeltype

-- QEPGS-Player v1.2
-- ビデオファイルフィールド追加
-- QEPGS-Player v1.3
-- recorded.name を halfWidthNameへ変更
CREATE OR REPLACE VIEW v_recorded AS
 SELECT recorded.id,
    channel.channeltype AS "種別",
    DATE_FORMAT(FROM_UNIXTIME(recorded.startAt / 1000), '%Y-%m-%d %H:%i') AS 録画日時,
    channel.halfWidthName AS "チャンネル",
    rule.keyword AS "ルール",
    genre1.name AS "ジャンル",
    genre2.name AS "サブジャンル",
    play.percent AS "再生",
    recorded.halfWidthName AS "番組タイトル",
    recorded.halfWidthDescription AS "番組詳細",
    recorded.halfWidthExtended AS "番組情報",
    GROUP_CONCAT(video_file.name SEPARATOR ', ') AS "ビデオファイル"
   FROM recorded
     LEFT JOIN channel ON recorded.channelid = channel.id
     LEFT JOIN rule ON recorded.ruleid = rule.id
     LEFT JOIN play ON recorded.id = play.id
     LEFT JOIN genre1 ON recorded.genre1 = genre1.id
     LEFT JOIN genre2 ON concat(lpad(recorded.genre1, 2, '0'), lpad(recorded.genre2, 2, '0')) = genre2.code
     LEFT JOIN video_file ON recorded.id = video_file.recordedId GROUP BY video_file.recordedId;

-- v1.2 tagの計算用に新たに追加
CREATE OR REPLACE VIEW v_recorded_light AS
 SELECT recorded.id,
    recorded.startAt,
    channel.halfWidthName AS "チャンネル",
    rule.keyword AS "ルール",
    genre1.name AS "ジャンル",
    genre2.name AS "サブジャンル",
    play.percent AS "再生",
    recorded.halfWidthName AS "番組タイトル"
   FROM recorded
     LEFT JOIN channel ON recorded.channelid = channel.id
     LEFT JOIN rule ON recorded.ruleid = rule.id
     LEFT JOIN play ON recorded.id = play.id
     LEFT JOIN genre1 ON recorded.genre1 = genre1.id
     LEFT JOIN genre2 ON concat(lpad(recorded.genre1, 2, '0'), lpad(recorded.genre2, 2, '0')) = genre2.code;
