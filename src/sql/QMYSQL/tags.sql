CREATE TABLE tags (
    id bigint NOT NULL AUTO_INCREMENT,
    tag text,
    field text,
    genre text,
    `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    defaultflag boolean DEFAULT false,
    newflag boolean DEFAULT false,
    endflag boolean DEFAULT false,
    blackflag boolean DEFAULT false,
    tag_watched_count integer DEFAULT 0,
    tag_video_count integer DEFAULT 0,
    tag_is_endprogram integer DEFAULT 0,
    tag_last_update timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    primary key(id)
)
