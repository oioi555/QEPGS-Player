#ifndef DIALOG_ENCORD_H
#define DIALOG_ENCORD_H

#include <QDialog>
#include "util/tableView_styleditemdelegate.h"

namespace Ui {
class DialogEncord;
}

class EncordDelegate : public TableViewQStyledItemDelegate
{
    Q_OBJECT

public:
    explicit EncordDelegate(QObject *parent = nullptr): TableViewQStyledItemDelegate(parent) {}
    ~EncordDelegate(){}

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        QStyleOptionViewItem optCopy = option;
        QVariant value = index.model()->data(index, Qt::DisplayRole);
        auto columnName = index.model()->headerData(index.column(), Qt::Horizontal).toString();

        if (columnName == "番組タイトル") {
            paintTextToHtml(getNameHtml(value.toString()), painter, option); return;
        }
        if (columnName == "ビデオファイル") {
            paintVideoType(value.toString(), painter, option); return;
        }
        QStyledItemDelegate::paint(painter, optCopy, index);
    }
};

class DialogEncord : public QDialog
{
    Q_OBJECT

public:
    explicit DialogEncord(const QList<int> &ids, QWidget *parent = nullptr);
    ~DialogEncord();

private slots:
    void on_btnDeleteEncVideo_clicked();
    void on_btnDeleteTsLeaveEncVideo_clicked();
    void on_buttonBox_accepted();

private:
    bool isMulti() { return ids.count() > 1; }
    QString getIdsSql(const QString &addWhere = "");
    void updateCmbSourceItems();
    void cmbSourceCurrentIndexChanged();
    void showAdjustTab();
    void deleteVideoConfirm(QList<int> deleteVideoIds);
    void deleteEncVideos(bool isLeaveFirst);

    QList<int> ids;
    QList<int> encIds;
    SqlModel *sqlModel;
    SqlModel *sqlModelIds;
    SqlModel *sqlModelVideo;
    MainWindow* main;
    EncordDelegate *delegate;
    Ui::DialogEncord *ui;
};

#endif // DIALOG_ENCORD_H
