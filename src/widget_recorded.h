#ifndef WIDGET_RECORDED_H
#define WIDGET_RECORDED_H

#include <QAction>
#include <QWidget>
#include <QFile>
#include "util/tableView_styleditemdelegate.h"

namespace Ui {class WidgetRecorded;}

class TableViewDelegate : public TableViewQStyledItemDelegate
{
    Q_OBJECT

public:
    explicit TableViewDelegate(QObject *parent = nullptr): TableViewQStyledItemDelegate(parent) {}
    ~TableViewDelegate(){}

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        QStyleOptionViewItem optCopy = option;
        QVariant value = index.model()->data(index, Qt::DisplayRole);
        auto columnName = index.model()->headerData(index.column(), Qt::Horizontal).toString();

        if (columnName == "id") {
            if (paintThumbnaill(value.toString(), painter, option)) return;
        }
        if (columnName == "録画日時") {
            auto id = index.model()->data(index.siblingAtColumn(0), Qt::DisplayRole).toString();
            if (paintRecordingDateTime(id, value.toString(), painter, option)) return;
        }
        if (columnName == "再生") {
            paintProgress(value.toInt(), painter, option); return;
        }
        if (columnName == "番組タイトル") {
            paintTextToHtml(getNameHtml(value.toString()), painter, option); return;
        }
        if (columnName == "ビデオファイル") {
            paintVideoType(value.toString(), painter, option); return;
        }
        QStyledItemDelegate::paint(painter, optCopy, index);
    }
};

class WidgetRecorded : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetRecorded(QWidget *parent = nullptr);
    ~WidgetRecorded();

    void setSearch(const QString &text);
    bool setFilter(const QString &field, const QString &tag, const int &selectID = -1);
    void resetFiler();
    bool isPlaylist() {return playlistView;}
    bool openPlaylist(const QString &file);

private:
    void addSearchMenu(const QString &text);
    void updateSearchMenu();
    static QString getTextTrim(const QString &text);
    void updateWindowTitle();
    void updateRowHeight();
    QString anchorAt(const QPoint &pos) const;
    int getTagId(int recordedId);
    bool playlistView;
    QString playlistFileName;
    QMenu *menuSearch;
    QList<QAction*> searchActions;
    QList<QAction*> needSelectActions;
    QString titleFilter;
    QString lastHoveredAnchor;
    bool isInitialize = false;

    SqlModel *sqlModel;
    SqlModel *sqlModelSearch;
    SqlModel *sqlModelTag;
    MainWindow* main;
    TableViewDelegate *delegate;
    Ui::WidgetRecorded *ui;

private slots:
    void on_playerStatusChanged(PlayerStatus status, int id);
    void on_mainStatusChanged(MainStatus status);
    void on_tableView_customContextMenuRequested(const QPoint &pos);

    // QWidget interface
protected:
    virtual void showEvent(QShowEvent *event) override;
};

#endif // WIDGET_RECORDED_H
