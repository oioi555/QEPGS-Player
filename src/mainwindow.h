#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProxyStyle>
#include <QMessageBox>

#include "util/base_mainwindow.h"
#include "util/recmodel.h"
#include "util/tagtabel.h"
#include "util/sqlmodel.h"
#include "util/popup.h"
#include "util/rsetapi.h"
#include "util/playlist.h"
#include "util/menu.h" // 各クラスで使う


static const QString releasesUrl = "https://gitlab.com/oioi555/QEPGS-Player/-/releases";
static const QStringList cmbfieldsList = QStringList() << "番組タイトル" << "チャンネル" << "ルール" << "ジャンル" << "サブジャンル" ;
static const QString nullStr = "任意";
static const QString dateShortFormat = "MM/dd (ddd) hh:mm";
static const QString htmlFormat = "<html><head><meta charset='utf-8'><style>%1</style></head><body>%2</body></html>";

class DockInfo;
class DockLive;
class DockTag;
class DockPlaylist;
class WidgetPlayer;
class WidgetRecorded;
class WidgetEPGStation;
class WidgetTag;
class QDBusInterface;
class QDBusConnection;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

enum class MainStatus {
    Database,
    VideoDeleted,
    FullScreen,
    MiniScreen,
    Resize,
    Move,
    DockInfo,
    TagTabel
};

enum class PlayerStatus {
    PlayerClose,
    PlayerSeek,
    PlayerEnd,
    ControlBarHide,
    ControlBarShow
};

enum class DockNaviType {
    Tag,
    Live,
    Playlist
};

enum class SettingMenu {
    EPGStation,
    Database,
    General,
    Player,
    Recorded,
    Tag,
    Playlist,
    LiveTV,
    SimpleView,
    Version
};

enum class InfoType {
    Overview,
    Recorded,
    Live,
    Tag,
    Playlist
};

// tooltip custom
class ProxyStyle : public QProxyStyle
{
public:
    int styleHint(StyleHint hint, const QStyleOption *option, const QWidget *widget, QStyleHintReturn *returnData) const override {
        if (hint == QStyle::SH_ToolTip_WakeUpDelay)
            return 90; // ツールチップ待ち時間ms
        if (hint == QStyle::SH_ToolTip_FallAsleepDelay)
            return 90;
        if (hint == QStyle::SH_ToolTipLabel_Opacity)
            return int((double(toolTipOpacity) / 100) * 255);
        return QProxyStyle::styleHint(hint, option, widget, returnData);
    }
    int toolTipOpacity = 90;
};

class MainWindow : public BaseMainWindow
{
    Q_OBJECT

private:
    // setting
    void readBaceSetting();
    void readStatusSettings();
    void writeStatusSettings();

    // screen
    void setForcedWindowView(bool enable);
    void updateMultiPlayWindowSize();
    bool miniScreen = false;
    bool miniScreenFrameless = false;
    bool moveEventBlock = false;
    bool pinEnable = false;
    bool multiPlayer = false;

    // dock
    DockInfo *dockInfo;
    DockLive *dockLive;
    DockTag *dockTag;
    DockPlaylist *dockPlaylist;

    QList<QDockWidget*> docks;
    bool dockInfoPrevState; // simpleView以前の状態保存
    bool dockNaviPrevState; // simpleView以前の状態保存
    int saveTabIndex;       // DockTabの表示・非表示の切替時にActiveタブの復元に使用
    QTabBar* getDockTabBarIndex(QDockWidget *dock,int &index);
    QTabBar* getDockTabBar(QDockWidget *dock) {
        int index = -1;
        return getDockTabBarIndex(dock, index);
    }

    // MPRIS;
    bool mpris;

    // sleep
    QDBusInterface * sleepBbusInterface = nullptr;
    QList<uint> sleepCookies;

    // db
    QSqlError openDbConnection(const QString &driver, const QString &dbName, const QString &host,
        const QString &user, const QString &pass, int port = -1);
    bool openDB();
    bool dbPrevConnect;
    SqlModel *sqlModel;
    void buildDbExtended(bool isRestore = false);

    // encord
    QList<RecElement> encordingItems;
    RecElement parseEncordItem(const QJsonValue &item);

    // other
    double versionCast(const QString &versionStr);
    RsetApi *rsetapi;
    PopUp *pop;
    TagTable *tagTable;
    QStringList listRegForward;     // 番組名抽出設定
    QStringList listRegBackward;    // 番組名抽出設定

    Ui::MainWindow *ui;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

    // RsetApi
    RsetApi* getApi() {return rsetapi;}

    // settings dialog
    void openSettings(SettingMenu menu = SettingMenu::General, int tabIndex = -1);
    void openSettings(const QString &name, int tabIndex = -1);

    // popup
    PopUp *getPop() {return pop;}

    // SimpleView
    void setSimpleView(bool enable);
    QAction *getSimpleViewAction();
    void setMiniScreenView(bool enable);
    void setMiniScreenFrameless(bool enable);
    void setFullScreenView(bool enable);
    bool isMiniScreen() { return miniScreen; }
    bool isMiniScreenFrameless() { return miniScreenFrameless; }
    void setPin(bool enable) { pinEnable = enable; emit pinChanged(pinEnable); }
    bool isPin() { return pinEnable; }

    // player
    void openPlayer(const PlayItem &item);
    void openPlayer(int recordedId);
    void openPlayerLive(const QString &channelId, const QString &title);
    void openPlayerVideoId(int videoId);

    void openPlayerRecordedIds(QList<int> ids, int selected = -1);
    void openPlayerTag(int tagId, int selected = -1);
    void openPlayerFile(const QString &playListFile);

    WidgetPlayer* getPlayer();
    WidgetPlayer* getPlayerActive();
    WidgetPlayer* getPlayerNew();
    QList<WidgetPlayer*> getPlayerAll();
    Playlist *getActivePlayerPlaylist();
    bool isPlaying();
    bool IsMultiPlayer() { return multiPlayer; }

    // sleep
    void inhibitSleep(bool isInhibit);
    void updateInhibitSleep();

    // MPRIS;
    void setupMprisDbusConnection(WidgetPlayer *player = nullptr);
    void sendMprisPropertiesChanged(const QObject* adaptor, const QVariantMap& properties);

    // dock info
    void setDockInfoId(InfoType type, const QString &idStr, const QString &searchWord = "");
    void setDcokInfoPlayingId(int id);
    void setDockInfoState(bool enable, bool savePrevState = true);
    QAction *getDockInfoAction();
    DockInfo* getDockInfo() {return dockInfo;}

    // dock navi
    void setActiveDockTab(DockNaviType type);
    void setDockNaviState(bool enable, bool savePrevState = true);
    bool getDockNaviState();
    QAction *getDockNaviAction();

    // tag (dock)
    static QStringList parseStr(const QString &value);
    QString parseStrTitle(const QString &value);

    void updateRegSetting();
    void openTagAddDlg(int id);
    bool openTagEditDlg(int id);
    bool openTagEditDlg(const QString &field, const QString &genre);
    void openTagDelete(int id);
    bool openTagThumbnail(int id);
    void addTagEndFlag(const QList<int> &ids);
    void updateTag(int id = -1);
    void selectTag(int id);
    void setSearchTag(const QString &tag);
    bool setFilterTag(const QString &field, const QString &tag, const int &selectID = -1);
    void searchWeb(const QString &word);
    void setBlackList(int id, bool checked);

    // playlist
    void updatePlaylist(const QString &selectFile = "");

    // redorded
    WidgetRecorded* openRecorded();
    WidgetRecorded* getRecorded();
    void openPlaylist(const QString &file);
    bool deleteRecorded(const QList<int> &ids);
    void deleteParsent(const QList<int> &ids);
    void setFullParsent(const QList<int> &ids);

    // epgstation
    void openEPGStation(const QString &path = "");
    void openEPGProgram(const QString &type = "");
    void openEPGChannel(const QString &channelId);
    WidgetEPGStation* getEPGStation();

    // tag (widget)
    void openTagWidget(int id = -1, const QString &field = "", const QString &genre = "" );
    WidgetTag *getTagWidget();
    void updateTagWidget() {
        emit updateGridView();
    }

    // tag tempdb
    void updateTagTable() {
        tagTable->updateTable();
        emit mainStatusChanged(MainStatus::TagTabel);
    }
    void updateTagTable(int id) {
        tagTable->updateTag(id);
        emit mainStatusChanged(MainStatus::TagTabel);
    }
    void updateTagTableRecorded(int id) {
        tagTable->updateRecorded(id);
        emit mainStatusChanged(MainStatus::TagTabel);
    }
    void updateTagTableRecorded(const QList<int> &ids) {
        tagTable->updateRecorded(ids);
        emit mainStatusChanged(MainStatus::TagTabel);
    }
    void updateTagTableFull() {
        tagTable->updateTagFull();
    }
    bool isTagTableInitUpdate() { return tagTable->isInitUpdate(); }

    // encord
    bool openEncordDlg(const QList<int> &ids);
    bool openEncordDlg(int id);
    bool openEncordDlg(const QString &field, const QString &tag);
    bool cancelEncord();
    bool cancelEncord(const int &encId);
    bool cancelEncord(const QList<int> &ids);
    bool cancelEncord(const QString &field, const QString &tag);
    int updateEncordingList();
    QList<RecElement> getEncordingItems() { return encordingItems; }
    RecElement getEncordElement(int id);

    // other
    static QString getFileSizeStr(double size);

signals:
    void mainStatusChanged(MainStatus, const QVariant &event = QVariant());
    void playerStatusChanged(PlayerStatus, int id);
    void updateGridView();
    void pinChanged(bool pin);
    void mprisActivePlayerChanged();
    void mprisNoPlayer();

public slots:
    void onSqlError(const QString &sql , const QSqlError &lastError);

private slots:
    void on_mdiArea_customContextMenuRequested(const QPoint &pos);

    // QWidget interface
protected:
    virtual void closeEvent(QCloseEvent *event) override;

    // QObject interface
public:
    virtual bool event(QEvent *event) override;

};
#endif // MAINWINDOW_H
