/**
 * MPRIS D-Bus Interface 2.2
 * https://specifications.freedesktop.org/mpris-spec/latest/index.html
 *
 * Interface MediaPlayer2
 * https://specifications.freedesktop.org/mpris-spec/latest/Media_Player.html
*/

#ifndef DBUS_MEDIAPLAYER2_H
#define DBUS_MEDIAPLAYER2_H

#include <QObject>
#include <QDBusAbstractAdaptor>
#include <QDBusObjectPath>

class MainWindow;
class WidgetPlayer;

class DBusMediaPlayer2 : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.mpris.MediaPlayer2")

    Q_PROPERTY(bool CanQuit READ CanQuit);
    Q_PROPERTY(bool Fullscreen READ Fullscreen WRITE setFullscreen)
    Q_PROPERTY(bool CanSetFullscreen READ CanSetFullscreen)
    Q_PROPERTY(bool CanRaise READ CanRaise)
    Q_PROPERTY(bool HasTrackList READ HasTrackList)
    Q_PROPERTY(QString Identity READ Identity)
    Q_PROPERTY(QString DesktopEntry READ DesktopEntry)
    Q_PROPERTY(QStringList SupportedUriSchemes READ SupportedUriSchemes)
    Q_PROPERTY(QStringList SupportedMimeTypes READ SupportedMimeTypes)

public:
    explicit DBusMediaPlayer2(WidgetPlayer *player, QObject *parent);

public slots:
    void Raise();
    void Quit();

private:
    bool CanQuit() const;
    bool Fullscreen() const;
    void setFullscreen(bool isFullscreen) const;
    bool CanSetFullscreen() const;
    bool CanRaise() const;
    bool HasTrackList() const;
    QString Identity() const;
    QString DesktopEntry() const;
    QStringList SupportedUriSchemes() const;
    QStringList SupportedMimeTypes() const;

    QVariantMap getDefaultProperties();
    MainWindow* main;
    WidgetPlayer* widgetPlayer;
};

#endif // DBUS_MEDIAPLAYER2_H
