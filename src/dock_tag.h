#ifndef DOCK_TAG_H
#define DOCK_TAG_H

#include <QDockWidget>
#include "mainwindow.h"
#include "util/tagitem.h"

class QStandardItemModel;

namespace Ui {class DockTag;}

class DockTag : public QDockWidget
{
    Q_OBJECT

public:
    explicit DockTag(QWidget *parent = nullptr);
    ~DockTag() override;

    void showTags(int id = -1);
    void selectTag(int id);
    void setUpdateAfterSelectId(int id) {
        saveSelectedId = id;
    }
private:
    SqlModel *sqlModel;
    SqlModel *sqlModelTag;
    bool updateFlag;
    QList<QAction*> needSelectActions;
    QStringList expandeds;
    int saveSelectedId = -1;
    QStandardItemModel* treeModel;
    MainWindow* main;

    TagItem *getTagItem(const QModelIndex &index);
    int getSelectedId();
    TagItem* getSelectedTagItem();
    QModelIndex getTagIdIndex(int id);
    void setRoot(const QString &field);
    void setChilds(QStandardItem* itemParent, const QString &where);
    void openTagDeleteGroup(const QString &goupName, const QList<TagItem*> &tagItems);
    void autoSize();
    Ui::DockTag *ui;

signals:
    void dragChanged(const QMimeData *mimeData = nullptr);

private slots:
    void showContextMenu( const QPoint & pos );

public:
    virtual bool event(QEvent *event) override;

    // QWidget interface
protected:
    virtual void closeEvent(QCloseEvent *event) override;
};

#endif // DOCK_TAG_H
