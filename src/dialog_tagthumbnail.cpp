#include "dialog_tagthumbnail.h"
#include "ui_dialog_tagthumbnail.h"
#include "util/thumbnailtag.h"

#include <QWebEngineProfile>

// 固定ダウンロードファイル名
const QString tempDownloadImageFileName = "tempDownloadImage.jpg";

DialogTagThumbnail::DialogTagThumbnail(int id, QWidget *parent) :
    QDialog(parent)
    , id(id)
    , sqlModel(new SqlModel("tags" , "id"))
    , main(qobject_cast<MainWindow*>(parent))
    , ui(new Ui::DialogTagThumbnail)
{
    ui->setupUi(this);

    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    setWindowTitle("Tagサムネイルダウンロード");
    setWindowIcon(main->getIcon("download"));

    ui->webEngineView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->webEngineView, &QWebEngineView::loadStarted, this, [=] () {
        QApplication::setOverrideCursor(Qt::WaitCursor);
    });
    connect(ui->webEngineView, &QWebEngineView::loadFinished, this, [=] () {
        QApplication::restoreOverrideCursor();
    });
    #if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
    connect(QWebEngineProfile::defaultProfile(), &QWebEngineProfile::downloadRequested, this, [=] (QWebEngineDownloadRequest *download) {
    #else
    connect(QWebEngineProfile::defaultProfile(), &QWebEngineProfile::downloadRequested, this, [=] (QWebEngineDownloadItem *download) {
    #endif
    #if QT_VERSION > QT_VERSION_CHECK(5, 14, 0)
        #if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
        connect(download, &QWebEngineDownloadRequest::isFinishedChanged, this, [=] () {
        #else
        connect(download, &QWebEngineDownloadItem::finished, this, [=] () {
        #endif
            QString filePath = QDir(main->getAppDataPath()).filePath(download->downloadFileName());
            if (getSuffix(filePath) != "jpg") return;
            if (imgTag->saveImageFile(filePath))
                accept();
            else
                QMessageBox::information(this, tr("画像ダウンロード"), tr("画像ファイルの変換に失敗しました"));
        });
        download->setDownloadDirectory(main->getAppDataPath());   // qt5.14から
        download->setDownloadFileName(tempDownloadImageFileName); // qt5.14から
        download->accept(); // ダウンロード許可
    #else
        connect(download, &QWebEngineDownloadItem::finished, this, [=] () {
            QString filePath = download->path();
            if (getSuffix(filePath) != "jpg") return;
            if (imgTag->saveImageFile(filePath))
                accept();
            else
                QMessageBox::information(this, tr("画像ダウンロード"), tr("画像ファイルの変換に失敗しました"));
        });
        download->setSavePageFormat(QWebEngineDownloadItem::SingleHtmlSaveFormat);
        download->setPath(QDir(main->getAppDataPath()).filePath(tempDownloadImageFileName));
        download->accept(); // ダウンロード許可
    #endif
    });

    // 検索サイトの定義
    searchSite = QStringList {
        "https://www.google.co.jp/search?q=%1&tbm=isch",
        "https://duckduckgo.com?q=%1&iar=images&iaf=layout:Wide"
    };
    ui->cmbSearchSite->setCurrentIndex(main->getSettings()->valueObj(this, "cmbSearchSite", 0).toInt());
    connect(ui->cmbSearchSite, &QComboBox::currentTextChanged, this, [=] () {
        int index = ui->cmbSearchSite->currentIndex();
        load(searchSite[index]);
        main->getSettings()->setValueObj(this, "cmbSearchSite", index);
    });

    load(searchSite[ui->cmbSearchSite->currentIndex()]);

    // Thumbnail
    imgTag = new ThumbnailTag(main, id);

    // 画面サイズの復元
    restoreGeometry( main->getSettings()->valueObj(this, "WindowSize").toByteArray());
}

DialogTagThumbnail::~DialogTagThumbnail()
{
    // 画面サイズの保存
    main->getSettings()->setValueObj(this, "WindowSize", saveGeometry());
    QApplication::restoreOverrideCursor();
    delete sqlModel;
    delete imgTag;
    delete ui;
}

void DialogTagThumbnail::load(const QString &uriFormat)
{
    auto tag = sqlModel->getValue("tag", id).toString();
    tag = tag.replace("&", "%26");
    tag = tag.replace("?", "%3F");

    auto uri = QString(uriFormat).arg(tag);
    auto uris = uri.split("?");
    QUrl url(uris[0]);
    url.setQuery(uris[1]);
    ui->webEngineView->load(url);
}

void DialogTagThumbnail::on_webEngineView_customContextMenuRequested(const QPoint &pos)
{
    Menu menu(this);
    menu.addAction(getActionDefault(QWebEnginePage::DownloadImageToDisk, "画像をダウンロードして登録", "download"));
    menu.addSeparator();
    menu.addAction(getActionDefault(QWebEnginePage::Back, "戻る", "go-previous"));
    menu.addAction(getActionDefault(QWebEnginePage::Forward, "進む", "go-next"));
    menu.addSeparator();
    menu.addAction(getActionDefault(QWebEnginePage::Reload, "更新", "view-refresh"));

    menu.exec(this->mapToGlobal(pos));
}

QAction *DialogTagThumbnail::getActionDefault(QWebEnginePage::WebAction type, const QString &text, const QString &iconName)
{
    auto action = ui->webEngineView->pageAction(type);
    action->setText(text);
    action->setIcon(main->getIcon(iconName));
    return action;
}
