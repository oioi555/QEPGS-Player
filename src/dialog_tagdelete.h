#ifndef DIALOG_TAGDELETE_H
#define DIALOG_TAGDELETE_H

#include <QDialog>
#include "mainwindow.h"
#include "util/tagitem.h"

namespace Ui {class DialogTagDelete;}

class DialogTagDelete : public QDialog
{
    Q_OBJECT

public:
    explicit DialogTagDelete(TagItem *tagItem, QWidget *parent = nullptr);
    ~DialogTagDelete();
    bool isDeletedTag;

private slots:
    void on_buttonBox_accepted();

private:
    int id;
    TagItem* tagItem;
    SqlModel *sqlModel;
    SqlModel *sqlModelTags;
    MainWindow* main;

    Ui::DialogTagDelete *ui;
};

#endif // DIALOG_TAGDELETE_H
