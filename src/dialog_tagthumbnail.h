#ifndef DIALOG_TAGTHUMBNAIL_H
#define DIALOG_TAGTHUMBNAIL_H

#include <QDialog>
#include <QFileInfo>
#include <QNetworkRequest>
#include <QWebEnginePage>
#include "mainwindow.h"

class ThumbnailTag;

namespace Ui {
class DialogTagThumbnail;
}

class DialogTagThumbnail : public QDialog
{
    Q_OBJECT

public:
    explicit DialogTagThumbnail(int id, QWidget *parent = nullptr);
    ~DialogTagThumbnail();
private slots:
    void on_webEngineView_customContextMenuRequested(const QPoint &pos);

private:
    void load(const QString &siteName);
    QAction* getActionDefault(QWebEnginePage::WebAction type, const QString &text, const QString &iconName);
    QString getSuffix(const QString &filePath) {
        QFileInfo fileInfo(filePath);
        return fileInfo.suffix().toLower();
    }

    int id;
    QStringList searchSite;
    ThumbnailTag *imgTag;
    SqlModel *sqlModel;
    MainWindow* main;
    Ui::DialogTagThumbnail *ui;
};

#endif // DIALOG_TAGTHUMBNAIL_H
