#ifndef DOCK_PLAYLIST_H
#define DOCK_PLAYLIST_H

#include <QDockWidget>
#include "mainwindow.h"

class QFileSystemModel;

namespace Ui {class DockPlaylist;}

class DockPlaylist : public QDockWidget
{
    Q_OBJECT

public:
    explicit DockPlaylist(QWidget *parent = nullptr);
    ~DockPlaylist();
    void updatePlaylist(const QString &selectFile = "");

private slots:
    void on_treeView_customContextMenuRequested(const QPoint &pos);

private:
    QString getSelectedFile();
    QList<QAction*> needSelectActions;
    QFileSystemModel *fileModel;
    MainWindow* main;
    Ui::DockPlaylist *ui;

};

#endif // DOCK_PLAYLIST_H
