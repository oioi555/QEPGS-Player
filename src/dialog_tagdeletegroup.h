#ifndef DIALOG_TAGDELETEGROUP_H
#define DIALOG_TAGDELETEGROUP_H

#include <QAbstractButton>
#include <QDialog>
#include "mainwindow.h"
#include "util/tagitem.h"

namespace Ui {class DialogTagDeleteGroup;}

class QStandardItemModel;

class DialogTagDeleteGroup : public QDialog
{
    Q_OBJECT

public:
    explicit DialogTagDeleteGroup(const QString &goupName, QList<TagItem*> tagItem, QWidget *parent = nullptr);
    ~DialogTagDeleteGroup() override;
    bool isDeletedTag = false;

private slots:
    void on_buttonBox_accepted();
    void on_treeView_customContextMenuRequested(const QPoint &pos);

private:
    void autoSize();
    void addTreeItem(TagItem* item);
    void clearTreeModel();
    TagItem* getTagItem(const QModelIndex &index);
    QList<TagItem*> tagItems;
    SqlModel *sqlModel;
    SqlModel *sqlModelTags;
    QStandardItemModel* treeModel;
    MainWindow* main;
    Ui::DialogTagDeleteGroup *ui;

    // QObject interface
public:
    virtual bool event(QEvent *event) override {
        if (event->type() == QEvent::Resize || event->type() == QEvent::Show)
            autoSize();
        return QWidget::event(event);
    }
};

#endif // DIALOG_TAGDELETEGROUP_H
