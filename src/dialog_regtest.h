#ifndef DIALOG_REGTEST_H
#define DIALOG_REGTEST_H

#include <QDialog>
#include "util/tableView_styleditemdelegate.h"

namespace Ui {
class DialogRegTest;
}

class RegDelegate : public TableViewQStyledItemDelegate {
    Q_OBJECT

public:
    explicit RegDelegate(QObject *parent = nullptr): TableViewQStyledItemDelegate(parent)
        , main(qobject_cast<MainWindow*>(parent)) { }
    ~RegDelegate(){}

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        QStyleOptionViewItem optCopy = option;
        QVariant value = index.model()->data(index, Qt::DisplayRole);
        auto columnName = index.model()->headerData(index.column(), Qt::Horizontal).toString();

        if (columnName == "id") {
            if (paintThumbnaill(value.toString(), painter, option)) return;
        }
        if (columnName == "番組タイトル") {
            auto tag = index.model()->data(index.siblingAtColumn(4), Qt::DisplayRole).toString();
            paintTextToHtml(getNameHtml(value.toString(), tag), painter, option);
            return;
        }
        QStyledItemDelegate::paint(painter, optCopy, index);
    }

private:
    MainWindow* main;
};

class DialogRegTest : public QDialog {
    Q_OBJECT

public:
    explicit DialogRegTest(QWidget *parent = nullptr);
    ~DialogRegTest();

private slots:
    void on_tableView_customContextMenuRequested(const QPoint &pos);

private:
    void updateRowHeight();

    SqlModel *sqlModel;
    MainWindow* main;
    RegDelegate *delegate;
    Ui::DialogRegTest *ui;
};

#endif // DIALOG_REGTEST_H
