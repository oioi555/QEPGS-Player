#include "dialog_tagnewdelete.h"
#include "ui_dialog_tagnewdelete.h"
#include "util/thumbnailtag.h"

#include <QStandardItemModel>

DialogTagNewDelete::DialogTagNewDelete(QWidget *parent)
    : QDialog(parent)
    , sqlModel(new SqlModel("recorded", "id", "v_recorded"))
    , sqlModelTags(new SqlModel("tags" , "id"))
    , treeModel(new QStandardItemModel(this))
    , main(qobject_cast<MainWindow*>(parent))
    , ui(new Ui::DialogTagNewDelete)
{
    ui->setupUi(this);
    
    // window
    setWindowTitle("余剰な新番組Tag削除");
                       
    // dbError  
    connect(sqlModelTags, &SqlModel::sqlError, main, &MainWindow::onSqlError);
    
    // action
    connect(ui->actionSelectAll, &QAction::triggered, this, [=] {
       ui->treeView->selectAll();
    });
    connect(ui->actionNotSelectAll, &QAction::triggered, this, [=] {
       ui->treeView->clearSelection();
    });
    
    QApplication::setOverrideCursor(Qt::WaitCursor);
    // 総当りで新番組検出Tagを取得
    QStringList newTags;
    for (auto rs: sqlModel->getModelList()) {
        newTags << main->parseStrTitle(rs["番組タイトル"].toString());
    }
    newTags.removeDuplicates();  // 重複削除

    sqlModelTags->setWhere("field = '番組タイトル' and newflag = true");
    for (auto rs: sqlModelTags->getModelList()) {
        if (!newTags.contains(rs["tag"].toString())) {
            addTreeItem(new TagItem(rs, this));
        }
    }

    // tree
    treeModel->setHorizontalHeaderLabels({"Tag", "数"});
    ui->treeView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->treeView->setModel(treeModel);
    
    QApplication::restoreOverrideCursor();
    
    // 画面サイズの復元
    restoreGeometry( main->getSettings()->valueObj(this, "WindowSize").toByteArray());
    autoSize();
}

DialogTagNewDelete::~DialogTagNewDelete()
{
    // 画面サイズの保存
    main->getSettings()->setValueObj(this, "WindowSize", saveGeometry());
    
    delete sqlModel;
    delete sqlModelTags;
    delete treeModel;
    delete ui;
}

void DialogTagNewDelete::autoSize()
{
    int fontWidth = ui->treeView->fontInfo().pixelSize();
    int width = ui->treeView->size().width() - (fontWidth * 6);
    ui->treeView->header()->resizeSection(0, width);
}

void DialogTagNewDelete::addTreeItem(TagItem *item)
{
    auto treeItem = item->getTreeItem();
    treeModel->invisibleRootItem()->appendRow(treeItem);
}

TagItem *DialogTagNewDelete::getTagItem(const QModelIndex &index)
{
    return static_cast<TagItem *>(treeModel->data(index, Qt::UserRole).value<QObject *>());
}

void DialogTagNewDelete::on_buttonBox_accepted()
{
    QList<int> ids;
    QModelIndexList selectedItems;
    foreach (auto index, ui->treeView->selectionModel()->selectedIndexes()) {
        if (treeModel->data(index, Qt::UserRole).isValid()) {
            ids << getTagItem(index)->id;
            selectedItems << index;
        }
    }

    if (!ids.length()) {
        QMessageBox::warning(this, tr("新番組Tag削除"), tr("Tagが選択されていません"));
        return;
    }

    // 確認メッセージ
    QStringList msg;
    msg << tr("削除するTag:　%1件").arg(ids.length());
    msg << "" << "本当に削除しますか？";

    QMessageBox msgBox(qobject_cast<QWidget*>(this->window()));
    msgBox.setWindowTitle("削除確認");
    msgBox.setText(msg.join("\n"));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    msgBox.setIcon(QMessageBox::Question);
    if (msgBox.exec() != QMessageBox::Yes) return;

    sqlModelTags->deleteRecord(ids);
    ThumbnailTag imgTag(main);
    foreach (auto id, ids)
        imgTag.remove(id, false);

    accept();
}

void DialogTagNewDelete::on_treeView_customContextMenuRequested(const QPoint &pos)
{
    Menu menu(this);
    menu.addAction(ui->actionSelectAll);
    menu.addAction(ui->actionNotSelectAll);
    menu.exec(ui->treeView->viewport()->mapToGlobal(pos));
}

