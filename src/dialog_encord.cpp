#include "dialog_encord.h"
#include "ui_dialog_encord.h"
#include "util/progressdlg.h"

DialogEncord::DialogEncord(const QList<int> &ids, QWidget *parent)
    : QDialog(parent)
    , ids(ids)
    , sqlModel(new SqlModel("recorded", "id", "v_recorded", "id, 番組タイトル, ビデオファイル"))
    , sqlModelIds(new SqlModel("recorded", "id", "v_recorded", "id, 番組タイトル, ビデオファイル"))
    , sqlModelVideo(new SqlModel("video_file", "id"))
    , main(qobject_cast<MainWindow*>(parent))
    , delegate(new EncordDelegate(main))
    , ui(new Ui::DialogEncord)
{
    ui->setupUi(this);

    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    // window
    setWindowTitle(isMulti()? "まとめてエンコード" : "エンコード");
    setWindowIcon(main->getIcon("tools-media-optical-burn-image"));

    // ソース設定を取得
    encIds = ids;   // 表示用のIDリストを保存
    updateCmbSourceItems();
    connect(ui->cmbSource, &QComboBox::currentTextChanged, this, [=] (QString /* text */) {
        cmbSourceCurrentIndexChanged();
    });

    // EPGStationの設定を取得
    auto config = main->getApi()->get("/api/config");

    // コーデック指定
    QStringList encodes;
    foreach (auto encord, config["encode"].toArray()) encodes << encord.toString();
    ui->cmbEncord->addItems(encodes);

    // 保存先指定
    QStringList recordeds;
    foreach (auto recorded, config["recorded"].toArray()) recordeds << recorded.toString();
    ui->cmbRecorded->addItems(recordeds);
    ui->txtSubDir->setPlaceholderText("サブディレクトリ（任意）");

    // 保存場所のオプション
    connect(ui->chkSaveOrigin, &QCheckBox::clicked, this, [=] (bool state) {
        ui->cmbRecorded->setEnabled(!state);
        ui->txtSubDir->setEnabled(!state);
    });

    // エンコード対象のレコードを表示
    sqlModel->setWhere(getIdsSql());
    ui->tableView->setSqlModel(sqlModel);
    ui->tableView->setItemDelegate(delegate);
    ui->tableView->initSql();
    ui->tableView->autoSizeColRow();

    // 整理対象のレコードを表示
    sqlModelIds->setWhere(getIdsSql());
    ui->tableViewIds->setSqlModel(sqlModelIds);
    ui->tableViewIds->setItemDelegate(delegate);
    ui->tableViewIds->initSql();
    ui->tableViewIds->autoSizeColRow();

    // 整理対象かの判定
    showAdjustTab();

    // エンコード対象の抽出
    cmbSourceCurrentIndexChanged();

    // 画面サイズの復元
    restoreGeometry( main->getSettings()->valueObj(this, "WindowSize").toByteArray());
}

DialogEncord::~DialogEncord()
{
    // 画面サイズの保存
    main->getSettings()->setValueObj(this, "WindowSize", saveGeometry());

    delete delegate;
    delete sqlModel;
    delete sqlModelIds;
    delete sqlModelVideo;
    delete ui;
}

QString DialogEncord::getIdsSql(const QString &addWhere /* = "" */)
{
    QStringList strIds;
    foreach (auto id, encIds) strIds << QString::number(id);
    QStringList wheres = QStringList() << tr("id in (%1)").arg(strIds.join(","));
    if (addWhere != "") wheres << addWhere;
    return wheres.join(" AND ");
}

void DialogEncord::updateCmbSourceItems()
{
    QStringList sources;
    if (isMulti()) {
        sources << "TSがある録画のみ" << "検出された最初のビデオ";
        // ソースが録画中で、ファイルサイズがゼロなら、対象から排除する
        QList<int> idsAfter;
        foreach (auto id, ids) {
            auto sizes = sqlModelVideo->getValues("size", tr("recordedId = %1").arg(id));
            if (sizes.at(0).toDouble() > 0) idsAfter << id;
        }
        encIds = idsAfter;
    } else {
        sqlModelVideo->setWhere(tr("recordedId = %1").arg(ids.at(0)));
        foreach (auto rs, sqlModelVideo->getModelList()) {
            if (rs["size"].toDouble() > 0) {
                sources << tr("%1 (%2)").arg(
                    rs["name"].toString(),
                    main->getFileSizeStr(rs["size"].toDouble()));
            }
        }
    }
    ui->cmbSource->clear();
    ui->cmbSource->addItems(sources);
}

void DialogEncord::cmbSourceCurrentIndexChanged()
{
    if (isMulti()) {
        QString where = (ui->cmbSource->currentIndex() == 0)? "ビデオファイル like 'TS%'": "";
        sqlModel->setWhere(getIdsSql(where));
        ui->tableView->reQuery(false);
        ui->tableView->autoSizeColRow();
    }
}

void DialogEncord::showAdjustTab()
{
    bool showTab = false;
    foreach (auto video, sqlModel->getValues("ビデオファイル", getIdsSql())) {
        auto videos = video.split(", ");
        if (videos[0] == "TS" && videos.count() == 1) continue;
        if (videos.count() > 1) {
            showTab = true; break;
        }
    }
#if QT_VERSION > QT_VERSION_CHECK(5, 15, 0)
    ui->tabWidget->setTabVisible(1, showTab);
#else
    // debian 10(qt5.11)対策
    ui->grpDeleteTs->setEnabled(showTab);
    ui->grpDeleteEncVideo->setEnabled(showTab);
#endif
}

void DialogEncord::on_btnDeleteEncVideo_clicked()
{
    QList<int> deleteVideoIds;
    QString order = (ui->cmbLeave->currentIndex() == 0)? "id ASC" : "id DESC";
    foreach (auto recordedId, sqlModelIds->getValuesId(getIdsSql())) {
        auto videoIds = sqlModelVideo->getValuesId(tr("recordedId = %1 AND name <> 'TS'").arg(recordedId), order);
        if (videoIds.count() > 1) {
            videoIds.removeFirst();
            deleteVideoIds << videoIds;
        }
    }
    deleteVideoConfirm(deleteVideoIds);
}

void DialogEncord::on_btnDeleteTsLeaveEncVideo_clicked()
{
    QList<int> deleteVideoIds;
    QString where = "ビデオファイル like 'TS%'";
    foreach (auto recordedId, sqlModelIds->getValuesId(getIdsSql(where))) {
        auto videoIds = sqlModelVideo->getValuesId(tr("recordedId = %1").arg(recordedId), "id ASC");
        if (videoIds.count() > 1)
            deleteVideoIds << videoIds.at(0);
    }
    deleteVideoConfirm(deleteVideoIds);
}

void DialogEncord::deleteVideoConfirm(QList<int> deleteVideoIds)
{
    if (deleteVideoIds.count()) {
        if (!main->deleteConfirm(this)) return;

        ProgressDlg progress(this, "ビデオファイルの整理", "ビデオファイル削除中...", deleteVideoIds.count());
        foreach (auto videoId, deleteVideoIds) {
            if (progress.setValueIncWasCanceled()) break;
            main->getApi()->deleteVideoFile(videoId);
        }
        ui->tableView->reQuery();
        ui->tableViewIds->reQuery();
        updateCmbSourceItems();
    } else {
        QMessageBox::information(this, tr("ビデオファイルの整理"), tr("削除対象のビデオファイルはありませんでした"));
    }
}

void DialogEncord::on_buttonBox_accepted()
{
    auto recordedIds = sqlModel->getValuesId(sqlModel->getWhere());
    if (!recordedIds.count()) {
        QMessageBox::information(this, "エンコード", "エンコードする録画がありませんでした");
        return;
    }

    main->updateEncordingList();
    QList<QJsonObject> encJsons;
    foreach (auto recordedId, recordedIds) {
        auto encElement = main->getEncordElement(recordedId);
        // 重複エンコード回避
        if (encElement.encId != -1) continue;

        QList<int> videoIds = sqlModelVideo->getValuesId(tr("recordedId = %1").arg(recordedId), "id ASC");
        if (ui->cmbSource->currentIndex() < videoIds.count()) {
            QJsonObject jsonObj;
            jsonObj["recordedId"] = recordedId;
            jsonObj["sourceVideoFileId"] = videoIds.at(ui->cmbSource->currentIndex());
            jsonObj["parentDir"] = ui->cmbRecorded->currentText();
            jsonObj["directory"] = ui->txtSubDir->text();
            jsonObj["isSaveSameDirectory"] = bool(ui->chkSaveOrigin->checkState());
            jsonObj["mode"] = ui->cmbEncord->currentText();
            jsonObj["removeOriginal"] = bool(ui->chkSourceDelete->checkState());
            encJsons << jsonObj;
        }
    }
    ProgressDlg progress(this, "エンコード", "エンコード登録中...", encJsons.count());
    foreach (auto jsonObj, encJsons) {
        if (progress.setValueIncWasCanceled()) break;
        main->getApi()->post("/api/encode", jsonObj);
    }
    main->updateEncordingList();
    accept();
}
