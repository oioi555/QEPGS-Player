/**
 * MPRIS D-Bus Interface 2.2
 * https://specifications.freedesktop.org/mpris-spec/latest/index.html
 *
 * Interface MediaPlayer2.Player
 * https://specifications.freedesktop.org/mpris-spec/latest/Player_Interface.html
*/

#include "dbus_mediaplayer2player.h"
#include "mainwindow.h"
#include "widget_player.h"

DBusMediaPlayer2Player::DBusMediaPlayer2Player(WidgetPlayer *player, QObject *parent)
    : QDBusAbstractAdaptor(parent),
    main(static_cast<MainWindow*>(parent)),
    widgetPlayer(player)
{
    // NoTrack Metadata NOTE: パスに「-」は使えない
    const QString dbusPath = "/org/QEPGS_Player/QEPGS_Player/NoTrack";
    noTrackMetadata["mpris:trackid"] = QVariant::fromValue<QDBusObjectPath>(QDBusObjectPath(dbusPath));
    noTrackMetadata["mpris:length"] = 0;
    noTrackMetadata["xesam:title"] = "再生中のメディアはありません";

    /**
     * NOTE: KDE Media Playerで、サービスを再構築するとプレイヤーが消えてしまい
     * 再利用できなくなるのでメディアなし状態を維持する
     */
    connect(main, &MainWindow::mprisNoPlayer, this, [=] () {
        widgetPlayer = nullptr;
        main->sendMprisPropertiesChanged(this, getDefaultProperties());
    });
    // NOTE: マルチプレイヤーモードで、アクティブなプレイヤーが変化した時の切替
    connect(main, &MainWindow::mprisActivePlayerChanged, this, [=] () {
        main->sendMprisPropertiesChanged(this, getDefaultProperties());
    });
    // 動画がロードされた時に呼ばれる
    connect(widgetPlayer, &WidgetPlayer::mprisCurrentItemChanged, this, [=] () {
        main->sendMprisPropertiesChanged(this, getDefaultProperties());
    });

    connect(widgetPlayer, &WidgetPlayer::mprisPlayngChanged, this, [=] () {
        QVariantMap properties;
        properties["PlaybackStatus"] = PlaybackStatus();
        main->sendMprisPropertiesChanged(this, properties);
    });
    connect(widgetPlayer, &WidgetPlayer::mprisLoopStatusChanged, this, [=] () {
        QVariantMap properties;
        properties["LoopStatus"] = LoopStatus();
        main->sendMprisPropertiesChanged(this, properties);
    });
    connect(widgetPlayer, &WidgetPlayer::mprisLengthChanged, this, [=] () {
        QVariantMap properties;
        properties["Metadata"] = Metadata();
        main->sendMprisPropertiesChanged(this, properties);
    });
    connect(widgetPlayer, &WidgetPlayer::mprisVolumeChanged, this, [=] () {
        QVariantMap properties;
        properties["Volume"] = Volume();
        main->sendMprisPropertiesChanged(this, properties);
    });
    connect(widgetPlayer, &WidgetPlayer::mprisSeeked, this, [=] (qlonglong position) {
        emit Seeked(position);
        QVariantMap properties;
        properties["CanGoPrevious"] = CanGoPrevious();
        main->sendMprisPropertiesChanged(this, properties);
    });
}

// Methods
void DBusMediaPlayer2Player::Next()
{
    if (widgetPlayer == nullptr) return;
    widgetPlayer->next();
}

void DBusMediaPlayer2Player::Previous()
{
    if (widgetPlayer == nullptr) return;
    widgetPlayer->previous();
}

void DBusMediaPlayer2Player::Pause()
{
    if (widgetPlayer == nullptr) return;
    widgetPlayer->pause();
}

void DBusMediaPlayer2Player::PlayPause()
{
    if (widgetPlayer == nullptr) return;
    widgetPlayer->playPause();
}

void DBusMediaPlayer2Player::Stop()
{
    if (widgetPlayer == nullptr) return;
    widgetPlayer->stop();
}

void DBusMediaPlayer2Player::Play()
{
    if (widgetPlayer == nullptr) return;
    widgetPlayer->play();
}

void DBusMediaPlayer2Player::Seek(qlonglong Offset)
{
    if (widgetPlayer == nullptr) return;
    widgetPlayer->seek(Offset);
}

void DBusMediaPlayer2Player::SetPosition(const QDBusObjectPath &TrackId, qlonglong Position)
{
    if (widgetPlayer == nullptr) return;
    widgetPlayer->setPosition(TrackId, Position);
}

void DBusMediaPlayer2Player::OpenUri(QString uri)
{
    if (widgetPlayer == nullptr) return;
    widgetPlayer->openUri(uri);
}

QString DBusMediaPlayer2Player::PlaybackStatus() const
{
    if (widgetPlayer == nullptr) return "Stopped";
    return widgetPlayer->playbackStatus();
}

QString DBusMediaPlayer2Player::LoopStatus() const
{
    if (widgetPlayer == nullptr) return "None";
    return widgetPlayer->loopStatus();
}

// Properties
void DBusMediaPlayer2Player::setLoopStatus(const QString &loopStatus) const
{
    if (widgetPlayer == nullptr) return;
    widgetPlayer->setLoopStatus(loopStatus);
}

double DBusMediaPlayer2Player::Rate() const
{
    if (widgetPlayer == nullptr) return 1.0;
    return widgetPlayer->rate();
}

void DBusMediaPlayer2Player::setRate(double rate) const
{
    if (widgetPlayer == nullptr) return;
    widgetPlayer->setRate(rate);
}

QVariantMap DBusMediaPlayer2Player::Metadata() const
{
    if (widgetPlayer == nullptr) return noTrackMetadata;
    return widgetPlayer->metaData();
}

double DBusMediaPlayer2Player::Volume() const
{
    if (widgetPlayer == nullptr) return 1.0;
    return widgetPlayer->volume();
}

void DBusMediaPlayer2Player::setVolume(double volume) const
{
    if (widgetPlayer == nullptr) return;
    widgetPlayer->setVolume(volume);
}

qlonglong DBusMediaPlayer2Player::Position() const
{
    if (widgetPlayer == nullptr) return 0;
    return widgetPlayer->position();
}

double DBusMediaPlayer2Player::MinimumRate() const
{
    return 0.1;
}

double DBusMediaPlayer2Player::MaximumRate() const
{
    return 2.0;
}

bool DBusMediaPlayer2Player::CanGoNext() const
{
    if (widgetPlayer == nullptr) return false;
    return widgetPlayer->canGoNext();
}

bool DBusMediaPlayer2Player::CanGoPrevious() const
{
    if (widgetPlayer == nullptr) return false;
    return widgetPlayer->canGoPrevious();
}

bool DBusMediaPlayer2Player::CanPlay() const
{
    if (widgetPlayer == nullptr) return false;
    return true;
}

bool DBusMediaPlayer2Player::CanPause() const
{
    if (widgetPlayer == nullptr) return false;
    return true;
}

bool DBusMediaPlayer2Player::CanSeek() const
{
    if (widgetPlayer == nullptr) return false;
    return true;
}

bool DBusMediaPlayer2Player::CanControl() const
{
    if (widgetPlayer == nullptr) return false;
    return true;
}

// Helper
QVariantMap DBusMediaPlayer2Player::getDefaultProperties()
{
    QVariantMap properties;
    properties["PlaybackStatus"] = PlaybackStatus();
    properties["LoopStatus"] = LoopStatus();
    properties["Rate"] = Rate();
    properties["Metadata"] = Metadata();
    properties["Volume"] = Volume();
    properties["Position"] = Position();
    properties["CanGoNext"] = CanGoNext();
    properties["CanGoPrevious"] = CanGoPrevious();
    properties["CanPlay"] = CanPlay();
    properties["CanPause"] = CanPause();
    properties["CanSeek"] = CanSeek();
    properties["CanControl"] = CanControl();

    return properties;
}


