﻿#ifndef DOCK_INFO_H
#define DOCK_INFO_H

#include <QDockWidget>
#include "mainwindow.h"

#include "util/tagmodel.h"
#include "util/recmodel.h"
#include "util/videomodel.h"
#include "util/thumbnailtag.h"

class TagItem;
class ThumbnailTag;

namespace Ui {class DockVideoInfo;}

struct OverviewElement {
    Q_GADGET
    Q_PROPERTY(QString epgsVersion MEMBER epgsVersion)
    Q_PROPERTY(QString dbType MEMBER dbType)
    Q_PROPERTY(QString freeSpase MEMBER freeSpase)
    Q_PROPERTY(int usedPercent MEMBER usedPercent)
    Q_PROPERTY(int totalRecoded MEMBER totalRecoded)
    Q_PROPERTY(int totalWatched MEMBER totalWatched)
    Q_PROPERTY(int totalTag MEMBER totalTag)
    Q_PROPERTY(int totalTagWatched MEMBER totalTagWatched)
    Q_PROPERTY(int totalTagNotWatched MEMBER totalTagNotWatched)
    Q_PROPERTY(int totalEndflag MEMBER totalEndflag)
    Q_PROPERTY(int totalTagHasVideo MEMBER totalTagHasVideo)
    Q_PROPERTY(int totalTagisEndProgram MEMBER totalTagisEndProgram)
    Q_PROPERTY(int totalNewflag MEMBER totalNewflag)
    Q_PROPERTY(int totalDefaultflag MEMBER totalDefaultflag)
    Q_PROPERTY(int totalBlackflag MEMBER totalBlackflag)
    // NOTE: qt5.12からは、QML内からsettingsにアクセス可能になる
    // debian10が、qt5.11なので、c++側でやる
    Q_PROPERTY(bool viewServerContents MEMBER viewServerContents)
    Q_PROPERTY(bool viewTagContents MEMBER viewTagContents)
    Q_PROPERTY(bool viewRecrodedContents MEMBER viewRecrodedContents)

public:
    QString epgsVersion = "unknown";
    QString dbType = "";
    QString freeSpase = "";
    int usedPercent;
    int totalRecoded = 0;
    int totalWatched = 0;
    int totalTag = 0;
    int totalTagWatched = 0;
    int totalTagNotWatched =0;
    int totalEndflag = 0;
    int totalTagHasVideo = 0;
    int totalTagisEndProgram = 0;
    int totalNewflag = 0;
    int totalDefaultflag = 0;
    int totalBlackflag = 0;
    bool viewServerContents = true;
    bool viewTagContents = true;
    bool viewRecrodedContents = true;

};
Q_DECLARE_METATYPE(OverviewElement)

class DockInfo : public QDockWidget
{
    Q_OBJECT
    Q_PROPERTY(QVariantMap Map MEMBER map NOTIFY mapChanged)
    Q_PROPERTY(OverviewElement overElement READ getOverElement WRITE setOverElement NOTIFY overElementChanged)
    Q_PROPERTY(TagElement tagElement READ getTagElement NOTIFY tagElementChanged)
    Q_PROPERTY(int currentIndex MEMBER currentIndex NOTIFY currentIndexChanged)
    Q_PROPERTY(bool hasEncord MEMBER hasEncord NOTIFY hasEncordChanged)
signals:
    void recordingChanged(int recCount, int encCount);
    // qml
    void mapChanged(QVariantMap& newMap);
    void progressChanged();
    void encProgressChanged();
    void hasEncordChanged();
    void overElementChanged();
    void tagElementChanged();
    void currentIndexChanged(int currentIndex);
    void updateTagTable();

public:
    explicit DockInfo(QWidget *parent = nullptr);
    ~DockInfo();

    void loadPage(InfoType type, const QString &idStr = "", const QString &searchWord = "");
    void setPlayingId(int id, const QString &searchWord = "");
    InfoType getInfoType() { return infoType; }
    int getTagId() { return tagId; }

    OverviewElement getOverElement() { return overElement; };
    void setOverElement(OverviewElement element) {
        overElement = element;
        saveSetting("viewOverServerContents", overElement.viewServerContents);
        saveSetting("viewOverTagContents", overElement.viewTagContents);
        saveSetting("viewOverRecrodedContents", overElement.viewRecrodedContents);
    }
    TagElement getTagElement() { return tagElement; };

    // qml overview
    Q_INVOKABLE void openEPGStation(const QString &uri = "") { main->openEPGStation(uri); };
    Q_INVOKABLE void openRecoded() { main->openRecorded(); };
    Q_INVOKABLE void openTagWidget() { main->openTagWidget(); };
    Q_INVOKABLE void openSettings(QString name) { main->openSettings(name); };
    Q_INVOKABLE void openProgram() { main->openEPGProgram(); };

    // qml tag menu
    Q_INVOKABLE void actionTagWidget() { main->openTagWidget(tagElement.id, tagElement.field, tagElement.genre); };
    Q_INVOKABLE void actionSearchNewTab(const QString &word);
    Q_INVOKABLE void actionSearch(const QString &word);
    Q_INVOKABLE void actionFilter() { main->setFilterTag(tagElement.field, tagElement.tag); };
    Q_INVOKABLE void actionFilterNewTab() { if (main->openRecorded()) actionFilter(); };
    Q_INVOKABLE void actionSearchWeb(const QString &word) { main->searchWeb(word); };
    Q_INVOKABLE void openTagEdit() { main->openTagEditDlg(tagId); };
    Q_INVOKABLE void openTagDelete() { main->openTagDelete(tagId); };
    Q_INVOKABLE void setBlackList(int id, bool checked);
    Q_INVOKABLE void saveSetting(const QString &name, const QVariant &value) {
        main->getSettings()->setValueObj(this, name, value);
    };
    Q_INVOKABLE void openSearchWebImageDlg(int id);
    Q_INVOKABLE void openImageFileSelectDlg(int id);
    Q_INVOKABLE void deleteImageFile(int id);
    Q_INVOKABLE void saveDropUrlFile(const QString &url, int id = -1);

    // qml tag recorded menu
    Q_INVOKABLE void openPlayerTag(const int &tagId, const int &selectId = -1){
        main->openPlayerTag(tagId, selectId);
    }
    Q_INVOKABLE void openPlayerPlaylist(const int &selectId = -1){
        main->openPlayerRecordedIds(recModel->getIds(), selectId);
    }
    Q_INVOKABLE void deleteRecorded(int id);
    Q_INVOKABLE void actionFullParsent(int id, bool isSendTaglist = false);
    Q_INVOKABLE void actionDeleteParsent(int id, bool isSendTaglist = false);

    // qml recorded menu
    Q_INVOKABLE void actionTagAddTag(int id) { main->openTagAddDlg(id); reload(id); }
    Q_INVOKABLE void openDetailPage(int id) { main->openEPGStation(tr("recorded/detail/%1").arg(id)); }
    Q_INVOKABLE void actionTagAddEndflag(int id) { main->addTagEndFlag(QList<int>() << id); reload(id); }
    Q_INVOKABLE void openPlayer(int id) { main->openPlayer(id); }
    Q_INVOKABLE void playVideo(const int &videoId) { main->openPlayerVideoId(videoId); }
    Q_INVOKABLE void deleteVideoFile(const int &videoId);
    Q_INVOKABLE void backPage() { loadPage(backInfoType, QString::number(backTagId)); };

    // qml tag recorded change select
    Q_INVOKABLE void setQmlRecordedID(int id) { loadPage(InfoType::Recorded, QString::number(id)); }
    Q_INVOKABLE void setQmlTagID(const int &tagid, int id) { recordedId = id; loadPage(InfoType::Tag, QString::number(tagid)); }

    // qml playlist
    Q_INVOKABLE void actionPlaylistSave();
    Q_INVOKABLE void deletePlaylistId(int id);

    // qml encord
    Q_INVOKABLE void openEncordDlg(int id) { if (main->openEncordDlg(id)) reload(); };
    Q_INVOKABLE void openEncordDlg(const QString &field, const QString &tag) {
        if (main->openEncordDlg(field, tag)) reload();
    };
    Q_INVOKABLE void cancelEncord() { if (main->cancelEncord()) reload(); }
    Q_INVOKABLE void cancelEncord(const int &encId) { if (main->cancelEncord(encId)) reload(); };
    Q_INVOKABLE void cancelEncord(const QString &field, const QString &tag) {
        if (main->cancelEncord(field, tag)) reload();
    };

private slots:

private:
    SqlModel *sqlModel;
    SqlModel *sqlModelDetail;
    SqlModel *sqlModelLive;
    SqlModel *sqlModelTag;
    RecModel *recModel;
    TagModel *tagModel;
    VideoModel *videoModel;
    ThumbnailTag *imgTag;
    Menu *menuIds;
    QList<QAction*> actionIds;

    bool initLoad = false;
    int recordedId = -1;
    QString liveId = ""; //chanelId
    QString programId = ""; // 放送中の番組
    int tagId = -1;
    InfoType infoType = InfoType::Overview;
    // 戻る処理
    bool isBack = false;
    InfoType backInfoType = InfoType::Tag;
    int backTagId = -1;

    QVariantMap map;
    int currentIndex = -1;
    bool hasEncord;
    QString qmlCss;

    TagItem *tagItem;
    TagElement tagElement;
    OverviewElement overElement;
    QDateTime startDateTime;
    QDateTime endDateTime;
    int lastStausId = -1;
    QTimer *timerCheckRecording;    // 録画中検出インターバルタイマー
    QList<int> recordingIds;

    QPropertyAnimation *animeCtlBarHide;
    QPropertyAnimation *animeCtlBarShow;
    MainWindow* main;

    void loadOverview();
    void setRecordedId(int id, const QString &searchWord = "");
    void setLiveTvId(const QString &idStr);
    void setTagId(int id);
    void loadPlaylist();
    void reload(int id = -1);
    QString getDateTimeFormat(QSqlRecord *rs);
    QString getDateTimeFormat(const QDateTime &startTime, const QDateTime &endTime);
    QString getGenre(QSqlRecord *rs);
    int getPercent(bool isOntime, int id = -1);
    int getPercent(bool isOntime, int id, const QDateTime &startTime, const QDateTime &endTime);
    bool checkRecording(int id = -1);
    int updateRecordingList();
    bool isRecording = false;
    void updateStatus(int id = -1);

    // map
    void clearMap();
    QString escape(const QVariant &value, const QString &idName, const QString &searchWord = "");
    void setMapData(const QString &idName, const QVariant &value) {
        map.insert(idName, value);
    }
    void setMapData(const QString &idName, const QSqlRecord *rs) {
        setMapData(idName, rs->value(idName));
    }
    void setMapDataHtml(const QString &idName, const QVariant &value, const QString &searchWord = "") {
        map.insert(idName, escape(value.toString(), idName, searchWord));
    }
    void setMapDataHtml(const QString &idName, const QSqlRecord *rs, const QString &searchWord = "") {
        setMapDataHtml(idName, rs->value(idName), searchWord);
    }
    Ui::DockVideoInfo *ui;

    // QWidget interface
protected:
    virtual void resizeEvent(QResizeEvent *event) override;
};

#endif // DOCK_INFO_H
