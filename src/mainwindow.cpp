#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "widget_epgstation.h"
#include "widget_recorded.h"
#include "widget_player.h"
#include "widget_tag.h"
#include "dock_info.h"
#include "dock_live.h"
#include "dock_tag.h"
#include "dock_playlist.h"
#include "dialog_settings.h"
#include "dialog_tagadd.h"
#include "dialog_tagedit.h"
#include "dialog_tagdelete.h"
#include "dialog_tagthumbnail.h"
#include "dialog_encord.h"
#include "dbus_mediaplayer2.h"
#include "dbus_mediaplayer2player.h"
#include "util/progressdlg.h"

#include <QMdiSubWindow>
#include <QProcess>
#include <QDir>
#include <QStyleFactory>
#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusReply>
#include <QRegularExpression>

MainWindow::MainWindow(QWidget *parent)
    : BaseMainWindow(parent)
    , dockInfoPrevState(true)
    , dockNaviPrevState(true)
    , dbPrevConnect(false)
    , sqlModel(new SqlModel("recorded", "id", "v_recorded"))
    , rsetapi(new RsetApi(settings))
    , pop(new PopUp(this))
    , tagTable(new TagTable(this))
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // url api
    connect(rsetapi, &RsetApi::netError, this, [=] (const QString &lastError) {
        if (lastError.contains("api/version")) return; // version判定に使うので無視
        pop->show(QStringList() << "ネットワークエラー" << lastError);
        qDebug() << lastError;
    });

    connect(sqlModel, &SqlModel::sqlError, this, &MainWindow::onSqlError);
    readBaceSetting();

    // mdiArea
    setupMdiAreaTabMode(ui->mdiArea);
    ui->mdiArea->addActions(this->findChildren<QAction*>());
    connect(ui->mdiArea, &QMdiArea::subWindowActivated, this, [=] (QMdiSubWindow *window) {
        pop->setHidden(true);
        auto windowTitle = qApp->applicationName();
        auto className = getSubWindowClassName(window);
        if (className == tr("WidgetPlayer")) {
            auto player = qobject_cast<WidgetPlayer*>(window->widget());
            player->setDockInfo();
            windowTitle += tr(" < %1 >").arg(player->getTabTitle());

            // MPRIS D-Bus Interface
            if (mpris) setupMprisDbusConnection(player);
        }
        this->setWindowTitle(windowTitle);

        // simple view settings
        if (miniScreen || this->isFullScreen()) return;   // SimplViewからmini/fullScreenの解除をしているので無視する

        bool isSimple = ui->actionSimpleView->isChecked();
        if (className == "WidgetPlayer") {
            if (!isSimple && settings->value("SimpleView/chkAutoPlayer").toBool()) {
                setSimpleView(true);
                // Note: Qt6でsetSimpleView(true)によってPlayerがActiveならない不具合への対応
                #if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
                ui->mdiArea->setActiveSubWindow(window);
                #endif
            }
        }
        if (className == "WidgetRecorded") {
            if (isSimple && settings->value("SimpleView/chkAutoRecorded").toBool())
                setSimpleView(false);
        }
        if (className == "WidgetEPGStation") {
            if (isSimple && settings->value("SimpleView/chkAutoEPG").toBool())
                setSimpleView(false);
        }
        if (className == "WidgetTag") {
            if (isSimple && settings->value("SimpleView/chkAutoTag").toBool())
                setSimpleView(false);
        }
    });

    // toolBar
    setupToolBar(ui->toolBar);

    // actions
    connect(ui->actionEPGStation, &QAction::triggered, this, [=] { openEPGStation(); });
    connect(ui->actionRecorded, &QAction::triggered, this, &MainWindow::openRecorded);
    connect(ui->actionTag, &QAction::triggered, this, [=] { openTagWidget(); });
    connect(ui->actionNavi, &QAction::triggered, this, [=] (bool isChecked) { setDockNaviState(isChecked); });
    connect(ui->actionInfo, &QAction::triggered, this, [=] (bool isChecked) { setDockInfoState(isChecked); });
    connect(ui->actionSimpleView, &QAction::triggered, this, &MainWindow::setSimpleView);
    connect(ui->actionSetting, &QAction::triggered, this, [=] { openSettings(); });

    connect(ui->actionPlayerCloseOther, &QAction::triggered, this, [=] { closeSubWindowOther("WidgetPlayer"); });
    connect(ui->actionPlayerCloseAll, &QAction::triggered, this, [=] { closeSubWindowAll("WidgetPlayer"); });
    connect(ui->actionRecordedCloseOther, &QAction::triggered, this, [=] { closeSubWindowOther("WidgetRecorded"); });
    connect(ui->actionRecordedCloseAll, &QAction::triggered, this, [=] { closeSubWindowAll("WidgetRecorded"); });

    // overview action
    QWidget *spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    ui->toolBar->addWidget(spacer);
    ui->toolBar->addSeparator();
    ui->toolBar->addAction(ui->actionOverview);
    ui->toolBar->addSeparator();
    ui->toolBar->addAction(ui->actionSetting);

    connect(ui->actionOverview, &QAction::triggered, this, [=] {
        setDockInfoState(true);
        dockInfo->loadPage(InfoType::Overview);
    });

    // DockWidget
    dockTag = new DockTag(this);
    dockLive = new DockLive(this);
    dockPlaylist = new DockPlaylist(this);
    dockInfo = new DockInfo(this);
    connect(dockInfo, &DockInfo::recordingChanged, this, [=] (int recCount, int encCount) {
        QStringList titles;
        if (recCount) titles << tr("REC(%1)").arg(recCount);
        if (encCount) titles << tr("ENC(%1)").arg(encCount);
        ui->actionOverview->setText(titles.count()? titles.join("｜") : "概要");
        ui->actionOverview->setIcon(QIcon::fromTheme(titles.count()? "media-record" : "go-home"));
    });

    // left dockNavi
    docks << dockTag << dockLive << dockPlaylist;
    foreach (auto dock, docks)
        addDockWidget(Qt::LeftDockWidgetArea, dock);

    setDockOptions(ForceTabbedDocks | VerticalTabs);
    tabifyDockWidget(dockTag, dockLive);
    tabifyDockWidget(dockLive, dockPlaylist);

    setActiveDockTab(DockNaviType::Tag);

    // right dockInfo
    addDockWidget(Qt::RightDockWidgetArea, dockInfo);

    // tag table
    connect(tagTable, &TagTable::tagTableUpdated, this, [=] () {
        emit mainStatusChanged(MainStatus::TagTabel);
    });
    updateTagTable();

    // sleep
    if (QDBusConnection::sessionBus().isConnected())
        sleepBbusInterface = new QDBusInterface("org.freedesktop.ScreenSaver" ,"/ScreenSaver" , "org.freedesktop.ScreenSaver" , QDBusConnection::sessionBus());

    // open mdi window
    if (settings->value("chkStartEPG").toBool())
        ui->actionEPGStation->trigger();
    if (settings->value("chkStartRecorded").toBool())
        ui->actionRecorded->trigger();
    if (settings->value("chkStartTagWidget").toBool())
        ui->actionTag->trigger();

    // geometry setting
    readStatusSettings();

    // SimpleView強制無効化
    setSimpleView(false);

    // toolbar前回のチェック状態を復元
    setDockNaviState(dockNaviPrevState, true);
    setDockInfoState(dockInfoPrevState, true);

    // tooltip custom
    auto proxyStyle = new ProxyStyle;
    QString style = settings->value("style").toString();
    proxyStyle->toolTipOpacity = settings->value("toolTipOpacity").toInt();
    proxyStyle->setBaseStyle(QStyleFactory::create(style));
    qApp->setStyle(proxyStyle);
}

MainWindow::~MainWindow()
{
    qDeleteAll(docks);
    delete dockInfo;
    delete sqlModel;
    delete rsetapi;
    delete pop;
    delete tagTable;
    delete ui;
    if (sleepBbusInterface != nullptr) {
        inhibitSleep(false);
        delete sleepBbusInterface;
    }
    QSqlDatabase db = QSqlDatabase::database("qt_sql_default_connection", false);
    db.close();
    db = QSqlDatabase();
    QSqlDatabase::removeDatabase("qt_sql_default_connection");
}

/*---------------------------------------------------
 * view change simple mini full
----------------------------------------------------*/
// シンプルビューのセット
void MainWindow::setSimpleView(bool enable)
{
    pop->setHidden(true);
    ui->actionSimpleView->setChecked(enable);

    // miniscreen, fullscreen の強制解除
    if (miniScreen) {
        setMiniScreenView(false);
        return;
    }
    if (this->isFullScreen()) {
        setFullScreenView(false);
        return;
    }

    settings->beginGroup("SimpleView");
    bool chkEnableToolBar = settings->value("chkEnableToolBar").toBool();
    bool chkEnableDock = settings->value("chkEnableDock").toBool();
    bool chkEnableInfo = settings->value("chkEnableInfo").toBool();
    settings->endGroup();

    if (chkEnableToolBar) ui->toolBar->setHidden(enable);
    if (chkEnableDock) setDockNaviState(enable? false : dockNaviPrevState, false);
    if (chkEnableInfo) setDockInfoState(enable? false : dockInfoPrevState, false);
}

// 他ウィジェットのコンテキストメニューで再利用する
auto MainWindow::getSimpleViewAction() -> QAction *
{
    return ui->actionSimpleView;
}

// subWindowをfullscreenにするのに必要
void MainWindow::setForcedWindowView(bool enable)
{
    ui->toolBar->setHidden(enable);
    setDockNaviState(!enable, true);
    setDockInfoState(!enable, true);
    setMdiAreaTabMode(!enable);
    int playerCount = 0;
    foreach (auto subWin, ui->mdiArea->subWindowList()) {
        bool isPlayer = (getSubWindowClassName(subWin) == "WidgetPlayer");
        if (isPlayer) playerCount++;
        subWin->setWindowFlag(Qt::FramelessWindowHint, enable);
        subWin->setVisible(enable? isPlayer : true);
        if (!enable) subWin->showMaximized();
    }
    multiPlayer = (playerCount > 1);
    if (enable) updateMultiPlayWindowSize();
}

void MainWindow::updateMultiPlayWindowSize()
{
    if (multiPlayer)
        ui->mdiArea->tileSubWindows();
    else
        ui->mdiArea->activeSubWindow()->showMaximized();
}

// FullScreenのサイズ可変版
// 位置やサイズは、別に記憶
void MainWindow::setMiniScreenView(bool enable)
{
    if (isFullScreen() && enable) setFullScreenView(false);
    if (miniScreen == enable) return;

    moveEventBlock = true;
    if (enable) {
        saveNormalGeometry();           // ノーマルサイズを保存
        setForcedWindowView(true);      // MdiTabモードを強制解除して、プレイヤーだけにする
        setGeometry(miniGeometry);      // ミニスクリーンのサイズ復元
        miniScreen = true;
        setMiniScreenFrameless(true);   // フレームレスにする
    } else {
        if (miniScreenFrameless) setMiniScreenFrameless(false);
        miniGeometry = geometry();      // ミニスクリーンのサイズ保存
        restoreNormalGeometry();        // 元のサイズに戻す
        setForcedWindowView(false);     // MdiTabモードに戻す
        miniScreen = false;

        // シンプル表示の復元
        setSimpleView(ui->actionSimpleView->isChecked());
    }
    emit mainStatusChanged(MainStatus::MiniScreen, miniScreen);
    moveEventBlock = false;
}

// Framelessに変えると、サイズ変更や移動ができなくなるので
// マウス移動イベントで切替える
void MainWindow::setMiniScreenFrameless(bool enable)
{
    if (!isActiveWindow("WidgetPlayer")) return;
    if (!miniScreen) return;
    if (miniScreenFrameless == enable) return;

    moveEventBlock = true;
    setWindowFlag(Qt::WindowStaysOnTopHint, enable && pinEnable);
    if (enable && !(windowFlags() & Qt::FramelessWindowHint)) {
        miniGeometry = getRectFrameless();
        setWindowFlag(Qt::FramelessWindowHint, true);
    } else {
        miniGeometry = getRectNotFrameless();
        setWindowFlag(Qt::FramelessWindowHint, false);
    }
    show();
    setGeometry(miniGeometry);
    updateMultiPlayWindowSize();
    // NOTE:setWindowFlagで非表示になったのをshowで復元しても
    // アプリがフォーカスを失い、ショートカットが動かなくなる
    // Qt6で、activeWindow()にするように警告がでるが、希望の動作にならないので保留
    qApp->setActiveWindow(this);
    miniScreenFrameless = enable;
    moveEventBlock = false;
}

// FullScreen
// MDIタブや、Dockタブを全て非表示にして、プレイヤーを
// MainWindow内で最大表示にして、MainWindowをFullScreenにする
void MainWindow::setFullScreenView(bool enable)
{
    if (miniScreen && enable) setMiniScreenView(false);

    if (enable && !isFullScreen()) {
        saveNormalGeometry();           // ノーマルサイズを保存
        setForcedWindowView(true);      // MdiTabモードを強制解除して、プレイヤーだけにする
        showFullScreen();
    } else {
        restoreNormalGeometry();        // 元のサイズに戻す
        setForcedWindowView(false);     // MdiTabモードに戻す
        // シンプル表示の復元
        setSimpleView(ui->actionSimpleView->isChecked());
    }
    emit mainStatusChanged(MainStatus::FullScreen, isFullScreen());
}

/*---------------------------------------------------
 * WidgetRecorded
----------------------------------------------------*/
auto MainWindow::openRecorded() -> WidgetRecorded*
{
    auto* widget = new WidgetRecorded(this);
    loadSubWindows(widget, { ui->actionRecordedCloseOther, ui->actionRecordedCloseAll });
    return widget;
}

// 録画済みウィジェットの取得
auto MainWindow::getRecorded() -> WidgetRecorded *
{
    QString className = "WidgetRecorded";

    // アクティブな録画済みウィジェットを取得
    auto active = getActiveWindow();
    if (active != nullptr) {
        if (getSubWindowClassName(active) == className) {
            auto recorded = qobject_cast<WidgetRecorded*>(active->widget());
            if (!recorded->isPlaylist())
                return recorded;
        }
    }
    // 現在開かれているタブから探す
    foreach (auto win, ui->mdiArea->subWindowList(QMdiArea::StackingOrder)) {
        if (getSubWindowClassName(win) == className) {
            auto recorded = qobject_cast<WidgetRecorded*>(win->widget());
            if (!recorded->isPlaylist()) {
                ui->mdiArea->setActiveSubWindow(win);
                return recorded;
            }
        }
    }
    // ウィジェットが無ければ新規作成
    return openRecorded();
}

void MainWindow::openPlaylist(const QString &file)
{
    WidgetRecorded *recorded = openRecorded();
    if (!recorded->openPlaylist(file)) {
        getActiveWindow()->close();
        pop->show(tr("プレイリストが無効です。"));
        return;
    }
}

void MainWindow::setSearchTag(const QString &tag)
{
    WidgetRecorded *recorded = getRecorded();
    if (recorded) recorded->setSearch(tag);
}

// tagでフィルター
bool MainWindow::setFilterTag(const QString &field, const QString &tag, const int &selectID) {
    WidgetRecorded *recorded = getRecorded();
    if (recorded) {
        QString value = tag;
        value.replace("[", "\\[");
        value.replace("]", "\\]");
        recorded->resetFiler();
        return recorded->setFilter(field, value, selectID);
    }
    return false;
}

void MainWindow::searchWeb(const QString &word)
{
    QString text = word;
    if (text.isEmpty()) return;
    text.truncate(30);
    text.replace(QRegularExpression("#"), "");
    text = text.trimmed();
    QString value = tr("https://www.google.com/search?q=%1").arg(text);
    QProcess::startDetached("xdg-open", QStringList() << QUrl(value).toString());
}

void MainWindow::setBlackList(int id, bool checked)
{
    sqlModel->setInit("tags", "id");
    sqlModel->setValueClear();
    sqlModel->setValue("blackflag", checked);
    sqlModel->saveValues(id);
    updateTag(id);
}

void MainWindow::updatePlaylist(const QString &selectFile)
{
    dockPlaylist->updatePlaylist(selectFile);
}

bool MainWindow::deleteRecorded(const QList<int> &ids)
{
    // NOTE: api経由の削除
    // 大量に件数がある場合、フリーズしている様に見えるので
    // プログレスダイアログ化
    if (!ids.count()) return false;
    if (!deleteConfirm(this)) return false;

    ProgressDlg progress(this, "録画削除", "録画削除中...", ids.count());
    foreach (auto id, ids) {
        if (progress.setValueIncWasCanceled()) break;
        getApi()->deleteRecorded(id);
    }
    progress.setValue(ids.count());
    sqlModel->setInit("play", "id");
    sqlModel->deleteRecord(ids);

    // 参照する録画レコードが無いので、Tag側をフル更新
    emit mainStatusChanged(MainStatus::VideoDeleted);

    return true;
}

void MainWindow::deleteParsent(const QList<int> &ids)
{
    if (!ids.count()) return;

    sqlModel->setInit("play", "id");
    sqlModel->deleteRecord(ids);
    updateTagTableRecorded(ids);
}

void MainWindow::setFullParsent(const QList<int> &ids)
{
    if (!ids.count()) return;

    sqlModel->setInit("play", "id");
    QString sql = "select (endat-startat)/1000 as videotime from recorded where id=%1";
    foreach (auto id, ids) {
        auto videoTime = sqlModel->getScholar(sql.arg(id)).toInt();
        if (videoTime) {
            sqlModel->setValueClear();
            sqlModel->setValue("seek", videoTime);
            sqlModel->setValue("percent", 100);
            sqlModel->saveValues(id);
        }
    }
    updateTagTableRecorded(ids);
}

/*---------------------------------------------------
 * WidgetEPGStation
----------------------------------------------------*/
void MainWindow::openEPGStation(const QString &path /*= ""*/)
{
    auto widget = getEPGStation();
    widget->load(path);
}

void MainWindow::openEPGProgram(const QString &type /*= ""*/)
{
    static const QStringList types = {"GR", "BS", "CS"};
    int index = settings->value("DockLive/btnGrpLiveDefault").toInt();
    auto param = getApi()->isV2? "guide" : "program";
    openEPGStation(tr("%1?type=%2").arg(param, type.isEmpty()? types.at(index) : type));
}

void MainWindow::openEPGChannel(const QString &channelId)
{
    auto format = getApi()->isV2? "guide?channelId=%1" : "program?ch=%1";
    openEPGStation(tr(format).arg(channelId));
}

// 多重起動防止
auto MainWindow::getEPGStation() -> WidgetEPGStation *
{
    auto subWin = getSubWindowInstance("WidgetEPGStation");
    if (subWin != nullptr) {
        ui->mdiArea->setActiveSubWindow(subWin);
        return qobject_cast<WidgetEPGStation*>(subWin->widget());
    }
    // ウィジェットが無ければ新規作成
    auto result = new WidgetEPGStation(this);
    loadSubWindows(result);
    return result;
}

/*---------------------------------------------------
 * WidgetTag
----------------------------------------------------*/
void MainWindow::openTagWidget(int id, const QString &field, const QString &genre)
{
    auto widget = getTagWidget();
    widget->setFilter(id, field, genre);
}

WidgetTag *MainWindow::getTagWidget()
{
    auto subWin = getSubWindowInstance("WidgetTag");
    if (subWin != nullptr) {
        ui->mdiArea->setActiveSubWindow(subWin);
        return qobject_cast<WidgetTag*>(subWin->widget());
    }
    // ウィジェットが無ければ新規作成
    auto result = new WidgetTag(this);
    loadSubWindows(result);
    return result;
}

/*---------------------------------------------------
 * DockInfo
----------------------------------------------------*/
void MainWindow::setDockInfoState(bool enable, bool savePrevState /*= true*/)
{
    dockInfo->setHidden(!enable);
    ui->actionInfo->setChecked(enable);
    emit mainStatusChanged(MainStatus::DockInfo, enable);
    // simplview操作で、現状を復元するのに使用
    if (savePrevState) dockInfoPrevState = enable;
}

// 他のウィジットで再利用
auto MainWindow::getDockInfoAction() -> QAction *
{
    return ui->actionInfo;
}

void MainWindow::setDockInfoId(InfoType type, const QString &idStr, const QString &searchWord)
{
    dockInfo->loadPage(type, idStr, searchWord);
}

void MainWindow::setDcokInfoPlayingId(int id)
{
    dockInfo->setPlayingId(id);
}

/*---------------------------------------------------
 * DockNavi
----------------------------------------------------*/
void MainWindow::setActiveDockTab(DockNaviType type)
{
    int index = -1;
    QTabBar* tabBar = getDockTabBarIndex(docks[static_cast<int>(type)], index);
    if (tabBar != nullptr && index != -1)
        tabBar->setCurrentIndex(index);
}

void MainWindow::setDockNaviState(bool enable, bool savePrevState)
{
    if (!enable) {
        auto tabBar = getDockTabBar(dockTag);
        if (tabBar != nullptr) saveTabIndex = tabBar->currentIndex();
    }

    foreach (auto dock, docks)
        dock->setHidden(!enable);
    ui->actionNavi->setChecked(enable);

    if (enable) {
        auto tabBar = getDockTabBar(dockTag);
        if (tabBar != nullptr) tabBar->setCurrentIndex(saveTabIndex);
    }

    // simplview操作で、現状を復元するのに使用
    if (savePrevState) dockNaviPrevState = enable;
}

auto MainWindow::getDockNaviState() -> bool
{
    foreach (auto dock, docks)
        if (dock->isVisible()) return true;
    return false;
}

// 他のウィジットで再利用
auto MainWindow::getDockNaviAction() -> QAction *
{
    return ui->actionNavi;
}

auto MainWindow::getDockTabBarIndex(QDockWidget *dock, int &index) -> QTabBar *
{
    foreach (QTabBar* tabBar, findChildren<QTabBar*>()) {
        for (int i = 0; i < tabBar->count(); ++i) {
            if (dock == (QWidget*)tabBar->tabData(i).toULongLong()) {
                index = i;
                return tabBar;
            }
        }
    }
    return nullptr;
}

/*---------------------------------------------------
 * tag 共通
----------------------------------------------------*/
// Tagキーワードに分解
auto MainWindow::parseStr(const QString &value) -> QStringList
{
    QStringList result;
    QString parseStr = value;
    QStringList splitWord;

    parseStr.replace("\r", "\n");
    parseStr.replace("\n\n", "\n");
    parseStr.replace("\n", " ");
    parseStr.replace("[", " [");
    parseStr.replace("]", "] ");
    parseStr.replace("第", " 第");
    parseStr.replace("話", "話 ");
    parseStr.replace("<", " <");
    parseStr.replace(">", "> ");

    splitWord << "[S]" <<  "「" << "」" << "『" <<  "』" << "【" <<  "】" <<  "★"
              <<  "☆" <<  "!?" <<  "!" <<  "。" <<  "." <<  "・" << "#"
              << "♯" <<  "(" <<  ")" <<  "~" <<  "、" <<  "《" <<  "》";
    foreach (auto word, splitWord)
        parseStr.replace(word, " ");

    parseStr = parseStr.trimmed();
    parseStr = parseStr.simplified();
    foreach (auto word, parseStr.split(" "))
        result << word;
    return result;
}

// 番組タイトルから、番組名抽出
auto MainWindow::parseStrTitle(const QString &value) -> QString
{
    QString result = value;
    QStringList baseTrim = {
        "\\[.\\]",    // [新][解][字][再][終]など
        "\\[SS\\]",
        "【.*】",     // サブタイトル、キャストなど
        "<.*>"       // 番組枠名など
    };
    QRegularExpression reg(baseTrim.join("|"), QRegularExpression::CaseInsensitiveOption);
    result.replace(reg, "");
    result = result.trimmed();

    // 話数が始まるキーワード以降は排除
    reg.setPattern(listRegBackward.join("|"));
    result.replace(reg, "");
    result = result.trimmed();

    // 番組枠名の排除
    reg.setPattern(listRegForward.join("|"));
    result.replace(reg, "");
    result = result.trimmed();

    // 鉤括弧の統一
    result.replace("『", "「");
    result.replace("』", "」");

    // 「で始まったら、カッコが閉じたところまで抽出
    reg.setPattern("^「.*");
    auto mach = reg.match(result);
    if (mach.hasMatch()) {
        reg.setPattern("^「|」.*");
        result.replace(reg, "");
    }
    // 後半に「」がある場合は切捨て（サブタイトルなど）
    reg.setPattern(".*「.*");
    mach = reg.match(result);
    if (mach.hasMatch()) {
        reg.setPattern("「.*");
        result.replace(reg, "");
    }
    result = result.trimmed();
    return result;
}

void MainWindow::updateRegSetting()
{
    listRegForward = QStringList();
    listRegBackward = QStringList();
    auto regForward = settings->value("DockTag/listRegForward").toStringList();
    auto regBackward = settings->value("DockTag/listRegBackward").toStringList();
    foreach (auto value, regForward)
        if (!value.isEmpty()) listRegForward << "^" + value.trimmed();
    foreach (auto value, regBackward)
        if (!value.isEmpty()) listRegBackward << value.trimmed() + ".*";
}

void MainWindow::openTagAddDlg(int id)
{
    DialogTagAdd dlg(id, this);
    if (dlg.exec() == QDialog::Accepted)
        updateTag(dlg.addTagId);
}

bool MainWindow::openTagEditDlg(int id)
{
    DialogTagEdit dlg(this, id);
    if (dlg.exec() == QDialog::Accepted) {
        updateTag(id);
        return true;
    }
    return false;
}

bool MainWindow::openTagEditDlg(const QString &field, const QString &genre)
{
    DialogTagEdit dlg(this, -1, field, genre);
    if (dlg.exec() == QDialog::Accepted) {
        updateTag(dlg.getId());
        return true;
    }
    return false;
}

void MainWindow::openTagDelete(int id)
{
    sqlModel->setInit("tags", "id");
    auto rs = sqlModel->getRecord(id);

    TagItem tagItem(rs);
    DialogTagDelete dlg(&tagItem, this);
    if (dlg.exec() == QDialog::Accepted) {
        if (dlg.isDeletedTag)
            updateTag(-1);
    }
}

bool MainWindow::openTagThumbnail(int id)
{
    DialogTagThumbnail dlg(id, this);
    if (dlg.exec() == QDialog::Accepted) {
        updateTag(id);
        return true;
    }
    return false;
}

void MainWindow::addTagEndFlag(const QList<int> &ids)
{
    if (!ids.length()) return;
    QStringList idsStr;
    foreach (auto id, ids) idsStr << QString::number(id);

    sqlModel->setInit("recorded", "id", "v_recorded", "番組タイトル, ジャンル" , tr("id in (%1)").arg(idsStr.join(",")));
    auto tagIds = tagTable->addTagEndFlag(sqlModel->getModelList());
    emit mainStatusChanged(MainStatus::TagTabel);
}

void MainWindow::updateTag(int id /*= -1*/)
{
    dockTag->setUpdateAfterSelectId(id);
    updateTagTable(id);
}

void MainWindow::selectTag(int id)
{
    dockTag->selectTag(id);
}

/*---------------------------------------------------
 * WidgetPlayer
----------------------------------------------------*/
// playListなしで、単独再生
void MainWindow::openPlayer(const PlayItem &item)
{
    auto player = getPlayer();
    player->getPlaylist()->reset();
    player->openPlayItem(item);
}

void MainWindow::openPlayer(int recordedId)
{
    PlayItem item;
    item.setValueRecorded(recordedId);
    openPlayer(item);
}

void MainWindow::openPlayerLive(const QString &channelId, const QString &title)
{
    PlayItem item;
    item.setValueLive(channelId, title);
    openPlayer(item); // playListなしで、単独再生
}

// DocInfoからの、videoId指定呼び出し
void MainWindow::openPlayerVideoId(int videoId)
{
    PlayItem item;
    item.setValueVideoId(videoId);
    if (getSettings()->value("WidgetRecorded/chkInnerPlayerRecorded", true).toBool()) {
        openPlayer(item); // playListなしで、単独再生
        return;
    }
    // 外部プレイヤー
    Playlist playList;
    playList.addItem(item);
    QString saveFileName = "recoeded_playlist.m3u8";
    if (playList.saveFile(saveFileName))
        QProcess::startDetached("xdg-open", QStringList() << saveFileName);
}

void MainWindow::openPlayerRecordedIds(QList<int> ids, int selected)
{
    if (ids.count() == 1) {
        PlayItem item;
        item.setValueRecorded(ids[0]);
        openPlayer(item); // playListなしで、単独再生
        return;
    }
    if (selected != -1) {
        // プレイヤーの再利用
        foreach (auto subWin, getSubWindowInstanceAll("WidgetPlayer")) {
            auto playlist = qobject_cast<WidgetPlayer*>(subWin->widget())->getPlaylist();
            // tag以外で開いているプレイヤーで、idがあれば
            if (!playlist->isTag()) {
                if (playlist->hasRecordedId(selected)) {
                    playlist->setSelectedId(selected);
                    ui->mdiArea->setActiveSubWindow(subWin);
                    return;
                }
            }
        }
    }
    auto player = getPlayer();
    auto playlist = player->getPlaylist();
    playlist->setRecordedIds(ids, selected);
}

void MainWindow::openPlayerTag(int tagId, int selected)
{
    if (selected != -1) {
        // プレイヤーの再利用
        foreach (auto subWin, getSubWindowInstanceAll("WidgetPlayer")) {
            auto playlist = qobject_cast<WidgetPlayer*>(subWin->widget())->getPlaylist();
            if (playlist->hasTagId(tagId, selected)) {
                playlist->setSelectedId(selected);
                ui->mdiArea->setActiveSubWindow(subWin);
                return;
            }
        }
    }
    auto player = getPlayer();
    auto playlist = player->getPlaylist();
    playlist->setTagId(tagId, selected);
}

// プレイリストファイル
void MainWindow::openPlayerFile(const QString &playListFile)
{
    if (!settings->value("chkInnerPlayerEPG").toBool()) {
        QProcess::startDetached("xdg-open", QStringList() << playListFile);
        return;
    }

    auto player = getPlayer();
    auto playlist = player->getPlaylist();
    if (!playlist->readPlayFile(playListFile)) {
        pop->show(QStringList() << "エラー" << "プレイリストが開けません" << "ファイル:" + playListFile);
        return;
    }
    if (!playlist->hasItem()) {
        pop->show(QStringList() << "エラー" << "アイテムがありません" << "ファイル:" + playListFile);
        return;
    }
}

WidgetPlayer *MainWindow::getPlayer()
{
    bool chkMultiPlayer = settings->value("WidgetPlayer/chkMultiPlayer").toBool();
    if (!chkMultiPlayer) {
        auto subWin = getSubWindowInstance("WidgetPlayer");
        if (subWin != nullptr) {
            ui->mdiArea->setActiveSubWindow(subWin);
            return qobject_cast<WidgetPlayer*>(subWin->widget());
        }
    }
    // ウィジェットが無ければ新規作成
    return getPlayerNew();
}

WidgetPlayer *MainWindow::getPlayerActive()
{
    auto subWin = getActiveWindow();
    if (subWin != nullptr) {
        if (getSubWindowClassName(subWin) == "WidgetPlayer")
            return qobject_cast<WidgetPlayer*>(subWin->widget());
    }
    return nullptr;
}

WidgetPlayer *MainWindow::getPlayerNew()
{
    auto player = new WidgetPlayer(this);
    connect(player, &WidgetPlayer::playerStatusChanged, this, [=] (PlayerStatus status, int id) {
        if (status == PlayerStatus::PlayerClose) {
            auto playerCount = getPlayerAll().count();
            // プレイヤー終了時のスクリーン表示処理
            if (isMiniScreen() || isFullScreen()) {
                multiPlayer = (playerCount > 1);
                updateMultiPlayWindowSize();
            }

            // MPRIS D-Bus Interface
            if (!playerCount) setupMprisDbusConnection(nullptr);
        } else {
            emit playerStatusChanged(status, id);
        }
    });

    loadSubWindows(player, { ui->actionPlayerCloseOther, ui->actionPlayerCloseAll });
    return player;
}

QList<WidgetPlayer*> MainWindow::getPlayerAll()
{
    QList<WidgetPlayer*> result = QList<WidgetPlayer*>();
    foreach (auto win, getSubWindowInstanceAll("WidgetPlayer"))
        result << qobject_cast<WidgetPlayer*>(win->widget());
    return result;
}

auto MainWindow::getActivePlayerPlaylist() -> Playlist*
{
    auto player = getPlayerActive();
    if (player != nullptr)
        return player->getPlaylist();
    return nullptr;
}

bool MainWindow::isPlaying()
{
    foreach (auto widgetPlayer, getPlayerAll()) {
        if (widgetPlayer->isPlaying())
            return true;
    }
    return false;
}

void MainWindow::inhibitSleep(bool isInhibit)
{
    if (sleepBbusInterface == nullptr) return;

    if (isInhibit) {
        QDBusReply<uint> reply = sleepBbusInterface->call("Inhibit", "QEPGS-Player", "再生中");
        if (reply.isValid()) sleepCookies << reply.value();
    } else {
        if (sleepBbusInterface != nullptr) {
            foreach(uint sleepCookie, sleepCookies)
                sleepBbusInterface->call("UnInhibit", sleepCookie);
            sleepCookies.clear();
        }
    }
}

void MainWindow::updateInhibitSleep()
{
    bool chkInhibitSleep = settings->value("WidgetPlayer/chkInhibitSleep").toBool();
    if (chkInhibitSleep) {
        inhibitSleep(isPlaying());
        return;
    }
    inhibitSleep(false);
}

// MPRIS D-Bus Interface
void MainWindow::setupMprisDbusConnection(WidgetPlayer* player/* = nullptr*/)
{
    static bool isInitial = false;
    auto mpris2Name = QString("org.mpris.MediaPlayer2.QEPGS_Player.instance%1")
            .arg(qApp->applicationPid());

    if (player != nullptr) {
        QDBusConnection::sessionBus().registerService(mpris2Name);
        new DBusMediaPlayer2(player, this);
        new DBusMediaPlayer2Player(player, this);
        QDBusConnection::sessionBus().registerObject("/org/mpris/MediaPlayer2", this, QDBusConnection::ExportAdaptors);
        if (isInitial) emit mprisActivePlayerChanged();
        if (!isInitial) isInitial = true;
    } else {
        // NOTE: serviceをunregistすると、Plasmaメディアプレイヤーからタブが消えるのでブランクに切り替える
        if (isInitial) emit mprisNoPlayer();
    }
}

void MainWindow::sendMprisPropertiesChanged(const QObject *adaptor, const QVariantMap &properties)
{
    auto message = QDBusMessage::createSignal("/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties", "PropertiesChanged" );

    QVariantList args;
    args << adaptor->metaObject()->classInfo(0).value();
    args << properties;
    args << QStringList();

    message.setArguments(args);
    QDBusConnection::sessionBus().send(message);
}

/*---------------------------------------------------
 * Encord
----------------------------------------------------*/
bool MainWindow::openEncordDlg(const QList<int> &ids)
{
    if (ids.count() == 1) {
        // 録画中かチェックする
        sqlModel->setInit("video_file", "id");
        int videoCount = 0;
        foreach (auto size, sqlModel->getValues("size", tr("recordedId = %1").arg(ids.at(0)))) {
            if (size.toDouble() > 0) videoCount++;
        }
        if (!videoCount) {
            QMessageBox::information(this, "エンコード",
                        "エンコードする動画がありません\n録画中の場合は録画が終了してから再度実行してください");
            return false;
        }
    }

    DialogEncord dlg(ids, this);
    if (dlg.exec() == QDialog::Accepted) return true;
    return false;
}

bool MainWindow::openEncordDlg(int id)
{
    return openEncordDlg(QList<int>{id});
}

bool MainWindow::openEncordDlg(const QString &field, const QString &tag)
{
    sqlModel->setInit("v_recorded", "id");
    QString escapedTag = sqlModel->escape(tag);
    auto ids = sqlModel->getValuesId(tr("%1 like '%%2%'").arg(field, escapedTag));
    return openEncordDlg(ids);
}

bool MainWindow::cancelEncord(const int &encId)
{
    if (encId == -1) return false;
    getApi()->deleteResource(tr("/api/encode/%1").arg(encId));
    return true;
}

bool MainWindow::cancelEncord()
{
    QList<int> ids;
    foreach (auto item, encordingItems) ids << item.id;
    return cancelEncord(ids);
}

bool MainWindow::cancelEncord(const QList<int> &ids)
{
    QList<int> encIds;
    updateEncordingList();
    foreach (auto id, ids) {
        int encId = getEncordElement(id).encId;
        if (encId != -1) encIds << encId;
    }
    if (!encIds.count()) return false;

    ProgressDlg progress(this, "エンコード", "エンコードキャンセル中...", encIds.count());
    foreach (auto encId, encIds) {
        if (progress.setValueIncWasCanceled()) break;
        getApi()->deleteResource(tr("/api/encode/%1").arg(encId));
    }
    return true;
}

bool MainWindow::cancelEncord(const QString &field, const QString &tag)
{
    sqlModel->setInit("v_recorded_light", "id");
    QString escapedTag = sqlModel->escape(tag);
    auto ids = sqlModel->getValuesId(tr("%1 like '%%2%'").arg(field, escapedTag));
    return cancelEncord(ids);
}

// DockInfoのインターバルタイマーで定期更新される
int MainWindow::updateEncordingList()
{
    encordingItems.clear();
    if (getApi()->isV2) {
        auto json = getApi()->get("/api/encode?isHalfWidth=true");
        foreach (auto item, json.value("runningItems").toArray()) {
            auto element = parseEncordItem(item);
            element.isEncording = true;
            element.encPercent = item.toObject()["percent"].toDouble() * 100;
            encordingItems << element;
        }
        foreach (auto item, json.value("waitItems").toArray()) {
            auto element = parseEncordItem(item);
            element.encPercent = 0;
            encordingItems << element;
        }
    }
    return encordingItems.count();
}

RecElement MainWindow::parseEncordItem(const QJsonValue &item)
{
    RecElement element;
    element.encId = item["id"].toInt();
    element.encMode = item["mode"].toString();
    element.id = item["recorded"]["id"].toInt();
    return element;
}

RecElement MainWindow::getEncordElement(int id)
{
    RecElement element;
    foreach (auto item, encordingItems) {
        if (id == item.id) { element = item; break; }
    }
    return element;
}

/*---------------------------------------------------
 * Setting
----------------------------------------------------*/
void MainWindow::openSettings(SettingMenu menu, int tabIndex /* = -1*/)
{
    DialogSettings dlg(menu, this, tabIndex);

    if (dlg.exec() == QDialog::Accepted) {
        readBaceSetting();
        updateInhibitSleep();
    }
}

void MainWindow::openSettings(const QString &name, int tabIndex /* = -1*/)
{
    SettingMenu nemu = SettingMenu::General;
    if (name == "EPGStation") nemu = SettingMenu::EPGStation;
    if (name == "Database") nemu = SettingMenu::Database;
    if (name == "General") nemu = SettingMenu::General;
    if (name == "Player") nemu = SettingMenu::Player;
    if (name == "Recorded") nemu = SettingMenu::Recorded;
    if (name == "Tag") nemu = SettingMenu::Tag;
    if (name == "Playlist") nemu = SettingMenu::Playlist;
    if (name == "LiveTV") nemu = SettingMenu::LiveTV;
    if (name == "SimpleView") nemu = SettingMenu::SimpleView;
    if (name == "Version") nemu = SettingMenu::Version;
    openSettings(nemu, tabIndex);
}

void MainWindow::readBaceSetting()
{

    // default make path
    settings->makePaths();

    // 番組タイトル、番組名抽出設定読込
    updateRegSetting();

    // epgstaition
    if (!rsetapi->readEpgstation()) {
        QStringList msg;
        msg << "EPGStationに接続できませんでした";
        msg << "接続設定を確認して下さい\n";

        QMessageBox::critical(this, "EPGStation接続", msg.join("\n"));
        DialogSettings dlg(SettingMenu::EPGStation, this);
        if (dlg.exec() != QDialog::Accepted) {
            // qApp->quit() だと終了できないのでハックする
            QTimer::singleShot(250, qApp, SLOT(quit()));
            return;
        }
        // テスト済なので、チェック不要
        rsetapi->readEpgstation();
    }

    // database
    if (!openDB()) {
        DialogSettings dlg(SettingMenu::Database, this);
        if (dlg.exec() != QDialog::Accepted) {
            // qApp->quit() だと終了できないのでハックする
            QTimer::singleShot(250, qApp, SLOT(quit()));
            return;
        }
        // テスト済なので、チェック不要
        openDB();
    }

    // リストア処理
    auto restoreFile = settings->value("DB/restoreDb").toString();
    if (QFileInfo(restoreFile).exists()) {
        // 既存の拡張を初期化
        sqlModel->execSqlFile(":/sql/delete_all.sql");
        // DB拡張の再構築
        buildDbExtended(true);
        // データの復元
        sqlModel->execSqlFile(restoreFile);
        // 自動採番再設定
        if (sqlModel->isPgsql()) {
            sqlModel->execSql("select setval ('search_id_seq', (select max(id) from public.search));");
            sqlModel->execSql("select setval ('tags_id_seq', (select max(id) from public.tags));");
        }
        // リストア指定解除
        settings->setValue("DB/restoreDb", "");
    } else {
        // 拡張テーブルの存在確認後、自動構築
        buildDbExtended();
    }

    // バージョンチェック
    if (settings->value("chkVersionCheck").toBool()) {
        if (!qApp->applicationVersion().isEmpty()) {
            double current = versionCast(qApp->applicationVersion());
            auto versions = getApi()->readRssTag();
            if (!versions.count()) return;

            double newest = versionCast(versions[0]);
            if (newest > current) {
                QStringList msg;
                msg << tr("新しいバージョンが見つかりました");
                msg << tr("現在のバージョン： %1").arg(qApp->applicationVersion());
                msg << tr("最新のバージョン： %1").arg(versions[0]);
                msg << tr("リリースページを確認しますか？");

                if (QMessageBox::Ok == QMessageBox::information(this, "バージョンチェック", msg.join("\n"), QMessageBox::Ok|QMessageBox::Cancel)) {
                    QProcess::startDetached("xdg-open", QStringList() << QUrl(releasesUrl).toString());
                }
            }
        }
    }

    // DBus MPRIS
    mpris = settings->value("WidgetPlayer/chkMPRIS").toBool();
}

void MainWindow::readStatusSettings()
{
    restoreGeometry(settings->valueObj(this, "geometry").toByteArray());
    restoreState(settings->valueObj(this, "windowState").toByteArray());
    dockInfoPrevState = settings->valueObj(this, "dockInfoPrevState", true).toBool();
    dockNaviPrevState = settings->valueObj(this, "dockNaviPrevState", true).toBool();
    normalGeometry = settings->valueObj(this, "normalGeometry").toRect();
    miniGeometry = settings->valueObj(this, "miniGeometry").toRect();
    // TODO: windowStateで、タブ化したQDockWidgetのWindows幅が保存復元できない
    // Qt6で改善
}

void MainWindow::writeStatusSettings()
{
    settings->setValueObj(this, "geometry", saveGeometry());
    settings->setValueObj(this, "windowState", saveState());
    settings->setValueObj(this, "dockInfoPrevState", dockInfoPrevState);
    settings->setValueObj(this, "dockNaviPrevState", dockNaviPrevState);
    settings->setValueObj(this, "normalGeometry", normalGeometry);
    settings->setValueObj(this, "miniGeometry", miniGeometry);

    // TODO: windowStateで、タブ化したQDockWidgetのWindows幅が保存復元できない
    // Qt6で改善
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    setSimpleView(false);
    ui->mdiArea->closeAllSubWindows();
    if (ui->mdiArea->currentSubWindow()) {
        event->ignore();
    } else {
        writeStatusSettings();
        foreach (auto doc, docks) doc->close();
        event->accept();
    }
}

/*---------------------------------------------------
 * Database
----------------------------------------------------*/
QSqlError MainWindow::openDbConnection(const QString &driver, const QString &dbName, const QString &host, const QString &user, const QString &pass, int port)
{
    QSqlError err;
    QSqlDatabase db = QSqlDatabase::database("qt_sql_default_connection", false);
    static QString prevDriver = "";
    if (driver != prevDriver || !db.isOpen()) {
        // driver変更なしで、切断すると不安定になる
        QSqlDatabase::database("qt_sql_default_connection", false).close();
        QSqlDatabase::removeDatabase("qt_sql_default_connection");
        db = QSqlDatabase::addDatabase(driver, "qt_sql_default_connection");
        prevDriver = driver;
    }
    db.setDatabaseName(dbName);
    db.setHostName(host);
    db.setPort(port);
    if (!db.open(user, pass)) {
        err = db.lastError();
        db = QSqlDatabase();
        QSqlDatabase::removeDatabase("qt_sql_default_connection");
    }
    return err;
}

void MainWindow::onSqlError(const QString &sql, const QSqlError &lastError)
{
    QStringList msg;
    switch (lastError.type()) {
    case QSqlError::NoError: return;
    case QSqlError::ConnectionError     : msg << "DB接続エラー"; break;
    case QSqlError::StatementError      : msg << "構文エラー"; break;
    case QSqlError::TransactionError    : msg << "トランザクションエラー"; break;
    default: msg << "未知のエラー"; break;
    }
    msg << "" << "クエリ:" << sql << "" << "詳細：" << lastError.text();
    pop->show(msg);
    qDebug() << msg;
}

auto MainWindow::openDB() -> bool
{
    settings->beginGroup("DB");
    auto driver = settings->value("dbType").toString();
    auto host = settings->value("host").toString();
    auto dbName = settings->value("dbName").toString();
    auto user = settings->value("user").toString();
    auto pass = settings->value("pass").toString();

    auto portStr = settings->value("editDbPort").toString();
    auto portDefault = settings->value("chkDbPortDefault").toBool();
    settings->endGroup();

    bool isNum = false;
    auto port = portStr.toInt(&isNum);
    if (!isNum || portDefault)
        port = -1;
    QSqlError err = openDbConnection(driver, dbName, host, user, pass, port);

    dbPrevConnect = (err.type() == QSqlError::NoError);
    if (dbPrevConnect) {
        emit mainStatusChanged(MainStatus::Database);
        return true;
    }

    QStringList msg;
    msg << "データベースに接続できませんでした";
    msg << "接続設定を確認して下さい\n";
    msg << "エラー詳細：";
    msg << err.text();

    QMessageBox msgBox;
    msgBox.setWindowTitle("DB接続エラー");
    msgBox.setText(msg.join("\n"));
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setIcon(QMessageBox::Icon::Warning);
    msgBox.exec();
    return false;
}

// DB拡張
void MainWindow::buildDbExtended(bool isRestore/* = false*/)
{
    auto db = QSqlDatabase::database();
    auto tables = db.tables(QSql::TableType::AllTables);

    // NOTE: 構築するTable、Viewのリスト
    // 名称と、スクリプト名を同一にしておく、依存関係の呼び出し順序も制御する
    // Functionは、関連する、table、viewと同じファイルに定義
    static const QStringList extTables = {
        "temp_vars", "genre1", "genre2", "play", "search", "tags",
        "v_recorded", "v_recorded_detail", "v_programs_detail" };

    static const QStringList extViewsV2 = { // v2で定義が変わったview
        "v_recorded", "v_recorded_detail", "v_programs_detail" };

    // テーブルの作成
    foreach (auto tableName, extTables) {
         if (!tables.contains(tableName)) {
            auto format = getApi()->isV2 && extViewsV2.contains(tableName)? ":/sql/%1/%2_v2.sql" : ":/sql/%1/%2.sql";
            sqlModel->execSqlFile(tr(format).arg(db.driverName(), tableName));
            if (tableName == "tags") {
                // v_tagsを廃止にしたので、ユーザー関数の部分のみ作成
                format = ":/sql/%1/%2.sql";
                sqlModel->execSqlFile(tr(format).arg(db.driverName(), "v_tags"));
            }
        }
    }

    // temp_vars 初期値の入力
    if (!isRestore) {
        auto count = sqlModel->getScholar("select count(*) from temp_vars").toInt();
        if (count == 0)
            sqlModel->execSqlFile(":/sql/default_temp_vars.sql");
    }

    // NOTE: v1.2 ビデオファイルフィールド追加 既存のDBを更新
    tables = db.tables(QSql::TableType::AllTables); // 再取得
    if (getApi()->isV2 && db.driverName() == "QMYSQL") {
         if (!tables.contains("v_recorded_light")) {
            sqlModel->execSqlFile(":/sql/QMYSQL/v_recorded_v2.sql");
            sqlModel->execSqlFile(":/sql/QMYSQL/v_tags.sql");
            sqlModel->execSql("ALTER TABLE play ADD PRIMARY KEY(id);");
        }
    }

    // NOTE: v1.3 v_tagを廃止して、tagsに統合
    if (tables.contains("v_tags")) {
        // v_tagsの削除
        sqlModel->execSql("DROP VIEW IF EXISTS v_tags;");
        // tagsにフィールド追加
        auto addColumn = "ALTER TABLE tags ADD COLUMN IF NOT EXISTS %1";
        sqlModel->execSql(tr(addColumn).arg("tag_watched_count integer DEFAULT 0;"));
        sqlModel->execSql(tr(addColumn).arg("tag_video_count integer DEFAULT 0;"));
        sqlModel->execSql(tr(addColumn).arg("tag_is_endprogram integer DEFAULT 0;"));
        auto currentTime = (db.driverName() == "QMYSQL")? "CURRENT_TIMESTAMP" : "now()";
        sqlModel->execSql(tr(addColumn).arg(tr("tag_last_update timestamp NOT NULL DEFAULT %1;").arg(currentTime)));

        // recorded.name を halfWidthNameへ変更
        if (getApi()->isV2 && db.driverName() == "QMYSQL") {
            sqlModel->execSqlFile(":/sql/QMYSQL/v_recorded_detail_v2.sql");
            sqlModel->execSqlFile(":/sql/QMYSQL/v_recorded_v2.sql");
        }
    }
}

/*---------------------------------------------------
 * Other
----------------------------------------------------*/
void MainWindow::on_mdiArea_customContextMenuRequested(const QPoint &pos)
{
    Menu menu(this);
    menu.addAction(ui->actionEPGStation);
    menu.addAction(ui->actionRecorded);
    menu.addAction(ui->actionTag);
    menu.addSeparator();
    menu.addAction(ui->actionNavi);
    menu.addAction(ui->actionInfo);
    menu.addSeparator();
    menu.addAction(ui->actionSimpleView);
    menu.addSeparator();
    menu.addAction(ui->actionSetting);

    menu.exec(ui->mdiArea->viewport()->mapToGlobal(pos));
}

auto MainWindow::event(QEvent *event) -> bool
{
    if (event->type() == QEvent::WindowActivate) {
        if (dbPrevConnect) {// セッション切れ対策
            // isOpenでは、判別できないので、接続をして確かめる
            if (!QSqlDatabase::database().tables(QSql::TableType::Views).count())
                if (openDB()) pop->show(QStringList() << "データベース再接続");
        }
        getTitlebarHeight(); // タイトルバーの高さを取得する
        //qDebug() << "QEvent::WindowActivate:";
    }
    if (event->type() == QEvent::WindowDeactivate) {
        //qDebug() << "QEvent::WindowDeactivate:";
    }
    if (event->type() == QEvent::Resize) {
        if (miniScreen && !moveEventBlock)
            emit mainStatusChanged(MainStatus::Resize);
        if (isMiniScreen() || isFullScreen())
            updateMultiPlayWindowSize();
    }
    if (event->type() == QEvent::Move) {
        if (miniScreen && !moveEventBlock)
            emit mainStatusChanged(MainStatus::Move);
    }
    return QMainWindow::event(event);
}

auto MainWindow::getFileSizeStr(double size) -> QString
{
    const QStringList fileSizeUnits = {"B", "KB", "MB", "GB", "TB"};
    int i;
    for (i = 0 ; i <= fileSizeUnits.count(); i++) {
        if (size < 1000) break;
        size /= 1024;
    }
    return QString("%1 %2").arg(QString::number(size, 'f', 1), fileSizeUnits[i]);
}

double MainWindow::versionCast(const QString &versionStr)
{
    double result = 0;
    QString version = versionStr;
    version.replace(QRegularExpression("^v|-.*"), "");

    auto digit = version.split(".");
    result = digit[0].toInt() + (digit[1].toDouble() * 0.1) + (digit[2].toDouble() * 0.01);
    //qDebug() << "str:" << version << " double:" << result;
    return result;
}

