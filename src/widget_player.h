#ifndef WIDGET_PLAYER_H
#define WIDGET_PLAYER_H

#include <QWidget>
#include "mainwindow.h"
#include "mpvwidget.h"

class QApplication;
class QToolButton;
class QDBusObjectPath;

namespace Ui {class WidgetPlayer;}

class WidgetPlayer : public QWidget
{
    Q_OBJECT

private:
    void saveSeek();
    void readSetting();
    void setCtlBarVisible(bool visible);
    void setFocusDefault();
    void timerHideControlReStart() {
        timerHideControl->start(timerHideControl->intervalAsDuration());
    }
    void setHwDec();

    MpvWidget *mpv;
    bool isMultiPlayer = false;
    bool isEndFile = false;

    int skipSec;
    int skipBackSec;
    int percentLimit;
    QString screenshotsPath;
    bool autoHideControl;
    bool autoFullScreen;
    bool autoMiniScreen;
    QString hwDec;
    bool hwDecAutoOff;
    QVariantMap mprisMetadata;

    SqlModel *sqlModel;
    SqlModel *sqlModelPlay;
    SqlModel *sqlModelTag;
    QTimer *timerSeekUpdate;
    QTimer *timerHideControl;

    Playlist playlist;
    PlayItem playItem;

    QAction *actionDockInfo;
    QActionGroup *trackActionGrp;
    QList<QAction*> trackActions;
    QActionGroup *audioActionGrp;

    QPropertyAnimation *animeCtlBarHide;
    QPropertyAnimation *animeCtlBarShow;

    QList<QAction*> playlistActions;
    QList<QAction*> liveActions;
    QList<QToolButton*> playlistButtons;

    MainWindow *main;
    Ui::WidgetPlayer *ui;

public:
    explicit WidgetPlayer(QWidget *parent = nullptr);
    ~WidgetPlayer() override;
    void openPlayItem(const PlayItem &playItem);
    void setDockInfo();
    QString getIdStr() { return QString::number(playItem.recordedId); }
    int getId() { return playItem.recordedId; }
    bool isLive() { return playItem.live; }
    QString getTitle() { return playItem.title; }
    Playlist* getPlaylist() { return &playlist; }
    bool isPlaying() { return mpv->getPlay(); }
    QString getTabTitle() {
        auto tabTitle = tr("%1%2").arg(isLive()? "ライブTV｜" : "", getTitle());
        tabTitle.truncate(20);
        return tabTitle;
    }
signals:
    void playerStatusChanged(PlayerStatus status, int id); // globl

    // MPRIS D-Bus Interface
public:
    void setFullscreen(bool isFullscreen);
    void next();
    void previous();
    void pause();
    void playPause();
    void stop();
    void play();
    void seek(qlonglong Offset);
    void setPosition(const QDBusObjectPath& TrackId, qlonglong Position);
    void openUri(QString uri);

    QString playbackStatus();
    QString loopStatus();
    void setLoopStatus(const QString& loopStatus);
    double rate();
    void setRate(double rate);
    QString trackId();
    QVariantMap metaData();
    double volume();
    void setVolume(double volume);
    qlonglong position();
    bool canGoNext();
    bool canGoPrevious();

signals:
    void mprisPlayngChanged();
    void mprisCurrentItemChanged();
    void mprisLoopStatusChanged();
    void mprisLengthChanged();
    void mprisVolumeChanged();
    void mprisSeeked(qlonglong position);

private slots:
    void onMpvContextMenuEvent(QContextMenuEvent *event);
    void onMainStatusChanged(MainStatus status, const QVariant &event);
    void onFileLoaded(QList<int> audioIds);

    // QWidget interface
protected:
    virtual void resizeEvent(QResizeEvent *event) override;
};

#endif // WIDGET_PLAYER_H
