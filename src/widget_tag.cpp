#include "widget_tag.h"
#include "util/tagitem.h"
#include "util/thumbnailtag.h"
#include "util/edit_search.h"
#include "util/tagmodel.h"
#include "util/combo_limit.h"
#include "util/combo_sort.h"

#include <QApplication>
#include <QElapsedTimer>
#include <QQuickWidget>
#include <QQmlContext>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QToolButton>
#include <QComboBox>

WidgetTag::WidgetTag(QWidget *parent) : QWidget(parent)
    , sqlModel(new SqlModel("recorded", "id", "v_recorded"))
    , sqlModelTag(new SqlModel("tags", "id"))
    , tagModel(new TagModel())
    , tagItem(new TagItem())
    , imgTag(new ThumbnailTag(parent))
    , main(qobject_cast<MainWindow*>(parent))
{
    setWindowTitle("検索Tag");
    auto icon = main->getIcon("tag");
    setWindowIcon(QIcon(icon.pixmap(icon.actualSize(QSize(22, 22)))));

    // mainwindow
    connect(main, &MainWindow::mainStatusChanged, this, [=] (MainStatus status) {
        if (status == MainStatus::TagTabel) {
            updateGridView();
        }
    });
    connect(main, &MainWindow::updateGridView, this, &WidgetTag::updateGridView);

    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);
    connect(sqlModelTag, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    // control create
    frameHead = new QFrame(this);
    frameHead->setObjectName("frameHead");

    layoutHeader = new QGridLayout;
    frameHead->setLayout(layoutHeader);

    layoutLeft = new QHBoxLayout;
    cmbField = new QComboBox(this);
    cmbField->setToolTip("フィールド\n抽出対象フィールド");
    cmbGenre = new QComboBox(this);
    cmbGenre->setToolTip("ジャンル\nフィールド指定が番組タイトル時のみ有効");
    cmbFlags = new QComboBox(this);
    cmbFlags->setToolTip("各種フラグ\nフィールド指定が番組タイトル時のみ有効");

    layoutLeft->addWidget(cmbField);
    layoutLeft->addWidget(cmbGenre);
    layoutLeft->addWidget(cmbFlags);

    layoutRight = new QHBoxLayout;
    editSearch = new EditSearch(this);
    connect(editSearch, &EditSearch::editingFinished, this, [=] { updateGridView(); });
    connect(editSearch, &EditSearch::searchClicked, this, [=] { updateGridView(); });

    laViewCount = new QLabel(this);
    laViewCount->setObjectName("headerSubLable");

    cmbLimit = new ComboLimit({ 0, 10, 20, 30, 50 }, this);
    connect(cmbLimit, &ComboLimit::currentTextChanged, this, [=] (const QString &/*text*/) {
        updateGridView();
    });

    cmbSort = new ComboSort(this);
    connect(cmbSort, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, [=] (int /*index*/) {
        updateGridView();
    });

    toolReflesh = new QToolButton(this);
    toolReflesh->setToolTip("更新");

    layoutRight->addWidget(editSearch);
    layoutRight->addStretch();
    layoutRight->addWidget(laViewCount);
    layoutRight->addWidget(cmbLimit);
    layoutRight->addWidget(cmbSort);
    layoutRight->addWidget(toolReflesh);

    quickWidget = new QQuickWidget(this);

    // 画面全体のレイアウト作成
    auto *wrap = new QVBoxLayout(this);
    wrap->setSpacing(0);
    wrap->setContentsMargins(0, 0, 0, 0);

    wrap->addWidget(frameHead);
    wrap->addWidget(quickWidget, 1);

    // tool
    toolReflesh->setIcon(main->getIcon("view-refresh"));
    connect(toolReflesh, &QToolButton::clicked, this, [=] () { main->updateTagTable(); });

    // flags
    static const QStringList flagStrs = {"未視聴", "番組終了", "動画あり", "動画なし", "新番組", "任意"};
        cmbFlags->addItems(flagStrs);
    static const QStringList flagDatas = {
        "(tag_video_count - tag_watched_count) > 0",
        "tag_is_endprogram = true",
        "tag_video_count > 0",
        "tag_video_count = 0",
        "newflag = true",
        ""
    };
    for (int i = 0; i < cmbFlags->count(); i++)
        cmbFlags->setItemData(i, flagDatas[i], Qt::UserRole);

    connect(cmbFlags, &QComboBox::currentTextChanged, this, [=] (const QString &/*text*/) {
        updateGridView();
    });

    setupComboBox();

    quickWidget->setResizeMode(QQuickWidget::SizeRootObjectToView);
    quickWidget->rootContext()->setContextProperty("WidgetTag", this);
    quickWidget->rootContext()->setContextProperty("TagModel", tagModel);
    #if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
    quickWidget->setSource(QUrl("qrc:/qml/tag/main.qml"));
    #else
    quickWidget->setSource(QUrl("qrc:/qml5/tag/main.qml"));
    #endif

    // NOTE:多重Update防止
    // mainwindowからのopenTagWidgetで、setFilterを呼出すので
    // updateGridView()は、実行しない
}

WidgetTag::~WidgetTag()
{
    quickWidget->setSource(QUrl()); // 終了時のエラーメッセージ抑制
    delete sqlModel;
    delete sqlModelTag;
    delete tagModel;
    delete tagItem;
    delete imgTag;

    delete frameHead;
    delete quickWidget;
}

void WidgetTag::setTagID(int id, bool reload /*= false*/)
{
    //qDebug() << tr("WidgetTag::setTagID()").arg(id);
    selectedId = id;
    main->setDockInfoId(InfoType::Tag, QString::number(id));
    main->selectTag(id);
    if (reload)
        updateGridView();
    else {
        emit currentIndexChanged(getIdToIndex(id));
    }
}

void WidgetTag::setFilter(int id /*= -1*/, const QString &field /*= ""*/, const QString &genre /*= ""*/ )
{
    //qDebug() << "id: " << id << " field: " << field << " genre: " << genre;
    // 初回起動なら、前回値の復元
    if (field.isEmpty() && genre.isEmpty()) {
        loadFilterSettings();
        updateGridView();
        return;
    }

    // NOTE: 多重Update防止で、signalをブロックする
    // signal block
    cmbField->blockSignals(true);
    cmbField->setCurrentText(field.isEmpty()? nullStr : field);
    setCurrentGenreFlags(genre);
    cmbField->blockSignals(false);
    // update
    selectedId = id;
    updateGridView();
}

void WidgetTag::loadFilterSettings()
{
    QList<QComboBox*> comboBoxs = { cmbField, cmbGenre, cmbFlags, cmbLimit, cmbSort };
    blockSignals(comboBoxs, true);

    // field
    auto fieldText = main->getSettings()->valueObj(this, "fieldText", nullStr).toString();
    cmbField->setCurrentText(fieldText);
    // genre
    auto genreText = main->getSettings()->valueObj(this, "genreText", nullStr).toString();
    cmbGenre->setCurrentText(genreText);
    // flags
    auto flagsIndex = main->getSettings()->valueObj(this, "flagsIndex", 0).toInt();
    cmbFlags->setCurrentIndex(flagsIndex);
    // limit
    auto limitIndex = main->getSettings()->valueObj(this, "limitIndex", 0).toInt();
    cmbLimit->setCurrentIndex(limitIndex);
    // sort
    auto sortIndex = main->getSettings()->valueObj(this, "sortIndex", 0).toInt();
    cmbSort->setCurrentIndex(sortIndex);

    setCurrentGenreFlags(genreText);
    saveGenre = main->getSettings()->valueObj(this, "saveGenre", nullStr).toString();
    saveFlags = main->getSettings()->valueObj(this, "saveFlags", nullStr).toString();

    blockSignals(comboBoxs, false);
}

void WidgetTag::saveFilterSettings()
{
    main->getSettings()->setValueObj(this, "fieldText", cmbField->currentText());
    main->getSettings()->setValueObj(this, "genreText", cmbGenre->currentText());
    main->getSettings()->setValueObj(this, "flagsIndex", cmbFlags->currentIndex());
    main->getSettings()->setValueObj(this, "limitIndex", cmbLimit->currentIndex());
    main->getSettings()->setValueObj(this, "sortIndex", cmbSort->currentIndex());

    main->getSettings()->setValueObj(this, "saveGenre", saveGenre);
    main->getSettings()->setValueObj(this, "saveFlags", saveFlags);
}

void WidgetTag::updateGridView()
{
    if (!main->isTagTableInitUpdate()) return;

    QElapsedTimer timer;
    timer.start();

    auto where = QStringList();

    // field
    auto field = cmbField->currentText();
    if (field != nullStr)
        where << tr("field='%1'").arg(field);

    // genre
    auto genre = cmbGenre->currentText();
    if (cmbField->currentIndex() == 0 && genre != nullStr) {
        if (genre != "もう見ないリスト")
            where << tr("genre='%1' and blackflag=false").arg(genre);
        else
            where << "blackflag=true";
    }

    // search
    auto search = sqlModelTag->escape(editSearch->text());
    if (!search.isEmpty())
        where << tr("tag like '%%1%'").arg(search);

    // flags
    auto flags = cmbFlags->currentData(Qt::UserRole).toString();
    if (!flags.isEmpty()) where << flags;

    sqlModelTag->setWhere(where.join(" and "));

    // limit
    int totalCount = sqlModelTag->getRecordCount();
    if (cmbLimit->currentData().toInt())
        sqlModelTag->setLimit(LimitType::limitTop, cmbLimit->currentData(Qt::UserRole).toInt());
    else
        sqlModelTag->setLimit(LimitType::limitAll);

    // sort
    sqlModelTag->setOrderBy(cmbSort->currentData(Qt::UserRole).toString());

    // get model
    auto model = sqlModelTag->getModel(SelectType::selectWhere);

    // limit 表示
    laViewCount->setText(tr("%1 / %2").arg(model->rowCount()).arg(totalCount));

    if (model->rowCount() == 0) {
        tagModel->resetModel();
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);

    tagModel->resetModel();
    currentIndex = -1;
    for (int i = 0; i < model->rowCount(); i++) {
        const auto rs = model->record(i);

        TagElement element = tagItem->getElement(rs);

        // サムネイルを取得する
        imgTag->setId(element.id);
        element.thumbnail = imgTag->getImageFilePath();
        element.hasThumbnail = imgTag->isFileExist();

        // 選択の復元
        if (selectedId == element.id) currentIndex = i;

        tagModel->append(element);
    }

    qDebug() << tr("WidgetTag::updateData() 実行時間: %1秒").arg(static_cast<double>(timer.elapsed()) / 1000, 0, 'f', 2);

    // qmlの選択状態復元
    emit currentIndexChanged(currentIndex);

    QApplication::restoreOverrideCursor();
    saveFilterSettings();
}

void WidgetTag::blockSignals(QList<QComboBox *> &comboBoxs, bool block)
{
    foreach (auto combo, comboBoxs)
        combo->blockSignals(block);
}

int WidgetTag::getIdToIndex(int id)
{
    for (int index = 0; index < tagModel->rowCount(); index++) {
        if (id == tagModel->getRow(index).id) return index;
    }
    return 0;
}

void WidgetTag::resizeEvent(QResizeEvent *event)
{
    // ヘッダーの折りたたみ
    if (layoutHeader->count()) {
        layoutHeader->removeItem(layoutLeft);
        layoutHeader->removeItem(layoutRight);
    }
    if (event->size().width() > 800) {
        layoutHeader->addLayout(layoutLeft, 0, 0);
        layoutHeader->addLayout(layoutRight, 0, 1);
    } else {
        layoutHeader->addLayout(layoutRight, 0, 0);
        layoutHeader->addLayout(layoutLeft, 1, 0);
    }
    frameHead->setLayout(layoutHeader);
}

void WidgetTag::setupComboBox()
{
    QList<QComboBox*> comboBoxs = { cmbField, cmbGenre };
    blockSignals(comboBoxs, true);

    // field
    cmbField->clear();
    sqlModel->setInit("tags", "id", "");
    auto fieldList = sqlModel->getGroupByValues("field", OrderType::desc);
    fieldList.removeAll(QString(""));
    cmbField->addItems(fieldList << nullStr);
    connect(cmbField, &QComboBox::currentTextChanged, this, [=] (const QString &/*text*/) {
        // NOTE: 番組情報以外では、ジャンルの指定は無効にする
        setCurrentGenreFlags();
        updateGridView();
    });

    // genre
    cmbGenre->clear();
    QStringList genres;
    sqlModel->setWhere("field='番組タイトル' and blackflag=false");
    foreach(auto value, sqlModel->getGroupByValues("genre", OrderType::asc, true)) {
        if (!value.isEmpty()) genres << value;
    }
    cmbGenre->addItems(genres << "もう見ないリスト" << nullStr);
    connect(cmbGenre, &QComboBox::currentTextChanged, this, [=] (const QString &/*text*/) {
        updateGridView();
    });

    blockSignals(comboBoxs, false);
}

void WidgetTag::setCurrentGenreFlags(const QString &genre)
{
    QList<QComboBox*> comboBoxs = { cmbGenre, cmbFlags };
    blockSignals(comboBoxs, true);

    auto isEndflag = (cmbField->currentIndex() == 0);
    cmbGenre->setEnabled(isEndflag);
    cmbFlags->setEnabled(isEndflag);
    if (isEndflag) {
        // 番組タイトル
        cmbGenre->setCurrentText(genre.isEmpty()? saveGenre : genre);
        if (genre == "もう見ないリスト") {
            // 強制的に全表示、指定を戻せるようにフラグの保存
            saveFlags = cmbFlags->currentText();
            cmbFlags->setCurrentText(nullStr);
        } else {
            cmbFlags->setCurrentText(saveFlags);
        }
    } else {
        saveGenre = cmbGenre->currentText();
        cmbGenre->setCurrentText(genre.isEmpty()? nullStr : genre);

        saveFlags = cmbFlags->currentText();
        cmbFlags->setCurrentText(nullStr);
    }

    blockSignals(comboBoxs, false);
}

/*---------------------------------------------------
 * QML Q_INVOKABLE Functions
----------------------------------------------------*/
void WidgetTag::actionSearchNewTab(const QString &word)
{
    if (word.isEmpty()) return;
    if (main->openRecorded())
        actionSearch(word);
}

void WidgetTag::actionSearch(const QString &word)
{
    QString text = word;
    if (text.isEmpty()) return;
    text.truncate(30);
    main->setSearchTag(text);
}

bool WidgetTag::actionFilter(const QString &field, const QString &tag) {
    return main->setFilterTag(field, tag);
}

void WidgetTag::actionFilterNewTab(const QString &field, const QString &tag)
{
    if (main->openRecorded()) actionFilter(field, tag);
}

void WidgetTag::actionSearchWeb(const QString &word)
{
    main->searchWeb(word);
}

void WidgetTag::openTagEdit(int id)
{
    if (main->openTagEditDlg(id)) setTagID(id);
}

void WidgetTag::openTagDelete(int id)
{
    int currentIndex = getIdToIndex(id);
    main->openTagDelete(id);
    if (currentIndex == (tagModel->rowCount() -1)) currentIndex--;
    emit currentIndexChanged(currentIndex);
}

void WidgetTag::setBlackList(int id, bool checked)
{
    main->setBlackList(id, checked);
}

void WidgetTag::openSearchWebImageDlg(int id)
{
    if (main->openTagThumbnail(id)) setTagID(id);
}

void WidgetTag::openImageFileSelectDlg(int id)
{
    if (imgTag->openFileSelectDlg(id)) setTagID(id, true);
}

void WidgetTag::deleteImageFile(int id)
{
    if (imgTag->remove(id)) setTagID(id, true);
}

void WidgetTag::saveDropUrlFile(const QString &url, int id/* = -1*/)
{
    if (imgTag->saveDropUrlFile(QUrl(url), id)) setTagID(id, true);
}

