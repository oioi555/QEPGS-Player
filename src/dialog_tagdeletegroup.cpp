#include "dialog_tagdeletegroup.h"
#include "ui_dialog_tagdeletegroup.h"
#include "util/thumbnailtag.h"

#include <QStandardItemModel>

DialogTagDeleteGroup::DialogTagDeleteGroup(const QString &goupName, QList<TagItem*> tagItem, QWidget *parent) :
    QDialog(parent)
  , tagItems(std::move(tagItem))
  , sqlModel(new SqlModel("recorded", "id", "v_recorded"))
  , sqlModelTags(new SqlModel("tags" , "id"))
  , treeModel(new QStandardItemModel(this))
  , main(qobject_cast<MainWindow*>(parent))
  , ui(new Ui::DialogTagDeleteGroup)
{
    ui->setupUi(this);

    // window
    setWindowTitle(tr("まとめて削除 [%1]").arg(goupName));

    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);
    connect(sqlModelTags, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    // action
    connect(ui->actionSelectAll, &QAction::triggered, this, [=] {
        ui->treeView->selectAll();
    });
    connect(ui->actionNotSelectAll, &QAction::triggered, this, [=] {
        ui->treeView->clearSelection();
    });

    // check
    connect(ui->chkDeleteWatched, &QCheckBox::clicked, this, [=] (bool checked) {
        if (checked) ui->chkDeleteVideoAll->setChecked(false);
    });
    connect(ui->chkDeleteVideoAll, &QCheckBox::clicked, this, [=] (bool checked) {
        if (checked) ui->chkDeleteWatched->setChecked(false);
    });

    // filter
    connect(ui->optViewAll, &QRadioButton::clicked, this, [=] (bool /*checked*/) {
        clearTreeModel();
        foreach (auto item, tagItems)
            addTreeItem(item);
        autoSize();
    });
    connect(ui->optEndProgram, &QRadioButton::clicked, this, [=] (bool /*checked*/) {
        clearTreeModel();
        foreach (auto item, tagItems) {
            if (item->isEndProgram) addTreeItem(item);
        }
        ui->treeView->selectAll();
        autoSize();
    });
    connect(ui->optNotVideo, &QRadioButton::clicked, this, [=] (bool /*checked*/) {
        clearTreeModel();
        foreach (auto item, tagItems) {
            if (item->count == 0 && item->watched == 0) addTreeItem(item);
        }
        ui->treeView->selectAll();
        autoSize();
    });
    connect(ui->optWatched, &QRadioButton::clicked, this, [=] (bool /*checked*/) {
        clearTreeModel();
        foreach (auto item, tagItems) {
            if (item->count == item->watched && item->count != 0) addTreeItem(item);
        }
        ui->treeView->selectAll();
        autoSize();
    });
    ui->optViewAll->setChecked(true);

    // tree
    clearTreeModel();
    foreach (auto item, tagItems)
        addTreeItem(item);
    ui->treeView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->treeView->setModel(treeModel);

    // 画面サイズの復元
    restoreGeometry( main->getSettings()->valueObj(this, "WindowSize").toByteArray());
    autoSize();
}

DialogTagDeleteGroup::~DialogTagDeleteGroup()
{
    // 画面サイズの保存
    main->getSettings()->setValueObj(this, "WindowSize", saveGeometry());

    delete sqlModel;
    delete sqlModelTags;
    delete treeModel;
    delete ui;
}

void DialogTagDeleteGroup::addTreeItem(TagItem *item)
{
    auto treeItem = item->getTreeItem();
    treeModel->invisibleRootItem()->appendRow(treeItem);
}

void DialogTagDeleteGroup::clearTreeModel()
{
    treeModel->clear();
    treeModel->setHorizontalHeaderLabels({"Tag", "数"});
}

TagItem *DialogTagDeleteGroup::getTagItem(const QModelIndex &index) {
    return static_cast<TagItem *>(treeModel->data(index, Qt::UserRole).value<QObject *>());
}

void DialogTagDeleteGroup::autoSize()
{
    int fontWidth = ui->treeView->fontInfo().pixelSize();
    int width = ui->treeView->size().width() - (fontWidth * 6);
    ui->treeView->header()->resizeSection(0, width);
}

void DialogTagDeleteGroup::on_buttonBox_accepted()
{
    QList<int> ids;
    QModelIndexList selectedItems;
    foreach (auto index, ui->treeView->selectionModel()->selectedIndexes()) {
        if (treeModel->data(index, Qt::UserRole).isValid()) {
            ids << getTagItem(index)->id;
            selectedItems << index;
        }
    }

    if (!ids.length()) {
        QMessageBox::warning(this, tr("Tagグループ削除"), tr("Tagが選択されていません"));
        return;
    }

    // 確認メッセージ
    QStringList msg;
    if (ui->chkDeleteTag->isChecked())
        msg << tr("削除するTag:　%1件").arg(ids.length());

    if (ui->chkDeleteWatched->isChecked()) {
        int count = 0;
        foreach (auto selected, selectedItems)
            count += getTagItem(selected)->watched;

        msg << tr("削除する視聴済み動画:　%1件").arg(count);
    }
    if (ui->chkDeleteVideoAll->isChecked()) {
        int count = 0;
        foreach (auto selected, selectedItems)
            count += getTagItem(selected)->count;

        msg << tr("削除する動画:　%1件").arg(count);
    }
    if (msg.isEmpty()) {
        QMessageBox::warning(this, tr("Tagグループ削除"), tr("削除条件を選択して下さい"));
        return;
    }

    msg << "" << "本当に削除しますか？";

    QMessageBox msgBox(qobject_cast<QWidget*>(this->window()));
    msgBox.setWindowTitle("削除確認");
    msgBox.setText(msg.join("\n"));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    msgBox.setIcon(QMessageBox::Question);
    if (msgBox.exec() != QMessageBox::Yes) return;

    if (ui->chkDeleteTag->isChecked()) {
        sqlModelTags->deleteRecord(ids);
        isDeletedTag = true;
        ThumbnailTag imgTag(main);
        foreach (auto id, ids)
            imgTag.remove(id, false);
    }

    // NOTE: recordedIdが、重複する可能性があるので
    // QStringListで、まとめてから重複削除する
    QStringList deleteIdStrs;
    if (ui->chkDeleteWatched->isChecked()) {
        foreach (auto selected, selectedItems) {
            TagItem *item = getTagItem(selected);
            QString fix = "::numeric";
            auto sql = tr("%1 like '%%2%' and 再生 > get_var('Threshold')%3")
                      .arg(item->field, item->tag, sqlModel->isPgsql()? fix : "");
            deleteIdStrs << sqlModel->getValuesIdStrs(sql);
        }
    }
    if (ui->chkDeleteVideoAll->isChecked()) {
        foreach (auto selected, selectedItems) {
            TagItem *item = getTagItem(selected);
            auto sql = tr("%1 like '%%2%'").arg(item->field, item->tag);
            deleteIdStrs << sqlModel->getValuesIdStrs(sql);
        }
    }
    deleteIdStrs.removeDuplicates(); // 重複IDの削除
    if (deleteIdStrs.count()) {
        QList<int> deleteIds;
        foreach (auto id, deleteIdStrs) deleteIds << id.toInt();
        main->deleteRecorded(deleteIds);
    }
    accept();
}

void DialogTagDeleteGroup::on_treeView_customContextMenuRequested(const QPoint &pos)
{
    Menu menu(this);
    menu.addAction(ui->actionSelectAll);
    menu.addAction(ui->actionNotSelectAll);
    menu.exec(ui->treeView->viewport()->mapToGlobal(pos));
}
