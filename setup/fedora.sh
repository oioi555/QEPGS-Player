#!/bin/sh
cd $(dirname $0)

# 引数1 Qt5 or Qt6 無指定:Qt5
[ $1 = Qt6 ] && QtCore=Qt6 || QtCore=Qt5

sudo dnf upgrade -y --refresh
sudo dnf install -y git cmake ninja-build rpmdevtools pkg-config mpv-libs-devel

if [ $QtCore = Qt6 ]; then
    sudo dnf install -y qt6-qtbase-devel qt6-qtwebengine-devel qt6-qt5compat-devel
else
    sudo dnf install -y qt5-qtbase-devel qt5-qtwebengine-devel qt5-qtquickcontrols2-devel
fi

rm -rf build
cmake -G Ninja -S ../ -B build -DCMAKE_PREFIX_PATH:PATH=/usr/lib64/cmake/${QtCore}
cmake --build build
cd build

# パッケージのビルド
cpack -G RPM
sudo dnf -y install *.rpm

# アイコンの強制更新
sudo gtk-update-icon-cache /usr/share/icons/hicolor
