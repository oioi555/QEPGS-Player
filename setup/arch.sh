#!/bin/sh
cd $(dirname $0)

# 引数1 Qt5 or Qt6 無指定:Qt5
# 引数2 local or dev 無指定:git(master)

# makepkgに必要
sudo pacman -S --noconfirm base-devel git

# PKGBUILD作成
./makePKGBUILD.sh ${1} ${2}

# makepkg option
# -f パッケージの上書き
# -c パッケージのクリーンナップ
# -i パッケージ作成後インストール
# -s 依存パッケージインストール
# time makepkg  --noconfirm -fc
time makepkg  --noconfirm -fcis
