#!/bin/sh
cd $(dirname $0)

# 引数1 Qt5 or Qt6 無指定:Qt5
[ $1 = Qt6 ] && QtCore=Qt6 || QtCore=Qt5

sudo apt update && sudo apt upgrade -y
sudo apt install -y git cmake ninja-build build-essential pkg-config libmpv-dev

if [ $QtCore = Qt6 ]; then
    # note:
    # qt6-5compat-devは、Ubuntu22.04に存在しない
    # libqt6core5compat6-devは、Debian12に存在しないが
    # 自動的にqt6-5compat-devがインストールされる
    # debian12は、qt6-webengine-devにtoolsが入っているが、Ubuntuには無い
    sudo apt -y install qt6-declarative-dev \
    qt6-webengine-dev qt6-webengine-dev-tools \
    libqt6core5compat6-dev
else
    sudo apt -y install qtdeclarative5-dev qtwebengine5-dev qtquickcontrols2-5-dev
fi

rm -rf build
cmake -G Ninja -S ../ -B build -DCMAKE_PREFIX_PATH:PATH=/usr/lib/x86_64-linux-gnu/cmake/${QtCore}
cmake --build build
cd build

# パッケージのビルド
cpack -G DEB

# パッケージのインストール
sudo apt install -y ./*deb

# アイコンの強制更新
sudo gtk-update-icon-cache /usr/share/icons/hicolor
