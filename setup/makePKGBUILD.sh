#!/bin/sh
cd $(dirname $0)

# 引数1 Qt5 or Qt6 無指定:Qt5
# 引数2 local or dev 無指定:git(master)

[ $1 = Qt6 ] && QtCore=Qt6 || QtCore=Qt5

# 依存関係
dependsQtPkg="'${QtCore,}-base' '${QtCore,}-declarative' '${QtCore,}-webengine' '${QtCore,}-svg'"
if [ $QtCore = Qt6 ]; then
    dependsQtPkg="${dependsQtPkg} 'qt6-5compat' 'qt6-shadertools'"
else
    dependsQtPkg="${dependsQtPkg} 'qt5-quickcontrols2' 'qt5-graphicaleffects'"
fi

# ソースの指定
SourceType=${2}
projectDir='${srcdir}/${pkgname}/'
source=""
if [ $SourceType = local ]; then
    projectDir='../../'
elif [ $SourceType = dev ]; then
    source='source=(git+${url}.git#branch=develop) sha256sums=(SKIP)'
else
    source='source=(git+${url}.git) sha256sums=(SKIP)'
fi

echo "QtCore:${QtCore} SourceType: ${SourceType}"

# PKGBUILD作成
pkgver=$(git describe --abbrev=0 --tags | sed -z 's/^v//;s/\\n//')

tee PKGBUILD <<EOF >/dev/null
pkgname=QEPGS-Player
pkgver=${pkgver}
pkgrel=0
pkgdesc='An application to watch videos recorded by EPGStation.'
arch=('i686' 'x86_64')
url='https://gitlab.com/oioi555/QEPGS-Player'
license=('GPL2')

depends=(${dependsQtPkg} 'mpv' 'postgresql-libs' 'mariadb-libs' 'breeze-icons')
makedepends=('cmake' 'ninja' 'pkgconf' '${QtCore,}-tools')

provides=(\${pkgname}=\${pkgver})
conflicts=(\${pkgname})
replaces=(\${pkgname})
${source}

pkgver() {
  cd ${projectDir}
  git describe --abbrev=0 --tags | sed -z 's/^v//;s/\\\n//'
}

build() {
    cd ${projectDir}
    cmake -G Ninja -B build -DCMAKE_PREFIX_PATH:PATH=/usr/lib/cmake/${QtCore}
    cmake --build build
}

package() {
    cd ${projectDir}
    DESTDIR=\${pkgdir} cmake --install build --config Release
}
EOF
