#!/bin/bash

selfDir=$(cd "$(dirname "$0")"; pwd)
. docker.conf         # 設定読み込み

for tag in ${TAGS[@]}; do
    . build.sh ${tag}; cd $selfDir
done
