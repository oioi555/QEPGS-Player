#!/bin/bash

cd $(dirname $0)    
. docker.conf       # 設定読み込み

TAG_NAME=${1}
echo "### Build ${TAG_NAME}"

cd ..               #プロジェクトRoot基準で処理
sudo docker build -t ${REGISTRY_PROJECT_PATH}${TAG_NAME} -f docker/${TAG_NAME}/Dockerfile .
