#!/bin/bash
set -eux
selfDir=$(cd "$(dirname "$0")"; pwd)  
. docker.conf         # 設定読み込み

. all_build.sh
. all_push.sh